begin
SET @statusnum = 'active';
SET @sentsms = 0;
SET @newuser = NEW.`username`;
SET @inmid = (SELECT inmid FROM tbl_instantmessage WHERE 
 `customerid` = NEW.`customerid`
AND `startdate` <= NEW.`lastlogin`
AND `enddate` >= NEW.`lastlogin`
AND `status` = @statusnum
AND `locationid` = NEW.`locationid` LIMIT 1);
SET @targetusers = (SELECT permisionlist FROM tbl_instantmessage WHERE `inmid` = @inmid);
SET @prioritynum = (SELECT priority FROM tbl_instantmessage WHERE `inmid` = @inmid);
    IF(@inmid = NULL)
    THEN
    SET @inmid = UUID();
    ELSE
            SET @inmid = @inmid;
            SET @messagetype = (SELECT messagetype FROM tbl_instantmessage WHERE `inmid` = @inmid);		
            SET @variabledatetype = (SELECT variabledatetype FROM tbl_instantmessage WHERE `inmid` = @inmid);
            SET @status = (SELECT status FROM tbl_instantmessage WHERE `inmid` = @inmid);
            SET @message = (SELECT message FROM tbl_instantmessage WHERE `inmid` = @inmid);
            INSERT into microlocationsms values(NEW.userid,NEW.name,NEW.surname,@newuser,NEW.locationid,NEW.customerid,@inmid,@prioritynum,@targetusers,@messagetype,@variabledatetype,@status,@message,NEW.smspermission,@sentsms,NEW.lastlogin);             
    END IF;
end
