/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * File : addCustomer.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Mansoor Khan
 */

$(document).ready(function(){
	
	var addCustomerForm = $("#addCustomer");
	
	var validator = addCustomerForm.validate({
		
		rules:{
			customername :{ required : true },
			femail : { required : true, email : true},
                        address :{ required : true },
                        city :{ required : true },
                        firmdescription :{ required : true },
                        ctname :{ required : true },
                        ctsurname :{ required : true },
                        ctphonenumber :{ required : true },
                        ctemail :{ required : true, email : true},
                        ctdescription :{ required : true }			
		},
		messages:{
			customername :{ required : "This field is required" },
			femail : { required : "This field is required", email : "Please enter valid email address" },
                        address :{ required : "This field is required" },
                        city :{ required : "This field is required" },
                        firmdescription :{ required : "This field is required" },
                        ctname :{ required : "This field is required" },
                        ctsurname :{ required : "This field is required" },
                        ctphonenumber :{ required : "This field is required" },
			ctemail : { required : "This field is required", email : "Please enter valid email address" },
                        ctdescription :{ required : "This field is required" }
		}
	});
});
