/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Mansoor Khan
 */
$(document).ready(function(){
	
	var editUserForm = $("#editUser");
	
	var validator = editUserForm.validate({
		
		rules:{
			customername :{ required : true },
			femail : { required : true, email : true},
                        address :{ required : true },
                        city :{ required : true },
                        firmdescription :{ required : true },
                        ctname :{ required : true },
                        ctsurname :{ required : true },
                        ctphonenumber :{ required : true },
                        ctemail :{ required : true, email : true},
                        ctdescription :{ required : true }			
		},
		messages:{
			customername :{ required : "This field is required" },
			femail : { required : "This field is required", email : "Please enter valid email address" },
                        address :{ required : "This field is required" },
                        city :{ required : "This field is required" },
                        firmdescription :{ required : "This field is required" },
                        ctname :{ required : "This field is required" },
                        ctsurname :{ required : "This field is required" },
                        ctphonenumber :{ required : "This field is required" },
			ctemail : { required : "This field is required", email : "Please enter valid email address" },
                        ctdescription :{ required : "This field is required" }
		}
	});
});