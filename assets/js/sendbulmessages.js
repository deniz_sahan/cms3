/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * File : sendbulkmessages.js
 * 
 * This funstion is used to populate the multi select option for locations
 * 
 * @author Mansoor Khan
 */

//var testing = '';
$(".ajax").click(function(){
    sendbulkmessages();
});
           function sendbulkmessages() {   
                var startdatenum = $('#datetimepickervaluestart').val();
                var enddatenum = $('#datetimepickervalueend').val();
                var customerid = $('#selectcustomermsg').val();
                var locationid = $('#selectlocationmsg').val();
                var text = $('#bulkmessagetext').val();
                ///alert(startdatenum);
                if(startdatenum != '' && enddatenum != '' && customerid != '' && locationid != '' && text != ''){
                $.ajax({
                        url: baseURL + 'bulkmessagestogw',
                        method: 'post',
                        data: {customerid: customerid,locationid: locationid,bulkmessagetext: text,datetimepickervaluestart: startdatenum,datetimepickervalueend: enddatenum},
                        dataType: 'json',
                        success: function(response){
                            
                            if(response == ''){
                            alert('No message Sent. 0 Users Found!');
                            }else{
                            alert(response + 'Messages Sent');
                            window.location = baseURL + 'bulkmessages';
                            }
                            $('#myModal').modal({'show' : true});
//                            $('.ajax-content').html('<hr>Update: '+response+' messages sent!');
//                            $( "#spinning" ).addClass( "fa-spin" );
//                            $('.stopservice').css('display','block');                            
                        }
                    });
            }else{
                alert('Please select Customer, Location , Start and End Date');
            }
        }