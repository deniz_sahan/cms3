/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Mansoor Khan
 */
$(document).ready(function(){
	
	var editUserForm = $("#editUser");
	
	var validator = editUserForm.validate({
		
		rules:{
			locationname :{ required : true },
			deviceid : { required : true},
                        address :{ required : true },
                        lanip1 :{ required : true },
                        lanip2 :{ required : true } 			
		},
		messages:{
			locationname :{ required : "This field is required" },
			deviceid : { required : "This field is required", email : "Please enter valid email address" },
                        address :{ required : "This field is required" },
                        lanip1 :{ required : "This field is required" },
                        lanip2 :{ required : "This field is required" }
		}
	});
});
