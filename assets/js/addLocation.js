/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * File : addLocation.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Mansoor Khan
 */

$(document).ready(function(){
	
	var addLocationForm = $("#addLocation");
	
	var validator = addLocationForm.validate({
		
		rules:{
			locationname :{ required : true },
			deviceid : { required : true},
                        address :{ required : true },
                        lanip1 :{ required : true },
                        lanip2 :{ required : true }                        		
		},
		messages:{
			locationname :{ required : "This field is required" },
			deviceid : { required : "This field is required", email : "Please enter valid email address" },
                        address :{ required : "This field is required" },
                        lanip1 :{ required : "This field is required" },
                        lanip2 :{ required : "This field is required" }
                        
		}
	});
});
