/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Mansoor Khan
 */
$(document).ready(function(){
	
	var editUserForm = $("#edittrans");
	
	var validator = editUserForm.validate({
		
		rules:{
			packetlimit:{ required : true },
			bip : { required : true},
                        descriptrion :{ required : true }
		
		},
		messages:{
			packagename :{ required : "This field is required" },
			bip : { required : "This field is required"},
                        descriptrion :{ required : "This field is required" }
		}
	});
});
