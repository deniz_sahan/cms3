$(document).ready(function(){
$.ajax({
		url: "<?php echo base_url(); ?>instantMessages/showDailyMessagesGraph",
		method: "GET",
		success: function(data) {
			console.log(data);
			var date = [];
			var total = [];

			for(var i in data) {
				player.push("date " + data[i].date);
				total.push(data[i].total);
			}

			var chartdata = {
				labels: datea,
				datasets : [
					{
						label: 'Dates Total messages',
						backgroundColor: 'rgba(200, 200, 200, 0.75)',
						borderColor: 'rgba(200, 200, 200, 0.75)',
						hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
						hoverBorderColor: 'rgba(200, 200, 200, 1)',
						data: score
					}
				]
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'bar',
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
        });