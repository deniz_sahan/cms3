/**
 * @author Mansoor Khan
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteWifiUser", function(){
		var wifiuserid = $(this).data("wifiuserid"),
			hitURL = baseURL + "deletewifivisitor",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { wifiuserid : wifiuserid } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
