/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * File : sendmessagescript.js
 * 
 * This funstion is used to populate the multi select option for locations
 * 
 * @author Mansoor Khan
 */
$(document).ready(function(){
                
                // Customer Change
                $('#selectcustomermsg').change(function(){
                    var customerid = $(this).val();
// AJAX request
                    $.ajax({
                        url: baseURL + 'getallLocationbyCustomerID',
                        method: 'post',
                        data: {customerid: customerid},
                        dataType: 'json',
                        success: function(response){

                            // Remove options
                            $('#selectlocationmsg').find('option').remove();                            
                            // Add options
                            $.each(response,function(index,data){
                                $('#selectlocationmsg').append('<option value="'+data['locationid']+'">'+data['locationname']+'</option>');
                            });
                        }
                    });
                });
                
                
            });