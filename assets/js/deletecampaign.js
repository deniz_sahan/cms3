/**
 * @author Mansoor Khan
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteCampaign", function(){
		var inmid = $(this).data("inmid"),
			hitURL = baseURL + "deleteCampaign",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this Campaign ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { inmid : inmid } 
			}).done(function(data){
				console.log(data);
                                $('.deleteid'+inmid).remove();
//                                currentRow.prev('tr').remove();
//				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Campaign successfully deleted"); }
				else if(data.status = false) { alert("Campaign deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
});
