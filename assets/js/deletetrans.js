/**
 * @author Mansoor Khan
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deletetrans", function(){
		var slid = $(this).data("slid"),
			hitURL = baseURL + "transactions",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this transaction ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { slid : slid } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
