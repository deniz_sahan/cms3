/**
 * @author Mansoor Khan
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteBranch", function(){
		var branchid = $(this).data("branchid"),
			hitURL = baseURL + "deletebranch",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this this Access Point ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { branchid : branchid } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
