<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';

if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$customerid = $cf->customerid;
		$customername = $cf->customername;
		$adress = $cf->adress;
		$femail = $cf->email;
		$city = $cf->email;
		$description = $cf->description;
		$ctname = $cf->ctname;
		$stsurname = $cf->stsurname;
		$ctphonenumber = $cf->ctphonenumber;
		$ctemail = $cf->ctemail;
		$ctdescription = $cf->ctdescription;

	}
}


?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->



				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo $this->lang->line('entercustomerdetail_menu'); ?>
						</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<form role="form" action="<?php echo base_url() ?>editcustomer" method="post" id="editUser" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customername">
											<?php echo $this->lang->line('customername_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="<?php echo $customername; ?>" id="customername" name="customername" maxlength="128" style="height: 32px;" width="50%">
										<input type="hidden" value="<?php echo $customerid; ?>" name="customerid" id="customerid"/>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="femail">
											<?php echo $this->lang->line('firmemailaddress_menu'); ?>
										</label>
										<input type="text" class="form-control required email" id="femail" value="<?php echo $femail; ?>" name="femail" maxlength="128" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="address">
											<?php echo $this->lang->line('address_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="address" value="<?php echo $adress; ?>" name="address" maxlength="500" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="city">
											<?php echo $this->lang->line('city_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="city" value="<?php echo $city; ?>" name="city" maxlength="120" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="firmdescription">
											<?php echo $this->lang->line('firmdescription_menu'); ?>
										</label>
										<textarea name="firmdescription" class="form-control required" id="firmdescription" max-length="500"><?php echo $description; ?></textarea>

									</div>
								</div>
							</div>
							<h3 class="box-title"><strong><?php echo $this->lang->line('contactperson_menu'); ?></strong></h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctname">
											<?php echo $this->lang->line('firstname_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="<?php echo $ctname ; ?>" id="ctname" name="ctname" maxlength="128" style="height: 32px;" width="50%">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctsurname">
											<?php echo $this->lang->line('surname_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="<?php echo $stsurname; ?>" id="ctsurname" name="ctsurname" maxlength="128" style="height: 32px;" width="50%">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctphonenumber">
											<?php echo $this->lang->line('mobilenumber_menu'); ?>
										</label>
										<input type="text" class="form-control required digits" id="ctphonenumber" value="<?php echo $ctphonenumber; ?>" name="ctphonenumber" maxlength="10" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctemail">
											<?php echo $this->lang->line('emailaddress_menu'); ?>
										</label>
										<input type="text" class="form-control required email" id="ctemail" value="<?php echo $ctemail; ?>" name="ctemail" maxlength="128" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="ctdescription">
											<?php echo $this->lang->line('description_menu'); ?>
										</label>
										<textarea name="ctdescription" class="form-control required" id="ctdescription" max-length="500" style="height: 32px;" width="50%"><?php echo $ctdescription; ?></textarea>

									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>





				</div>
			</div>
			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editCustomer.js" type="text/javascript"></script>