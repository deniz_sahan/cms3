<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>
		<?php echo $pageTitle; ?>
	</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
	<!-- Bootstrap 3.3.4 -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
	<!-- FontAwesome 4.3.0 -->
	<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!-- Ionicons 2.0.0 -->
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
	<!-- Select2 -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
	<!-- Theme style -->
	<link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>
	<!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
	<link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css"/>
	<!-- daterange picker -->

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
	<!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

	<!--    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrapcombined.css" rel="stylesheet" type="text/css" />    -->
	<link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">

	<!--    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />-->
	<!--    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>-->

	<!-- jQuery 2.1.4 -->
	<script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
	<script type="text/javascript">
		var baseURL = "<?php echo base_url(); ?>";
	</script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		.navbar .nav>li>.dropdown-menu:before {
			position: absolute;
			top: -7px;
			left: 9px;
			display: inline-block;
			border-right: 0;
			border-bottom: 0;
			border-left: 0;
			border-bottom-color: rgba(0, 0, 0, 0.2);
			content: '';
		}
		
		.navbar .nav>li>.dropdown-menu:after {
			position: absolute;
			top: -6px;
			left: 10px;
			display: inline-block;
			border-right: 0;
			border-bottom: 0;
			border-left: 0;
			content: '';
		}
		
		.select2-container .select2-selection--single {
			height: 32px;
		}
		
		.select2-container--default .select2-selection--single {
			border-radius: 0;
		}
		
		.select2-container--default .select2-selection--single .select2-selection__arrow {
			height: 32px;
		}
		
		.select2-container--default .select2-selection--multiple {
			border-radius: 0;
		}
		
		.select2-container--default .select2-selection--multiple {
			height: 32px;
		}
		
		.select2-container {
			width: 150px;
		}
	</style>
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/dist/img/favicon.png" sizes="16x16">
</head>

<body class="skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo base_url(); ?>" class="logo" style="background-color: #1a2226; ">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>Locus</b></span>
				<!-- logo for regular state and mobile devices -->
				<!--          <span class="logo-lg"><b>LOCUS</b></span>-->
				<img src="<?php echo base_url(); ?>assets/dist/img/ust-locus-logo.png" style="height: 80%; margin-left: -10px; margin-top: 10px;"/>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
			



				<div class="navbar-custom-menu" style="float:  left;margin: 3px;font-size: 15px;">
					<ul class="nav navbar-nav">
						<li>
							<!--
          <a href="<?php echo base_url(); ?>" class="">
          <span class=""><?php echo $customername; ?> &nbsp; <small>Welcome to LOCUS!</small></span>
          </a>
		  -->
						</li>
					</ul>
				</div>

				<div class="navbar-custom-menu" style="padding: 4px 4px;">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<?php 
        if($this->session->userdata('lang') == 'english'){
        ?>
							<a class="btn-md" href="<?php echo base_url().'language/turkish'; ?>"><img src="http://flags.fmcdn.net/data/flags/w580/tr.png" width="20px"/> Turkish</a>
							<?php }elseif($this->session->userdata('lang') == 'turkish'){ ?>
							<a class="btn-md" href="<?php echo base_url().'language/english'; ?>"><img src="http://i64.tinypic.com/fd60km.png"/> English</a>
							<?php }else{ ?>
							<a class="btn-md" href="<?php echo base_url().'language/english'; ?>"><img src="http://i64.tinypic.com/fd60km.png"/> English</a>
							<?php } ?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-bell-o"></i>
                            <span class="label label-warning"><?php if(!empty($getrunningcampaign)){ echo count($getrunningcampaign); } ?></span>
                        </a>
							


								<ul class="dropdown-menu notify-drop" style="width:auto;left: 1px;">
									<div class="notify-drop-title">
										<h5 style="padding-left: 16px;border-bottom:  1px solid;padding-bottom: 4px;">
											<?php echo $this->lang->line('instantmessages_menu'); ?> </h5>
										<div class="row">

										</div>
									</div>

									<!-- end notify title -->

									<!-- notify content -->
									<div class="drop-content">
										<li>
											<!-----Instant message box----->

											<div class="box box-primary" style="border-color:white;">
												<?php 
                                    if(!empty($getrunningcampaign)){
                                    foreach($getrunningcampaign as $currenticampaign){ ?>
												<div class="box box-info" style="border-color:white;">
													<div class="box-header with-border" style="border-color:white;">
														<h3 class="box-title"><a href="<?php echo base_url(); ?>/importcsvdataInstantmessage"><?php echo $currenticampaign->inmname; ?></a></h3>

													</div>
													<div class="box-body">
														<!--                                           <p><?php echo $currenticampaign->message; ?></p>-->
														<p class="time label-warning text-center">
															<?php 
                                           $to_time = strtotime(date('Y-m-d H:i:s'));
                                            $from_time = strtotime($currenticampaign->startdate);
                                                //echo round(abs($to_time - $from_time) / 60); 
                                                  $originalDate = $currenticampaign->startdate;
                                                  $newDate = date("d-m-Y", strtotime($originalDate));
                                                echo "Start Date: ".$newDate;
                                                ?>
														</p>
													</div>
												</div>
												<?php }
                                    }
                                    ?>
											</div>

											<!-----Instant message box----->
										</li>
									</div>
									<div class="notify-drop-footer text-center">
										<a href=""><i class="fa fa-stop"></i> Stop messages</a>
									</div>
								</ul>
							</li>
							<!-- /.navbar-collapse -->
							<!--              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : date("d-m-Y H:i", strtotime($last_login)); ?></li>
                </ul>
              </li>-->
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<?php if(!empty($profileimage)) { ?>
									<img src="<?php echo base_url(); ?>uploads/images/<?php echo $profileimage; ?>" class="user-image" alt="User Image"/>
									<?php } else { ?>
									<img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
									<?php } ?>
									<span class="hidden-xs">
										<?php echo $name; ?>
									</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<?php if(!empty($profileimage)) { ?>
										<img src="<?php echo base_url(); ?>uploads/images/<?php echo $profileimage; ?>" class="img-circle" alt="User Image"/>
										<?php } else { ?>
										<img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image"/>
										<?php } ?>
										<p>
											<?php echo $name; ?>
											<small>
												<?php echo $role_text; ?>
											</small>
										</p>
									</li>
									<li class="user-body"></li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<a href="<?php echo base_url(); ?>loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i><?php echo $this->lang->line('Change_Password'); ?></a>
										</div>
										<div class="pull-right">
											<a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> <?php echo $this->lang->line('signout_menu'); ?></a>
										</div>
									</li>
								</ul>
							</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar" style=" background-color: #1a2226;">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- sidebar menu: : style can be found in sidebar.less -->

				<ul class="sidebar-menu">
					<li class="header"></li>
					<li class="treeview">
				<a href="#">
                <i class="fa fa-wifi"></i>
                <span><?php echo $this->lang->line('wifi_menu'); ?> </span>
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>


			<ul class="treeview-menu">
			<li class="treeview">
			<a href="<?php echo base_url(); ?>dashboard">
            <i class="fa fa-dashboard"></i> <span><?php echo $this->lang->line('executivedashboard_menu'); ?></span></i>
            </a>	
			</li>
			<li class="treeview">
			<a href="<?php echo base_url(); ?>barchartsdata">
			<i class="fa fa-bar-chart"></i>
                <span><?php echo $this->lang->line('dashboardperlocation_menu'); ?></span>
             </a>						
			 </li>
							
			<li class="treeview">
			<a href="#">
			<i class="fa fa-envelope"></i>
			<span> Courses </span>
			<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
			<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>courses"><i class="fa fa-group"></i>Courses </a>
			</li>
			</ul>
			</li>
			  
			  <li class="treeview">
			  <a href="#">
			  <i class="fa fa-envelope"></i>
			  <span> Teacher Management </span>
			  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
			  </span>
              </a>
			  <ul class="treeview-menu">
			  <li><a href="<?php echo base_url(); ?>classroom"><i class="fa fa-group"></i>Teacher</a>
			  </li>
			  </ul>
			  </li>						
			  
			  <?php
			  if ( $role == ROLE_ADMIN || $role == ROLE_MANAGER ) {
			  ?>
			  <li class="treeview">
			  <a href="<?php echo base_url(); ?>showcustomers">
			  <i class="fa fa-group"></i>
			  <span><?php echo $this->lang->line('customermanagement_menu'); ?></span>
              </a>
			  </li>
			  <?php	}?>
				
			  <li class="treeview">
			  <a href="#">
			  <i class="fa fa-map-marker"></i>
			  <span><?php echo $this->lang->line('locationlist_menu'); ?></span>
			  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
			  </span>
              </a>
			  <ul class="treeview-menu">
			  <li><a href="<?php echo base_url(); ?>showlocations"><i class="fa fa-map-marker"></i><?php echo $this->lang->line('locations_menu'); ?></a>
			  </li>
			  <li><a href="<?php echo base_url(); ?>showbranches"><i class="fa fa-hdd-o"></i><?php echo $this->lang->line('branche_menu'); ?><?php echo $this->lang->line('accesspoint_menu'); ?></a>
			  </li>
			  </ul>
			  </li>

			  <li class="treeview">
			  <a href="#">
			  <i class="fa fa-wifi"></i>
			  <span><?php echo $this->lang->line('wifistatistic_menu'); ?> </span>
			  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
			  </span>
			  </a>
			  <ul class="treeview-menu">
			  <li>
			  <a href="<?php echo base_url(); ?>wifivisitors">
			  <i class="fa fa-wifi"></i>
			  <span><?php echo $this->lang->line('wifivisitors_menu'); ?></span>
			  </a>
			  </li>
			  <li>
			  <a href="<?php echo base_url(); ?>listVisitorActivity">
			  <i class="fa fa-wifi"></i>
			  <span><?php echo $this->lang->line('totaluniqueusers_menu'); ?></span>
			  </a>								
			  </li>
			  <?php
			  if ( $role == ROLE_ADMIN || $role == ROLE_MANAGER ) {
			  ?>
			  <?php }?>
			  </ul>
			  </li>
							
			  <li class="treeview">
			  <a href="<?php echo base_url(); ?>userListing">
			  <i class="fa fa-user"></i>
			  <span><?php echo $this->lang->line('users_menu'); ?></span>
              </a>					
			  </li>
			
                            <li class="treeview">
			  <a href="<?php echo base_url(); ?>students">
			  <i class="fa fa-files-o"></i>
			  <span>Students</span>
                          </a>				
			  </li>
			  <li class="treeview">
			  <a href="<?php echo base_url(); ?>userstats">
			  <i class="fa fa-files-o"></i>
			  <span><?php echo $this->lang->line('reports_menu'); ?></span>
                          </a>				
			  </li>
			  
			  </ul>
			  </li>
			  </ul>
			  </section>
			<!-- /.sidebar -->
			
			
			
			
			<div class="logo2">
				<img src="<?php echo base_url(); ?>assets/dist/img/elogo1.png" style="height: 60%; margin-left: 10px;"/>
				<style>
					.logo2 {
						height: 170px;
						/*can be anything*/
						width: 230px;
						/*can be anything*/
						position: fixed;
						bottom: -70px;
						margin: auto;
					}
				</style>
			</div>



		</aside>