<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>LOCUS</b> CMS Admin System | Version 1.6
	</div>
	<strong>Copyright &copy; 2018 <a href="<?php echo base_url(); ?>">LOCUS</a>.</strong> All rights reserved.
</footer>

<!-- jQuery UI 1.11.2 -->
<!-- <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/chartjs/chart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/appgraph.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<script type="text/javascript">
	var windowURL = window.location.href;
	pageURL = windowURL.substring( 0, windowURL.lastIndexOf( '/' ) );
	var x = $( 'a[href="' + pageURL + '"]' );
	x.addClass( 'active' );
	x.parent().addClass( 'active' );
	var y = $( 'a[href="' + windowURL + '"]' );
	y.addClass( 'active' );
	y.parent().addClass( 'active' );

	/////////////////instant message ajax call ====start==

	//
	$( document ).ajaxStart( function () {
		Pace.restart()
	} );

	function callAjax() {

		//alert(text);
		$.ajax( {
			url: '<?php echo base_url();?>cronscript',
			success: function ( result ) {},
			complete: function ( jqXHR, status ) {
				setInterval( function () {
					$.ajax( {
						url: '<?php echo base_url();?>cronscript',
						success: function ( result ) {}
					} );
				}, 3000 );
			}
		} );
	}
	//callAjax();
	///////////////////////////////////====end=====


	// $(document).ready(function() {
	//  setInterval(function() {
	//    cache_clear()
	//  }, 360000);
	//});

	function cache_clear() {
		window.location.reload( true );
		// window.location.reload(); use this if you do not remove cache
	}
</script>

<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
</script>
<script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
</script>
<script type="text/javascript">
	$( '#datetimepicker' ).datetimepicker( {
		format: 'dd-MM-yyyy hh:mm'
	} );
</script>
<script type="text/javascript">
	$( '#datetimepickerend' ).datetimepicker( {
		format: 'dd-MM-yyyy hh:mm'
	} );
	$( '#datetimepicker1' ).datetimepicker( {
		format: 'dd-MM-yyyy hh:mm'
	} );
</script>
<script type="text/javascript">
	$( '#datetimepickerend1' ).datetimepicker( {
		format: 'dd-MM-yyyy hh:mm'
	} );
	$( document ).ready( function () {
		$( '.js-example-basic-multiple' ).select2();
		$( '.js-example-basic-single' ).select2();
	} );
</script>

</body>
</html>