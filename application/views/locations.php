<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
        <i class="fa fa-map"></i><?php echo $this->lang->line('locationmanagement_menu'); ?>

      </h1>
	
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNewloc">
						<?php echo $this->lang->line('addnewlocation_menu'); ?>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--                    <h3 class="box-title"><?php echo $this->lang->line('location_list'); ?></h3>--><br>
						<div class="box-tools">
							<form action="<?php echo base_url() ?>showlocations" method="POST" id="searchList">
								<div class="input-group">
									<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;height: 30px;" placeholder="Search"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
									</div>
									<a href="locations.php"></a>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<th>
									<?php echo $this->lang->line('locationid_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('locationname_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('deviceid_menu'); ?>
								</th>
								<th style="width: 200px !important;">
									<?php echo $this->lang->line('address_menu'); ?>
								</th>

								<th>
									<?php echo $this->lang->line('session_control'); ?>
								</th>
								<th>Lan IP (1)</th>
								<th>Lan IP (2)</th>
								<th>Status</th>
								<th class="text-center">
									<?php echo $this->lang->line('actions_menu'); ?>
								</th>
							</tr>
							<?php
							if ( !empty( $locationRecords ) ) {
								foreach ( $locationRecords as $record ) {
									$date = date( "Y-m-d H:i:s" );
									$time = strtotime( $date );
									$time = $time - ( 5 * 60 );
									$date = date( "Y-m-d H:i:s", $time );
									?>
							<tr>
								<td>
									<?php echo $record->locationid ?>
								</td>
								<td>
									<?php echo $record->locationname ?>
								</td>
								<td>
									<?php echo $record->deviceid ?>
								</td>
								<td style="width: 200px !important;">
									<?php echo $record->address ?>
								</td>

								<td>
									<?php echo $record->schours." Hours and ".$record->scminutes." minutes" ?>
								</td>
								<td>
									<?php echo $record->lanip ?>
								</td>
								<td>
									<?php echo $record->lanip2 ?>
								</td>
								<td>
									<?php 
                      if($record->statusdate > $date){if($record->status == '1'){echo '<span class = "label label-success">Active</span>';}}elseif(empty($record->status)){echo 'Setting is missing';}else{
//                      $ApiUrl = "http://8bit.mobilus.net/";
//                        $SmsUser  =  'elogo-LG1001';
//                        $SmsPass  =  '8975';
//                        $Companyode = 'LG1001';
//                        $number = '5539803313';
//                        $originator = 'LOGOELEKT';
//                        $message = "Hello Bekir, Netgate Device Not working: Customer Name:".$onddata->custname." and Device ID is :".$onddata->deviceid."";
//                        //$originator = 'ALPET';
//                        $strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$number</Numbers><Originator>$originator</Originator><SDate></SDate></MainmsgBody>";
//
//                            $ch = curl_init();
//                            curl_setopt($ch, CURLOPT_URL,$ApiUrl); 
//                            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
//                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
//                            curl_setopt($ch, CURLOPT_POSTFIELDS, $strXML); 
//                            $result = curl_exec($ch);
//
//                            $pieces = explode(" ", $result);
//                            $smscode = $pieces[1]; // piece2
//                            curl_close($ch);
                      echo 'Device has some problem';}
                      ?>
								</td>
								<td class="text-center">
									<a class="btn btn-sm btn-info" href="<?php echo base_url().'editloc/'.$record->locationid; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
									<a class="btn btn-sm btn-danger deleteLocation" href="#" data-locationid="<?php echo $record->locationid; ?>" title="Delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletelocation.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "showlocations/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>