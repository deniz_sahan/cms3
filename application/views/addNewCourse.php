<?php
if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $classroominfo ) ) {
//pre($courseInfo);
//exit;
	foreach ( $classroominfo as $cf ) {
		$coursename = $cf->locationid;
		$courseid = $cf->locationname;


	}
}
?>


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->




				<div class="box box-primary">
					<div class="box-header">
						<!--                        <h3 class="box-title"><?php echo $this->lang->line('entercustomerdetail_menu'); ?></h3>-->
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<?php $this->load->helper("form"); ?>
					<form role="form" id="addCustomer" action="<?php echo base_url() ?>addNewcourse" method="post" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="cname">
											Course Name
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('cname'); ?>" id="cname" name="cname" maxlength="128">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctitle">
											Course Title
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('ctitle'); ?>" id="ctitle" name="ctitle" maxlength="128">
									</div>

								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="cinstructor">
											Course Instructor
										</label>
										<input type="text" class="form-control required digits" id="cinstructor" style="height: 32px;" value="<?php echo set_value('cinstructor'); ?>" name="cinstructor" maxlength="128">
									</div>
								</div>
									<div class="col-xs-3">
								<div class="form-group">
									<label>
									Select Classroom
									</label>
									<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">

                                                   
                                     <?php foreach($location as $courserecord){ ?>
                                    <option value="<?php echo $courserecord->locationid;?>"><?php echo $courserecord->locationname; ?></option>
                                   <?php } ?>
                                  </select>

								
								</div>
							</div>
								<div>
								</div>
								
								
								
							</div>
							<!-- /.start Date -->
				<div class="col-md-3" style="margin-top: 5px;">
					<div id="datetimepicker" class="input-append date">
					<label>
					<?php echo $this->lang->line('startdate_menu'); ?>
					</label>
					<p>
					<input type="text" id="datetimepickervaluestart" name="datetimepickervaluestart" onblur="validation()" style="height: 26px; " required> </input>
					<span class="add-on" style="height: 26px;">
					<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>					
					</div>
				   </div>
				   <!-- /.end Date -->
				   <div class="col-md-3" style=" margin-top: 5px;">
				   <div id="datetimepickerend" class="input-append date">
				   <label>
				   <?php echo $this->lang->line('enddate_menu'); ?>
				   </label>
				   <p>
				   <input type="text" id="datetimepickervalueend" name="datetimepickervalueend" onblur="validation()" style="height: 26px;" required></input>
				   <span class="add-on" style="height: 26px;">
				   <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
				   </span>
				   </div>
				   </div>		
				    <!-- /.Select Time -->
					<div class="col-md-3" style=" margin-top: 5px;">
					<label>
							Select Start Time
						</label>
					<div id="datetimepickerend" class="input-append date">
						
						<input type="time" name="usr_starttime">
					</div>
					</div>
					 <!-- /.Select End Time -->
					<div>
					<label>
							Select End Time
						</label>
					<div id="datetimepickerend" class="input-append date">
						
						<input type="time" name="usr_endtime">
					</div>
					
				   </div>
				  </div>
				    
								  <div class="form-group">
									<label>
										Select Day
									</label>
									<select class="form-control js-example-basic-multiple" multiple="multiple" id="selectday" name="selectday[]">
										
										 <option value="mon">Monday</option>
										 <option value="tue">Tuesday</option>
										 <option value="wed">Wednesday</option>
										 <option value="thu">Thursday</option>
										 <option value="fri">Friday</option>
										 <option value="sat">Saturday</option>
										 <option value="sun">Sunday</option>
										 </select>
										 </div>
				  
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>
				</div>
			</div>



			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url(); ?>assets/js/editcustomer.js" type="text/javascript"></script>