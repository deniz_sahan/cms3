<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('visited_websites'); ?>
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-xs-12">
				<div class="box-tools">

				</div>
				<div class="box">
					<div class="box-header" style="text-align: center">

						&nbsp;&nbsp;&nbsp;
						<?php if(!empty($countrecords)){echo '<code>'.$countrecords . ' Results found </code>';} ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<th>
									<?php echo $this->lang->line('visited_time'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('website_menu'); ?>
								</th>
							</tr>
							<?php
							//pre($visitedwebsites);
							//exit;
							if ( !empty( $visitedwebsites ) ) {
								foreach ( $visitedwebsites as $record ) {
									if ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->MethodId ) ) {
										$vistedurl = $record->MethodId;
									} elseif ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->SchemeId ) ) {
										$vistedurl = $record->SchemeId;
									} elseif ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->HostID ) ) {
										$vistedurl = $record->HostID;
									}
									elseif ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->PortNo ) ) {
										$vistedurl = $record->PortNo;
									} elseif ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->Resource ) ) {
										$vistedurl = $record->Resource;
									} elseif ( preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $record->UserId ) ) {
										$vistedurl = $record->UserId;
									} else {
										$vistedurl = '';
									}

									?>
							<tr>
								<td>
									<?php echo $record->Time; ?>
								</td>

								<td>
									<?php 
                      $vistedurl = strlen($vistedurl) > 30 ? substr($vistedurl,0,30)."..." : $vistedurl;
                      echo $vistedurl; ?>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletewifivisitors.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			var customerid = '<?php echo $customerid; ?>';
			var locationid = '<?php echo $locationid; ?>';
			jQuery( "#searchList" ).attr( "action", baseURL + "wifivisitors/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>