<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';

if ( !empty( $branchinfo ) ) {
	foreach ( $branchinfo as $cf ) {
		$branchid = $cf->branchid;
		$bname = $cf->bname;
		$bip = $cf->bip;
		$descriptrion = $cf->descriptrion;
	}
}


?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('branchmanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->



				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo $this->lang->line('enterbranchdetails_menu'); ?>
						</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<form role="form" action="<?php echo base_url() ?>editbranch" method="post" id="editUser" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="branchname">
											<?php echo $this->lang->line('branchname_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="<?php echo $bname; ?>" id="bname" name="bname" maxlength="128" style="height: 32px;" width="50%">
										<input type="hidden" value="<?php echo $branchid; ?>" name="branchid" id="branchid"/>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="bip">
											<?php echo $this->lang->line('accesspointip_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="bip" value="<?php echo $bip; ?>" name="bip" maxlength="128" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="descriptrion">
											<?php echo $this->lang->line('details_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="descriptrion" value="<?php echo $descriptrion; ?>" name="descriptrion" maxlength="500" style="height: 32px;" width="50%">
									</div>
								</div>

							</div>
						</div>
						<!-- /.box-body -->
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editBranch.js" type="text/javascript"></script>