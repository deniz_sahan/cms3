<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('addSmslimit'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->

				<div class="box box-primary">
					<div class="box-header">
						<!--                        <h3 class="box-title">Enter SMS Package Detail</h3>-->
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<?php $this->load->helper("form"); ?>
					<form role="form" id="addsmslimit" action="<?php echo base_url() ?>addNewSmsLimit" method="post" role="form">
						<div class="box-body">
							<?php 
                                if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
							<!-- /For Selecting Values Start here-->
							<div class="form-group">
								<label>
									<?php echo $this->lang->line('selectlocation_menu'); ?>
								</label>
								<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
									<?php foreach($locations as $locationsrecord){ ?>
									<option value="<?php echo $locationsrecord->locationid;?>">
										<?php echo $locationsrecord->locationname; ?>
									</option>
									<?php } ?>
								</select>
							</div>
							<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
							<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
							<?php }else{
                                ?>
							<div class="form-group">
								<label>
									<?php echo $this->lang->line('selectcustomer_menu'); ?>
								</label>
								<select class="form-control" id="selectcustomermsg" name="selectcustomermsg" style="width: 50%;">
                          
									<option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                                      <?php foreach($customers as $customersrecord){ ?>
                                       <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                                         <?php } ?>
                                  </select>

							
							</div>
							<div class="form-group">
								<label>
									<?php echo $this->lang->line('selectlocation_menu'); ?>
								</label>
								<select class="form-control" id="selectlocationmsg" name="selectlocationmsg" style="width: 50%;">
									<?php if(!empty($locationinfo)){  ?>
									<option value="<?php echo $locationid; ?>">
										<?php echo $locationname; ?>
									</option>
									<?php } ?>
								</select>
							</div>

							<?php } ?>
							<!-- /For Selecting Values End here-->





							<div class="form-group">
								<label>
									<?php echo $this->lang->line('Packet_Limit'); ?>
								</label> <br>
								<input class="form-control " type="text" id="pakagename" name="pakagename" maxlength="128" ; style="height: 33px; width: 50% " required>
							</div>




							<!-- /.box-body -->

							<div class="box-footer">
								<input type="submit" class="btn btn-primary" style="width: 70px;" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
								<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
							</div>
						</div>
					</form>
				</div>
			</div>



			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>