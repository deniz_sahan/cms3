<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css"/>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
        <i class="fa fa-users"></i> Login History
      </h1>
	
	</section>
	<section class="content">
		<div class="row">
			<form action="<?php echo base_url() ?>login-history" method="POST" id="searchList">
				<div class="col-md-2 form-group">
					<label>
						<?php echo $this->lang->line('startdate_menu'); ?>
					</label>
					<p>
						<input for="fromDate" type="text" name="fromDate" value="<?php echo $fromDate; ?>" class="form-control datepicker" style="height: 26px; "/>
						<span class="add-on" style="height: 26px;">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
            </span>
					
				</div>
				<div class="col-md-2 form-group">
					<label>
						<?php echo $this->lang->line('enddate_menu'); ?>
					</label>
					<p>
						<input id="toDate" type="text" name="toDate" value="<?php echo $toDate; ?>" class="form-control datepicker" style="height: 26px; "/>
						<span class="add-on" style="height: 26px;">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
            </span>
					
				</div>
				<div class="col-md-2 form-group">
					<label>
						<?php echo $this->lang->line(''); ?>
					</label>
					<p>
						<input id="searchText" type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control" style="height: 30px;" placeholder="Search"/>
						<span class="fa fa-search form-control-feedback" style="height:  30px;margin-top: 27px;margin-right: 17px;"></span>
				</div>
				<div class="col-md-2 form-group">

					<input class="btn btn-success" type="submit" value="<?php echo $this->lang->line('showresults_menu'); ?>" style=" background: #367fa9; border: blue; margin-top: 18px; "/>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?= $userInfo->name." : ".$userInfo->email ?></h3>
						<div class="box-tools">
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th>User role</th>
								<th>IP Address</th>
								<th>User Agent</th>
								<th>Agent Full String</th>
								<th>Platform</th>
								<th>Date-Time</th>
							</tr>
							<?php
							if ( !empty( $userRecords ) ) {
								foreach ( $userRecords as $record ) {
									$sessionData = $record->sessionData;
									$arr_php = json_decode( $sessionData, true );
									$userroletext = $arr_php[ 'roleText' ];
									?>
							<tr>
								<td>
									<?php echo $userroletext ?>
								</td>
								<td>
									<?php echo $record->machineIp ?>
								</td>
								<td>
									<?php echo $record->userAgent ?>
								</td>
								<td>
									<?php echo $record->agentString ?>
								</td>
								<td>
									<?php echo $record->platform ?>
								</td>
								<td>
									<?php $originalDate = $record->createdDtm; $newDate = date("d-m-Y H:i", strtotime($originalDate)); echo $newDate; ?>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			jQuery( "#searchList" ).attr( "action", link );
			jQuery( "#searchList" ).submit();
		} );

		jQuery( '.datepicker' ).datepicker( {
			autoclose: true,
			format: "dd-mm-yyyy"
		} );
	} );
</script>