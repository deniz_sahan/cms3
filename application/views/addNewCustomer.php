<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->




				<div class="box box-primary">
					<div class="box-header">
						<!--                        <h3 class="box-title"><?php echo $this->lang->line('entercustomerdetail_menu'); ?></h3>-->
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<?php $this->load->helper("form"); ?>
					<form role="form" id="addCustomer" action="<?php echo base_url() ?>addNewCustomer" method="post" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customername">
											<?php echo $this->lang->line('customername_menu'); ?>
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('customername'); ?>" id="customername" name="customername" maxlength="128">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="femail">
											<?php echo $this->lang->line('firmemailaddress_menu'); ?>
										</label>
										<input type="text" class="form-control required email" id="femail" style="height: 32px;" value="<?php echo set_value('femail'); ?>" name="femail" maxlength="128">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="password">
											<?php echo $this->lang->line('password_menu'); ?>
										</label>
										<input type="password" class="form-control required" id="password" name="password" maxlength="20" style="height: 32px;">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="cpassword">
											<?php echo $this->lang->line('confirmpassword_menu'); ?>
										</label>
										<input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="20" style="height: 32px;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="address">
											<?php echo $this->lang->line('address_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="address" style="height: 32px;" value="<?php echo set_value('address'); ?>" name="address" maxlength="500">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="city">
											<?php echo $this->lang->line('city_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="city" style="height: 32px;" value="<?php echo set_value('city'); ?>" name="city" maxlength="120">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="firmdescription">
											<?php echo $this->lang->line('firmdescription_menu'); ?>
										</label>
										<textarea name="firmdescription" class="form-control required" id="firmdescription" style="height: 32px;" value="<?php echo set_value('firmdescription'); ?>" max-length="500"></textarea>

									</div>
								</div>
							</div>
							<h3 class="box-title"><strong><?php echo $this->lang->line('contactperson_menu'); ?></strong></h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctname">
											<?php echo $this->lang->line('firstname_menu'); ?>
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('ctname'); ?>" id="ctname" name="ctname" maxlength="128">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctsurname">
											<?php echo $this->lang->line('surname_menu'); ?>
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('ctsurname'); ?>" id="ctsurname" name="ctsurname" maxlength="128">
									</div>

								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctphonenumber">
											<?php echo $this->lang->line('mobilenumber_menu'); ?>
										</label>
										<input type="text" class="form-control required digits" id="ctphonenumber" style="height: 32px;" value="<?php echo set_value('ctphonenumber'); ?>" name="ctphonenumber" maxlength="10">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ctemail">
											<?php echo $this->lang->line('emailaddress_menu'); ?>
										</label>
										<input type="text" class="form-control required email" id="ctemail" style="height: 32px;" value="<?php echo set_value('ctemail'); ?>" name="ctemail" maxlength="128">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="firmdescription">
											<?php echo $this->lang->line('description'); ?>
										</label>
										<textarea name="ctdescription" class="form-control required" id="ctdescription" style="height: 32px;" value="<?php echo set_value('ctdescription'); ?>" max-length="500"></textarea>

									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>
				</div>
			</div>



			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url(); ?>assets/js/editcustomer.js" type="text/javascript"></script>