<?php

if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;

	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('campaignlist'); ?>
		</h1>
		<!--    <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home_menu'); ?></a></li> 
      <li><a href="#"><?php echo $this->lang->line('messages_menu'); ?></a></li>
      <li class="active"><?php echo $this->lang->line('instantmessages_menu'); ?></li>
    </ol>-->
	</section>

	<!-- Main content -->
	<section class="content">
		<?php //print_r($results); ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">

					<div class="row" style="padding-top: 20px;">
						<div class="col-md-2">
							<form action="<?php echo base_url() ?>advancecampaignlist" method="POST" id="-`15M6K8LO9P0[                                         			">
								<div class="input-group">
									<div class="form-group" style="padding-left: 18px;">
										<label>
											<?php echo $this->lang->line('campaignname_menu'); ?>
										</label>
										<input type="text" name="advsearchText" value="<?php if(isset($advsearchText)){echo $advsearchText;} ?>" class="form-control input-sm" style="width: 150px;height: 32px;"/>

										<!--                                <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>-->
									</div>

								</div>
								<!--                        </form>-->
						</div>
						<div class="col-md-4" style="padding-left: 0px;padding-right:  0;">
							<?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
							<div class="col-xs-12">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control js-example-basic-multiple" multiple="multiple" id="selectlocationmsg" name="selectlocationmsg[]">
										<?php foreach($locations as $locationsrecord){ ?>
										<option value="<?php echo $locationsrecord->locationid;?>">
											<?php echo $locationsrecord->locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
							<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
							<?php }else{
            ?>
							<div class="col-xs-5" style="padding-left:0;">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectcustomer_menu'); ?>
									</label>
									<select class="form-control js-example-basic-single" id="selectcustomermsg" name="selectcustomermsg">
                      <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                      <?php foreach($customers as $customersrecord){ ?>
                      <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                    <?php } ?>
                </select>
								
								</div>
							</div>
							<div class="col-xs-7" style="padding-left:3px;">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control js-example-basic-multiple" multiple="multiple" id="selectlocationmsg" name="selectlocationmsg[]">
										<?php if(!empty($locationinfo)){  ?>
										<option value="<?php echo $locationidd; ?>">
											<?php echo $locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<?php 
            }
            ?>
						</div>
						<div class="col-md-4" style="padding-left:0;">
							<div class="col-xs-6" style="padding-left:0;padding-right:0;">
								<div id="datetimepicker1" class="input-append date form-group">
									<label>
										<?php echo $this->lang->line('startdate2_menu'); ?>
									</label><br>
									<input type="text" id="datetimepickervaluestart" name="datetimepickervaluestart" value="<?php if(isset($datetimepickervaluestart)){echo $datetimepickervaluestart;} ?>" style="height: 32px;width: 150px;" placeholder='<?php echo $this->lang->line(' startdate2_menu '); ?>'> </input>
									<span class="add-on" style="height: 32px;">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
								
								</div>
							</div>

							<div class="col-xs-6" style="padding-left:0;padding-right:0;">
								<div id="datetimepickerend1" class="input-append date form-group">
									<label>
										<?php echo $this->lang->line('enddate2_menu'); ?>
									</label><br>
									<input type="text" id="datetimepickervalueend" name="datetimepickervalueend" value="<?php if(isset($datetimepickervalueend)){echo $datetimepickervalueend;} ?>" style="height: 32px;width: 150px;" placeholder='<?php echo $this->lang->line(' enddate2_menu '); ?>'></input>
									<span class="add-on" style="height: 32px;">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                      </span>
								
								</div>
							</div>
						</div>
						<div class="col-md-2" style="padding-top: 24px;padding-left: 0px;">
							<input type="hidden" name="statusactive" value="active"/>
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('filterresults_menu'); ?>" style="margin-right: 10px;"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
						</form>
					</div>
					<div class="box-header" style="margin-bottom: 10px;">
						<h3 class="box-title">
							<?php  ?>
						</h3>
						<div class="box-tools" style="width:100%">
							<form action="<?php echo base_url() ?>importcsvdataInstantmessage" method="POST" id="searchList">
								<div class="input-group" style="width:100%">
									<input name="statusactive" type="hidden" value="active">
									<input name="statusnotactive" type="hidden" value="notactive">
									<div class="col-md-12" style="text-align:  center;">
										<button class="btn" type="submit" name="running" style="border-right: 1px solid darkgrey;" value="active">Running</button>
										<button class="btn" type="submit" name="notrunning" value="notactive">Not Active</button>
									</div>

								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<!--                  <th><?php //echo $this->lang->line('campaignid_menu'); ?></th>-->
								<th>
									<?php echo $this->lang->line('campaignname_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('startdate2_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('status_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('messagerepeat_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('locations_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('enddate2_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('smscount_menu'); ?>
								</th>
								<th>Priority</th>
								<th>Target Users</th>
								<th>
									<?php echo $this->lang->line('campaigntext_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('operations_menu'); ?>
								</th>
							</tr>
							<?php 
                if(!empty($results)){
                foreach($results as $row){ ?>
							<tr class="deleteid<?php echo $row->inmid; ?>">
								<!--                  <td><?php //echo $row->inmid; ?></td>-->
								<td>
									<?php echo $row->inmname; ?>
								</td>
								<td>
									<?php $originalDate = $row->startdate; $newDate = date("d-m-Y H:i", strtotime($originalDate)); echo $newDate; ?>
								</td>
								<td>
									<span class="label <?php if($row->status == 'active'){?>label-success<?php }else{?>label-danger<?php } ?>">
										<?php if($row->status == 'active'){ echo 'Runing';}else{echo $row->status;} ?>
									</span>
								</td>
								<td>
									<?php if(empty($row->messagetype)){ ?>
									<select class="form-control" id="selectmessagetype" name="messagetype" data-bind="<?php echo $row->inmid; ?>">
										<option>---Choose Type---</option>
										<option value='everytime'>
											<?php echo $this->lang->line('everytime_menu'); ?>
										</option>
										<option value='eachday'>
											<?php echo $this->lang->line('inevery24hours_menu'); ?>
										</option>
										<option value='xhours'>
											<?php echo $this->lang->line('manualrepeattime_menu'); ?>
										</option>
									</select>
									<div class="modal fade" id="modal-default2<?php echo $row->inmid; ?>" style="width: 100%;margin-left: 0px;">
										<div class="modal-dialog">
											<form name="" action="" method="POST" id="typexhour">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
													
														<h4 class="modal-title">
															<?php echo $this->lang->line('selectrepeattime_menu'); ?>
														</h4>
													</div>
													<div class="modal-body">
														<div id="qwerty" class="input-append">
															<input type="text" name="datetimepickervaluexhour" id="datetimepickervalueendxhour" class="datetimepickervalueendxhour" style="height: 33px; border-radius:0px;" placeholder='For example, 2  = Two Days' required> </input>
															<span class="add-on" style="height: 33px;">
                         <i class="icon-time"></i>
                     </span>
														
														</div>
														<input name="inmidxhour" type="hidden" value="<?php echo $row->inmid; ?>">
														<input name="messagexhour" type="hidden" value="xhours">
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
															<?php echo $this->lang->line('close_menu'); ?>
														</button>
														<button type="button" id="xhoursubmit" class="btn btn-primary">
															<?php echo $this->lang->line('savechanges_menu'); ?>
														</button>
													</div>
												</div>
											</form>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									<?php }else{ if($row->messagetype == 'everytime'){ echo 'Each Login'; }elseif($row->messagetype == 'eachday'){echo 'Every 24 hours';}elseif($row->messagetype == 'xhours'){echo 'Manual Repeat Time';}} ?>
								</td>
								<td>
									<?php echo $row->locationname; ?>
								</td>
								<td>
									<?php if($row->enddate == '0000-00-00 00:00:00' && $row->messagetype != ''){ ?>
									<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default<?php echo $row->inmid; ?>">
										<?php echo $this->lang->line('setenddateandtime_menu'); ?>
									</button>
									<div class="modal fade" id="modal-default<?php echo $row->inmid; ?>" style="width: 100%;margin-left: 0px;">
										<div class="modal-dialog">
											<form name="" action="" method="POST" id="enddateform">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
													
														<h4 class="modal-title">
															<?php echo $this->lang->line('settingenddateforcampaign_menu'); ?>
														</h4>
													</div>
													<div class="modal-body">
														<div id="datetimepicker" class="input-append date">
															<input type="text" name="datetimepickervalueend" id="datetimepickervalueend" class="datetimepickervalueend" style="height: 33px; border-radius:0px;" placeholder='End Date' required> </input>
															<span class="add-on" style="height: 33px;">
                         <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                     </span>
														
														</div>
														<input name="inmidenddate" type="hidden" value="<?php echo $row->inmid; ?>">
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
															<?php echo $this->lang->line('close_menu'); ?>
														</button>
														<button type="button" id="enddatesubmit" class="btn btn-primary">
															<?php echo $this->lang->line('savechanges_menu'); ?>
														</button>
													</div>
												</div>
											</form>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>

									<?php }else{ if($row->messagetype == ''){echo $this->lang->line('setrepeatmessage_menu');}else{$originalDatee = $row->enddate; $newDatee = date("d-m-Y H:i", strtotime($originalDatee)); echo $newDatee;}} ?>
								</td>
								<td>
									<?php echo $row->countsms; ?>
								</td>
								<td>
									<span class="label <?php if($row->priority == '1'){echo 'label-success';}elseif($row->priority == '2'){echo 'label-info';}elseif($row->priority == '3'){echo 'label-warning';} ?>">
										<?php if($row->priority == '1'){echo 'High';}elseif($row->priority == '2'){echo 'Medium';}elseif($row->priority == '3'){echo 'Low';} ?>
									</span>
								</td>
								<td>
									<?php if($row->permisionlist == 'permissionlist'){echo 'General';}else{ echo $row->permisionlist; } ?>
								</td>
								<td>
									<a href="#" class="toggler" data-prod-cat="<?php echo $row->inmid;?>">
										<?php echo $this->lang->line('show_menu'); ?>
									</a>
								</td>
								<td>
									<div class="onoffswitch">
										<a href="<?php echo base_url(); ?>setInstantstatus/<?php echo $row->inmid; ?>/<?php if($row->status == 'not active'){ echo '0';}else{ echo '1';} ?>/<?php echo $row->customerid; ?>/<?php echo $row->locationid; ?>/<?php echo $row->priority; ?>" class="onoffswitch-checkbox"><span class="label <?php if($row->status == 'active'){?>label-danger<?php }else{?>label-success<?php } ?>"><?php if($row->status == 'active'){ echo 'STOP Campaign';}else{if($row->messagetype == '' || $row->enddate == '0000-00-00 00:00:00'){}else{echo 'START Campaign';}} ?></span></a>
										<label class="onoffswitch-label" for="myonoffswitch">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
									
									</div>


								</td>
							</tr>
							<tr class="deleteid<?php echo $row->inmid; ?> cat<?php echo $row->inmid; ?>" style="border-bottom: 1px solid #3c8dbc; display: none;">
								<td>
									<strong>
										<?php echo $this->lang->line('messages_menu'); ?>
									</strong>
								</td>
								<td colspan="8" class="text-black">
									<?php echo $row->message; ?>
								</td>
								<td>
									<a class="btn btn-sm btn-danger deleteCampaign" href="#" data-inmid="<?php echo $row->inmid; ?>" title="Delete Campaign"><i class="fa fa-trash"></i></a>
									<a class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-default3<?php echo $row->inmid; ?>" href="<?php //echo base_url().'editinstantcampaign/'.$row->inmid; ?>" title="Edit Message"><i class="fa fa-pencil"></i></a>
									<!------Edit Modal section --------->
									<div class="modal fade" id="modal-default3<?php echo $row->inmid; ?>" style="width: 100%;margin-left: 0px;">
										<div class="modal-dialog">
											<form name="editcampaignform" action="" method="POST" id="editcampaignform<?php echo $row->inmid; ?>">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
													
														<h4 class="modal-title">
															<?php echo $this->lang->line('edit_menu'); ?> "
															<?php echo $row->inmname; ?>"
															<?php echo $this->lang->line('campaign_menu'); ?>
														</h4>
													</div>
													<div class="modal-body">
														<label for="instantmessagetext">
															<?php echo $this->lang->line('messagetext_menu'); ?>: </label>
														<textarea class="form-control" rows="3" placeholder="Send instant message after user logged In (limited to 160 characters)" maxlength="160" id="instantmessagetext" name="instantmessagetext"><?php echo $row->message; ?></textarea>
														<div class="form-group" style="text-align: right;height: 0;">
															<span id='remainingC'></span>
														</div>
														<input name="inmideditmessage" type="hidden" value="<?php echo $row->inmid; ?>">
														<div class="form-group">
															<label>Priority</label>
															<select class="form-control" id="priority" name="priority">
																<option value="1">High</option>
																<option value="2">Medium</option>
																<option value="3">Low</option>
															</select>
														</div>
													</div>

													<div class="modal-footer">
														<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
															<?php echo $this->lang->line('close_menu'); ?>
														</button>
														<button type="button" id="editcampaign" class="editcampaign btn btn-primary">
															<?php echo $this->lang->line('savechanges_menu'); ?>
														</button>
													</div>
												</div>
											</form>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									<!--------------End edit section-------------------->
								</td>
							</tr>
							<?php }
                }else{
                ?>
							<tr>
								<td colspan="10">No Records Found</td>
							</tr>
							<?php 
                }
                ?>
						</table>
					</div>
					<!-- /.box-body -->
					<div class="modal fade" id="modal-default-confirm-div" style="width: 100%;margin-left: 0px;">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
								
									<h4 class="modal-title">Attention</h4>
								</div>
								<div class="modal-body confirm-div-text">

								</div>
								<div class="modal-footer">
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				</div>
				<div class="box-footer clearfix">
					<?php echo $this->pagination->create_links(); ?>
				</div>
				<!-- /.box -->
			</div>
		</div>

	</section>
	<!-- /.content -->
</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "importcsvdataInstantmessage/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>

<script>
	$( document ).ready( function () {
		$( ".toggler" ).click( function ( e ) {
			e.preventDefault();
			$( '.cat' + $( this ).attr( 'data-prod-cat' ) ).toggle();
		} );
	} );
	$( document ).ready( function () {

		// set message type
		$( '#selectmessagetype' ).change( function () {
			var inmid = $( '#selectmessagetype' ).data( "bind" );
			var messagetype = $( this ).val();
			// AJAX request
			if ( messagetype == 'everytime' || messagetype == 'eachday' ) {
				$.ajax( {
					url: baseURL + 'setInstantMessagetype',
					method: 'post',
					data: {
						inmid: inmid,
						messagetype: messagetype
					},
					dataType: 'json',
					success: function ( response ) {
						window.location = baseURL + 'importcsvdataInstantmessage';
					}
				} );
			} else {
				$( '#modal-default2' + inmid ).modal( "show" );
			}
		} );
		////end date
		$( "#enddatesubmit" ).click( function () {
			$.ajax( {
				type: 'POST',
				url: baseURL + 'setInstantenddate',
				data: $( '#enddateform' ).serialize(),
				success: function ( response ) {
					//alert(response);
					window.location = baseURL + 'importcsvdataInstantmessage';
				}
			} );
		} );

		/////xhour 
		$( "#xhoursubmit" ).click( function () {
			$.ajax( {
				type: 'POST',
				url: baseURL + 'setInstantMessagetypexhour',
				data: $( '#typexhour' ).serialize(),
				success: function ( response ) {
					//alert(response);
					window.location = baseURL + 'importcsvdataInstantmessage';
				}
			} );
		} );
		////Edit message
		$( ".editcampaign" ).click( function () {
			var id = $( this ).closest( "form" ).attr( 'id' );
			$.ajax( {
				type: 'POST',
				url: baseURL + 'editInstantmessage',
				data: $( '#' + id ).serialize(),
				success: function ( response ) {
					//alert(response);
					window.location = baseURL + 'importcsvdataInstantmessage';
				}
			} );
		} );
		//////////


	} );
	$( document ).ready( function () {
		var len = 0;
		var maxchar = 160;

		$( '#instantmessagetext' ).keyup( function () {
			len = this.value.length
			if ( len > maxchar ) {
				return false;
			} else if ( len > 0 ) {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar - len ) );
			} else {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar ) );
			}
		} )
	} );
</script>
<script>
	// assumes you're using jQuery
	$( document ).ready( function () {
		$( '#modal-default-confirm-div' ).hide();
		<?php if($this->session->flashdata('reportmessage')){ ?>
		$( '.confirm-div-text' ).html( '<?php echo $this->session->flashdata('
			reportmessage '); ?>' );
		$( '#modal-default-confirm-div' ).modal( "show" );
	} );
	<?php } ?>
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletecampaign.js" charset="utf-8"></script>