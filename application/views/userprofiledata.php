<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
        User Profile
      </h1>
	
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a>
			</li>
			<li><a href="#">Examples</a>
			</li>
			<li class="active">User profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>assets/dist/img/avatar.png" alt="User profile picture">

						<h3 class="profile-username text-center">
							<?php echo $thisuserdata[0]->name." ". $thisuserdata[0]->surname;?>
						</h3>

						<p class="text-muted text-center">Wifi User</p>

						<ul class="list-group list-group-unbordered" style="margin-left: 0px;">
							<li class="list-group-item">
								<b>
									<?php echo $this->lang->line('numberofentrances_menu'); ?>
								</b>
								<a class="pull-right">
									<?php echo $thisuserdata[0]->countsuccesslogin; ?>
								</a>
							</li>
							<!--                <li class="list-group-item">
                  <b>Total Time on Network</b> <a class="pull-right">5 Hours</a>
                </li>
                <li class="list-group-item">
                  <b>Places Visit</b> <a class="pull-right">4</a>
                </li>-->
							<li class="list-group-item">
								<b>
									<?php echo $this->lang->line('password_menu'); ?>
								</b>
								<a class="pull-right">
									<?php echo $thisuserdata[0]->password; ?>
								</a>
							</li>
						</ul>

						<a href="#" class="btn btn-primary btn-block"><b>Send message</b></a>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

				<!-- About Me Box -->
				<!--          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
             /.box-header 
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
             /.box-body 
          </div>-->
				<!-- /.box -->
			</div>
			<!--/.col-->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#timeline" data-toggle="tab">Timeline</a>
						</li>
						<li>
							<a href="#tableview" data-toggle="tab">
								<?php echo $this->lang->line('visited_websites'); ?>
							</a>
						</li>
						<li>
							<a href="#visitedplaces" data-toggle="tab">
								<?php echo $this->lang->line('visited_locations'); ?>
							</a>
						</li>

					</ul>
					<div class="tab-content">
						<!-- /.tab-pane -->
						<div class="active tab-pane" id="timeline">
							<!-- The timeline -->
							<ul class="timeline timeline-inverse">
								<?php foreach($usertimeline as $tldata){ ?>
								<!-- timeline time label -->
								<li class="time-label">
									<span class="bg-red">
										<?php echo $tldata->lastlogin; ?>
									</span>
								</li>
								<!-- /.timeline-label -->
								<!-- timeline item -->
								<li>
									<i class="fa fa-envelope bg-blue"></i>

									<div class="timeline-item">
										<h3 class="timeline-header"><a href="#"><?php echo $tldata->locationname; ?></a></h3>

										<div class="timeline-body">
											<table class="table table-hover">
												<tr>
													<td>
														<?php echo $this->lang->line('password_menu'); ?>
													</td>
													<td>
														<?php echo $tldata->password; ?>
													</td>
												</tr>
												<tr>
													<td>Ip Address</td>
													<td>
														<?php echo $tldata->ip; ?>
													</td>
												</tr>
												<tr>
													<td>Mac ID</td>
													<td>
														<?php echo $tldata->macid; ?>
													</td>
												</tr>
											</table>
										</div>
										<!--                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>-->
									</div>
								</li>
								<?php } ?>
								<!-- END timeline item -->

								<li>
									<i class="fa fa-clock-o bg-gray"></i>
								</li>
							</ul>
						</div>
						<!-- /.tab-pane -->
						<!-- /.tab-pane -->
						<div class="tab-pane" id="visitedplaces">
							<!-- Post -->

							<div class="post">
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<th>
												<?php echo $this->lang->line('locationname_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('numberofentrances_menu'); ?>
											</th>
										</tr>
										<?php foreach($visitedplaces as $vpdata){?>
										<tr>
											<td>
												<?php echo $vpdata->locationname; ?>
											</td>
											<td>
												<?php echo $vpdata->visitedcount; ?>
											</td>
										</tr>
										<?php } ?>
									</table>
								</div>
							</div>
							<!-- /.post -->
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tableview">
							<!-- Post -->
							<div class="post">
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<th>
												<?php echo $this->lang->line('visited_time'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('visited_websites'); ?>
											</th>
										</tr>
										<?php foreach($webstats as $record){ 
                          if(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->MethodId)){
                             $vistedurl = $record->MethodId;
                            }
                            elseif(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->SchemeId)){
                             $vistedurl = $record->SchemeId;
                            }elseif(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->HostID)){
                             $vistedurl = $record->HostID;
                            }
                            elseif(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->PortNo)){
                             $vistedurl = $record->PortNo;
                            }elseif(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->Resource)){
                             $vistedurl = $record->Resource;
                            }elseif(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$record->UserId)){
                             $vistedurl = $record->UserId;
                            }else{
                                    $vistedurl = '';
                            }
                          ?>
										<tr>
											<td>
												<?php echo $record->Time; ?>
											</td>
											<td>
												<?php  $vistedurl = strlen($vistedurl) > 30 ? substr($vistedurl,0,30)."..." : $vistedurl;
                      echo $vistedurl; ?>
											</td>

										</tr>
										<?php } ?>

									</table>
								</div>
							</div>
							<!-- /.post -->
						</div>
						<!-- /.tab-pane -->

						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->