<?php

if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;

	}
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
      SMS STATS
      </h1>
	
		<section class="content">

			<div class="box-body no-padding">

				<table class="table table-striped">
					<tr style="border-bottom: 3px solid #d2d6de;">
						<th>User Name<i class="fa fa-user" style="margin-left: 10px;"></i>
						</th>
						<th>Received Messages<i class="fa fa-message" style="margin-left: 10px;"></i>
						</th>
						<th>Date<i class="fa fa-exchange" style="margin-left: 10px;"></i>
						</th>

					</tr>

					<form action="<?php echo base_url() ?>smsStats" method="POST" id="searchList">
						<div class='row'>

							<?php 
               if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
										<?php foreach($locations as $locationsrecord){ ?>
										<option value="<?php echo $locationsrecord->locationid;?>">
											<?php echo $locationsrecord->locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
							<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
							<?php }else{
               ?>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectcustomer_menu'); ?>
									</label>
									<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
                        <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                     <?php foreach($customers as $customersrecord){ ?>
                        <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                     <?php } ?>
                 </select>
								
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
										<?php if(!empty($locationinfo)){  ?>
										<option value="<?php echo $locationid; ?>">
											<?php echo $locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<?php } ?>
							<div class="col-xs-3">
								<div class="form-group">
									<!--For daily , weekly and monthly --->
									<label>
										<?php echo $this->lang->line('time_interval'); ?>
									</label>
									<select class="form-control" name="barselectdata" id="barselectdata">
										<option value="">
											<?php echo $this->lang->line(''); ?>
										</option>
										<option name="daily" value="daily" <?php if(isset($postdata) && $postdata=='daily' ){echo 'selected';} ?>>
											<?php echo $this->lang->line('today_menu'); ?> </option>
										<option name="week" value="week" <?php if(isset($postdata) && $postdata=='week' ){echo 'selected';} ?>>
											<?php echo $this->lang->line('lastweek_menu'); ?> </option>
										<option name="monthly" value="monthly" <?php if(isset($postdata) && $postdata=='monthly' ){echo 'selected';} ?>>
											<?php echo $this->lang->line('lastmonth_menu'); ?> </option>
										<option name="selectdate" value="selectdate" <?php if(isset($postdata) && $postdata=='selectdate' ){echo 'selected';} ?>>
											<?php echo $this->lang->line('selectdate_menu'); ?> </option>
									</select>
									<!--For daily , weekly and monthly end here --->
								</div>
							</div>
							<div class="col-xs-3" style="margin-top: 27px;">
								<div class="form-group">
									<div class="input-group">
										<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-lg " style="height: 30px;" placeholder="Search"/>
										<span class="fa fa-search form-control-feedback" style="height:  30px;margin-top: -9px;"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="margin-top: 5px;">
								<div id="datetimepicker" class="input-append date">
									<label>
										<?php echo $this->lang->line('startdate_menu'); ?>
									</label>
									<p>
										<input type="text" id="datetimepickervaluestart" name="datetimepickervaluestart" onblur="validation()" style="height: 26px; " required> </input>
										<span class="add-on" style="height: 26px;">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
              </span>
									
								</div>
							</div>
							<div class="col-md-3" style=" margin-top: 5px;">
								<div id="datetimepickerend" class="input-append date">
									<label>
										<?php echo $this->lang->line('enddate_menu'); ?>
									</label>
									<p>
										<input type="text" id="datetimepickervalueend" name="datetimepickervalueend" onblur="validation()" style="height: 26px;" required></input>
										<span class="add-on" style="height: 26px;">
             <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
             </span>
									
								</div>
							</div>

							<div class="col-md-3" style="margin-top: 27px;">
								<div class="input-group-btn">
									<input class="btn btn-success" type="submit" value="<?php echo $this->lang->line('showresults_menu'); ?>" style=" background: #367fa9; border: blue; "/>
								</div>
							</div>
						</div>

			</div>



</div>


</form>
<?php
//pre($transaction);

                    if(!empty($smsstats))
                    {
					
                        foreach($smsstats as $record)
                        {
						// to hid the numbers from administrator users
				if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){
					$numb=$record->username;
					$securenumb=$numb;
				}else{
					$numb=$record->username;
					$securenumb=substr_replace($numb, "****", 4);
				}
// end here hiding the numbers from administrator users
                    ?>
                     
                    <tr>   
                        <td><?php echo $securenumb; ?></td>
                        <td> <?php echo $record->count;  ?></td>
                        <td><?php echo $record->date;  ?></td>
                       
                    
                       
                    </tr>
                    <?php
                        }
                    }
                    ?>
					 </h1>
				   
				</table>
				
				</div>
			</div>
		</section>			
               
                     
              
              
    </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletewifivisitors.js" charset   ="utf-8"></script>

<script>
	var name = document.getElementById( "datetimepickervaluestart" ).value;
	var email = document.getElementById( "datetimepickerend" ).value;
	$( "#datetimepickervaluestart" ).attr( "disabled", true );
	$( "#datetimepickervalueend" ).attr( "disabled", true );
	$( "#barselectdata" ).change( function () {
		var select = $( "#barselectdata option:selected" ).val();
		switch ( select ) {
			case "selectdate":
				$( "#datetimepickervaluestart" ).attr( "disabled", false );
				$( "#datetimepickervalueend" ).attr( "disabled", false );
				break;
			case "daily":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			case "monthly":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			case "week":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			default:
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;

		}
	} );
</script>