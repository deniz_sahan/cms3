<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('Transaction_menu'); ?>
		</h1>
		<section class="content">
			<div class="box">
				<div class="box-body no-padding">

					<table class="table table-striped">
						<tr style="border-bottom: 3px solid #d2d6de;">
							<th>
								<?php echo $this->lang->line('customerid_menu'); ?><i class="fa fa-user" style="margin-left: 10px;"></i>
							</th>
							<th>
								<?php echo $this->lang->line('Customer_Name'); ?><i class="fa fa-user" style="margin-left: 10px;"></i>
							</th>
							<th>
								<?php echo $this->lang->line('Packet_Limit'); ?><i class="fa fa-exchange" style="margin-left: 10px;"></i>
							</th>
							<th>
								<?php echo $this->lang->line('Send_Messages'); ?><i class="fa fa-exchange" style="margin-left: 10px;"></i>
							</th>
							<th>
								<?php echo $this->lang->line('SMS_Remaining'); ?><i class="fa fa-exchange" style="margin-left: 10px;"></i>
							</th>
							<th>
								<?php echo $this->lang->line('Activation_Date'); ?><i class="fa fa-calendar-o" style="margin-left: 10px;"></i>
							</th>
							<th>Edit</th>
						</tr>

						<?php
						//pre($transaction);
						if ( !empty( $transaction ) ) {
							foreach ( $transaction as $record ) {
								?>

						<tr>
							<td>
								<?php echo $record->customerid; ?>
							</td>
							<td>
								<?php echo $record->customername;;  ?>
							</td>
							<td>
								<?php echo $record->smsplan; ?>
							</td>
							<td>
								<?php echo $record->smsused ?>
							</td>
							<td>
								<?php echo $record->smsremain ?>
							</td>
							<td>
								<?php echo $record->date ?>
							</td>

							<td><a href='stats/edittransac/?id=<?php echo $record->slid; ?>'> Edit </a> </td>
						</tr>
						<?php
						}
						}
						?>
						</h1>




					</table>

				</div>
			</div>
		</section>




	</section>



</div>