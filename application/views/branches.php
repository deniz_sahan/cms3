<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('accesspointmanagement_menu'); ?>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNewacc"></i> <?php echo $this->lang->line('addnewaccesspoint_menu'); ?></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--                    <h3 class="box-title"><?php echo $this->lang->line('accesspointlist_menu'); ?></h3>--><br>
						<div class="box-tools">
							<form action="<?php echo base_url() ?>showcustomers" method="POST" id="searchList">
								<div class="input-group">
									<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;height:30px;" placeholder="Search"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<th>
									<?php echo $this->lang->line('branchid_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('branchname_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('branchip_menu'); ?>
								</th>
								<th>Description</th>
								<th class="text-center">
									<?php echo $this->lang->line('actions_menu'); ?>
								</th>
							</tr>
							<?php
							if ( !empty( $branchRecords ) ) {
								foreach ( $branchRecords as $record ) {
									?>
							<tr>
								<td>
									<?php echo $record->branchid ?>
								</td>
								<td>
									<?php echo $record->bname ?>
								</td>
								<td>
									<?php echo $record->bip ?>
								</td>
								<td>
									<?php echo $record->descriptrion ?>
								</td>
								<td class="text-center">
									<a class="btn btn-sm btn-info" href="<?php echo base_url().'editbra/'.$record->branchid; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
									<a class="btn btn-sm btn-danger deleteBranch" href="#" data-branchid="<?php echo $record->branchid; ?>" title="Delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletebranches.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "showbranches/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>