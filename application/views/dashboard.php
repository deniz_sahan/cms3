<?php

if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('executivedashboard_menu'); ?>
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>
							<?php echo $totalloginattempts; ?>
						</h3>
						<p>
							<?php echo $this->lang->line('signups_menu'); ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
					<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>
							<?php echo $totaluniqueusers; ?><sup style="font-size: 20px"></sup>
						</h3>
						<p>
							<?php echo $this->lang->line('totaluniqueusers_menu'); ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-stalker"></i>
					</div>
					<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<?php 
               if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>

						<h3>
							<?php 
							if(!empty($topsms ))echo $topsms; else echo 0; ?>/
							<?php if(!empty($totalsmssent1 )) echo $totalsmssent1[0]->smsplan; else echo 0 ?>
						</h3>
						<?php }
			   else{
               ?>
						<h3>
							<?php if(!empty($topsms )) echo $topsms; else echo 0; ?> / 200000</h3>
						<?php  } ?>

						<p>
							<?php echo $this->lang->line('totalsmssent_menu'); ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-android-textsms"></i>
					</div>
					<!--                <a href="<?php echo base_url(); ?>userListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>
							<?php echo $download.'/'.$upload; ?>
						</h3>
						<p>
							<?php echo $this->lang->line('currentdownloadsuploads_menu'); ?>
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-social-markdown"></i>
					</div>
					<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
		</div>

		<!-----google Maps start here------>
<?php /*?>		<div class="col-md-12" style=" margin-top: 10px;">
			<div class="box box-primary" style=" height: 385px;">
				<div class="col-md-8" style=" margin-top: 5px;">

					<div id="map"></div>


				</div>


				<!-- /.progress-Bar -->

				<!-- OUR FORM -->
				<form action="<?php echo base_url()?>User/index" method="POST">
					<div class="col-md-4" style=" margin-top: 10px;">
						<p class="text-center">
							<strong></strong>
						</p>

						<div class="col-md-12">
							<label>
								<?php echo $this->lang->line('selectlocation_menu'); ?>
							</label>

							<select class="form-control" id="selectlocationmsg" name="selectlocationmsg" style=" width: 83%;">
				<option value="<?php if(empty($locationinfo)){ ?><?php } else { echo $locationidd; }?>"><?php if(empty($locationinfo)){ ?><?php } else { echo $locationname; }?></option>
                      <?php foreach($locations as $locationsrecord){ ?>
                      <option value="<?php echo $locationsrecord->locationid;?>"><?php echo $locationsrecord->locationname; ?></option>
                    <?php } ?>
                  </select>
						

						</div>
						<br>
						<br>
						<br>

						<div style=" margin-top: 5px; margin-left: 47px; margin-top: 5%;">
							<input class="btn btn-success btn-default" type="submit" name="today" id="today" value="today" style=" background: #367fa9; border: blue; color: white; "/>
							<input class="btn btn-success btn-default" type="submit" name="week" id="weed" value="week" style=" background: #367fa9; border: blue; color: white; "/>
							<input class="btn btn-success btn-default" type="submit" name="month" id="month" value="month" style=" background: #367fa9; border: blue; color: white; "/>
						</div>
				</form>

				</div>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>



				<div class="progress-group" style=" width:95%; margin-top: 5%">
					<?php
					for ( $j = 0; $j <= $totalsmssent; $j++ ) {}
					$j = $j - 1;
					?>

					<span class="progress-text">
						<?php echo $this->lang->line('totalsentmessage_menu'); ?>
					</span>
					<span class="progress-number"><b><?php echo $totalsmssent; ?></b></span>

					<div class="progress sm">
						<div class="progress-bar progress-bar-aqua" id="progress1" style="width: <?php echo $j; ?>%"></div>


					</div>
				</div>

				<div class="progress-group" style=" width:95%;">
					<span class="progress-text">
						<?php echo $this->lang->line('average_time'); ?>
					</span>
					<?php
					//for barchart average time
					//pre($dailyavg);
					for ( $x = 0; $x <= $dailyavg; $x++ ) {

					}
					$x = $x - 1;
					?>
					<span class="progress-number"><b><?php echo $dailyavg; ?></span>

					<div class="progress sm">
						<div class="progress-bar progress-bar-green" style="width:<?php echo $x; ?>%"></div>
					</div>
				</div>
				<!-- /.progress-group -->
				<div class="progress-group" style=" width:95%;">

					<?php 
					  	
						
						?>

					<span class="progress-text">
						<?php echo $this->lang->line('totalwifiuser_menu'); ?>
					</span>
					<?php
					//for barchart average time
					//pre($dailyavg);
					for ( $y = 0; $y <= $dailyavg; $y++ ) {

					}
					$y = $y - 1;
					?>
					<span class="progress-number"><b><?php echo $totaluniqueusers1; ?></b></span>

					<div class="progress sm">
						<div class="progress-bar progress-bar-yellow" style="width: <?php echo $y; ?>%"></div>
					</div>
				</div>
				<!-- /.progress-group -->
			</div>
		</div><?php */?>


		<!-----google Maps end here------>

		<div class="row">
			<div class="col-md-12">
				<!-- TABLE: LATEST ORDERS -->
				<div class="box box-primary">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">
								<?php echo $this->lang->line('useractivity_menu'); ?>
							</h3>

							<!--              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>-->
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="table-responsive">
								<table class="table no-margin">
									<thead style=" color: #367fa9;">
										<tr>
											<th>
												<?php echo $this->lang->line('customername_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('username_menu'); ?>
											</th>
											<th>MacID</th>
											<th>
												<?php echo $this->lang->line('startdate_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('lastseen_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('sessiontime_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('uploads_menu'); ?>
											</th>
											<th>
												<?php echo $this->lang->line('downloads_menu'); ?>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php 
                if(!empty($onlineusersdata)){
                    foreach ($onlineusersdata as $ondata){
                        $date = date("Y-m-d H:i:s");
                        $time = strtotime($date);
                        $time = $time - (5 * 60);
                        $date = date("Y-m-d H:i:s", $time);
                ?>
										<tr style="font-weight: 400;">
											<td>
												<?php echo $ondata->custname; ?>
											</td>
											<td>
												<?php echo $ondata->username; ?>
											</td>
											<td>
												<?php echo $ondata->macaddress; ?>
											</td>
											<td>
												<?php echo $ondata->sessionstart; ?>
											</td>
											<td>
												<span class="label <?php if($ondata->lastactivity >= $date){ ?>label-success <?php }else{?>label-warning<?php }?>">
													<?php
													if ( $ondata->lastactivity >= $date ) {
														echo "ONLINE";
													} else {
														echo $ondata->lastactivity;
													}
													?>
												</span>
											</td>
											<td>
												<?php echo $ondata->sessionduration; ?>
											</td>
											<td>
												<?php echo $ondata->downloads; ?>
											</td>
											<td>
												<?php echo $ondata->uploads; ?>
											</td>
										</tr>
										<?php }
                }else{
                    echo "No one is connected to the network this time";
                }?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.box-body -->
						<div class="box-footer clearfix">
							<a href="<?php //echo base_url();?>listVisitorActivity" class="btn btn-success btn-default  pull-right" style=" background: #367fa9; border: blue; color: white;">
								<?php echo $this->lang->line('viewalluser_menu'); ?>
							</a>
						</div>
						<!-- /.box-footer -->
					</div>
				</div>

				<!-- /.box -->
			</div>
		</div>

		<!-----graph start------>
		<div class="row">

			<div class="col-md-6" style=" margin-top: 20px;">
				<div class="box box-primary">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">
								<?php echo $this->lang->line('userspermonth_menu'); echo ' '.date('M/Y'); ?>
							</h3>

							<div class="box-tools pull-right">
								<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
							</div>
						</div>
						<div class="box-body">
							<div class="chart">
								<canvas id="myChart2" style="height:250px"></canvas>
							</div>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
			<div class="col-md-6" style=" margin-top: 20px;">
				<div class="box box-primary">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">
								<?php echo $this->lang->line('textmessagespermonth_menu'); echo ' '.date('M/Y'); ?>
							</h3>

							<div class="box-tools pull-right">
								<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
							</div>
						</div>
						<div class="box-body">
							<div class="chart">
								<canvas id="myChart3" style="height:250px"></canvas>
							</div>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</div>
		<div class='row'>
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box box-info">
						<div class="box-header with-border">
							<h3 class="box-title">
								<?php echo $this->lang->line('downloadsanduploadsinmbs_menu'); echo " ".date('M/Y'); ?>
							</h3>

							<div class="box-tools pull-right">
								<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
							</div>
						</div>
						<div class="box-body">
							<div class="chart">
								<canvas id="myChart4" style="height:250px"></canvas>
							</div>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
			<!--        <div class="col-md-6">online devices
            <div class="box box-primary">
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><th><?php echo $this->lang->line('devicestatus_menu'); ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body no-padding">
              <table class="table">
                <tr>
                  <th><?php echo $this->lang->line('customername_menu'); ?></th>
                  <th><?php echo $this->lang->line('deviceid_menu'); ?></th>
                  <th><?php echo $this->lang->line('status_menu'); ?></th>
                  <th><?php echo $this->lang->line('lastcheck_menu'); ?></th>
                </tr>
                <?php 
                if(!empty($onlinedevicesdata)){
                    foreach ($onlinedevicesdata as $onddata){
                        $date = date("Y-m-d H:i:s");
                        $time = strtotime($date);
                        $time = $time - (5 * 60);
                        $date = date("Y-m-d H:i:s", $time);
                ?>
                <tr style="font-weight: 400;">
                  <td><?php echo $onddata->custname; ?></td>
                  <td><?php echo $onddata->deviceid; ?></td>
                  <td><?php if($onddata->statusdate > $date){if($onddata->status == '1'){echo '<span class = "label label-success">Active</span>';}}else{
//                      $ApiUrl = "http://8bit.mobilus.net/";
//                        $SmsUser  =  'elogo-LG1001';
//                        $SmsPass  =  '8975';
//                        $Companyode = 'LG1001';
//                        $number = '5539803313';
//                        $originator = 'LOGOELEKT';
//                        $message = "Hello Bekir, Netgate Device Not working: Customer Name:".$onddata->custname." and Device ID is :".$onddata->deviceid."";
//                        //$originator = 'ALPET';
//                        $strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$number</Numbers><Originator>$originator</Originator><SDate></SDate></MainmsgBody>";
//
//                            $ch = curl_init();
//                            curl_setopt($ch, CURLOPT_URL,$ApiUrl); 
//                            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
//                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
//                            curl_setopt($ch, CURLOPT_POSTFIELDS, $strXML); 
//                            $result = curl_exec($ch);
//
//                            $pieces = explode(" ", $result);
//                            $smscode = $pieces[1]; // piece2
//                            curl_close($ch);
                      echo 'Device has some problem';} ?></td>
                  <td><?php echo $onddata->statusdate; ?></td>
                </tr>
                <?php
                }
                }
                ?>
              </table>
            </div>
             /.box-body 
          </div>
        </div>
        </div>-->
			<?php 
//            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){
//                
//            }else{
            ?>
			<!--            <div class="col-md-6">
        <div class="box box-primary">
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->lang->line('piechartforuserspermonth_menu'); echo ' '.date('M/Y'); ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="pieChart" style="height:250px"></canvas>
              </div>
            </div>
             /.box-body 
          </div>
        </div>
        </div>-->
			<?php 
//            }
            ?>
		</div>

	</section>

	<!-----google Maps start here------>
	<title>Map For Location</title>
	<meta name="viewport" content="initial-scale=1.0">
	<meta charset="utf-8">
	<style>
		#map {
			width: 100%;
			height: 370px;
			background-color: grey;
		}
	</style>

</div>







<script>
	var map;

	function initMap() {
		/*
        //map = new google.maps.Map(document.getElementById('map'), {
		///var uluru = {lat: 41.0082, lng: 28.9784};
          //center: {lat: 41.0082, lng: 28.9784},
         var uluru = {lat: 40.962032, lng: 29.108784};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
		*/

		var minZoomLevel = 12;
		<?php if($zoom==6){  ?>
		var minZoomLevel = 6;
		<?php } ?>

		//var uluru = {lat: 40.962032, lng: 29.108784};
		//var uluru1 = {lat: 36.8969, lng: 30.7133};
		var uluru = {
			lat: <?php echo $latitude ?>,
			lng: <?php echo $longitude ?>
		};
		var map = new google.maps.Map( document.getElementById( 'map' ), {
			zoom: minZoomLevel,
			center: new google.maps.LatLng( <?php echo $latitude ?>, <?php echo $longitude ?> ),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		} );

		// Bounds for North America



		// Listen for the dragend event

		<?php if($zoom==12){  ?>
		var marker1 = new google.maps.Marker( {
			position: uluru,
			map: map
		} );
		<?php } ?>
		// Limit the zoom level
		google.maps.event.addListener( map, 'zoom_changed', function () {
			if ( map.getZoom() < minZoomLevel ) map.setZoom( minZoomLevel );
		} );



	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?region=cn&language=tr&region=TR&key=AIzaSyAkhj_jZo7wpIJ__bOLORcAG_KeL5Qn0FQ&callback=initMap" async defer></script>
</div>
<div> </div>

<script>
	var ctx = document.getElementById( 'myChart2' ).getContext( '2d' );
	var myChart = new Chart( ctx, {
		type: 'bar',
		data: {
			labels: <?php echo $label; ?>,
			datasets: [ {
				label: '<?php echo $this->lang->line('
				numberofwifiusers_menu '); ?>',
				data: <?php echo $result; ?>,
				backgroundColor: 'rgb(54, 127, 169)',
				borderColor: 'rgb(24, 58, 78)',
				borderWidth: 1
			} ]
		},
		options: {
			scales: {
				yAxes: [ {
					ticks: {
						beginAtZero: true
					}
				} ]
			}
		}
	} );
	var ctx = document.getElementById( 'myChart3' ).getContext( '2d' );
	var myChart = new Chart( ctx, {
		type: 'bar',
		data: {
			labels: <?php echo $labeltext; ?>,
			datasets: [ {
				label: '<?php echo $this->lang->line('
				numberoftextmessages_menu '); ?>',
				data: <?php echo $resulttext; ?>,
				backgroundColor: 'rgb(54, 127, 169)',
				borderColor: 'rgb(24, 58, 78)',
				borderWidth: 1
			} ]
		},
		options: {
			scales: {
				yAxes: [ {
					ticks: {
						beginAtZero: true
					}
				} ]
			}
		}
	} );
	var ctx = document.getElementById( 'myChart4' ).getContext( '2d' );
	var myChart = new Chart( ctx, {
		type: 'bar',
		data: {
			labels: <?php echo $labeltext; ?>,
			datasets: [ {
				label: 'Downloads',
				data: <?php echo $resultdownload; ?>,
				backgroundColor: 'rgb(54, 127, 169)',
				borderColor: 'rgb(24, 58, 78)',
				borderWidth: 1


			}, {
				label: "Uploads",
				backgroundColor: "#8e5ea2",
				data: <?php echo $resultupload; ?>
			} ]
		},
		options: {
			scales: {
				yAxes: [ {
					ticks: {
						beginAtZero: false
					}
				} ]
			}
		}
	} );
	new Chart( document.getElementById( "pieChart" ), {
		type: 'pie',
		data: {
			labels: [ "Elogo", "BaytTeknoloji" ],
			datasets: [ {
				label: "<?php echo $this->lang->line('numberofuserspercustomer_menu'); ?>",
				backgroundColor: [ "#3e95cd", "#8e5ea2" ],
				data: [ <?php echo $elogo; ?>, <?php echo $baytgate; ?> ]
			} ]
		},
		options: {
			title: {
				display: true,
				text: '<?php echo $this->lang->line('
				totalusersperlocation_menu '); ?>'
			}
		}
	} );
</script>

<script>
	// magic.js
	$( document ).ready( function () {
				$( "week" ).click( function () {
							alert( "clicked here" );
							$.ajax( {
								alert( "clicked here" );
								type: 'POST',
								url: 'script.php',
								success: function ( data ) {
									alert( data );
									$( "p" ).text( data );

								}


							} );
</script>