<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';

if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationid = $cf->locationid;
		$locationname = $cf->locationname;
		$adress = $cf->address;
		$lat = $cf->lat;
		$long = $cf->long;
		$deviceid = $cf->deviceid;
		$lanip = $cf->lanip;
		$lanip2 = $cf->lanip2;
		$schours = $cf->schours;
		$scminutes = $cf->scminutes;
	}
}


?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('locationmanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->



				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo $this->lang->line('enterlocationdetail_menu'); ?>
						</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<form role="form" action="<?php echo base_url() ?>editlocation" method="post" id="editUser" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="locationname">
											<?php echo $this->lang->line('locations_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="<?php echo $locationname; ?>" id="locationname" name="locationname" maxlength="128" style="height: 32px;" width="50%">
										<input type="hidden" value="<?php echo $locationid; ?>" name="locationid" id="locationid"/>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="deviceid">
											<?php echo $this->lang->line('deviceid_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="deviceid" value="<?php echo $deviceid; ?>" name="deviceid" maxlength="128" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="address">
											<?php echo $this->lang->line('address_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="address" value="<?php echo $adress; ?>" name="address" maxlength="500" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="lat">Latitude</label>
										<input type="text" class="form-control required" id="lat" value="<?php if(!empty($lat)){ echo $lat; } ?>" name="lat" maxlength="50" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="long">Longitude</label>
										<input type="text" class="form-control required" id="long" value="<?php if(!empty($long)){ echo $long; } ?>" name="long" maxlength="50" style="height: 32px;" width="50%">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="lanip1">Lan Ip (1)</label>
										<input type="text" class="form-control required" id="lanip1" value="<?php echo $lanip; ?>" name="lanip1" maxlength="120" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="lanip2">Lan Ip (2)</label>
										<input type="text" class="form-control required" id="lanip2" value="<?php echo $lanip2; ?>" name="lanip2" maxlength="120" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group col-xs-4">
										<label for="sessioncontrol1">
											<?php echo $this->lang->line('session_control'); ?>(Hours)</label>
										<input type="text" placeholder="Hours" class="form-control required" id="sessioncontrol1" style="height: 32px;" value="<?php echo $schours; ?>" name="sessioncontrol1" maxlength="120">
									</div>
									<div class="form-group col-xs-4">
										<label for="sessioncontrol2">
											<?php echo $this->lang->line('session_control'); ?>(Minute)</label>
										<input type="text" placeholder="Minutes" class="form-control required" id="sessioncontrol2" style="height: 32px;" value="<?php echo $scminutes; ?>" name="sessioncontrol2" maxlength="120">
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-body -->
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editLocation.js" type="text/javascript"></script>