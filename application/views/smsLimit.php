<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('smsLimit_menu'); ?>
		</h1>

		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<?php 
               if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
					<section class="content">
						<div class="box">
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tr style="border-bottom: 3px solid #d2d6de;">
										<th>
											<?php echo $this->lang->line('Customer_Name'); ?>
										</th>
										<th>
											<?php echo $this->lang->line('Packet_Limit'); ?>
										</th>
										<th>
											<?php echo $this->lang->line('Send_Messages'); ?>
										</th>
										<th>
											<?php echo $this->lang->line('SMS_Remaining'); ?>
										</th>
										<th>Package Activation Date</th>
									</tr>
									<tr>
										<td>
											<?php echo  $customername; ?> </td>
										<td>
											<?php echo $smsplan; ?>
										</td>
										<td>
											<?php echo $sentsms; ?>
										</td>
										<td>
											<?php echo $smsremain; ?> </td>
										<td>
											<?php echo $smsdate; ?> </td>
									</tr>
								</table>
							</div>
						</div>
					</section>
					<?php }else{
               ?>
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addSmslimit">
						<?php echo $this->lang->line('addSmslimit'); ?>
					</a>
					<a class="btn btn-primary" href="<?php echo base_url(); ?>transactions">
						<?php echo $this->lang->line('Transaction_menu'); ?>
					</a>


					<?php if(empty($customerinfo)){ ?>
					<?php } else { echo $customeridd; }?>

					<section class="content">
						<div class="box">
							<div class="box-body no-padding">
								<table class="table table-striped">
									<tr style="border-bottom: 3px solid #d2d6de;">
										<th>
											<?php echo $this->lang->line('Customer_Name'); ?> <i class="fa fa-user" style="margin-left: 10px;"></i>
										</th>
										<th>
											<?php echo $this->lang->line('Packet_Limit'); ?> <i class="fa fa-exchange" style="margin-left: 10px;"></i>
										</th>
										<th>
											<?php echo $this->lang->line('Send_Messages'); ?> <i class="fa fa-exchange" style="margin-left: 10px;"></i>
										</th>
										<th>
											<?php echo $this->lang->line('SMS_Remaining'); ?> <i class="fa fa-exchange" style="margin-left: 10px;"></i>
										</th>
									</tr>
									<tr>
										<td>
											<?php echo $customername; ?> </td>
										<td>
											<?php echo $totalsms1; ?>
										</td>
										<td>
											<?php echo $smsused; ?>
										</td>
										<td>
											<?php echo $smslimit; ?> </td>
									</tr>
								</table>
							</div>
						</div>
					</section>
					<?php } ?>

				</div>
			</div>
		</div>


	</section>

</div>