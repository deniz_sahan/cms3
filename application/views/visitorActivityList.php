<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('wifivisitorslist_menu'); ?>
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="text-align: center">

						&nbsp;&nbsp;&nbsp;
						<?php if(!empty($countrecords)){echo '<code>'.$countrecords . ' Results found </code>';} ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table no-margin">
							<thead style=" color: #367fa9;">
								<tr>
									<th>
										<?php echo $this->lang->line('username_menu'); ?>
									</th>
									<th>
										<?php echo $this->lang->line('mobilenumber_menu'); ?>
									</th>
									<th>
										<?php echo $this->lang->line('password_menu'); ?>
									</th>
									<th>Ip</th>
									<th>Mac </th>
									<th>
										<?php echo $this->lang->line('lastseen_menu'); ?>
									</th>
									<th>
										<?php echo $this->lang->line('numberofentrances_menu'); ?>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php 
                if(!empty($uniqueusersdata)){
                    foreach ($uniqueusersdata as $ondata){
                        $date = date("Y-m-d H:i:s");
                        $time = strtotime($date);
                        $time = $time - (5 * 60);
                        $date = date("Y-m-d H:i:s", $time);
                        $numb=$ondata->username;
			$securenumb=substr_replace($numb, "****", 4);
                ?>
								<tr style="font-weight: 400;">
									<td>
										<a href="<?php echo base_url(); ?>userprofiledetails/<?php echo base64_encode($ondata->username); ?>">
											<?php echo $ondata->name." ".$ondata->surname; ?>
										</a>
									</td>
									<td>
										<?php echo $securenumb; ?>
									</td>
									<td>
										<?php echo $ondata->password; ?>
									</td>
									<td>
										<?php echo $ondata->ipaddress; ?>
									</td>
									<td>
										<?php echo $ondata->macaddress; ?>
									</td>
									<td>
										<span class="label <?php if($ondata->lastactivity >= $date){ ?>label-success <?php }else{?>label-warning<?php }?>">
											<?php
											if ( $ondata->lastactivity >= $date ) {
												echo "ONLINE";
											} else {
												echo $ondata->lastactivity;
											}
											?>
										</span>
									</td>
									<td>
										<?php echo $ondata->countsuccesslogin; ?>
									</td>
								</tr>
								<?php }
                }else{
                    echo "No one is connected to the network this time";
                }?>
							</tbody>
						</table>

					</div>
					<!-- /.box-body -->

				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			var customerid = '<?php echo $customerid; ?>';
			var locationid = '<?php echo $locationid; ?>';
			jQuery( "#searchList" ).attr( "action", baseURL + "onlineusersdata/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>