<style>
	label {
		display: inline-block;
		max-width: 100%;
		margin-bottom: 5px;
		font-weight: 600;
	}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('instantmessages_menu'); ?>
			<!--     <small>LOCUS</small>-->
		</h1>
		<!--    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line('home_menu'); ?></a></li>
      <li><a href="#"><?php echo $this->lang->line('messages_menu'); ?></a></li>
      <li class="active"><?php echo $this->lang->line('instantmessages_menu'); ?></li>
    </ol>-->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box" style=" background-color: #ffffff">
			<!--      <div class="box-header with-border">
        <h3 class="box-title"><?php echo $this->lang->line('instantmessagesection_menu'); ?></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>-->
		</div>
		<div class="box-body" style=" background-color: #ffffff;">

			<br/>
			<div class="row">
				<div class="col-xs-12" style="border-bottom:  1px solid lightgrey; width:  81.5%; margin-left: 10px; padding-bottom: 10px;">
					<span style="font-size:  16px;font-weight: 600;">
						<?php echo $this->lang->line('campaigninformation_menu'); ?>
					</span>
				</div>

				<div class="form_error">
					<?php echo validation_errors(); ?>
				</div>
				<form id="uploadForm" action="importcsvdataInstantmessage" name="campaignform" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="form-group col-md-10">
							<div class="row" style="padding-top: 15px;">
								<div class="col-xs-4">
									<div class="form-group">
										<label>
											<?php echo $this->lang->line('nameforInstantmessagecampaign_menu'); ?>
										</label>
										<input type="text" class="form-control required" value="" id="inmname" name="inmname" maxlength="128" placeholder="Campaign name" style="height: 33px;" required/>
									</div>
								</div>
								<?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
								<div class="col-xs-8">
									<div class="form-group">
										<label>
											<?php echo $this->lang->line('selectlocation_menu'); ?>
										</label>
										<select class="form-control" id="selectlocationmsg" multiple name="selectlocationmsg[]">
											<?php foreach($locations as $locationsrecord){ ?>
											<option value="<?php echo $locationsrecord->locationid;?>">
												<?php echo $locationsrecord->locationname; ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
								<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
								<?php }else{
            ?>
								<div class="col-xs-4">
									<div class="form-group">
										<label>
											<?php echo $this->lang->line('selectcustomer_menu'); ?>
										</label>
										<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
											<option>---Customers---</option>
											<?php foreach($customers as $customersrecord){ ?>
											<option value="<?php echo $customersrecord->customerid;?>">
												<?php echo $customersrecord->cusername; ?>
											</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<label>
											<?php echo $this->lang->line('selectlocation_menu'); ?>
										</label>
										<select class="form-control" id="selectlocationmsg" multiple name="selectlocationmsg[]" required>             
                  </select>
									
									</div>
								</div>
								<?php 
            }
            ?>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-12" style="border-bottom:  1px solid lightgrey; width:  98%; margin-left: 10px; padding-bottom: 10px;">
									<span style="font-size:  16px;font-weight: 600;">
										<?php echo $this->lang->line('campaigntext_menu'); ?>
									</span>
								</div>
							</div>
							<div class="row" style="padding-top: 15px;">
								<!--                <lable for='showfilters'><?php echo $this->lang->line('selectpermissionorblacklist_menu'); ?></lable>&nbsp;&nbsp;
                <span id="showfilters" style="background-color: #3c8dbc;color:white; border: 0px;padding: 5px;"><?php echo $this->lang->line('selectfilters_menu'); ?></span>
                <br /><br />
                
                
                <div class="form-group" style="border:  1px solid;padding: 10px; display: none;" id="blklist">
                  <label for="blacklistfile"><?php echo $this->lang->line('uploadblacklist_menu'); ?></label>
                  <input type="file" id="blacklistfile" name='blacklistfile'>
                      <div id='blacklistcount'></div>
                  <p class="help-block"><?php echo $this->lang->line('format_menu'); ?>: CSV,TXT</p>
                  <input type="checkbox" name ='blacklistcheck' value='blacklist'> <?php echo $this->lang->line('blacklist_menu'); ?>                  
                </div>-->

								<div class="col-xs-8">
									<div class="form-group">
										<label>
											<?php echo $this->lang->line('message_menu'); ?>
										</label>
										<textarea class="form-control" rows="4" placeholder="Send instant message after user logged In (limited to 160 characters)" maxlength="160" id="instantmessagetext" name="instantmessagetext" required></textarea>
									</div>
									<div class="form-group" style="text-align: right;height: 0;">
										<span id='remainingC'></span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<label for='showfilters'>Target List</label>&nbsp;&nbsp;
									</div>
									<div class="form-group">
										<select class="form-control" id="targetlistcheckselect" name="targetlistcheckselect" required>
<!--                            <option>Select Target group</option>-->
                        </select>
									
									</div>
									<div class="form-group">
										<div style="text-align:  right;">For New Target List : <span id="showfilters" style="background-color: #eee;color:black; border: 0px;padding: 7px;">Upload File</span>
										</div>
									</div>
									<div class="form-group" style="border:  1px solid;padding: 10px; display : none;" id="permlist">
										<label for="targetlistfile">Target List</label>
										<input type="file" id="targetlistfile" name='targetlistfile'>
										<div id='targetlistfilecount'></div>
										<p class="help-block">
											<?php echo $this->lang->line('format_menu'); ?>: CSV</p>
										<input type="text" name="targetlistcheck" id="targetlistcheck" class="form-control" maxlength="128" placeholder="TargetList Name" style="height: 33px;">
									</div>
								</div>
							</div>
							<br><br>
							<div class="row">
								<div class="col-xs-12" style="border-bottom:  1px solid lightgrey; width:  98%; margin-left: 10px; padding-bottom: 10px;">
									<span style="font-size:  16px;font-weight: 600;">
										<?php echo $this->lang->line('campaigndate_menu'); ?>
									</span>
								</div>
							</div>
							<div class="row" style="padding-top: 15px;">

								<div class="col-xs-4">
									<label>
										<?php echo $this->lang->line('startdate2_menu'); ?>
									</label>
									<div id="datetimepicker" class="input-append date">
										<input type="text" id="datetimepickervaluestart" name="campaignstartdate" style="height: 33px;" required> </input>
										<span class="add-on" style="height: 33px;">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
									
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group">
										<label>Priority</label>
										<select class="form-control" id="priority" name="priority">
											<option value="1">High</option>
											<option value="2">Medium</option>
											<option value="3">Low</option>
										</select>
									</div>
								</div>
								<div class="col-xs-4">

								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
								</div>
								<div class="col-xs-4">
								</div>
								<div class="col-xs-4" style="text-align:  right;">
									<input type="submit" value="<?php echo $this->lang->line('continuetosetup_menu'); ?>" class="btnSubmit" style="background-color: #3c8dbc;color:white; border: 0px;padding: 5px;"/>
								</div>
								<!-- radio -->
								<!--                <div class="box box-success"style="background-color: #FDF2FF; padding: 3px;">
            <div class="box-header with-border">
              <h3 class="box-title">Message Type</h3>
            </div>
                <div class="form-group">
                    
                  <div class="radio">
                    <label>
                      
                      <input type="radio" class="messagetype" name="messagetype" id="optionsRadios1" value="testmessage" checked>
                      
                      TestMessage PhoneNumber 
                      <div id="testuserdiv" class="input-group">
                     <input id="testuser" type="text" name="testuser" value="" class="form-control" placeholder="5xxxxxxxxx"></input>                  
                    </div>
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" class="messagetype" name="messagetype" id="optionsRadios2" value="onetimeinlife">
                      Only one message to wifiusers for the first time after login
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" class="messagetype" name="messagetype" id="optionsRadios3" value="onemessageinoneday" >
                      One message in 24 hours
                    </label>
                  </div>
                    <div class="radio">
                    <label>
                      <input type="radio" class="messagetype" name="messagetype" id="optionsRadios3" value="datetimemessage" >
                          Users will not recieve a second message till this time you set
                      <div id="datetimepicker" name="datetimepicker" class="input-append date ">
                     <input type="text" name="datetimepicker" id="datetimepickervalue" style="height: 26px;"></input>
                     <span class="add-on" style="height: 26px;">
                         <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                     </span>                     
                    </label>
                  </div>
                </div>  </div>-->
								<!------------radio end--->

								<!--              <a href="<?php echo base_url(); ?>stopinstantmessages" class="stopservice" style="display: none;"><?php echo $this->lang->line('stopsendingmessage_menu'); ?>! </a>-->
							</div>
						</div>
					</div>
			</div>
			<!-- /.box-body -->

			<!-- /.box-footer-->
			<input type="hidden" name="searchText" value="">
			</form>
			<div id="targetLayer"></div>
		</div>
		<div class="box-footer" style="background-color: #ffffff;">
			<!--          Using SmsGW for Messages with username <code>elogopgsms</code>-->
		</div>
		<!-- /.box-body -->

		<!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/sendmessagetonumbers.js" type="text/javascript"></script>-->
<script>
	$( "#showfilters" ).click( function () {
		$( "#permlist" ).toggle( 'slow' );
		$( "#blklist" ).toggle( 'slow' );
	} );
</script>
<script type="text/javascript">
	//    $(document).ready(function (e) {
	//    $("#uploadForm").on('submit',(function(e) {
	//        e.preventDefault();
	//        $.ajax({
	//            url: "<?php echo base_url(); ?>importcsvdataInstantmessage",
	//            type: "POST",
	//            data:  new FormData(this),
	//            mimeType:"multipart/form-data",
	//            contentType: false,
	//            cache: false,
	//            processData:false,
	//            success: function(data)
	//            {
	//                var myObj = JSON.parse(data);
	//        document.getElementById("blacklistcount").innerHTML = '<code>'+myObj.blacklistcount+' Numbers uploaded</code>';
	//        document.getElementById("permissioncount").innerHTML = '<code>'+myObj.permissioncount+' Numbers uploaded</code>';
	////            $("#targetLayer").html(data);
	////            $("#blacklistcount").append(data.blacklistcount);
	////            $("#permissioncount").append(data.permissioncount);
	//            },
	//            error: function() 
	//            {
	//            }           
	//       });
	//    }));
	//});
	$( document ).ready( function () {
		var len = 0;
		var maxchar = 160;

		$( '#instantmessagetext' ).keyup( function () {
			len = this.value.length
			if ( len > maxchar ) {
				return false;
			} else if ( len > 0 ) {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar - len ) );
			} else {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar ) );
			}
		} )
	} );
	$( document ).ready( function () {

		// Customer Change
		$( '#selectcustomermsg' ).change( function () {
			var customerid = $( this ).val();
			// AJAX request
			$.ajax( {
				url: baseURL + 'getalltargetgroupsbyCustomerID',
				method: 'post',
				data: {
					customerid: customerid
				},
				dataType: 'json',
				success: function ( response ) {

					// Remove options
					$( '#targetlistcheckselect' ).find( 'option' ).remove();
					// Add options
					//$('#targetlistcheckselect').append('<option>Select Target group</option>');
					$.each( response, function ( index, data ) {
						$( '#targetlistcheckselect' ).append( '<option value="' + data[ 'targetlist' ] + '">' + data[ 'targetlist' ] + '</option>' );
					} );
				}
			} );
		} );


	} );
</script>