<?php
$locationidd = '';
$customeridd = '';
if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;


	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('permissionlist_menu'); ?> -
			<?php echo $this->lang->line('blacklist_menu'); ?>
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-xs-12">
				<div class="box-tools">

					<!--Date Range start--->
					<div class='row'>
						<div class='col-lg-6'>
							<form action="<?php echo base_url() ?>addremovepblist" method="POST" id="searchList">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">
											<?php echo $this->lang->line('filterresults_menu'); ?>
										</h3>
									</div>
									<div class="box-body">
										<?php 
                                if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
										<div class="form-group">
											<label>
												<?php echo $this->lang->line('selectlocation_menu'); ?>
											</label>
											<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
												<?php foreach($locations as $locationsrecord){ ?>
												<option value="<?php echo $locationsrecord->locationid;?>">
													<?php echo $locationsrecord->locationname; ?>
												</option>
												<?php } ?>
											</select>
										</div>
										<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
										<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
										<?php }else{
                                ?>
										<div class="form-group">
											<label>
												<?php echo $this->lang->line('selectcustomer_menu'); ?>
											</label>
											<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
                                        <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?>---Customers---<?php } else { echo $stsurname; }?></option>
                                      <?php foreach($customers as $customersrecord){ ?>
                                       <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                                         <?php } ?>
                                  </select>
										
										</div>
										<div class="form-group">
											<label>
												<?php echo $this->lang->line('selectlocation_menu'); ?>
											</label>
											<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
												<?php if(!empty($locationinfo)){  ?>
												<option value="<?php echo $locationid; ?>">
													<?php echo $locationname; ?>
												</option>
												<?php } ?>
											</select>
										</div>
										<?php } ?>

										<div class="form-group">
											<div class="input-group">
												<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-lg" style="width: 240px;height: 30px;" placeholder="Search"/>
												<br>
												<div class="input-group-btn">
													<button class="btn btn-sm btn-default searchList">
														<?php echo $this->lang->line('filterresults_menu'); ?>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>

							</form>
						</div>

						<div class='col-lg-6'>
							<!-- general form elements -->
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Add GSM Numbers</h3>
								</div>
								<!-- /.box-header -->
								<!-- form start -->
								<form id="uploadFormcsv" action="<?php echo base_url() ?>addremovepermblacklist" method="post" enctype="multipart/form-data">
									<div class="box-body">

										<!--                                          <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                          </div>-->
										<div class="form-group">
											<label>Add Numbers to</label>
											<select class="form-control" id="selectpermblack" name="selectpermblack">
												<option value="perm">PermissionList</option>
												<option value="black">BlackList</option>
											</select>
										</div>
										<div class="form-group">
											<label for="exampleInputFile">Upload CSV</label>
											<input type="file" id="csvlistfile" name='csvlistfile'>
											<input name="customerid" type="hidden" value="<?php if($customeridd != ''){echo $customeridd;} ?>">
											<input name="locationid" type="hidden" value="<?php if($locationidd != ''){echo $locationidd;} ?>">
											<p class="help-block">File Extension: CSV</p>
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<?php if(!empty($customeridd && $locationidd)){?>
										<button type="submit" class="btn btn-primary" name="formuploadcsv">Upload</button>
										<?php }else{ ?>
										<?php echo '<code>You could select location and customer for adding lists</code>'; } ?>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<?php if($this->session->flashdata('flsh_msg_permblack') != ''){ ?>
		<div class="callout callout-success">
			<h4>
				<?php echo $this->session->flashdata('flsh_msg_permblack'); ?>
			</h4>
		</div>
		<?php } ?>
		<!-----------start permission black list tables---------->
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Permission List</h3> &nbsp;&nbsp;&nbsp;
						<?php if(!empty($countperm)){echo '<code>'.$countperm . ' Results found </code>';} ?>
						<?php if(!empty($customeridd && $locationidd)){?>
						<a class="btn btn-primary" href="#" onclick="showAddnewnumber('perm');" style="margin-left: 178px;"><i class="fa fa-plus"></i><?php echo $this->lang->line('addnumber_menu'); ?></a>
						<?php } ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th style="width: 10px">#</th>
								<th>GSM Number</th>
								<th colspan="2">Operations</th>
							</tr>
							<?php
							if ( !empty( $permissionlist ) ) {
								foreach ( $permissionlist as $record ) {
									// to hid the numbers from administrator users
									if ( $role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE ) {
										$numb = $record->username;
										$securenumb = $numb;
									} else {
										$numb = $record->username;
										$securenumb = substr_replace( $numb, "****", 4 );
									}
									// end here hiding the numbers from administrator users
									?>
							<tr>
								<td>
									<?php echo $record->pid; ?>
								</td>
								<td>
									<?php echo $securenumb; ?>
								</td>
								<td><span class="badge bg-yellow-active" data-toggle="modal" data-target="#modal-default<?php echo $record->pid; ?>">Edit</span>
								</td>
								<td><span class="badge bg-red-active" onclick="deletethisrow('perm',<?php echo $record->username; ?>);">Delete</span>
								</td>
							</tr>
							<div class="modal fade" id="modal-default<?php echo $record->pid; ?>" style="width: 100%;margin-left: 0px;">
								<div class="modal-dialog">
									<form name="" action="" method="POST" id="editgsmnumber<?php echo $record->pid; ?>">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
											
												<h4 class="modal-title">Edit GSM Number (Permission List)</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gsmnumber<?php echo $record->pid; ?>"></label>
															<input type="text" class="form-control required" value="<?php echo $record->username; ?>" id="gsmnumber<?php echo $record->pid; ?>" name="ctname" maxlength="128" style="height: 35px;">
														</div>

													</div>
												</div>
												<input name="pid" type="hidden" value="<?php echo $record->pid; ?>">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
													<?php echo $this->lang->line('close_menu'); ?>
												</button>
												<button type="button" onclick="editnumber('<?php echo $record->pid; ?>','perm',<?php echo $record->username; ?>);" id="enddatesubmit" class="btn btn-primary">
													<?php echo $this->lang->line('savechanges_menu'); ?>
												</button>
											</div>
										</div>
									</form>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<?php
							}
							} else {
								echo '<tr><td colspan="4"> No Record Found! </td></tr>';
							}
							?>
						</table>
					</div>
				</div>
				<!-- /.box -->
				<div class="box-footer clearfix">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
			<!-- /.col -->
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Black List</h3> &nbsp;&nbsp;&nbsp;
						<?php if(!empty($countblack)){echo '<code>'.$countblack . ' Results found </code>';} ?>
						<?php if(!empty($customeridd && $locationidd)){?>
						<a class="btn btn-primary" href="#" onclick="showAddnewnumber('black');" style="margin-left: 226px;"><i class="fa fa-plus"></i><?php echo $this->lang->line('addnumber_menu'); ?></a>
						<?php } ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th style="width: 10px">#</th>
								<th>GSM Number</th>
								<th colspan="2">Operations</th>
							</tr>
							<?php
							if ( !empty( $blacklist ) ) {
								foreach ( $blacklist as $record ) {
									?>
							<tr>
								<td>
									<?php echo $record->bid; ?>
								</td>
								<?php 		  
	// to hid the numbers from administrator users
				if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){
					$numb=$record->username;
					$securenumb=$numb;
				}else{
					$numb=$record->username;
					$securenumb=substr_replace($numb, "****", 4);
				}
				?>
								<td>
									<?php echo $securenumb; ?>
								</td>
								<td><span class="badge bg-yellow-active" data-toggle="modal" data-target="#modal-default<?php echo $record->bid; ?>">Edit</span>
								</td>
								<td><span class="badge bg-red-active" onclick="deletethisrow('black',<?php echo $record->username; ?>);">Delete</span>
								</td>
							</tr>
							<div class="modal fade" id="modal-default<?php echo $record->bid; ?>" style="width: 100%;margin-left: 0px;">
								<div class="modal-dialog">
									<form name="" action="" method="POST" id="editgsmnumber<?php echo $record->bid; ?>">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
											
												<h4 class="modal-title">Edit GSM Number (Black List)</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gsmnumber<?php echo $record->bid; ?>"></label>
															<input type="text" class="form-control required" value="<?php echo $record->username; ?>" id="gsmnumber<?php echo $record->bid; ?>" name="ctname" maxlength="128" style="height: 35px;">
														</div>

													</div>
												</div>
												<input name="pid" type="hidden" value="<?php echo $record->bid; ?>">
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
													<?php echo $this->lang->line('close_menu'); ?>
												</button>
												<button type="button" onclick="editnumber('<?php echo $record->bid; ?>','black',<?php echo $record->username; ?>);" id="enddatesubmit" class="btn btn-primary">
													<?php echo $this->lang->line('savechanges_menu'); ?>
												</button>
											</div>
										</div>
									</form>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<?php
							}
							} else {
								echo '<tr><td colspan="4"> No Record Found! </td></tr>';
							}
							?>
						</table>
					</div>
				</div>
				<!-- /.box -->
				<?php if(!empty($pagination_link_1)){ ?>
				<div class="box-footer clearfix">
					<?php echo $pagination_link_1; ?>
				</div>
				<?php }?>
			</div>
			<!-- /.col -->

		</div>
		<!---End-->
		<!--------model for add neew number------>
		<div class="modal fade" id="modal-default1" style="width: 100%;margin-left: 0px;">
			<div class="modal-dialog">
				<form name="addpermnumbertodb" action="addpermnumbertodb" method="POST" id="permnumbermanual">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
						
							<h4 class="modal-title">Add Phone Number to Permisssion List</h4>
						</div>
						<div class="modal-body">
							<div id="qwerty" class="input-append">
								<textarea class="form-control" rows="3" placeholder="Seperate numbers with comma e.g 5xxxxxxxxx,5xxxxxxxxx" maxlength="160" id="permittednumbers" name="permittednumbers"></textarea>
							</div>
							<input name="customerid" type="hidden" value="<?php if(!empty($customeridd && $locationidd)){ echo $customeridd; } ?>">
							<input name="locationid" type="hidden" value="<?php if(!empty($customeridd && $locationidd)){ echo $locationidd; } ?>">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
								<?php echo $this->lang->line('close_menu'); ?>
							</button>
							<button type="submit" id="permsubmit" class="btn btn-primary">
								<?php echo $this->lang->line('savechanges_menu'); ?>
							</button>
						</div>
					</div>
				</form>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!---------------------end----------->
		<!--------model for add neew number------>
		<div class="modal fade" id="modal-default2" style="width: 100%;margin-left: 0px;">
			<div class="modal-dialog">
				<form name="" action="addblacknumbertodb" method="POST" id="blacknumbermanual">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
						
							<h4 class="modal-title">Add Phone Number to BlackList</h4>
						</div>
						<div class="modal-body">
							<div id="qwerty" class="input-append">
								<textarea class="form-control" rows="3" placeholder="Send instant message after user logged In" maxlength="160" id="blackednumbers" name="blackednumbers"></textarea>
							</div>
							<input name="customerid" type="hidden" value="<?php if(!empty($customeridd && $locationidd)){ echo $customeridd; } ?>">
							<input name="locationid" type="hidden" value="<?php if(!empty($customeridd && $locationidd)){ echo $locationidd; } ?>">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">
								<?php echo $this->lang->line('close_menu'); ?>
							</button>
							<button type="submit" id="blacksubmit" class="btn btn-primary">
								<?php echo $this->lang->line('savechanges_menu'); ?>
							</button>
						</div>
					</div>
				</form>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!---------------------end----------->

	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletewifivisitors.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	function showAddnewnumber( type ) {
		if ( type == 'perm' ) {
			$( '#modal-default1' ).modal( "show" );
		} else {
			$( '#modal-default2' ).modal( "show" );
		}
	}
	$( function () {
		$( 'input[type=file]' ).change( function () {
			var val = $( this ).val().toLowerCase(),
				regex = new RegExp( "(.*?)\.(csv)$" );

			if ( !( regex.test( val ) ) ) {
				$( this ).val( '' );
				$( ".help-block" ).html( " <code>Please select correct file format</code>." );
				setTimeout( function () {
					$( '.help-block' ).html( 'File Extension: CSV' );
				}, 5000 );
			}
		} );
	} );

	function editnumber( id, table, oldnumber ) {
		var number = document.getElementById( "gsmnumber" + id ).value;
		//alert(number);

		if ( table == 'perm' ) {
			$.ajax( {
				type: 'POST',
				url: baseURL + 'editphonenumberperm',
				data: {
					username: number,
					pid: id,
					oldusername: oldnumber
				},
				success: function ( response ) {
					//alert(response);
					window.location = baseURL + 'addremovepblist';
				}
			} );
		}
		if ( table == 'black' ) {
			$.ajax( {
				type: 'POST',
				url: baseURL + 'editphonenumberblack',
				data: {
					username: number,
					bid: id,
					oldusername: oldnumber
				},
				success: function ( response ) {
					//alert(response);
					window.location = baseURL + 'addremovepblist';
				}
			} );
		}

	}

	function deletethisrow( table, username ) {
		if ( table == 'perm' ) {
			var r = confirm( "Are you want to Delete this (" + username + ") number from Permisssion List?!" );
			if ( r == true ) {
				$.ajax( {
					type: 'POST',
					url: baseURL + 'deletephonenumberperm',
					data: {
						username: username
					},
					success: function ( response ) {
						//alert(response);
						window.location = baseURL + 'addremovepblist';
					}
				} );
			} else {
				txt = "You pressed Cancel!";
			}
		}
		if ( table == 'black' ) {
			var rr = confirm( "Are you want to Delete this (" + username + ") number from Black List?!" );
			if ( rr == true ) {
				$.ajax( {
					type: 'POST',
					url: baseURL + 'deletephonenumberblack',
					data: {
						username: username
					},
					success: function ( response ) {
						//alert(response);
						window.location = baseURL + 'addremovepblist';
					}
				} );
			} else {
				txt = "You pressed Cancel!";
			}
		}

	}
</script>