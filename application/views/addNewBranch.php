<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('accesspointmanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->



				<div class="box box-primary">
					<div class="box-header">
						<!--                        <h3 class="box-title"><?php echo $this->lang->line('entercustomerdetail_menu'); ?></h3>-->
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<?php $this->load->helper("form"); ?>
					<form role="form" id="addCustomer" action="<?php echo base_url() ?>addNewBranch" method="post" role="form">
						<div class="box-body">
							<!-- select -->
							<?php 
                            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ 
                            $customeruserid = str_replace("8791", "", $customeridsess); ?>
							<input type="hidden" id="selectcustomer" name="selectcustomer" value="<?php echo $customeruserid; ?>">
							<?php }else{ ?>
							<div class="form-group">
								<label>
									<?php echo $this->lang->line('selectcustomer_menu'); ?>
								</label>
								<select class="form-control" id="selectcustomer" name="selectcustomer">
									<?php
									if ( !empty( $customers ) ) {
										foreach ( $customers as $cus ) {
											?>
									<option value="<?php echo $cus->customerid; ?>">
										<?php echo $cus->customername; ?>
									</option>
									<?php
									}
									}
									?>
								</select>
							</div>
							<?php } ?>
							<!-- select -->
							<div class="form-group">
								<label>Location</label>
								<select class="form-control" id="selectlocation" name="selectlocation">
									<?php
									if ( !empty( $locations ) ) {
										foreach ( $locations as $loc ) {
											?>
									<option value="<?php echo $loc->locationid; ?>">
										<?php echo $loc->locationname; ?>
									</option>
									<?php
									}
									}
									?>
								</select>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="customername">
											Classroom Name
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('bname'); ?>" id="bname" name="bname" maxlength="128">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="femail">
											<?php echo $this->lang->line('branchip_menu'); ?>
										</label>
										<input type="text" class="form-control required" id="bip" style="height: 32px;" value="<?php echo set_value('bip'); ?>" name="bip" maxlength="128">
									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<label for="description">
											<?php echo $this->lang->line('description'); ?>
										</label>
										<textarea name="description" class="form-control required" id="description" style="height: 32px;" value="<?php echo set_value('description'); ?>" max-length="500"></textarea>

									</div>
								</div>
							</div>
						</div>


						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Submit"/>
							<input type="reset" class="btn btn-default" value="Reset"/>
						</div>
					</form>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<div class="col-md-4">
			<?php
			$this->load->helper( 'form' );
			$error = $this->session->flashdata( 'error' );
			if ( $error ) {
				?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
			<?php } ?>
			<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
			<?php } ?>

			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
			</div>
		</div>
</div>
</section>

</div>
<script src="<?php echo base_url(); ?>assets/js/editbranch.js" type="text/javascript"></script>