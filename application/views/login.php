<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LOCUS | Admin System Log in</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-page" style="background: whitesmoke;">
	<div class="login-box">
		<div class="login-logo">
			<img src="<?php echo base_url(); ?>assets/dist/img/locus-logo.png" align="center" style="width: 115%;margin-left: -30px;">
			<!--        <a href="#"><b>LOCUS</b><br<b></a>-->
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body" style="background: none; margin-top: -30px ">
			<h1><b style="font-size: 30px; color: black;">ÜYE GİRİŞİ </b></h1>
			<?php $this->load->helper('form'); ?>
			<div class="row">
				<div class="col-md-12">
					<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
				</div>
			</div>
			<?php
			$this->load->helper( 'form' );
			$error = $this->session->flashdata( 'error' );
			if ( $error ) {
				?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $error; ?>
			</div>
			<?php }
        $success = $this->session->flashdata('success');
        if($success)
        {
            ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $success; ?>
			</div>
			<?php } ?>

			<form action="<?php echo base_url(); ?>loginMe" method="post">
				<div class="form-group has-feedback">
					<label style="color: black;">E-posta Adresi</label>
					<input type="email" class="form-control" placeholder="" name="email" required/>
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>

				<div class="form-group has-feedback">
					<label style="color: black;">Şifre</label>
					<input type="password" class="form-control" placeholder="" name="password" required/>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<!--            
            <td><b>Eposta Adresi</b><br/></td>
             <td></td>
 <td><input type="email" class="form-control" name="email" required ></td>
 <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
 </tr>
 <tr>
 <td>Şifre</td> <br/>
 <td><input type="password" class="form-control" name="password" required ></td>
 <span class="glyphicon glyphicon-lock form-control-feedback"></span>
 </tr>
 <tr>
 <td></td>
 <td></td>-->
				<div class="row">
					<!--            <div class="col-xs-8">    
               <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                         
            </div> /.col -->
					<div class="col-xs-4" style="width: 50%;">
						<a href="<?php echo base_url() ?>forgotPassword" style=" position: right; color: #4969b2;"><h5><b>Şifremi unuttum </b></h5></a>

						<input type="submit" class="btn btn-primary btn-block btn-flat" value="Giriş Yap" style="position: left; width:100%; background: #4969b2; "/>
					</div>
					<!-- /.col -->
				</div>
				</tr>
			</form>



		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.js" type="text/javascript"></script>
	<script>
		$( document ).ajaxStart( function () {
			Pace.restart()
		} );

		function callAjax() {

			//alert(text);
			$.ajax( {
				url: '<?php echo base_url();?>cronscript',
				success: function ( result ) {},
				complete: function ( jqXHR, status ) {
					setInterval( function () {
						$.ajax( {
							url: '<?php echo base_url();?>cronscript',
							success: function ( result ) {}
						} );
					}, 3000 );
				}
			} );
		}
		///callAjax();
	</script>
</body>
</html>