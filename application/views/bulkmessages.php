<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('bulkmessages_menu'); ?>
		</h1>
		<!--    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i><?php echo $this->lang->line('home_menu'); ?></a></li>
      <li><a href="#"><?php echo $this->lang->line('messages_menu'); ?></a></li>
      <li class="active"><?php echo $this->lang->line('bulkmessages_menu'); ?></li>
    </ol>-->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box">
			<!--      <div class="box-header with-border">
        <h3 class="box-title"> <?php echo $this->lang->line('bulkmessagesection_menu'); ?></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>-->
			<!---show this box after send messages--->

			<!--------end box after send message------->
			<div class="box-body" style="padding: 17px; background-color: #ffffff;">
				<div class="row">
					<div class="form-group">
						<div class="form-group col-xs-6">
							<div class="col-xs-12" style="margin-top: 25px;">
								<?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" multiple name="selectlocationmsg">
										<?php foreach($locations as $locationsrecord){ ?>
										<option value="<?php echo $locationsrecord->locationid;?>">
											<?php echo $locationsrecord->locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
								<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
								<input type="hidden" name="selectcustomermsg" id="selectcustomermsg" value="<?php echo $customeruserid; ?>">
								<?php }else{
            ?>
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectcustomer_menu'); ?>
									</label>
									<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
										<option> </option>
										<?php foreach($customers as $customersrecord){ ?>
										<option value="<?php echo $customersrecord->customerid;?>">
											<?php echo $customersrecord->cusername; ?>
										</option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" multiple name="selectlocationmsg">             
                  </select>
								


								</div>
								<?php } ?>
								<div class="form-group">
									<div class="row">

										<div class="col-xs-6">
											<div id="datetimepicker" class="input-append date">
												<label>
													<?php echo $this->lang->line('startdate2_menu'); ?>
												</label><br>
												<input type="text" id="datetimepickervaluestart" style="height: 32px;width: 150px;" required> </input>
												<span class="add-on" style="height: 32px;">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                  </span>
											


											</div>
										</div>

										<div class="col-xs-6">
											<div id="datetimepickerend" class="input-append date">
												<label>
													<?php echo $this->lang->line('enddate2_menu'); ?>
												</label><br>
												<input type="text" id="datetimepickervalueend" style="height: 32px;width: 150px;" required></input>
												<span class="add-on" style="height: 32px;">
                 <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                 </span>
											


											</div>
										</div>
										<div class="" style="margin-left: 15px; margin-top:45px;">
											<span><a href="#" onclick="getphonenumbersbylocid()"><?php echo $this->lang->line('checkphonenumber_menu'); ?> &nbsp &nbsp<i class="fa fa-search"></i></a></span>
											<br>
										</div>
									</div>
								</div>
								<span class="numberofusers"></span>
								<br><br>
								<label>
									<?php echo $this->lang->line('message_menu'); ?>
								</label>
								<textarea class="form-control" rows="4" placeholder="Write text here for bulk message (limited to 160 characters)" maxlength="160" id="bulkmessagetext" name="bulkmessagetext"></textarea>
								<div class="form-group" style="text-align: right;height: 0;">
									<span id='remainingC'></span>
								</div>
								<button type="button" class="btn btn-default btn-lrg ajax" title="Ajax Request" style="background-color: #3c8dbc;color:white; border: 0px;padding: 5px;">
									<?php echo $this->lang->line('sendbulkmessages_menu'); ?>
								</button>
							</div>
						</div>
						<div class="ajax-content" style="font-size: 12px; text-align: justify; color: forestgreen;">
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer" style="background-color: #ffffff;">
						<?php echo $this->lang->line('usingsmsgwformessageswithusername_menu'); ?><code>elogopgsms</code>
					</div>
					<!-- /.box-footer-->
				</div>

				<!-- /.box-body -->

				<!-- /.box-footer-->
			</div>
		</div>
		<!-- /.box -->

	</section>
	<!-- /.content -->
</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	//$(function() {
	/////alert('Bulk Messages is under Development');
	//////window.location = "<?php echo base_url();?>/dashboard";
	//
	//    var start = moment().subtract(29, 'days');
	//    var end = moment();
	//
	//    function cb(start, end) {
	//        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	//    }
	//
	//    $('#reportrange').daterangepicker({
	//        startDate: start,
	//        endDate: end
	//    }, cb);
	//
	//    cb(start, end);
	//    
	//});
</script>
<script>
	function getphonenumbersbylocid() {
		var startdatenum = $( '#datetimepickervaluestart' ).val();
		var enddatenum = $( '#datetimepickervalueend' ).val();
		var customerid = $( '#selectcustomermsg' ).val();
		var locationid = $( '#selectlocationmsg' ).val();

		if ( startdatenum != '' && enddatenum != '' && customerid != '' && locationid != '' ) {
			//alert(startdatenum+enddatenum + customerid +locationid);
			$.ajax( {
				url: baseURL + 'getphonenumbersbylocation',
				method: 'post',
				data: {
					customerid: customerid,
					locationid: locationid,
					datetimepickervaluestart: startdatenum,
					datetimepickervalueend: enddatenum
				},
				dataType: 'json',
				success: function ( result ) {
					$( '.numberofusers' ).html( '<hr> =  ' + result );
					//alert(result);
				}
			} );
		} else {
			alert( 'Please select Customer, Location , Start and End Date' );
		}
	}
	$( document ).ready( function () {
		var len = 0;
		var maxchar = 160;

		$( '#bulkmessagetext' ).keyup( function () {
			len = this.value.length
			if ( len > maxchar ) {
				return false;
			} else if ( len > 0 ) {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar - len ) );
			} else {
				$( "#remainingC" ).html( "Remaining characters: " + ( maxchar ) );
			}
		} )
	} );
</script>
<script src="<?php echo base_url(); ?>assets/js/sendbulmessages.js" type="text/javascript"></script>