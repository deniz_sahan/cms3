<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<!-- add code by abdul qadir for languages turkish and english -->
			<?php echo $this->lang->line('usermanagment_menu'); ?>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNew">
						<?php echo $this->lang->line('addnew_menu'); ?>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--                    <h3 class="box-title"><?php echo $this->lang->line('userlist_menu'); ?></h3>--><br>
						<div class="box-tools">
							<form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
								<div class="input-group">
									<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;height:30px;" placeholder="Search"/>
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<!-- /added different languages -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<!--                      <th><?php echo $this->lang->line('id_menu'); ?></th>-->
								<th>
									<?php echo $this->lang->line('name_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('email_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('mobile_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('role_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('customername_menu'); ?>
								</th>
								<th class="text-center">
									<?php echo $this->lang->line('actions_menu'); ?>
								</th>
							</tr>
							<?php
							if ( !empty( $userRecords ) ) {
								foreach ( $userRecords as $record ) {
									// to hid the numbers from administrator users
									if ( $role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE ) {
										$numb = $record->mobile;
										$securenumb = $numb;
									} else {
										$numb = $record->mobile;
										$securenumb = substr_replace( $numb, "****", 4 );
									}
									// end here hiding the numbers from administrator users
									?>
							<tr>
								<!--                      <td><?php echo $record->userId ?></td>-->
								<td>
									<?php echo $record->name ?>
								</td>
								<td>
									<?php echo $record->email ?>
								</td>
								<td>
									<?php echo $securenumb ?>
								</td>
								<td>
									<?php echo $record->role ?>
								</td>
								<td>
									<?php echo $record->customername ?>
								</td>
								<td class="text-center">
									<a class="btn btn-sm btn-primary" href="<?= base_url().'login-history/'.$record->userId; ?>" title="Login history"><i class="fa fa-history"></i></a> |
									<a class="btn btn-sm btn-info" href="<?php echo base_url().'editOld/'.$record->userId; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
									<a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="<?php echo $record->userId; ?>" title="Delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "userListing/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>