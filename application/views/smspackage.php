<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNewcus">
						<?php echo $this->lang->line('addnewcustomer_menu'); ?>
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo $this->lang->line('customerlist_menu'); ?>
						</h3>


						<div class="box-footer clearfix">
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
					<!-- /.box -->
				</div>
			</div>
	</section>
	</div>
</div>