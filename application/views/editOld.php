<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';
$roleId = '';

if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
        $userId = $uf->userId;
        $name = $uf->name;
        $email = $uf->email;
        $mobile = $uf->mobile;
        $roleId = $uf->roleId;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $this->lang->line('usermanagment_menu'); ?>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $this->lang->line('enteruserdetail_menu'); ?></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname"><?php echo $this->lang->line('fullname_menu'); ?></label>
                                        <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $name; ?>" maxlength="128"style="height: 32px;" width="50%">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email"><?php echo $this->lang->line('emailaddress_menu'); ?></label>
                                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" maxlength="128" style="height: 32px;" width="50%">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password"><?php echo $this->lang->line('password_menu'); ?></label>
                                        <input type="password" class="form-control" id="password" name="password" maxlength="20" style="height: 32px;" width="50%">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword"><?php echo $this->lang->line('confirmpassword_menu'); ?></label>
                                        <input type="password" class="form-control" id="cpassword"  name="cpassword" maxlength="20 "style="height: 32px;" width="50%">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile"><?php echo $this->lang->line('mobilenumber_menu'); ?></label>
                                        <input type="text" class="form-control" id="mobile"  name="mobile" value="<?php echo $mobile; ?>" maxlength="10"style="height: 32px;" width="50%">
                                    </div>
                                </div>
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image Upload</label>
                                        <input type="file" name="image" id="image" >
                                   
                                    </div>
                                </div>
								
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role"><?php echo $this->lang->line('role_menu'); ?></label>
                                        <select class="form-control" id="role" name="role">
                                            <option value="0"><?php echo $this->lang->line('selectrole_menu'); ?></option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->roleId; ?>" <?php if($rl->roleId == $roleId) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    
                            </div>
                            <?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){
                $customeruserid = str_replace("8791", "", $customeridsess);
                ?>
                <input type="hidden" id="customeruser" name="customeruser" value="<?php echo $customeruserid; ?>">
                <?php }else{ ?>
                            <div class="row">
                              <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Customer"><?php echo $this->lang->line('customername_menu'); ?></label>
                                        <select class="form-control required" id="customeruser" name="customeruser">
                                            <option value="0">Select Customer</option>
                                            <?php
                                            if(!empty($customers))
                                            {
                                                foreach ($customers as $cust)
                                                {
                                                    ?>
                                                    <option value="<?php echo $cust->customerid ?>"><?php echo $cust->customername; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  
                                
                            </div>
            <?php } ?>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>" />
                            <input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>