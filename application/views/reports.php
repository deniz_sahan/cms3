<?php

if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;

	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('reports_menu'); ?>
		</h1>
	</section>

	<section class="content">
		<form action="<?php echo base_url() ?>userstats" method="POST" id="searchList">
			<div class='row'>

				<?php 
               if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
				<div class="col-xs-3">
					<div class="form-group">
						<label>
							<?php echo $this->lang->line('selectlocation_menu'); ?>
						</label>
						<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
							<?php foreach($locations as $locationsrecord){ ?>
							<option value="<?php echo $locationsrecord->locationid;?>">
								<?php echo $locationsrecord->locationname; ?>
							</option>
							<?php } ?>
						</select>
					</div>
				</div>
				<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
				<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
				<?php }else{
               ?>
				<div class="col-xs-3">
					<div class="form-group">
						<label>
							<?php echo $this->lang->line('selectcustomer_menu'); ?>
						</label>
						<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
                        <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                     <?php foreach($customers as $customersrecord){ ?>
                        <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                     <?php } ?>
                 </select>
					

					</div>
				</div>
				<div class="col-xs-3">
					<div class="form-group">
						<label>
							<?php echo $this->lang->line('selectlocation_menu'); ?>
						</label>
						<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
							<?php if(!empty($locationinfo)){  ?>
							<option value="<?php echo $locationid; ?>">
								<?php echo $locationname; ?>
							</option>
							<?php } ?>
						</select>
					</div>
				</div>
				<?php } ?>
				<div class="col-xs-3">
					<div class="form-group">
						<!--For daily , weekly and monthly --->
						<label>
							<?php echo $this->lang->line('time_interval'); ?>
						</label>
						<select class="form-control" name="barselectdata" id="barselectdata">
							<option value="">
								<?php echo $this->lang->line(''); ?>
							</option>
							<option name="daily" value="daily" <?php if(isset($postdata) && $postdata=='daily' ){echo 'selected';} ?>>
								<?php echo $this->lang->line('today_menu'); ?> </option>
							<option name="week" value="week" <?php if(isset($postdata) && $postdata=='week' ){echo 'selected';} ?>>
								<?php echo $this->lang->line('lastweek_menu'); ?> </option>
							<option name="monthly" value="monthly" <?php if(isset($postdata) && $postdata=='monthly' ){echo 'selected';} ?>>
								<?php echo $this->lang->line('lastmonth_menu'); ?> </option>
							<option name="selectdate" value="selectdate" <?php if(isset($postdata) && $postdata=='selectdate' ){echo 'selected';} ?>>
								<?php echo $this->lang->line('selectdate_menu'); ?> </option>
						</select>
						<!--For daily , weekly and monthly end here --->
					</div>
				</div>
				<div class="col-xs-3" style="margin-top: 27px;">
					<div class="form-group">
						<div class="input-group">
							<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-lg " style="height: 30px;" placeholder="Search"/>
							<span class="fa fa-search form-control-feedback" style="height:  30px;margin-top: -9px;"></span>
						</div>
					</div>
				</div>



			</div>
			<div class="row">

				<div class="col-md-3" style="margin-top: 5px;">
					<div id="datetimepicker" class="input-append date">
						<label>
							<?php echo $this->lang->line('startdate_menu'); ?>
						</label>
						<p>
							<input type="text" id="datetimepickervaluestart" name="datetimepickervaluestart" onblur="validation()" style="height: 26px; " required> </input>
							<span class="add-on" style="height: 26px;">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
              </span>
						

					</div>
				</div>
				<div class="col-md-3" style=" margin-top: 5px;">
					<div id="datetimepickerend" class="input-append date">
						<label>
							<?php echo $this->lang->line('enddate_menu'); ?>
						</label>
						<p>
							<input type="text" id="datetimepickervalueend" name="datetimepickervalueend" onblur="validation()" style="height: 26px;" required></input>
							<span class="add-on" style="height: 26px;">
             <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
             </span>
						

					</div>
				</div>
				
				
				<div class="col-md-3" style="margin-top: 27px;">
					<div class="input-group-btn">
						<input class="btn btn-success" type="submit" value="<?php echo $this->lang->line('showresults_menu'); ?>" style=" background: #367fa9; border: blue; "/>
					</div>
				</div>
			</div>
		</form>


		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header" style="text-align: center">

						<?php if(!empty($countrecords)){echo '<code>'.$countrecords . ' Results found </code>';} ?>
						<div class="box-tools">
							<div class="input-group">
								<?php 
                                if(empty($cusidfordownload)){
                                    $cusidfordownload = 0;
                                }
                                if(empty($locidfordownload)){
                                    $locidfordownload = 0;
                                }
                                if(empty($searchText)){
                                    $searchText = 0;
                                }
                                ?>
								<a href="<?= base_url() ?>downloadexcel/<?php echo $cusidfordownload; ?>/<?php echo $locidfordownload; ?>/<?php echo $searchText; ?>" class="btn btn-success" style=" background: #367fa9; border: blue;">Download</a>


								<!--                               <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 240px;height: 30px;" placeholder="Search Phone Numbers"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>-->
							</div>

						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top:3px solid #d2d6de;">
								<th>
									<?php echo $this->lang->line('username_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('startdate_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('lastseen_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('uploads_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('downloads_menu'); ?>
								</th>
								<th>IP</th>
								<th>MAC</th>
								<th>
									<?php echo $this->lang->line('sessiontime_menu'); ?>
								</th>

							</tr>
							<?php
							if ( !empty( $statsreport ) ) {
								foreach ( $statsreport as $record ) {
									$upload = round( $record->uploads / 1024 / 1024, 2 );
									$download = round( $record->downloads / 1024 / 1024, 2 );
									// to hid the numbers from administrator users
									if ( $role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE ) {
										$numb = $record->username;
										$securenumb = $numb;
									} else {
										$numb = $record->username;
										$securenumb = substr_replace( $numb, "****", 4 );
									}
									// end here hiding the numbers from administrator users


									if ( $record->duration < 60 ) {
										$sure = $record->duration . " seconds";
									} else {
										$gun = floor( $record->duration / 86400 );
										$saat = floor( ( $record->duration - $gun * 86400 ) / 3600 );
										$dakika = floor( ( $record->duration - $gun * 86400 - $saat * 3600 ) / 60 );
										$sure = "";
										if ( $dakika > 0 ) {
											$sure = $dakika . " minutes";
										}
										if ( $saat > 0 ) {
											$sure = $saat . " hours " . $sure;
										}
										if ( $gun > 0 ) {
											$sure = $gun . " day " . $sure;
										}
									}
									?>
							<tr>

								<td>
									<?php echo $securenumb; ?>
								</td>
								<td>
									<?php echo date("d-m-Y H:i", strtotime('+10 hours', strtotime($record->logindate)));  ?>
								</td>
								<td>
									<?php echo date("d-m-Y H:i", strtotime('+10 hours', strtotime($record->lastseen)));  ?>
									<!--                          <a id="<?php echo $numb; ?>" href="javascript:;" onclick="checkstatus('<?php echo $numb; ?>','<?php echo $record->downloads; ?>');">Check Status</a>-->
								</td>
								<td>
									<?php echo $download; ?> MB</td>
								<td>
									<?php echo $upload; ?> MB</td>
								<td>
									<?php echo $record->framedipaddress; ?>
								</td>
								<td>
									<?php echo $record->callingstationid; ?>
								</td>
								<td>
									<?php echo $sure ?>
								</td>


							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletewifivisitors.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "userstats/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>

<script>
	var name = document.getElementById( "datetimepickervaluestart" ).value;
	var email = document.getElementById( "datetimepickerend" ).value;
	$( "#datetimepickervaluestart" ).attr( "disabled", true );
	$( "#datetimepickervalueend" ).attr( "disabled", true );
	$( "#barselectdata" ).change( function () {
		var select = $( "#barselectdata option:selected" ).val();
		switch ( select ) {
			case "selectdate":
				$( "#datetimepickervaluestart" ).attr( "disabled", false );
				$( "#datetimepickervalueend" ).attr( "disabled", false );
				break;
			case "daily":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			case "monthly":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			case "week":
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;
			default:
				$( "#datetimepickervaluestart" ).attr( "disabled", true );
				$( "#datetimepickervalueend" ).attr( "disabled", true );
				break;

		}
	} );

	function checkstatus( username, downloaddata ) {
		$.ajax( {
			url: baseURL + 'checkuserstatus',
			method: 'post',
			data: {
				username: username,
				downloaddata: downloaddata
			},
			dataType: 'json',
			success: function ( response ) {
				document.getElementById( username ).innerHTML = "" + response + "";
			}
		} );
		// document.getElementById(username).innerHTML = "<span class='label label-success'>Online</span>";
	}

	//$("#datetimepickervaluestart").attr("disabled",false);
</script>