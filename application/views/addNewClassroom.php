<?php
if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $courseInfo ) ) {
	foreach ( $courseInfo as $cf ) {
		$coursename = $cf->course_id;
		$courseid = $cf->course_name;


	}
}
if ( !empty( $classroominfo ) ) {

	foreach ( $classroominfo as $cf ) {
		$classid = $cf->locationid;
		$classname = $cf->locationname;


	}
}

?>


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->




				<div class="box box-primary">
					<div class="box-header">
						<!--                        <h3 class="box-title"><?php echo $this->lang->line('entercustomerdetail_menu'); ?></h3>-->
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<?php $this->load->helper("form"); ?>
					<form role="form" id="addCustomer" action="<?php echo base_url() ?>addNewclassroom" method="post" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="cname">
											Teacher Name
										</label>
										<input type="text" class="form-control required" style="height: 32px;" value="<?php echo set_value('cname'); ?>" id="cname" name="cname" maxlength="128">
									</div>

								</div>
								
									<div class="col-xs-3">
								<div class="form-group">
									<label>
									Select Course
									</label>
									<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">

                                                    <option value="<?php if(empty($courseinfo)){ ?><?php } else { echo $courseid; }?>"><?php if(empty($courseinfo)){ ?><?php } else { echo $coursename; }?></option>
                                     <?php foreach($customers as $courserecord){ ?>
                                    <option value="<?php echo $courserecord->course_id;?>"><?php echo $courserecord->course_name; ?></option>
                                   <?php } ?>
                                  </select>						
								  </div>
								  </div>
								
									<div class="col-xs-3">
								<div class="form-group">
									<label>
									Select Classroom
									</label>
									<select class="form-control" id="selectclass" name="selectclass">

                                     <?php foreach($location as $courserecord){ ?>
                                    <option value="<?php echo $courserecord->locationid;?>"><?php echo $courserecord->locationname; ?></option>
                                   <?php } ?>
                                  </select>						
								  </div>
								  </div>
								  </div>
								  </div>
						<!-- /.box-body -->		
							<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>
				</div>
			</div>



			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<script src="<?php echo base_url(); ?>assets/js/editcustomer.js" type="text/javascript"></script>