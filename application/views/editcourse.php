<?php

$userId = '';
$name = '';
//$email = '';
$mobile = '';
$roleId = '';

if ( !empty( $courseinfo ) ) {
	foreach ( $courseinfo as $cf ) {
		$courseid = $cf->course_id;
		$coursename = $cf->course_name;
		$coursetitle = $cf->course_title;
		$courseinstructor = $cf->course_instructor;
		//$classroom_id = $cf->email;
		$sstartdate= $cf->cstartdate;
		$eenddate = $cf->cenddate;
		$starttime = $cf->cstarttime;
		$endtime=$cf->cendtime;
		$wweekdays=$cf->weekdays;
		//$ctphonenumber = $cf->ctphonenumber;
		//$ctemail = $cf->ctemail;
		//$ctdescription = $cf->ctdescription;

	}
	$startdate=date('d-m-y h:i:s', strtotime($sstartdate));
	$enddate=date('d-m-y h:i:s', strtotime($eenddate));
	//$days=implode(',', $selectday);
	$weekdays=explode(',', $wweekdays);
	//pre($weekdays);
	//pre($wweekdays);
	//exit;
}
if ( !empty( $classroominfo ) ) {
//pre($courseInfo);
//exit;
	foreach ( $classroominfo as $cf ) {
		$classid = $cf->locationid;
		$classname = $cf->locationname;


	}
}

?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Course Management
		</h1>
	</section>

	<section class="content">

		<div class="row">
			<!-- left column -->
			<div class="col-md-8">
				<!-- general form elements -->



				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo $this->lang->line('entercustomerdetail_menu'); ?>
						</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<form role="form" action="<?php echo base_url() ?>editcoursedata" method="post" id="editUser" role="form">
						<div class="box-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="coursename">
											coursename
										</label>
										<input type="text" class="form-control required" value="<?php echo $coursename; ?>" id="coursename" name="coursename" maxlength="128" style="height: 32px;" width="50%">
										<input type="hidden" value="<?php echo $courseid; ?>" name="courseid" id="courseid"/>
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="coursetitle">
											Course Title
										</label>
										<input type="text" class="form-control required " id="coursetitle" value="<?php echo $coursetitle; ?>" name="coursetitle" maxlength="128" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="address">
											Course Instructor
										</label>
										<input type="text" class="form-control required" id="courseinstructor" value="<?php echo $courseinstructor; ?>" name="courseinstructor" maxlength="500" style="height: 32px;" width="50%">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="classroom">
											Select Classroom
										</label>
										<select class="form-control" id="selectcustomermsg" name="selectcustomermsg" value="<?php echo $classname; ?>" >
										<?php foreach($classroominfo as $courserecord){ ?>
										<option value="<?php echo $courserecord->locationid;?>"><?php echo $courserecord->locationname; ?></option>
                                   <?php } ?>
                                  </select>
									</div>
								</div>
											<!-- /.start Date -->
				<div class="col-md-3" style="margin-top: 5px;">
					<div id="datetimepicker" class="input-append date">
					<label>
					<?php echo $this->lang->line('startdate_menu'); ?>
					</label>
					<p>
					<input type="text" id="datetimepickervaluestart" value="<?php echo $startdate; ?>" name="datetimepickervaluestart"  onblur="validation()" style="height: 26px;  " required> </input>
					<span class="add-on" style="height: 26px;">
					<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
					</span>					
					</div>
				   </div>
				   <!-- /.end Date -->
				   <div class="col-md-3" style=" margin-top: 5px;">
				   <div id="datetimepickerend" class="input-append date">
				   <label>
				   <?php echo $this->lang->line('enddate_menu'); ?>
				   </label>
				   <p>
				   <input type="text" id="datetimepickervalueend" value="<?php echo $enddate; ?>" name="datetimepickervalueend"  onblur="validation()" style="height: 26px;" required></input>
				   <span class="add-on" style="height: 26px;">
				   <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
				   </span>
				   </div>
				   </div>		
				   </div>
				    <!-- /.Select Time -->
					<div class="col-md-3" style=" margin-top: 5px;">
					<label>
							Select Start Time
						</label>
					<div id="datetimepickerend" class="input-append date">
						
						<input type="time" name="usr_starttime" value="<?php echo $starttime; ?>"  >
				
					</div>
					</div>
					 <!-- /.Select End Time -->
					<div>
					<label>
							Select End Time
						</label>
					<div id="datetimepickerend" class="input-append date">
						
						<input type="time" name="usr_endtime" value="<?php echo $endtime; ?>" >
					</div>
					
				   </div>
				    </div>
					<div>
								  <div class="form-group">
									<label>
										Select Day
									</label>
									<select class="form-control js-example-basic-multiple" multiple="multiple"  id="selectday"  name="selectday[]">
										 <option value="mon" <?php if (in_array("mon", $weekdays)){ echo "selected";}?>>Monday</option>
										 <option value="tue" <?php if (in_array("tue", $weekdays)){ echo "selected";}?>>Tuesday</option>
										 <option value="wed" <?php if (in_array("wed", $weekdays)){ echo "selected";}?>>Wednesday</option>
										 <option value="thu" <?php if (in_array("thu", $weekdays)){ echo "selected";}?>>Thursday</option>
										 <option value="fri" <?php if (in_array("fri", $weekdays)){ echo "selected";}?>>Friday</option>
										 <option value="sat" <?php if (in_array("sat", $weekdays)){ echo "selected";}?>>Saturday</option>
										 <option value="sun" <?php if (in_array("sun", $weekdays)){ echo "selected";}?>>Sunday</option>
										 </select>
										 </div>
	
				
							
							</div>
							
						</div>
						<!-- /.box-body -->
						<!-- /.box-body -->

						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit_menu'); ?>"/>
							<input type="reset" class="btn btn-default" value="<?php echo $this->lang->line('reset_menu'); ?>"/>
						</div>
					</form>





				</div>
			</div>
			<div class="col-md-4">
				<?php
				$this->load->helper( 'form' );
				$error = $this->session->flashdata( 'error' );
				if ( $error ) {
					?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
				<?php } ?>
				<?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editCustomer.js" type="text/javascript"></script>