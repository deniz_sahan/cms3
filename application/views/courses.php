

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('customermanagement_menu'); ?>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 text-right">
				<div class="form-group">
					<a class="btn btn-primary" href="<?php echo base_url(); ?>addNewcourse">
						Add New Courses
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--                    <h3 class="box-title"><?php echo $this->lang->line('customerlist_menu'); ?></h3>--><br>
						<div class="box-tools">
							<form action="<?php echo base_url() ?>showcustomers" method="POST" id="searchList">
								<div class="input-group">
									<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;height: 30px;" placeholder="Search">
									<div class="input-group-btn">
										<button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<th>
									Course Name
								</th>
								<th>
								 Couse Title
								</th>
								<th>
									Course Instructor
								</th>
								<th>
									Date 
								</th>
								<th class="text-center">
									<?php echo $this->lang->line('actions_menu'); ?>
								</th>
							</tr>
							<?php
							if ( !empty( $courseRecord ) ) {
								foreach ( $courseRecord as $record ) {
									?>
							<tr>
								<td>
									<?php echo $record->course_name ?>
								</td>
								<td>
									<?php echo $record->course_title ?>
								</td>
								<td>
									<?php echo $record->course_instructor?>
								</td>
								<td>
									<?php echo $record->createdate ?>
								</td>
								<td class="text-center">
									<a class="btn btn-sm btn-info" href="<?php echo base_url().'editcourse/'.$record->course_id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
									<a class="btn btn-sm btn-danger deleteCustomer" href="#" data-customerid="<?php echo $record->course_id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletecustomer.js" charset="utf-8"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			jQuery( "#searchList" ).attr( "action", baseURL + "showcustomers/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>