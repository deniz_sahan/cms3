<?php
if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;


	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $this->lang->line('wifivisitorslist_menu'); ?>
		</h1>
	</section>
	<section class="content">

		<div class="row">
			<div class="col-xs-12">
				<div class="box-tools">
					<form action="<?php echo base_url() ?>wifivisitors" method="POST" id="searchList">
						<!--Date Range start--->
						<div class='row' style="margin-left: -6px;">

							<!--                                <div class="form-group">
                                   <label>Filteg by Date</label>

                                   <div class="input-group">
                                     <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                       <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                   </div> 
                                   </div>
                                    /.input group 
                                </div>-->

							<?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
										<?php foreach($locations as $locationsrecord){ ?>
										<option value="<?php echo $locationsrecord->locationid;?>">
											<?php echo $locationsrecord->locationname; ?>
										</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
							<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
							<?php }else{
            ?>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectcustomer_menu'); ?>
									</label>
									<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">

                                                    <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                  <?php foreach($customers as $customersrecord){ ?>
                   <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                     <?php } ?>
              </select>

								
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group">
									<label>
										<?php echo $this->lang->line('selectlocation_menu'); ?>
									</label>
									<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
										<?php if(!empty($locationinfo)){  ?>
										<option value="<?php echo $locationid; ?>">
											<?php echo $locationname; ?>
										</option>
										<?php } ?>
									</select><br>
								</div>
							</div>
							<?php } ?>
							<div class="col-xs-3">
								<div class="form-group">
									<div class="input-group">
										<input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-lg " style="height: 30px; margin-top: 25px;" placeholder="Search"/>
										<span class="fa fa-search form-control-feedback" style="height:  30px;margin-right: -11px;margin-top: 16px;"></span>

									</div>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group" style="margin-top: 5px;">
									<br>
									<!--                                       <button class="btn btn-sm btn-default searchList"style="background: #367fa9; border: blue;">
                                   </button>-->
									<input class="btn btn-success" type="submit" value="<?php echo $this->lang->line('showresults_menu'); ?>" style=" background: #367fa9; border: blue; "/>
								</div>
							</div>


						</div>
					</form>
				</div>
				<div class="box">
					<div class="box-header" style="text-align: center">

						&nbsp;&nbsp;&nbsp;
						<?php if(!empty($countrecords)){echo '<code>'.$countrecords . ' Results found </code>';} ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tr style="border-bottom: 3px solid #d2d6de; border-top: 3px solid #d2d6de;">
								<th>
									<?php echo $this->lang->line('fullname_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('phonenumber_menu'); ?>
								</th>
								<th>MacID</th>
								<th>IP</th>
								<th>
									<?php echo $this->lang->line('locations_menu'); ?>
								</th>
								<th>
									<?php echo $this->lang->line('lastlogin_menu'); ?>
								</th>
								<th class="text-center">
									<?php echo $this->lang->line('actions_menu'); ?>
								</th>
							</tr>
							<?php
							if ( !empty( $wifivisitors ) ) {





								foreach ( $wifivisitors as $record ) {
									// to hid the numbers from administrator users
									if ( $role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE ) {
										$numb = $record->username;
										$securenumb = $numb;
									} else {
										$numb = $record->username;
										$securenumb = substr_replace( $numb, "****", 4 );
									}
									// end here hiding the numbers from administrator users
									?>
							<tr>
								<td>
									<?php echo $record->name.' '.$record->surname ?>
								</td>
								<td>
									<?php echo $securenumb ?>
								</td>
								<td>
									<?php echo $record->macid ?>
								</td>
								<td>
									<?php echo $record->ip ?>
								</td>
								<td>
									<?php echo $record->locationname ?>
								</td>
								<td>
									<?php $originalDate = $record->lastlogin; $newDate = date("d-m-Y H:i", strtotime($originalDate)); echo $newDate; ?>
								</td>
								<td class="text-center">
									<a class="btn btn-sm btn-danger deleteWifiUser" href="#" data-wifiuserid="<?php echo $record->userid; ?>" title="Delete"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<?php
							}
							}
							?>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer clearfix">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/deletewifivisitors.js" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery( document ).ready( function () {
		jQuery( 'ul.pagination li a' ).click( function ( e ) {
			e.preventDefault();
			var link = jQuery( this ).get( 0 ).href;
			var value = link.substring( link.lastIndexOf( '/' ) + 1 );
			var customerid = '<?php echo $customerid; ?>';
			var locationid = '<?php echo $locationid; ?>';
			jQuery( "#searchList" ).attr( "action", baseURL + "wifivisitors/" + value );
			jQuery( "#searchList" ).submit();
		} );
	} );
</script>