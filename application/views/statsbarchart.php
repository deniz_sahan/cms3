<?php
if ( !empty( $locationinfo ) ) {
	foreach ( $locationinfo as $cf ) {
		$locationname = $cf->locationname;
		$locationidd = $cf->locationid;
	}
}
if ( !empty( $customerinfo ) ) {
	foreach ( $customerinfo as $cf ) {
		$stsurname = $cf->customername;
		$customeridd = $cf->customerid;

	}
}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Students Stats
			<!--        <small><?php echo $this->lang->line('controlpanel_menu'); ?></small>-->
		</h1>
	</section>

	<section class="content">

		<div id="forms">
			<div class="row">
				<form method="post" action="<?php echo base_url()?>/barchartsdata">

					<?php 
            if($role == ROLE_CLIENT_MANAGER || $role == ROLE_CLIENT_EMPLOYEE){ ?>
					<div class="col-xs-3">
						<div class="form-group">
							<label>
								<?php echo $this->lang->line('selectlocation_menu'); ?>
							</label>
							<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
								<?php foreach($locations as $locationsrecord){ ?>
								<option value="<?php echo $locationsrecord->locationid;?>">
									<?php echo $locationsrecord->locationname; ?>
								</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<?php $customeruserid = str_replace("8791", "", $customeridsess); ?>
					<input type="hidden" name="selectcustomermsg" value="<?php echo $customeruserid; ?>">
					<?php }else{
            ?>
					<div class="col-xs-3">
						<div class="form-group">
							<label>
								<?php echo $this->lang->line('selectcustomer_menu'); ?>
							</label>
							<select class="form-control" id="selectcustomermsg" name="selectcustomermsg">
                        <option value="<?php if(empty($customerinfo)){ ?><?php } else { echo $customeridd; }?>"><?php if(empty($customerinfo)){ ?><?php } else { echo $stsurname; }?></option>
                       <?php foreach($customers as $customersrecord){ ?>
                       <option value="<?php echo $customersrecord->customerid;?>"><?php echo $customersrecord->cusername; ?></option>
                     <?php } ?>
                 </select>
						
						</div>

					</div>

					<div class="col-xs-3">
						<div class="form-group">
							<label>
								Class Room
							</label>
							<select class="form-control" id="selectlocationmsg" name="selectlocationmsg">
								<?php if(!empty($locationinfo)){  ?>
								<option value="<?php echo $locationid; ?>">
									<?php echo $locationname; ?>
								</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<?php } ?>
					<div class="col-xs-3">
						<div class="form-group">
							<label>
								<?php echo $this->lang->line('time_interval'); ?>
							</label>
							<select class="form-control" name="barselectdata">
								<option value=""></option>
								<option value="daily" <?php if(isset($messageselected) && $messageselected=='daily' ){echo 'selected';} ?>>
									<?php echo $this->lang->line('today_menu'); ?> </option>
								<option value="week" <?php if(isset($messageselected) && $messageselected=='week' ){echo 'selected';} ?>>
									<?php echo $this->lang->line('lastweek_menu'); ?> </option>
								<option value="monthly" <?php if(isset($messageselected) && $messageselected=='monthly' ){echo 'selected';} ?>>
									<?php echo $this->lang->line('lastmonth_menu'); ?> </option>
								<option value="yearly" <?php if(isset($messageselected) && $messageselected=='yearly' ){echo 'selected';} ?>>
									<?php echo $this->lang->line('lastyear_menu'); ?> </option>
							</select>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group" style="margin-top: 5px;">
							<br>
							<input class="btn btn-success" type="submit" value="<?php echo $this->lang->line('showresults_menu'); ?>" style=" background: #367fa9; border: blue; "/>
						</div>
					</div>
			</div>
			</form>
		</div>





		<!--    <div class="box-title"><?php if(isset($messageselected)){echo $messageselected;} ?></div>-->
		<!-- LINE CHART -->

		<!-- /.box -->
		<div class="row">
			<div class="col-md-12">
				<div class="wificountweek">
					<div class="inner">
						<h4>
							<?php  if(isset($weekCount)){echo $this->lang->line('totalwifiuser_menu').  $weekCount;} ?>
						</h4>
						<p> </p>
					</div>
					<div class="icon">
						<i class="ion ion-person-stalker"></i>
					</div>
					<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">
							<?php echo $this->lang->line('graphrepresentationforwifiusers_menu'); ?>
						</h3>

						<div class="box-tools pull-right">
							<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
						</div>
					</div>
					<div class="box-body">
						<div class="chart">
							<canvas id="myChart" style="height:250px"></canvas>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- for sentmessages count -->
				<!-- small box -->
				<!-- small box -->


			</div>
<?php /*?>			<div class="col-md-6">
				<div class="sentmessagecountweek">
					<div class="inner">
						<h4>
							<?php if(isset($weekCount)){echo $this->lang->line('totalsentmessage_menu'). $MessageweekCount;} ?>
						</h4>
						<p> </p>
					</div>
					<div class="icon">
						<i class="ion ion-person-stalker"></i>
					</div>
					<!--                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>


				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">
							<?php echo $this->lang->line('graphrepresentationforsentmessages_menu'); ?>
						</h3>

						<div class="box-tools pull-right">

						</div>
					</div>
					<div class="box-body">
						<div class="chart">
							<canvas id="myChart2" style="height:250px"></canvas>
						</div>
					</div>
					<!-- /.box-body -->

					<!-- /*******************.Google Map -->


				</div>
			</div><?php */?>
		</div>

	</section>
</div>
<script>
	var ctx = document.getElementById( 'myChart' ).getContext( '2d' );
	var chart = new Chart( ctx, {
		// The type of chart we want to create
		type: 'bar',

		// The data for our dataset
		data: {
			labels: <?php echo $label; ?>,
			datasets: [ {
				label: "Wifi Users",
				backgroundColor: 'rgb(54, 127, 169)',
				borderColor: 'rgb(24, 58, 78)',
				data: <?php echo $result; ?>,
			} ]
		},

		// Configuration options go here
		options: {}
	} );
</script>
<?php
// this is the code for the sms counting from DashBoard Per Location
// $resulttext value is returning from Stats
?>
<script>
	var ctx = document.getElementById( 'myChart2' ).getContext( '2d' );
	var chart = new Chart( ctx, {
		// The type of chart we want to create
		type: 'bar',

		// The data for our dataset
		data: {
			labels: <?php echo $label; ?>,
			datasets: [ {
				label: "Messages Sent",
				backgroundColor: 'rgb(54, 127, 169)',
				borderColor: 'rgb(24, 58, 78)',
				data: <?php echo $resulttext; ?>,
			} ]
		},

		// Configuration options go here
		options: {}
	} );
</script>
<script src="<?php echo base_url(); ?>assets/js/sendmessagescript.js" type="text/javascript"></script>