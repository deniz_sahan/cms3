<?php
$lang['executivedashboard_menu']="Yönetim Paneli";
$lang['dashboard_menu']="Yönetim Paneli";
$lang['dashboardperlocation_menu']="Lokasyon İstatistikleri";
$lang['per_menu']="bazlı";
$lang['locations_menu']="Lokasyonlar";
$lang['messages_menu']="Kampanya Yönetimi";
$lang['campaignlist']="Kampanya Listesi";
$lang['customers_menu']="Müşteriler";
$lang['wifivisitors_menu']="Wifi Kullanıcıları";
$lang['users_menu']="Kullanıcı Yönetimi";
$lang['reports_menu']="Raporlar";
$lang['usermanagment_menu']="Kullanıcı Yönetimi";
$lang['add_menu']="Ekle";
$lang['delete_menu']="Sil";
$lang['edit_menu']="Düzenle";
$lang['addnew_menu']="Yeni Kullanıcı Ekle";
$lang['userlist_menu']="Locus Kullanıcı Listesi";
$lang['edituser_menu']="Kullanıcıyı Düzenle";
$lang['id_menu']="Id";
$lang['name_menu']="İsim";
$lang['email_menu']="Email";
$lang['mobile_menu']="Telefon";
$lang['role_menu']="Role";
$lang['actions_menu']="İşlemler";
$lang['user_menu']="Kullanıcı";
$lang['enteruserdetail_menu']="Kullanıcı Bilgisini Girin";
$lang['fullname_menu']="Ad Soyad";
$lang['emailaddress_menu']="Email Adresi";
$lang['password_menu']="Şifre";
$lang['confirmpassword_menu']="Şifreyi doğrula";
$lang['mobilenumber_menu']="Telefon";
$lang['role_menu']="Rol";
$lang['selectrole_menu']="Rol ";
$lang['submit_menu']="Ekle";
$lang['reset_menu']="Temizle";
$lang['userspermonth_menu']="Aylık bazda kullanıcı istatistiği";
$lang['textmessagespermonth_menu']="Aylık gönderilen mesaj istatistiği";
$lang['downloadsanduploadsinmbs_menu']="Downloads ve Uploads (MB)'";
$lang['piechartforuserspermonth_menu']="Lokasyonlara göre kullanıcı istatistiği";
$lang['totalusersperlocation_menu']="Kayıtlı kullanıcı";
//$lang['controlpanel_menu']="Kontrol Paneli";
$lang['signups_menu']="Kayıtlı Kullanıcı";
$lang['totaluniqueusers_menu']="Tekil kullanıcı";
$lang['totalsmssent_menu']="Gönderilen Mesaj ";
$lang['currentdownloadsuploads_menu']="Downloads/Uploads";
$lang['wifivisitorslist_menu']="Wifi Kullanıcıları";
//this is for Reports
$lang['username_menu']="Kullanıcı Adı";
$lang['userstats_menu']="Kullanıcı İstatistiği";
$lang['startdate_menu']="Giriş Zamanı";
$lang['enddate_menu']="Çıkış Zamanı";
$lang['downloads_menu']="Upload";
$lang['uploads_menu']="Download";
$lang['sessiontime_menu']="Toplam Oturum Süresi";
//this part is for instant message
$lang['bulkmessages_menu']="Intelligent SMS";
$lang['instantmessages_menu']="Micro Lokasyon Bazlı Kampanya";
$lang['selectcustomer_menu']="Müşteri";
$lang['selectlocation_menu']="Lokasyon";
$lang['filterresults_menu']="Filtrele";
$lang['fullname_menu']="Ad Soyad";
$lang['phonenumber_menu']="Telefon";
$lang['lastlogin_menu']="Son Kayıt Zamanı";
$lang['instantmessagesection_menu']="Micro Lokasyon Bazlı Kampanya Kontrolü";
$lang['nameforInstantmessagecampaign_menu']="Kampanya Adı";
$lang['selectpermissionorblacklist_menu']="İzinli Listesi ya da Yasaklı Listesi Seçimi";
$lang['selectfilters_menu']="Filtrele";
$lang['uploadpermissionlist_menu']="İzinli Listesi Yükle";
$lang['format_menu']="Format";
$lang['permissionlist_menu']="İzinli";
$lang['uploadblacklist_menu']="Yasaklı Listesi Yükle";
$lang['blacklist_menu']="Yasaklı Liste";
$lang['stopsendingmessage_menu']="Kampanyayı Durdur";
$lang['continuetosetup_menu']="Kampanya Oluştur";

$lang['bulkmessagesection_menu']="Intelligent SMS Kontrolü";
$lang['checkphonenumber_menu']="Intelligent Message Section";
$lang['sendbulkmessages_menu']="Intelligent SMS Gönder";
$lang['usingsmsgwformessageswithusername_menu']="SMS hizmeti kullanan müşteri:";
//for customermanagement_menu
$lang['customerid_menu']="Müşteri ID";
$lang['customermanagement_menu']="Müşteriler";
$lang['customerlist_menu']="Müşteri Listesi";
$lang['customername_menu']="Müşteri İsmi";
$lang['customeruserrname_menu']="Müşteri Kullanıcı İsmi";
$lang['companyemail_menu']="Email";
$lang['contactperson_menu']="Müşteri Temsilcisi";
//for location
$lang['locationmanagement_menu']="Lokasyon Yönetimi";
$lang['addnewlocation_menu']="Yeni Lokasyon Ekle";
$lang['location_list']="Lokasyon Listesi";
$lang['locationid_menu']="Lokasyon ID";
$lang['locationname_menu']="Lokasyon Adı";
$lang['deviceid_menu']="Cihaz Id";
$lang['address_menu']="Adres";
$lang['enterlocationdetail_menu']="Lokasyon Detayı";
//for branches
$lang['accesspointmanagement_menu']="Micro Lokasyonlar";
$lang['addnewlocation_menu']="Yeni Lokasyon Ekle";
$lang['branchid_menu']="Cihaz Id";
$lang['branchname_menu']="Cihaz Adı";
$lang['accesspointname_menu']="Micro Lokasyon adı";
$lang['description']="Detay";
$lang['branchip_menu']="Wifi IP";
$lang['addnewaccesspoint_menu']="Yeni Micro Lokasyon Ekle";
$lang['accesspointlist_menu']="Micro Lokasyon Listesi";
//$lang['address_menu']="Adres";
$lang['selectoption_menu']="Tarih ";
$lang['showresultsfor_menu']="Sonuçlarını Göster";
$lang['graphrepresentationforwifiusers_menu']="Wifi Kullanıcı Durumu";
$lang['graphrepresentationforsentmessages_menu']="Gönderilen Mesaj Durumu";
$lang['time_interval']="Zaman Aralığı";
$lang['today_menu']="Bugün";
$lang['lastweek_menu']="Bu Hafta";
$lang['lastmonth_menu']="Bu Ay";
$lang['showresults_menu']="Sonuçları Göster";
$lang['numberofwifiusers_menu']="Wifi kullanıcıları";
$lang['numberoftextmessages_menu']="Gönderilen Mesaj";
$lang['totalusersperlocation_menu']="Toplam Kullanıcı";
$lang['numberofuserspercustomer_menu']="Müşteri Bazlı Kullanıcı Sayısı";
//for header
$lang['locationlist_menu']="Lokasyon Yönetimi";
$lang['accesspoint_menu']="Micro Lokasyonlar";
$lang['branches_menu']="Dallar";
$lang['home_menu']="Ana Sayfa";
$lang['entercustomerdetail_menu']="Müşteri Detayını Girin";
$lang['addnewcustomer_menu']="Yeni Müşteri Ekle";
$lang['firmemailaddress_menu']="Firma eposta adresi";
$lang['city_menu']="Şehir";
$lang['firmdescription_menu']="Firma Açıklaması";
$lang['firstname_menu']="Ad";
$lang['surname_menu']="Soyad";
$lang['setnewpassword_menu']="Yeni Şifre Belirle";
$lang['enterdetails_menu']="Detayları Girin";
$lang['newpassword_menu']="Yeni Şifre";
$lang['confirmnewpassword_menu']="Yeni şifreyi onayla";
$lang['oldpassword_menu']="Eski şifre";
//for Branch Edit
$lang['branchmanagement_menu']="Şube Yönetimi";
$lang['editbranch_menu']="Şube Düzenle";
$lang['enterbranchdetails_menu']="Şube Detaylarını Düzenle";
$lang['accesspointip_menu']="Erişim noktası IP";
$lang['details_menu']="Ayrıntılar";
//new words added for instant messages
//new words added for the page instant messages
$lang['campaignid_menu']="Kampanya Kimliği";
$lang['campaignname_menu']="Kampanya Adı";
$lang['messagerepeat_menu']="Mesaj tekrarı";
$lang['smscount_menu']="MesajSayısı";
$lang['status_menu']="Durum";
$lang['operations_menu']="Operasyonlar";

$lang['everytime_menu']="Her Giriş";
$lang['inevery24hours_menu']="Her 24 Saatte";
$lang['manualrepeattime_menu']="Manuel Tekrar Süresi";
$lang['selectrepeattime_menu']="Tekrarlama Zamanı";
$lang['savechanges_menu']="Değişiklikleri Kaydet";
$lang['setenddateandtime_menu']="Bitiş Tarihi Ve Saatini Ayarla";
$lang['setenddateandtime_menu']="bitiş tarihi ve saati";
$lang['edittext_menu']="Metni düzenle";
$lang['settingenddateforcampaign_menu']="Kampanya için Bitiş Tarihini Ayarlama";
$lang['messagetext_menu']="Message Text";
$lang['close_menu']="Kapat";
$lang['setrepeatmessage_menu']="Not Set";
$lang['startdate2_menu']="Başlangıç Zamanı";
$lang['enddate2_menu']="Bitiş Zamanı";

$lang['editlocation_menu']="Konumunu düzenle";
$lang['addremovepblist_menu']="Add/Remove";
$lang['addremovepblisttitle_menu']="Permission List and Blacklist";
$lang['intelligentsms_menu']="Intelligent SMS";
$lang['editlocation_menu']="Lokasyonu düzenle";
$lang['addremovepblist_menu']="İzinli - Yasaklı Liste";
$lang['addremovepblisttitle_menu']="İzinli Liste ve Yasaklı Liste Kontrolü";
$lang['show_menu']="Göster";
$lang['addnumber_menu']="Numara Ekle";
$lang['lastseen_menu']="Son Görülme";
$lang['selectdate_menu']="Tarih Seç";
//SMSLimit 
$lang['smsLimit_menu']="Paket Durumu";
$lang['Customer_Name']="Müşteri Adı";
$lang['Packet_Limit']="Paket Limiti";
$lang['Send_Messages']="Kullanım Miktarı";
$lang['SMS_Remaining']="Kalan Gönderim Limiti";

?>

