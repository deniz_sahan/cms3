<?php
//header translation
$lang['executivedashboard_menu']="Executive Dashboard";
$lang['dashboardperlocation_menu']="Location Stats";
$lang['dashboard_menu']="Dashboard";
$lang['campaignlist']="Campaign List";
$lang['per_menu']="per";
$lang['locations_menu']="Locations";
$lang['messages_menu']="Campaign Management ";
$lang['customers_menu']="Customers";
$lang['wifivisitors_menu']="Wifi Visitors";
$lang['users_menu']="User Management";
$lang['reports_menu']="Reports";
$lang['usermanagment_menu']="User Management";
$lang['add_menu']="Add";
$lang['delete_menu']="Delete";
$lang['edit_menu']="Edit";
$lang['addnew_menu']="Add New User";
$lang['userlist_menu']="Users List";
$lang['edituser_menu']="Edit User";
$lang['id_menu']="Id";
$lang['name_menu']="Name";
$lang['email_menu']="Email";
$lang['mobile_menu']="Mobile";
$lang['role_menu']="Role";
$lang['actions_menu']="Actions";
$lang['user_menu']="User";
$lang['enteruserdetail_menu']="Enter User Details";
$lang['fullname_menu']="Full Name";
$lang['emailaddress_menu']="Email Address";
$lang['password_menu']="Password";
$lang['confirmpassword_menu']="Confirm Password";
$lang['mobilenumber_menu']="Mobile Number";
$lang['role_menu']="Role";
$lang['selectrole_menu']="Select Role";
$lang['submit_menu']="Submit";
$lang['reset_menu']="Reset";
$lang['userspermonth_menu']="Users Per Month";
$lang['textmessagespermonth_menu']="Text Messages Per Month";
$lang['downloadsanduploadsinmbs_menu']="Downloads and Uploads In MB'S";
$lang['piechartforuserspermonth_menu']="Pie Chart For Users Per Month";
$lang['totalusersperlocation_menu']="Total Users Per Location";
$lang['controlpanel_menu']="Control Panel";
$lang['signups_menu']="Sign Ups";
$lang['totaluniqueusers_menu']="Total Unique Users";
$lang['totalsmssent_menu']="Total SMS Sent";
$lang['currentdownloadsuploads_menu']="Current Downloads/Uploads";
$lang['wifivisitorslist_menu']="Wifi Visitors";
//this is for Reports
$lang['username_menu']="User Name";
$lang['userstats_menu']="User Stats";
$lang['startdate_menu']="Start Date";
$lang['enddate_menu']="End Date";
$lang['downloads_menu']="Uploads";
$lang['uploads_menu']="Downloads";
$lang['sessiontime_menu']="Total Session Time";
//this part is for instant message
$lang['instantmessages_menu']="Micro Location Based Campaign ";
$lang['bulkmessages_menu']="Intelligent SMS";
$lang['selectcustomer_menu']="Select Customer";
$lang['selectlocation_menu']="Select Location";

$lang['selectcustomer_menu']="Customer";
$lang['selectlocation_menu']="Location";
$lang['filterresults_menu']="Filter Results";
$lang['fullname_menu']="Full Name";
$lang['phonenumber_menu']="Phone Number";
$lang['lastlogin_menu']="Last Login";
$lang['phonnumber_menu']="Phone Number";
$lang['instantmessagesection_menu']="Micro Location SMS Control";
$lang['nameforInstantmessagecampaign_menu']="Campaign Name";
$lang['selectpermissionorblacklist_menu']="Select Permission or Black List_menu";
$lang['selectfilters_menu']="Select Filters";
$lang['uploadpermissionlist_menu']="Upload Permission List";
$lang['format_menu']="Format";
$lang['permissionlist_menu']="Permission";
$lang['uploadblacklist_menu']="Upload Black List";
$lang['blacklist_menu']="Black List";
$lang['stopsendingmessage_menu']="Stop Campaign";
$lang['continuetosetup_menu']="Create Campaign";
$lang['bulkmessagesection_menu']="Intelligent SMS Section";

$lang['bulkmessagesection_menu']="Intelligent SMS Section";
$lang['checkphonenumber_menu']="Intelligent Message Section";
$lang['sendbulkmessages_menu']="Send Intelligent SMS";
$lang['usingsmsgwformessageswithusername_menu']="Using SmsGW for Messages with username";
//for customermanagement_menu
$lang['customerid_menu']="Customer ID";
$lang['customermanagement_menu']="Customers";
$lang['customerlist_menu']="Customer List";
$lang['customername_menu']="Customer Name";
$lang['customeruserrname_menu']="Customer UserName";
$lang['companyemail_menu']="Company Email";
$lang['contactperson_menu']="Contact Person";
//for location
$lang['locationmanagement_menu']="Location Management";
$lang['addnewlocation_menu']="Add New Location";
$lang['location_list']="Location List";
$lang['locationid_menu']="Location ID";
$lang['locationname_menu']="Location Name";
$lang['deviceid_menu']="Device Id";
$lang['address_menu']="Address";
$lang['enterlocationdetail_menu']="Enter Location Detail";
//for branches
$lang['accesspointmanagement_menu']="Micro Locations";
$lang['addnewlocation_menu']="Add New Location";
$lang['branchid_menu']="Device Id";
$lang['branchname_menu']="Device Name";
$lang['accesspointname_menu']="Micro Location Name";
$lang['addnewaccesspoint_menu']="Add New Micro Location";
$lang['description']="Description";
$lang['branchip_menu']="Device IP";
$lang['accesspointlist_menu']="Micro Location List";
//Dashboard Per Location
$lang['selectoption_menu']="Select Option";
$lang['showresultsfor_menu']="Show Results For";
$lang['graphrepresentationforwifiusers_menu']="Graph Representation For Wifiusers";
$lang['graphrepresentationforsentmessages_menu']="Graph Representation For Sent Messages";
$lang['time_interval']="Time Interval";
$lang['today_menu']="Today";
$lang['lastweek_menu']="This Week";
$lang['lastmonth_menu']="This Month";
$lang['showresults_menu']="Show Results";
$lang['numberofwifiusers_menu']="Number Of Wifi Users";
$lang['numberoftextmessages_menu']="Number Of Messages";
$lang['totalusersperlocation_menu']="Total Users Per Location";
$lang['numberofuserspercustomer_menu']="Number Of Users Per Customer";
//$lang['address_menu']="Address";
$lang['locationlist_menu']="Location Management";
$lang['accesspoint_menu']="Micro Locations";
$lang['branches_menu']="Branches";
//for add new customer
$lang['home_menu']="Home";
$lang['entercustomerdetail_menu']="Enter Customer Detail";
$lang['addnewcustomer_menu']="Add New Customer";
$lang['firmemailaddress_menu']="Firm Email Address";
$lang['city_menu']="City";
$lang['firmdescription_menu']="Firm Description";
$lang['firstname_menu']="First Name";
$lang['surname_menu']="Surname";
$lang['setnewpassword_menu']="Set New Password";
$lang['enterdetails_menu']="Enter Details";
$lang['newpassword_menu']="New Password";
$lang['confirmnewpassword_menu']="Confirm New Password";
$lang['oldpassword_menu']="Old Password";
//for Branch Edit
$lang['branchmanagement_menu']="Branch Management";
$lang['editbranch_menu']="Edit Branch";
$lang['enterbranchdetails_menu']="Edit Branch Details";
$lang['accesspointip_menu']="Access Point IP";
$lang['details_menu']="Details";
//new words added for the page instant messages
$lang['campaignid_menu']="Campaign ID";
$lang['campaignname_menu']="Campaign Name";
$lang['repeatmessage_menu']="Message Repeat ";
$lang['smscount_menu']="Message Count";
$lang['status_menu']="Status";
$lang['operations_menu']="Operations";

$lang['everytime_menu']="Each Login";
$lang['inevery24hours_menu']="In Every 24 Hours";
$lang['manualrepeattime_menu']="Manual Repeat Time";
$lang['selectrepeattime_menu']="Repeat Time";
$lang['savechanges_menu']="Save Changes";
$lang['setenddateandtime_menu']="Set End Date And Time";
$lang['setenddateandtime_menu']="Set End Date And Time";
$lang['edittext_menu']="Edit Text";
$lang['settingenddateforcampaign_menu']="Setting End Date For Campaign";
$lang['messagetext_menu']="Message Text";
$lang['close_menu']="Close";
$lang['messagerepeat_menu']="Message Repeat";
$lang['setrepeatmessage_menu']="Not Set";
$lang['startdate2_menu']="Start Date";
$lang['enddate2_menu']="End Date";
$lang['editlocation_menu']="Edit Location";
$lang['addremovepblist_menu']="Permission - Black List";
$lang['addremovepblisttitle_menu']="Permission List and Black list";
$lang['intelligentsms_menu']="Intelligent SMS";
$lang['show_menu']="Show";
$lang['addnumber_menu']="Add Number";
$lang['lastseen_menu']="Last Seen";
$lang['selectdate_menu']="Select Date";
//SMSLimit 
$lang['smsLimit_menu']="Package Status";
$lang['Customer_Name']="Customer Name";
$lang['Packet_Limit']="Packet Limit";
$lang['Send_Messages']="Amount of Use ";
$lang['SMS_Remaining']="Remaining Sending Limit";




?>

