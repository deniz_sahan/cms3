<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class stats_model extends CI_Model {


	/**
	 * This function is used to get the statistics of users activity
	 */
	function getCountUsersStats( $searchText = '', $customerid = '', $locationid = '' ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate, MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "logindate", "desc" );
		$this->db->group_by( "username" );
		$query = $this->db->get();
		return $query->num_rows();
	}
	/**
	 * This function is used to get the Reports by Joining two tables for filter 
	 */
	function getUsersStats( $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate, MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "logindate", "desc" );
		$this->db->group_by( "username" );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();

		return $result;

	}

	/**  
	For Daily Reports
	*/
	function getDailyReports( $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'acctstarttime  >', date( 'Y-m-d 00:00:00' ) );
			$this->db->where( 'acctstarttime  <', date( 'Y-m-d 23:59:59' ) );
			//$this->db->where('acctstarttime BETWEEN DATE_SUB(NOW() , INTERVAL 14 HOUR) AND NOW()');		
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->where( 'acctstarttime  >', date( 'Y-m-d 00:00:00' ) );
			$this->db->where( 'acctstarttime  <', date( 'Y-m-d 23:59:59' ) );
			//$this->db->where('acctstarttime DATE(NOW() , INTERVAL 14 HOUR) AND NOW()');	
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "acctstarttime", "desc" );
		$this->db->group_by( "username" );
		//  $this->db->group_by('HOUR(acctstarttime)');
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		//pre($result);

		return $result;

	}
	/**  
	End of Daily Reports
	*/
	/**  
	
	For Daily Reports Counts
	*/

	function getCountDailyReports( $searchText = '', $customerid = '', $locationid = '' ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );
		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'acctstarttime  >', date( 'Y-m-d 00:00:00' ) );
			$this->db->where( 'acctstarttime  <', date( 'Y-m-d 23:59:59' ) );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'acctstarttime  >', date( 'Y-m-d 00:00:00' ) );
			$this->db->where( 'acctstarttime  <', date( 'Y-m-d 23:59:59' ) );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		//  $this->db->order_by("logindate", "desc");
		$this->db->order_by( "acctstarttime", "desc" );
		$this->db->group_by( "username" );
		$query = $this->db->get();
		return $query->num_rows();
	}

	/**  
	End of Daily Reports Counts
	*/

	/*********** 
	For Weekly Reports
	*/
	function getWeeklyReports( $monday, $sunday, $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			/* Queries added by AQ  we need to make another codition on monday that if the day is monday show current data*/
			$this->db->where( 'acctstarttime >', $monday );
			$this->db->where( 'acctstarttime <', $sunday );
			/* Queries added by AQ*/
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->where( 'acctstarttime >', $monday );
			$this->db->where( 'acctstarttime <', $sunday );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "acctstarttime", "desc" );
		$this->db->group_by( "username" );
		//  $this->db->group_by('DAYOFMONTH(acctstarttime)');
		//  $this->db->group_by('HOUR(acctstarttime)');
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		//pre($result);

		return $result;

	}
	/**  
	End Weekly Reports
	*/

	/*********** 
	For Weekly Count Reports
	*/
	function getCountWeeklyReports( $monday, $sunday, $searchText = '', $customerid = '', $locationid = '' ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			/* Queries added by AQ  we need to make another codition on monday that if the day is monday show current data*/
			$this->db->where( 'acctstarttime >', $monday );
			$this->db->where( 'acctstarttime <', $sunday );
			/* Queries added by AQ*/
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->where( 'acctstarttime >', $monday );
			$this->db->where( 'acctstarttime <', $sunday );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "acctstarttime", "desc" );
		$this->db->group_by( "username" );
		//  $this->db->group_by('HOUR(acctstarttime)');
		$query = $this->db->get();
		$result = $query->result();
		//pre($result);

		return $query->num_rows();

	}
	/**  
	End Weekly count Reports
	*/

	/*********** 
	For Monthly Reports
	*/
	function getMonthlyReports( $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );

		$this->db->from( 'radacct' );
		$day = date( 'd' ) - 1;
		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->where( 'acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->group_by( 'acctstarttime', "desc" );
		$this->db->order_by( "username" );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();


		return $result;

	}
	/**  
	End Monthly Reports
	*/
	/*******************
	For Count Monthly Reports
	*/
	function getCountMonthlyReports( $searchText = '', $customerid = '', $locationid = '' ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );
		$this->db->from( 'radacct' );
		$day = date( 'd' ) - 1;
		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->where( 'acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			// if($searchText==username)
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->group_by( 'acctstarttime', "desc" );
		$this->db->order_by( "username" );
		$query = $this->db->get();
		$result = $query->result();
		return $query->num_rows();
	}
	/**  
	End Count Monthly Reports
	*/
	/************** Reports Between selected dates
	 *   This function is used to get the date between two dates count
	 */
	function getCountsBetweenDatesReports( $searchText = '', $customerid = '', $locationid = '', $sDate, $eDate ) {
		//$startDate=date("Y m d",strtotime($startDate)).' 00:00'; //00:00 start day time
		//$endDate=date("Y m d",strtotime($endDate)).' 23:59'; //23:59 end day time
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );
		$this->db->from( 'radacct' );
		$day = date( 'd' ) - 1;
		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			//$this->db->where('acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' ); 
			// $this->db->where('acctstarttime >=',$sDate);
			// $this->db->where('acctstarttime <=',$eDate);	
			$this->db->where( 'acctstarttime BETWEEN "' . $sDate . '" and "' . $eDate . '"' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			//$this->db->where('acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' );  
			$this->db->where( 'acctstarttime >=', $sDate );
			$this->db->where( 'acctstarttime <=', $eDate );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->group_by( 'acctstarttime', "desc" );
		$this->db->order_by( "acctstarttime" );
		$query = $this->db->get();
		$result = $query->result();
		return $query->num_rows();
	}
	/************** Reports Between selected dates
	 *   This function is used to get the date between two dates
	 */
	function getBetweenDatesReports( $searchText = '', $customerid = '', $locationid = '', $sDate, $eDate, $page, $segment ) {
		//$startDate=date("Y m d",strtotime($startDate)).' 00:00'; //00:00 start day time
		//$endDate=date("Y m d",strtotime($endDate)).' 23:59'; //23:59 end day time
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate,MAX(radacct.acctupdatetime) AS lastseen' );
		$this->db->from( 'radacct' );
		$day = date( 'd' ) - 1;
		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			//$this->db->where('acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' ); 
			// $this->db->where('acctstarttime >=',$sDate);
			// $this->db->where('acctstarttime <=',$eDate);	
			$this->db->where( 'acctstarttime BETWEEN "' . $sDate . '" and "' . $eDate . '"' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			//$this->db->where('acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' );  
			$this->db->where( 'acctstarttime >=', $sDate );
			$this->db->where( 'acctstarttime <=', $eDate );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username LIKE '%" . $searchText . "%' OR callingstationid LIKE '%" . $searchText . "%' OR calledstationid LIKE '%" . $searchText . "%' )";
			$this->db->where( $likeCriteria );
		}
		$this->db->group_by( 'acctstarttime', "desc" );
		$this->db->order_by( "acctstarttime" );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		//pre($sDate);
		//pre($result);
		return $result;
	}

	/** End Reports Between selected dates*******************
	 */






	/** Weekly Wifi Users
	 * This function is used to get barcharts weekly data
	 */
	function barchartsweek( $monday, $sunday, $customerid, $locationid ) {
		$this->db->select( 'COUNT(username) AS `count`,lastlogin as day,username' );
		$this->db->from( 'tbl_userprofiles' );
		//        $this->db->where('lastlogin >=', date('Y-m-d', strtotime('-1 week')));
		/* Queries added by AQ  we need to make another codition on monday that if the day is monday show current data*/
		$this->db->where( 'lastlogin >', $monday );
		$this->db->where( 'lastlogin <', $sunday );
		/* Queries added by AQ*/
		$this->db->where( 'customerid', $customerid );
		$this->db->where( 'locationid', $locationid );
		$this->db->group_by( 'DAYOFMONTH(lastlogin)' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/** Daily Wifi User 
	 * This function is used to get barcharts Daily data
	 */
	function barchartsdaily( $day, $customerid, $locationid ) {
		$this->db->select( 'COUNT(username) AS `count`,lastlogin as hour' );
		$this->db->from( 'tbl_userprofiles' );
		$this->db->where( 'lastlogin >', date( 'Y-m-d 06:00:00' ) );
		$this->db->where( 'lastlogin <', date( 'Y-m-d 23:59:59' ) );
		$this->db->where( 'customerid', $customerid );
		$this->db->where( 'locationid', $locationid );
		$this->db->group_by( 'HOUR(lastlogin)' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/** Monthly wifi Users 
	 * This function is used to get barcharts Monthly Users data for executive dashboard
	 */
	function barchartsmonthly( $month, $customerid, $locationid ) {
		$status = 'A';
		$this->db->select( 'COUNT(username) AS `count`,lastlogin as month,username' );
		$this->db->from( 'tbl_userprofiles' );
		//for Monthly bug fixed Abdul Qadir
		$day = date( 'd' ) - 1;
		//echo date('d');
		//this is for interval data
		//$this->db->where('lastlogin >=DATE_SUB(CURDATE(), INTERVAL 30 DAY)' );
		$this->db->where( 'lastlogin >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
		/** this is one for getting the this month value**/
		//$this->db->where('lastlogin >=DATE_SUB(NOW() ,  INTERVAL 1 MONTH)' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->group_by( 'DAYOFMONTH(lastlogin)' );
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}
	/** Monthly Sent messages Developer Abdul Qadir
	 * This function is used to get barcharts sent messages monthly text messages
	 */

	function barchartstextmonthly( $month, $customerid, $locationid ) {
		$this->db->select( 'COUNT(username) AS `count`,date as month,username' );
		$this->db->from( 'sentmessages' );

		$day = date( 'd' ) - 1;
		// $this->db->where('date >=', date('Y-m-d', strtotime('-1 month')));
		//code by Abdul Qadir
		$this->db->where( 'date >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
		//code by Abdul Qadir
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->group_by( 'DAYOFMONTH(date)' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	//model to get yearly data from database	
	function barchartsyearly( $month, $customerid, $locationid ) {
		$status = 'A';
		$this->db->select( 'COUNT(username) AS `count`,lastlogin ,lastlogin as month' );
		$this->db->from( 'tbl_userprofiles' );
		$mm = date( 'm' ) - 1;
		//$this->db->where('lastlogin >=DATE_SUB(CURDATE(), INTERVAL 30 DAY)' );
		$this->db->where( 'lastlogin >=DATE_SUB(NOW(), INTERVAL ' . $mm . ' MONTH)' );
		/** this is one for getting the this month value**/
		//$this->db->where('lastlogin >=DATE_SUB(NOW() ,  INTERVAL 1 MONTH)' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->group_by( 'MONTH(lastlogin)' );
		$query = $this->db->get();
		$result = $query->result();
		//pre($result);
		return $result;
	}
	//model to get yearly messages 
	//model to get yearly data from database	
	function barchartstextyearly( $month, $customerid, $locationid ) {
		$status = 'A';
		$this->db->select( 'COUNT(username) AS `count`,date ,date as month' );
		$this->db->from( 'sentmessages' );
		$mm = date( 'm' ) - 1;
		//$this->db->where('lastlogin >=DATE_SUB(CURDATE(), INTERVAL 30 DAY)' );
		$this->db->where( 'date >=DATE_SUB(NOW(), INTERVAL ' . $mm . ' MONTH)' );
		/** this is one for getting the this month value**/
		//$this->db->where('lastlogin >=DATE_SUB(NOW() ,  INTERVAL 1 MONTH)' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->group_by( 'MONTH(date)' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}




	/** Daily Sent Messages Developer Abdul Qadir
	 *  This function is used to get barcharts sent daily text messages
	 */
	function barchartstextdaily( $day, $customerid, $locationid ) {
		/* CODE EDITED HERE BY ABDUL QADIR  */
		$this->db->select( 'COUNT(username) AS `count`,date as hour' );
		$this->db->from( 'sentmessages' );
		/* BY MANSOOR*/
		//$this->db->where('date >=', date('d', strtotime('-1 month')));
		/*Code by AQ*/
		$this->db->where( 'date >', date( 'Y-m-d 06:00:00' ) );
		$this->db->where( 'date <', date( 'Y-m-d 23:59:59' ) );
		$this->db->where( 'customerid', $customerid );
		$this->db->where( 'locationid', $locationid );

		$this->db->group_by( 'HOUR(date)' );
		/*Code by AQ*/


		$query = $this->db->get();
		$result = $query->result();
		//pre($result);
		//exit;		
		return $result;

	}

	/** Weekly Sent Messages Developer Abdul Qadir
	 * This function is used to get barcharts sent messages weekly data
	 */
	function barchartsweektext( $monday, $sunday, $customerid, $locationid ) {
		$this->db->select( 'COUNT(username) AS `count`,date as day,username' );
		$this->db->from( 'sentmessages' );
		//        $this->db->where('lastlogin >=', date('Y-m-d', strtotime('-1 week')));
		/* Queries added by AQ  we need to make another codition on monday that if the day is monday show current data*/
		$this->db->where( 'date >', $monday );
		$this->db->where( 'date <', $sunday );
		/* Queries added by AQ*/
		$this->db->where( 'customerid', $customerid );
		$this->db->where( 'locationid', $locationid );
		/* Queries added by AQ*/
		//  $this->db->where('customerid',$customerid);
		// $this->db->where('locationid',$locationid);
		$this->db->group_by( 'DAYOFMONTH(date)' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/* This function is used for getting all the customers
	 */
	function getallcustomers() {
		$this->db->select( '*' );
		$this->db->from( 'tbl_customer' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}



	////for excel export
	function get_users( $customerid, $locationid, $searchText ) {
		$this->db->select( '*,SUM(radacct.acctinputoctets) AS downloads,SUM(radacct.acctoutputoctets) AS uploads,SUM(radacct.acctsessiontime) AS duration,MAX(radacct.acctstarttime) AS logindate, MAX(radacct.acctupdatetime) AS lastseen' );
		$this->db->from( 'radacct' );

		if ( !empty( $customerid ) && !empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );
			$this->db->where( 'tbl_location.locationid', $locationid );

		}
		if ( !empty( $customerid ) && empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );
			$this->db->where( 'tbl_location.customerid', $customerid );

		}
		if ( empty( $customerid ) && empty( $locationid ) ) {
			$this->db->join( 'tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid', 'left' );

		}
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$this->db->order_by( "logindate", "desc" );
		$this->db->group_by( "username" );

		$query = $this->db->get();

		return $query->result_array();

	}

	function addNewSmsLimitmodel( $pakageInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_smslimit', $pakageInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
	// to get sms Limit of the user
	function sentSms( $customerid ) {
		$this->db->select( 'customerid, sent' );
		$this->db->from( 'sentmessages' );
		$this->db->where( 'customerid', $customerid );
		$query = $this->db->get();

		return $query->result();
		//pre($query->result);
	}

	function smsLimit( $customerid ) {
		$this->db->select( 'customerid, slid, smsused , smsremain , customerid, userid, date , smsplan' );
		$this->db->from( 'tbl_smslimit' );
		$this->db->where( 'customerid', $customerid );
		$query = $this->db->get();

		return $query->result();
		//pre($query->result);
	}

	function getTransaction() {
		$this->db->select( '*' );
		$this->db->from( 'tbl_smslimit' );
		$this->db->join( 'tbl_customer', 'tbl_smslimit.customerid=tbl_customer.customerid' );
		//$this->db->where('customerid', $customerid);
		$query = $this->db->get();
		// pre($query->result());
		//exit;       
		return $query->result();
		//pre($query->result);
	}

	function storeSms( $customerid, $val1, $val2 ) {
		$this->db->set( 'smsused', $val1 );
		$this->db->set( 'smsremain', $val2 );
		$this->db->where( 'customerid', $customerid );
		$this->db->update( 'tbl_smslimit' );
		return true;
		// return $query->result();
		//pre($query->result);
	}

	function deletetrans( $id ) {


		$this->db->select( 'slid, customerid' );
		$this->db->where( 'slid', $id );
		$this->db->delete( 'tbl_smslimit' );
		// return $query->result();
		//pre($query->result);
		return true;
	}

	function edittrans( $id, $pakageInfo ) {
		$this->db->where( 'slid', $id );
		$this->db->update( 'tbl_smslimit', $id );

		return TRUE;
	}

	function package_exists( $key ) {
		$this->db->select( 'customerid' );
		$this->db->from( 'tbl_smslimit' );
		$this->db->where( 'customerid', $key );
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	function getTransactionByid( $id ) {
		$this->db->select( 'customerid, slid, smsused , smsremain , userid, date , smsplan' );
		$this->db->from( 'tbl_smslimit' );
		$this->db->where( 'slid', $id );
		$query = $this->db->get();
		// pre($query->result());
		// pre($query->result());
		return $query->result();
		//pre($query->result);
	}

	function updatetransbyid( $customerid, $val1 ) {

		$this->db->where( 'slid', $customerid );
		$this->db->update( 'tbl_smslimit', $val1 );
		return true;
		// return $query->result();
		//pre($query->result);
	}
	//to get all message from cutomers
	function getsms() {
		$this->db->select( 'smsplan' );
		$this->db->from( 'tbl_smslimit' );
		// $this->db->where('slid', $id);
		$query = $this->db->get();
		// pre($query->result());
		//pre($query->result());
		return $query->result();
		//pre($query->result);
	}
	/*
	 *check user status whether it is online or not 
	 */
	function checkuserstatsmodel( $username, $downloaddata ) {
		$this->db->select( 'radpostauth.authdate,SUM(radacct.acctinputoctets) AS downloads' );
		$this->db->from( 'radpostauth' );
		$this->db->join( 'radacct', 'radacct.username = radpostauth.username', 'left' );
		$this->db->where( 'radpostauth.username', $username );
		//$this->db->where('authdate >', $date);
		$this->db->order_by( "radpostauth.authdate", "desc" );
		$this->db->group_by( "radacct.username" );
		$query = $this->db->get();
		return $query->result();
	}
	/*
	 *check user status whether it is online or not 
	 */
	function checkuserdownloads( $username ) {
		$this->db->select( 'SUM(radacct.acctinputoctets) AS downloads' );
		$this->db->from( 'radacct' );
		$this->db->where( 'radacct.username', $username );
		//$this->db->where('authdate >', $date);
		$this->db->group_by( "radacct.username" );
		$query = $this->db->get();
		return $query->result();
	}

	function smsstatsmodel() {
		$this->db->select( 'COUNT(sent) AS `count` ,username, date' );
		$this->db->from( 'sentmessages' );
		// $this->db->where('date >=', date('Y-m-d', strtotime('-1 week')));

        $this->db->group_by('username');
        $query = $this->db->get();       
        $result = $query->result();  		
        return $result;
    }
	function dailysmsStats($searchText, $customerid,$locationid){
        $this->db->select('COUNT(sent) AS `count` ,username, date');        
        $this->db->from('sentmessages');
		$this->db->where('date >', date('Y-m-d'));
		if(!empty($customerid) && !empty($locationid)){
            $this->db->where('customerid',$customerid);
            $this->db->where('locationid',$locationid);
            
        }
        if(!empty($customerid) && empty($locationid)){
            $this->db->where('customerid',$customerid);            
        }
		 if(!empty($searchText)) {
           // if($searchText==username)
	        $likeCriteria = "(username LIKE '%".$searchText."%' )";
		$this->db->where($likeCriteria);
        }
        $this->db->group_by('username');
        $query = $this->db->get();       
        $result = $query->result(); 	
        return $result;
    }
		function weeklysmsStats($searchText, $monday, $sunday , $customerid, $locationid){
        $this->db->select('COUNT(sent) AS `count` ,username , date');        
        $this->db->from('sentmessages');
		$this->db->where('date >', $monday);
        $this->db->where('date <', $sunday);
		if(!empty($customerid) && !empty($locationid)){
            $this->db->where('customerid',$customerid);
            $this->db->where('locationid',$locationid);
            
        }
        if(!empty($customerid) && empty($locationid)){
            $this->db->where('customerid',$customerid);
            
        }
		if(!empty($searchText)) {
           // if($searchText==username)
	        $likeCriteria = "(username LIKE '%".$searchText."%' )";
		$this->db->where($likeCriteria);
        }
        $this->db->group_by('username');
        $query = $this->db->get();       
        $result = $query->result();  			
        return $result;
    }
		function monthlysmsStats($searchText,$customerid, $locationid){
		 $day=date('d')-1;
        $this->db->select('COUNT(sent) AS `count` ,username, date');        
        $this->db->from('sentmessages');
		 $this->db->where('date >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' );
		 if(!empty($customerid) && !empty($locationid)){
            $this->db->where('customerid',$customerid);
            $this->db->where('locationid',$locationid);
            
        }
        if(!empty($customerid) && empty($locationid)){
            $this->db->where('customerid',$customerid);
            
        }
		if(!empty($searchText)) {
           // if($searchText==username)
	        $likeCriteria = "(username LIKE '%".$searchText."%' )";
		$this->db->where($likeCriteria);
        }
        $this->db->group_by('username');
        $query = $this->db->get();       
        $result = $query->result();  		
        return $result;
    }
		function smsStatsBetweenDates($searchText,$customerid, $locationid  , $sDate , $eDate){
		 $day=date('d')-1;
        $this->db->select('COUNT(sent) AS `count` ,username, date');        
        $this->db->from('sentmessages');	
				 		 	 		 		 
		if(!empty($customerid) && !empty($locationid)){
		 $this->db->where('date BETWEEN "'. $sDate. '" and "'. $eDate.'"');
          $this->db->where('customerid',$customerid);
          $this->db->where('locationid',$locationid);            
        }
        if(!empty($customerid) && empty($locationid)){
		  $this->db->where('date >=',$sDate);
       $this->db->where('date <=',$eDate); 	
            $this->db->where('customerid',$customerid);
            
        }
		if(!empty($searchText)) {
           // if($searchText==username)
	        $likeCriteria = "(username LIKE '%".$searchText."%' )";
		$this->db->where($likeCriteria);
        }
        $this->db->group_by('username');
        $query = $this->db->get();       
        $result = $query->result(); 
//pre($result); 		
        return $result;
    }
	
	
}
