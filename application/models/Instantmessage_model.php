<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class instantmessage_model extends CI_Model {
	/**
	 * This function is used to get the user listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function getphonenumbersforinstantmessage( $searchText = '' ) {
		$this->db->select( 'sent.username' );
		$this->db->from( 'sentmessages as sent' );
		$this->db->where( 'sent.smscode', '1' );
		$sub_query = $this->db->get_compiled_select();

		//        $this->db->select('sent.username');
		//        $this->db->from('sentmessages as sent');
		//        $this->db->where('sent.smscode', '2');
		//        $sub_query2 = $this->db->get_compiled_select();

		$this->db->distinct();
		$this->db->select( 'l.ad, l.soyad,l.username,rpa.reply' );
		$this->db->from( 'log as l' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = l.username' );
		//$this->db->join('sentmessages as s', 's.username = r.username');
		$this->db->where( 'rpa.authdate >=', $searchText );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		$this->db->where( "l.username NOT IN ($sub_query)" );
		//$this->db->where("r.username NOT IN ($sub_query2)");
		//$this->db->where("r.username IN ($sub_query)");
		//$this->db->where('s.smscode !=', '1');
		//        $this->db->where('BaseTbl.roleId !=', 1);
		$this->db->group_by( 'l.username' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*
	 *This function is user for adding sent messages record via instant messages
	 *       */
	function insertNewSentMessage( $messageInfo ) {

		$this->db->trans_start();
		$this->db->insert( 'sentmessages', $messageInfo );
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();

		return $insert_id;

	}
	/*
	 *This function is user for adding campaing for instant messages
	 *       */
	function insertcampaigntbl( $messageInfo ) {

		$this->db->trans_start();
		$this->db->insert( 'tbl_instantmessage', $messageInfo );
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();

		return $insert_id;

	}
	/*
	 *This function is user for adding campaing for instant messages
	 *       */
	function insertpermisiionlist( $data ) {
		$this->db->select( 'b.username' );
		$this->db->from( 'tbl_blacklist as b' );
		$sub_query = $this->db->get_compiled_select();

		$this->db->insert_batch( 'tbl_permisionlist', $data );
		$this->db->where( "username NOT EXISTS ($sub_query)" );
		//    $this->db->trans_start();
		//    $this->db->insert('tbl_permisionlist', $data);
		$insert_id = $this->db->insert_id();
		//    $this->db->trans_complete();
		return $insert_id;

	}
	/*
	 *This function is user for adding campaing for instant messages
	 *       */
	function insertblactlist( $data ) {
		$this->db->insert_batch( 'tbl_blacklist', $data );
		//    $this->db->trans_start();
		//    $this->db->insert('tbl_blacklist', $data);
		$insert_id = $this->db->insert_id();
		//    $this->db->trans_complete();

		return $insert_id;

	}
	/*
	 *This function is user for adding csv users userprofiles
	 *       */
	function insertcvstouserprofile( $messageInfo ) {

		$this->db->trans_start();
		$this->db->insert( 'tbl_userprofiles', $messageInfo );
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();

		return $insert_id;

	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not getadvcampaignlist
	 */
	function getinstantmessagecampaingviaID( $searchText = '', $customerid = '', $page, $segment, $status ) {
		//$this->db->distinct();
		$this->db->select( 'inm.*,loc.locationname' );
		$this->db->from( 'tbl_instantmessage as inm' );
		$this->db->join( 'tbl_location as loc', 'loc.locationid = inm.locationid' );
		if ( !empty( $searchText ) ) {
			$this->db->like( 'LOWER(inm.inmname)', strtolower( $searchText ) );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'inm.customerid', $customerid );
		}
		if ( !empty( $status ) ) {
			if ( $status == "notactive" ) {
				$this->db->where( 'inm.status', 'not active' );
			} else {
				$this->db->where( 'inm.status', $status );
			}
		}

		$this->db->order_by( 'inm.inmid', 'desc' );
		// $this->db->where('inmid', $inmid); 
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not getadvcampaignlist count
	 */
	function getcountinstantmessagecampaing( $searchText = '', $customerid = '', $status ) {
		//$this->db->distinct();
		$this->db->select( 'inm.*,loc.locationname' );
		$this->db->from( 'tbl_instantmessage as inm' );
		$this->db->join( 'tbl_location as loc', 'loc.locationid = inm.locationid' );
		if ( !empty( $searchText ) ) {
			$this->db->like( 'LOWER(inm.inmname)', strtolower( $searchText ) );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'inm.customerid', $customerid );
		}
		if ( !empty( $status ) ) {
			if ( $status == "notactive" ) {
				$this->db->where( 'inm.status', 'not active' );
			} else {
				$this->db->where( 'inm.status', $status );
			}
		}
		$this->db->order_by( 'inm.inmid', 'desc' );
		// $this->db->where('inmid', $inmid); 
		$query = $this->db->get();

		return $query->num_rows();
	}
	/* This function will be used for getting all campaigns with advance 
	 */
	function getadvcampaignlistcount( $searchText = '', $customerid = '', $locationid = '', $startdate = '', $enddate = '', $statusactive = '' ) {
		//$this->db->distinct();
		$this->db->select( 'inm.*,loc.locationname' );
		$this->db->from( 'tbl_instantmessage as inm' );
		$this->db->join( 'tbl_location as loc', 'loc.locationid = inm.locationid' );
		if ( !empty( $searchText ) ) {
			$this->db->like( 'LOWER(inm.inmname)', strtolower( $searchText ) );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'inm.customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'inm.locationid', $locationid );
		}
		if ( !empty( $startdate ) ) {
			$this->db->where( 'inm.startdate >=', $startdate );
		}
		if ( !empty( $enddate ) ) {
			$this->db->where( 'inm.enddate <=', $enddate );
		}
		//$this->db->where('inm.status',$statusactive);
		$this->db->order_by( 'inm.inmid', 'desc' );
		$query = $this->db->get();

		return $query->num_rows();
	}
	/* This function will be used for getting all campaigns with advance 
	 */
	function getadvcampaignlist( $searchText = '', $customerid = '', $locationid = '', $startdate = '', $enddate = '', $page, $segment, $statusactive = '' ) {
		//$this->db->distinct();
		$this->db->select( 'inm.*,loc.locationname' );
		$this->db->from( 'tbl_instantmessage as inm' );
		$this->db->join( 'tbl_location as loc', 'loc.locationid = inm.locationid' );
		if ( !empty( $searchText ) ) {
			$this->db->like( 'LOWER(inm.inmname)', strtolower( $searchText ) );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'inm.customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'inm.locationid', $locationid );
		}
		if ( !empty( $startdate ) ) {
			$this->db->where( 'inm.startdate >=', $startdate );
		}
		if ( !empty( $enddate ) ) {
			$this->db->where( 'inm.enddate <=', $enddate );
		}
		//$this->db->where('inm.status',$statusactive);
		$this->db->order_by( 'inm.inmid', 'desc' );
		$this->db->limit( $page, $segment );
		// $this->db->where('inmid', $inmid);  
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/* This function will be used for setting message type for instant message 
	 */
	function setinstantmessagetype( $inmid, $messagetype ) {
		//echo $message;exit;
		$this->db->set( 'messagetype', $messagetype );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
		return TRUE;
	}
	/* This function will be used for setting message type for instant message setinstantmessagetype
	 */
	function setinstantmessagetypexhour( $inmid, $messagetype, $xhourdate ) {
		//echo $message;exit;
		$this->db->set( 'messagetype', $messagetype );
		$this->db->set( 'variabledatetype', $xhourdate );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
		return TRUE;
	}
	/* This function will be used for setting message type for instant message
	 */
	function setinstantenddate( $inmid, $enddate ) {
		//echo $message;exit;
		$this->db->set( 'enddate', $enddate );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
		return TRUE;
	}
	/* This function will be used for setting message type for instant message
	 */
	function setinstantstartdate( $inmid, $startdate ) {
		//echo $message;exit;
		$this->db->set( 'startdate', $startdate );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
		return TRUE;
	}
	/* This function will be used for setting messagestatus e for instant message
	 */
	function setinstantstatus( $inmid, $status ) {
		//echo $message;exit;
		$this->db->set( 'status', $status );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
		return TRUE;
	}

	function getDailymessageGraphModel() {
		//        $this->db->select('date_format(sent.date, "%Y-%m-%d") AS date,COUNT(sent.smsid) as total');
		//        $this->db->from('sentmessages as sent');
		//        $this->db->group_by('date');
		$this->db->select( 'DISTINCT DATE_FORMAT(`date`, "%Y-%m-%d") `date`, count(smsid) as total', FALSE )->from( 'sentmessages' )->group_by( 'DAY(date)' )->order_by( 'date', 'DESC' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function is used for getting all the campaigns with customer and location id
	 */
	function getallcampaigncusloc( $customerid, $locationid, $priority ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_instantmessage' );
		$this->db->where( 'customerid', $customerid );
		$this->db->where( 'locationid', $locationid );
		$this->db->where( 'priority', $priority );
		$this->db->where( 'status', 'active' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function is used for getting all the customers
	 */
	function getallcustomers() {
		$this->db->select( '*' );
		$this->db->from( 'tbl_customer' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function is used for getting all the location via customer id for seperate customers
	 */
	function getalllocationsbyidforcustomer( $customerid ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		} else {

		}
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function is used for getting all the location via customer id for json
	 */
	function getalllocationsbyid( $customerID ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );
		$this->db->where( 'customerid', $customerID[ 'customerid' ] );
		$query = $this->db->get();
		$response = $query->result_array();
		return $response;
	}
	/* This function is used for getting all the target groups via customer id for json
	 */
	function getalltargetgroupbyid( $customerID ) {
		$this->db->select( 'targetlist' );
		$this->db->from( 'tbl_permisionlist' );
		$this->db->where( 'customerid', $customerID[ 'customerid' ] );
		$this->db->group_by( 'targetlist' );
		$query = $this->db->get();
		$response = $query->result_array();
		return $response;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not
	 */
	function getallphonenumbersbylocationID( $locationID, $searchText, $type, $usermessagestatus ) {
		//$this->db->distinct();
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as u' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = u.username' );
		//$this->db->join('sentmessages as sent','sent.username = u.username');
		//$this->db->not_like('sent.message',$messageText);
		$this->db->where( 'rpa.authdate >=', $searchText );
		$this->db->where( 'u.lastlogin >=', $searchText );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		if ( $type == '2' ) {
			$this->db->where( 'u.smspermission !=', $type );
		} elseif ( $type == '3' ) {
			$this->db->where( 'u.smspermission !=', '3' );
			$this->db->where( 'u.smspermission !=', '2' );
		}
		$this->db->where( 'u.message', $usermessagestatus );
		//$this->db->where('u.smspermission', '');
		$this->db->where_in( 'u.locationid', $locationID, 'FALSE' );
		$this->db->group_by( 'u.username' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not
	 */
	function getallphonenumbersbylocationIDdaily( $locationID, $searchText, $type, $usermessagestatus ) {
		//$this->db->distinct();
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as u' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = u.username' );
		//$this->db->join('sentmessages as sent','sent.username = u.username');
		//$this->db->not_like('sent.message',$messageText);
		$this->db->where( 'rpa.authdate >=', $searchText );
		$this->db->where( 'u.lastlogin >=', $searchText );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		if ( $type == '2' ) {
			$this->db->where( 'u.smspermission !=', $type );
		} elseif ( $type == '3' ) {
			$this->db->where( 'u.smspermission !=', '3' );
			$this->db->where( 'u.smspermission !=', '2' );
		}
		if ( $usermessagestatus == 'olduserloginagain' ) {
			$this->db->where( 'u.message', $usermessagestatus );
		}
		//$this->db->where('u.smspermission', '');
		$this->db->where_in( 'u.locationid', $locationID, 'FALSE' );
		$this->db->group_by( 'u.username' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not
	 */
	function getallphonenumbersbylocationIDdatetime( $locationID, $searchText, $type, $usermessagestatus, $date ) {
		//$this->db->distinct();
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as u' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = u.username' );
		$this->db->join( 'sentmessages as sent', 'sent.username = u.username' );
		//$this->db->join('sentmessages as sent','sent.username = u.username');
		//$this->db->not_like('sent.message',$messageText);
		$this->db->where( 'rpa.authdate >=', $searchText );
		$this->db->where( 'u.lastlogin >=', $searchText );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		if ( $type == '2' ) {
			$this->db->where( 'u.smspermission !=', $type );
		} elseif ( $type == '3' ) {
			$this->db->where( 'u.smspermission !=', '3' );
			$this->db->where( 'u.smspermission !=', '2' );
		}
		if ( $usermessagestatus == 'olduserloginagain' ) {
			$this->db->where( 'u.message', $usermessagestatus );
		}
		$this->db->where( 'sent.date >', $date );
		$this->db->where( 'sent.smscode', 'datetimeuser' );
		//$this->db->where('u.smspermission', '');
		$this->db->where_in( 'u.locationid', $locationID, 'FALSE' );
		$this->db->group_by( 'u.username' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to update the userprofiles
	 
	 */
	function updateuserprofilespage( $userid, $message, $type ) {
		//echo $message;exit;
		$this->db->set( 'message', $message );
		$this->db->set( 'smspermission', $type );
		$this->db->where( 'userid', $userid );
		$this->db->update( 'tbl_userprofiles' );

		return TRUE;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not
	 */
	function getAllPhonenumberforInstantMessages( $locationID ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles' );
		$this->db->where( 'locationid IN', $locationID );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/* This function will be used for getting all the phonenumbers via location id connected it with radpostauth to get user is login or not
	 */
	function getphonenumbersbylocationmodel( $locationIDs, $startdatenum, $enddatenum ) {
		//$this->db->distinct();
		$this->db->select( 'perm.username' );
		$this->db->from( 'tbl_permisionlist as perm' );
		$this->db->where_in( 'perm.locationid', $locationIDs, 'FALSE' );
		$sub_query = $this->db->get_compiled_select();

		$this->db->select( 'black.username' );
		$this->db->from( 'tbl_blacklist as black' );
		$this->db->where_in( 'black.locationid', $locationIDs, 'FALSE' );
		$sub_query2 = $this->db->get_compiled_select();


		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as u' );
		$this->db->where( 'u.lastlogin <=', $enddatenum );
		$this->db->where( 'u.lastlogin >=', $startdatenum );
		$this->db->where_in( 'u.locationid', $locationIDs, 'FALSE' );
		$this->db->where( "u.username NOT IN ($sub_query2)" );
		$this->db->where( "u.username IN ($sub_query)" );
		$this->db->group_by( 'u.username' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function addcampaign( $messageInfo ) {

		$this->db->trans_start();
		$this->db->insert( 'tbl_campaign', $messageInfo );
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();

		return $insert_id;

	}
	/**
	 * This function is used to delete the Instant message campaign
	 */
	function deleteCampaign( $inmid ) {
		$this->db->where( 'inmid', $inmid );
		$this->db->delete( 'tbl_instantmessage' );
		return $this->db->affected_rows();
	}
	/**
	 * This function is used to update the instant message
	 
	 */
	function editInstantmessage( $inmid, $message, $priority ) {
		//echo $message;exit;
		$this->db->set( 'message', $message );
		$this->db->set( 'priority', $priority );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );

		return TRUE;
	}
}