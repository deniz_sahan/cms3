<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class course_model extends CI_Model {


	/**
	 * This function is used to get the course listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function courseListingCount( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		//$this->db->where( 'tc.status', 'active' );
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function is used to get the course listing values
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function courseListing( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_course as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$query = $this->db->get();
		$result = $query->result();
		
		return $result;
	}
	
	
	/**
	 * This function is used to get the data of all classrooms listing values
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function getallclassroom( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_classroom as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		//$this->db->where( 'tc.status', 'active' );
	//$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to add new Course to the system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewCouse( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_course', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
	
	/**
	 * This function is used to update the course information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editcourse( $courseInfo, $courseid ) {
		$this->db->where( 'course_id', $courseid );
		$this->db->update( 'tbl_course', $courseInfo );

		return TRUE;
	}


	
	
	       /**
	 * show courses
	 */
	function getallcourses() {
		$this->db->select( '*' );
		$this->db->from( 'tbl_course' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	       /**
	 * show courses
	 */
	function getcourses($courseid) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_course' );
		$this->db->where( 'course_id', $courseid );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
        /**count students
	 */
	function studentListingCount( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_student' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tbl_student.student_name  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$query = $this->db->get();

		return $query->num_rows();
	}
        
        /**
	 This function use to show all studetnts from database
	 */
	function studentListing( $searchText = '', $page, $segment) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_student' );
                if ( !empty( $searchText ) ) {
			$likeCriteria = "(tbl_student.student_name  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
 
        /**
	 * show locations
	 */
	function getalllocations() {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );

		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
}