<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class DeleteNumbersModel extends CI_Model {
	function deleteradcheckmodel( $number ) {
		$this->db->where( 'username', $number );
		$this->db->delete( 'radcheck' );
		return $this->db->affected_rows();
	}

	function deletepermlistmodel( $number ) {
		$this->db->where( 'username', $number );
		$this->db->delete( 'tbl_permisionlist' );
		return $this->db->affected_rows();
	}

	function deleteblacklistmodel( $number ) {
		$this->db->where( 'username', $number );
		$this->db->delete( 'tbl_blacklist' );
		return $this->db->affected_rows();
	}

	function deletemicrosms( $number ) {
		$this->db->where( 'username', $number );
		$this->db->delete( 'microlocationsms' );
		return $this->db->affected_rows();
	}

	function deleteuserprofiles( $number ) {
		$this->db->where( 'username', $number );
		$this->db->delete( 'tbl_userprofiles' );
		return $this->db->affected_rows();
	}

}