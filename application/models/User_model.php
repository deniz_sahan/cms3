<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class User_model extends CI_Model {
	/**
	 * This function is used to get the user listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function userListingCount( $searchText = '', $customerid = '' ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role' );
		$this->db->from( 'tbl_users as BaseTbl' );
		$this->db->join( 'tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'BaseTbl.customerid', $customerid );
			////// 5 is constant for Emplyee role
			$this->db->where( 'BaseTbl.roleId', '5' );
		}
		$this->db->where( 'BaseTbl.isDeleted', 0 );
		$this->db->where( 'BaseTbl.roleId !=', 1 );
		$query = $this->db->get();

		return $query->num_rows();
	}

	/**
	 * This function is used to get the user listing count
	 * @param string $searchText : This is optional search text
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function userListing( $searchText = '', $page, $segment, $customerid = '' ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role,cust.customername' );
		$this->db->from( 'tbl_users as BaseTbl' );
		$this->db->join( 'tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left' );
		$this->db->join( 'tbl_customer as cust', 'cust.customerid = BaseTbl.customerid', 'left' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'BaseTbl.customerid', $customerid );
			////// 5 is constant for Emplyee role
			$this->db->where( 'BaseTbl.roleId', '5' );
		}
		$this->db->where( 'BaseTbl.isDeleted', 0 );
		$this->db->where( 'BaseTbl.roleId !=', 1 );
		$this->db->order_by( 'BaseTbl.userId', 'DESC' );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}

	/**
	 * This function is used to get the user roles information
	 * @return array $result : This is result of the query
	 */
	function getUserRoles( $customerid = '' ) {
		$this->db->select( 'roleId, role' );
		$this->db->from( 'tbl_roles' );
		$this->db->where( 'roleId !=', 1 );
		if ( !empty( $customerid ) ) {
			////// 5 is constant for Emplyee role
			$this->db->where( 'roleId', '5' );
		}
		$query = $this->db->get();

		return $query->result();
	}

	/**
	 * This function is used to check whether email id is already exist or not
	 * @param {string} $email : This is email id
	 * @param {number} $userId : This is user id
	 * @return {mixed} $result : This is searched result
	 */
	function checkEmailExists( $email, $userId = 0 ) {
		$this->db->select( "email" );
		$this->db->from( "tbl_users" );
		$this->db->where( "email", $email );
		$this->db->where( "isDeleted", 0 );
		if ( $userId != 0 ) {
			$this->db->where( "userId !=", $userId );
		}
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to add new user to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewUser( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_users', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getUserInfo( $userId ) {
		$this->db->select( 'userId, name, email, mobile, roleId' );
		$this->db->from( 'tbl_users' );
		$this->db->where( 'isDeleted', 0 );
		$this->db->where( 'roleId !=', 1 );
		$this->db->where( 'userId', $userId );
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to update the user information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editUser( $userInfo, $userId ) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'tbl_users', $userInfo );

		return TRUE;
	}



	/**
	 * This function is used to delete the user information
	 * @param number $userId : This is user id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteUser( $userId, $userInfo ) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'tbl_users', $userInfo );

		return $this->db->affected_rows();
	}


	/**
	 * This function is used to match users password for change password
	 * @param number $userId : This is user id
	 */
	function matchOldPassword( $userId, $oldPassword ) {
		$this->db->select( 'userId, password' );
		$this->db->where( 'userId', $userId );
		$this->db->where( 'isDeleted', 0 );
		$query = $this->db->get( 'tbl_users' );

		$user = $query->result();

		if ( !empty( $user ) ) {
			if ( verifyHashedPassword( $oldPassword, $user[ 0 ]->password ) ) {
				return $user;
			} else {
				return array();
			}
		} else {
			return array();
		}
	}

	/**
	 * This function is used to change users password
	 * @param number $userId : This is user id
	 * @param array $userInfo : This is user updation info
	 */
	function changePassword( $userId, $userInfo ) {
		$this->db->where( 'userId', $userId );
		$this->db->where( 'isDeleted', 0 );
		$this->db->update( 'tbl_users', $userInfo );

		return $this->db->affected_rows();
	}


	/**
	 * This function is used to get user login history
	 * @param number $userId : This is user id
	 */
	function loginHistoryCount( $userId, $searchText, $fromDate, $toDate ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $fromDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '" . date( 'Y-m-d', strtotime( $fromDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $toDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '" . date( 'Y-m-d', strtotime( $toDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'BaseTbl.userId', $userId );
		$this->db->from( 'tbl_last_login as BaseTbl' );
		$query = $this->db->get();

		return $query->num_rows();
	}

	/**
	 * This function is used to get user login history
	 * @param number $userId : This is user id
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function loginHistory( $userId, $searchText, $fromDate, $toDate, $page, $segment ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm' );
		$this->db->from( 'tbl_last_login as BaseTbl' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $fromDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '" . date( 'Y-m-d', strtotime( $fromDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $toDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '" . date( 'Y-m-d', strtotime( $toDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'BaseTbl.userId', $userId );
		$this->db->order_by( 'BaseTbl.id', 'DESC' );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getUserInfoById( $userId ) {
		$this->db->select( 'userId, name, email, mobile, roleId' );
		$this->db->from( 'tbl_users' );
		$this->db->where( 'isDeleted', 0 );
		$this->db->where( 'userId', $userId );
		$query = $this->db->get();

		return $query->row();
	}

	/**
	 * This function used to get all login attempts
	 */
	function totalloginattempts( $customerid = '' ) {
		$this->db->select( 'up.*,lo.locationname' );
		$this->db->from( 'tbl_userprofiles as up' );
		$this->db->join( 'tbl_location as lo', 'lo.locationid = up.locationid' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'up.customerid', $customerid );
		}
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function used to get unique users
	 */
	function totaluniqueusers( $customerid = '' ) {
		$this->db->select( 'userid' );
		$this->db->from( 'tbl_userprofiles' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		$this->db->group_by( 'username' );
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}
	/**
	 * This function is used to get unique users daily
	 */
	function totaluniqueusersdaily( $locationid ) {
		$this->db->select( 'userid' );
		$this->db->from( 'tbl_userprofiles' );
		$this->db->where( 'lastlogin >', date( 'Y-m-d 06:00:00' ) );
		$this->db->where( 'lastlogin <', date( 'Y-m-d 23:59:59' ) );
		$this->db->where( 'locationid', $locationid );
		$this->db->group_by( 'username' );
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}

	/**
	 * This function is used to get unique users weekly
	 */
	function totaluniqueusersweekly( $monday, $sunday, $locationid ) {
		$this->db->select( 'userid' );
		$this->db->from( 'tbl_userprofiles' );
		//        $this->db->where('lastlogin >=', date('Y-m-d', strtotime('-1 week')));
		/* Queries added by AQ  we need to make another codition on monday that if the day is monday show current data*/
		$this->db->where( 'lastlogin >', $monday );
		$this->db->where( 'lastlogin <', $sunday );
		/* Queries added by AQ*/
		$this->db->where( 'locationid', $locationid );
		$this->db->group_by( 'username' );
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}

	/**
	 * This function is used to get unique users weekly
	 */
	function totaluniqueusersmonthly( $month, $locationid ) {
		$this->db->select( 'userid' );
		$this->db->from( 'tbl_userprofiles' );
		//        $this->db->where('lastlogin >=', date('Y-m-d', strtotime('-1 week')));
		$day = date( 'd' );
		//code by Abdul Qadir
		$this->db->where( 'lastlogin >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
		//code by Abdul Qadir
		$this->db->where( 'locationid', $locationid );
		// $this->db->group_by('username'); 
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}
	//all unique users

	function entries( $customerid ) {
		$this->db->select( 'userid' );
		$this->db->from( 'tbl_userprofiles' );
		//        $this->db->where('lastlogin >=', date('Y-m-d', strtotime('-1 week')));
		$day = date( 'd' );
		//code by Abdul Qadir
		$this->db->where( 'lastlogin >=DATE_SUB(CURDATE(), INTERVAL ' . $day . ' DAY)' );
		//code by Abdul Qadir
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		// $this->db->group_by('username'); 
		$query = $this->db->get();
		$count = $query->num_rows();
		return $count;
	}

	/**
	 * This function used to get sent sms
	 */
	function totalsmssent( $customerid = '' ) {
		$this->db->select( 'smsid' );
		$this->db->from( 'sentmessages' );
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		$query = $this->db->get();
		$result = $query->result();
		//pre($result);
		//exit;

        return $query->num_rows();
    }
    /**
     * This function used to get download aupload
     */
    function totaldownloadupload()
    {
        $this->db->select('id');
        $this->db->from('radpostauth');
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getCountUsersStats($customerid = '')
    {
        $this->db->select('*');
        $this->db->from('radacct');
        if(!empty($customerid)){
          $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
          $this->db->where('tbl_location.customerid',$customerid);  
        }
        
        $this->db->order_by("radacct.radacctid", "desc");
        
       $query =  $this->db->get();
        
        return $query->num_rows();
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getUsersStats($customerid = '')
    {
        $this->db->select('*');
        $this->db->from('radacct');
        if(!empty($customerid)){
          $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
          $this->db->where('tbl_location.customerid',$customerid);  
        }
        
        $this->db->order_by("radacct.radacctid", "desc");
        //$this->db->limit($kacar,$nereden);
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the statistics of usrs activity for exective dashboard
     */
    function getUsersStatsfordashboard($customerid = '')
    {
       
        $this->db->select('radacct.username,count(radacct.username),SUM(radacct.acctoutputoctets) AS `uploadcount`,SUM(radacct.acctinputoctets) AS `downloadcount`,radacct.acctstarttime as month');        
        $this->db->from('radacct');
        if(!empty($customerid)){
          $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
          $this->db->where('tbl_location.customerid',$customerid);  
        }
        $this->db->like('radacct.acctstarttime', date('Y-m'));
        $this->db->group_by('DAYOFMONTH(radacct.acctstarttime)');                
        $this->db->order_by("radacct.acctstarttime", "asc");
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get user counts percustomer
     */
    function countusersbycustomers()
    {
        $this->db->select('count(distinct(username)) as count,customerid');
        $this->db->from('tbl_userprofiles');
         $this->db->group_by('customerid');
       $query =  $this->db->get();
        
        return $query->result();
    }
    /*

     * geting running instant message campaign for showing in header
     *
     *      */
    function checkinstantcampaigns($customerid = ''){
        $currentdate = date('Y-m-d H:i:s');
        $this->db->select('*');
        $this->db->from('tbl_instantmessage');
        if(!empty($customerid)){
          $this->db->where('customerid',$customerid);  
        }
        $this->db->where('status','active');
        $this->db->where('startdate <',$currentdate);
        $query =  $this->db->get();
        
        return $query->result();
    }
   
   /** code by Abdul Qadir
     * This function is used to get the sentSms between two dates on dashboard
     */
    function sentSms($locationid){

        $this->db->select('COUNT(sent)  AS `count`,date as hour ');
        $this->db->from('sentmessages');		
		$this->db->where('date >', date('Y-m-d 06:00:00'));
        $this->db->where('date <', date('Y-m-d 23:59:59'));
        $this->db->where('locationid',$locationid);		
        $query = $this->db->get();        
        return $query->result();
    }
	/** code by Abdul Qadir
     * This function is used to get the sentSms on weekly base
     */
     function sentSmsWeekly($locationid, $monday, $sunday){
         $this->db->select('COUNT(sent)  AS `count`,date as day,username ');
         $this->db->from('sentmessages');		
		 $this->db->where('date >', $monday);
         $this->db->where('date <', $sunday);
         $this->db->where('locationid',$locationid);		
         $query = $this->db->get(); 
         return $query->result();
    }
	/** code by Abdul Qadir
     * This function is used to get the sentSms on MOnthly base
     */
  function sentSmsMonthly($month, $locationid)
    {
        $this->db->select('COUNT(sent) AS `count`,date as month,username');        
        $this->db->from('sentmessages');      
        $day=date('d');
	    $this->db->where('date >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' );
        $this->db->where('locationid',$locationid);
        $query = $this->db->get();  		      
        return $query->result();
    }
	/**  
	For Daily Average Time
	*/
	 function getDailyAverageTime($locationid)
    {
     $this->db->select('*,Avg(radacct.acctsessiontime) AS duration');
     $this->db->from('radacct');		
     $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
     $this->db->where('acctstarttime  >', date('Y-m-d 00:00:00'));
     $this->db->where('acctstarttime  <', date('Y-m-d 23:59:59'));
     $this->db->where('tbl_location.locationid',$locationid);            
     $query = $this->db->get();       
     $result = $query->result();   
     
     return $result;
   
}
/**
     * This function is used to get the average time of MOnthly users of usrs activity
     */
 function getWeeklyAverageTime($monday, $sunday, $locationid)
    {
        $this->db->select('*,Avg(radacct.acctsessiontime) AS duration');
        $this->db->from('radacct');		
        $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
        $this->db->where('acctstarttime >', $monday);
        $this->db->where('acctstarttime <', $sunday);
        $this->db->where('tbl_location.locationid',$locationid);            
        $query = $this->db->get();       
        $result = $query->result();   
		//pre($result);
     
        return $result;
   
}
/**
     * This function is used to get the average time of monthly usrs activity
     */
function getMonthlyAverageTime($locationid)
    {
        $this->db->select('*,Avg(radacct.acctsessiontime) AS duration');
        $this->db->from('radacct');
	    $day=date('d');
        $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
        $this->db->where('acctstarttime >=DATE_SUB(CURDATE(), INTERVAL ' .$day. ' DAY)' );  
	    $this->db->where('tbl_location.locationid',$locationid);
        $query = $this->db->get();       
        $result = $query->result();   
     
        return $result;
   
}
/**
     * This function is used to get the average time of all users of usrs activity
     */
function getAverageAllTime($customerid)
    {
         $this->db->select('*,Avg(radacct.acctsessiontime) AS duration');
         $this->db->from('radacct');		
         $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
         $this->db->where('acctstarttime  >', date('Y-m-d 00:00:00'));
         $this->db->where('acctstarttime  <', date('Y-m-d 23:59:59'));
		  if(!empty($customerid)){
          $this->db->where('customerid',$customerid);  
        }
        $query = $this->db->get();       
        $result = $query->result();   
		//pre($result);
     
        return $result;
        
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getallonlineusers($customerid = '',$limit)
    {
        $this->db->select('*');
        $this->db->from('tbl_onlineusers');
        
//        if(!empty($customerid)){
//          $this->db->join('tbl_location', 'tbl_location.lanip = radacct.calledstationid OR tbl_location.lanip2 = radacct.calledstationid' ,'left');
//          $this->db->where('tbl_location.customerid',$customerid);  
//        }
        
        $this->db->order_by("lastactivity", "desc");
        if($limit != ''){
            $this->db->limit($limit);
        }
        
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getalluniqueusers($customerid = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_cusersdata');
        $this->db->order_by("lastactivity", "desc");
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getthisuserdata($username)
    {
        $this->db->select('*');
        $this->db->from('tbl_cusersdata');
        $this->db->where('username', $username);
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getallLocationVisitbyThisUser($username)
    {
        $this->db->select('loc.locationname,count(tup.locationid) as visitedcount');
        $this->db->from('tbl_userprofiles as tup');
        $this->db->join('tbl_location as loc', 'loc.locationid = tup.locationid','left');
        $this->db->where('tup.username', $username);
        //$this->db->order_by("tup.lastlogin", "desc");
        //$this->db->query("GROUP BY MONTH('tup.lastlogin')");
        //$this->db->group_by('DAY(tup.lastlogin)');
        $this->db->group_by('tup.locationid');
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getThisUserTimeline($username)
    {
        $this->db->select('loc.locationname,tup.lastlogin,tup.password,tup.ip,tup.macid');
        $this->db->from('tbl_userprofiles as tup');
        $this->db->join('tbl_location as loc', 'loc.locationid = tup.locationid','left');
        $this->db->where('tup.username', $username);
        $this->db->order_by("tup.lastlogin", "desc");
        //$this->db->query("GROUP BY MONTH('tup.lastlogin')");
        //$this->db->group_by('DAY(tup.lastlogin)');
        //$this->db->group_by('tup.locationid');
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getwebstatsofThisIP($ip)
    {
        $this->db->select("Time,MethodId,SchemeId,HostID,PortNo,Resource,UserId");
        $this->db->from("access_log");
        $this->db->where('MethodId', $ip);
        $this->db->or_where('SchemeId', $ip);
        $this->db->or_where('HostID', $ip);
        $this->db->or_where('PortNo', $ip);
        $this->db->or_where('Resource', $ip);
        $this->db->or_where('UserId', $ip);
        
        $this->db->order_by("Time", "desc");
        $this->db->group_by("Time");
        //$this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();    
        return $result;
    }
    /**
     * This function is used to get the statistics of usrs activity
     */
    function getallusersdevices($customerid = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_devicestatus');
        $query = $this->db->get();       
        $result = $query->result();        
        return $result;
    }
	/**
     * This function is used to get the Profile Picture of user
     */
   function getImage($userId){
     $this->db->select('image_file');
     $this->db->from('tbl_users');
	 $this->db->where('userId', $userId);
	   $query = $this->db->get();       
        $result = $query->result(); 		
        return $result;
	
}
	
	
	}
