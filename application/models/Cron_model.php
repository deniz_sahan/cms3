<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class Cron_model extends CI_Model {
	/*This function used to get online users from DB and check blaclist and permissionlist*/
	function getonlineusers( $userid, $targetlist ) {
		$this->db->select( 'perm.username' );
		$this->db->from( 'tbl_permisionlist as perm' );
		$this->db->where( 'perm.targetlist', $targetlist );
		$sub_query = $this->db->get_compiled_select();

		$this->db->select( 'black.username' );
		$this->db->from( 'tbl_blacklist as black' );
		$sub_query2 = $this->db->get_compiled_select();

		$this->db->select( '*' );
		$this->db->from( 'microlocationsms as micro' );
		$this->db->where( "micro.username IN ($sub_query)" );
		$this->db->where( "micro.username NOT IN ($sub_query2)" );
		$this->db->where( 'micro.userid', $userid );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*This function used to get target list according to radpost authotantication*/
	function gettargetlist( $currentdateforauth ) {

		$this->db->select( 'micro.targetusers,micro.userid,micro.username,micro.lastlogin,micro.locationid' );
		$this->db->from( 'microlocationsms as micro' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = micro.username' );
		$this->db->where( 'rpa.authdate >= micro.lastlogin' );
		$this->db->where( 'micro.lastlogin >=', $currentdateforauth );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		$this->db->where( 'micro.smspermission', 'permitted' );
		$this->db->where( 'micro.sentsms', '0' );
		$this->db->order_by( 'micro.priority', 'asc' );
		//$this->db->where('micro.priority', '1');
		$this->db->limit( 1 );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/*This function used to update micro location sms when sms sent*/
	function updatemicrolocationsms( $userid ) {
		$this->db->set( 'sentsms', '1' );
		$this->db->where( 'userid', $userid );
		$this->db->update( 'microlocationsms' );
	}
	/*This function used to update message in user profile */
	function updateuserprofiles( $message, $userid ) {
		$this->db->set( 'message', $message );
		$this->db->where( 'userid', $userid );
		$this->db->update( 'tbl_userprofiles' );
	}
	/*This function used to update count of instant message in companies */
	function updateinstantmessages( $inmid ) {
		$this->db->set( 'countsms', 'countsms+1', FALSE );
		$this->db->where( 'inmid', $inmid );
		$this->db->update( 'tbl_instantmessage' );
	}
	/*This function used to insert message  */
	function insertsentmessages( $smsInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'sentmessages', $smsInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
	/*This function used to delete all data of microsmsm */
	function deletemicrosmsdata( $username ) {
		$this->db->where( 'username', $username );
		$this->db->delete( 'microlocationsms' );

	}

	function checksentmessagesEveryday( $username, $lastlogin ) {
		$this->db->select( 'username' );
		$this->db->from( 'sentmessages' );
		$this->db->where( 'date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY )' );
		$this->db->where( 'username', $username );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function checksentmessagesxhours( $username, $variabledate ) {
		$this->db->select( 'username' );
		$this->db->from( 'sentmessages' );
		$this->db->where( 'date >= DATE_SUB(CURDATE(), INTERVAL ' . $variabledate . ' DAY )' );
		$this->db->where( 'username', $username );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function checkblackanddelete( $username ) {
		$this->db->select( 'black.username' );
		$this->db->where( 'black.username', $username );
		$this->db->from( 'tbl_blacklist as black' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	function deletemicrosms( $username ) {
		$this->db->where( 'username', $username );
		$this->db->delete( 'microlocationsms' );
		return $this->db->affected_rows();
	}

	function deletemicrosmsrough() {
		$this->db->where( 'targetusers', 'nolist' );
		$this->db->where( 'messagetype', 'notfound' );
		$this->db->where( 'message', 'nocampaignfound' );
		$this->db->delete( 'microlocationsms' );
		return $this->db->affected_rows();
	}

	function deactivateExpireCampaign( $currentdate ) {
		$this->db->set( 'status', 'not active' );
		$this->db->where( 'enddate <=', $currentdate );
		$this->db->update( 'tbl_instantmessage' );
	}
	/*
	 * session control script this script will log off users  model
	 */
	function controlsession( $today ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_userslogoff' );
		$this->db->where( 'status', '0' );
		$this->db->where( 'expirydate <=', $today );
		$this->db->limit( 1 );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	// expire users
	function updateuserlogofftable( $username, $today ) {
		$this->db->set( 'status', '1' );
		$this->db->where( 'username', $username );
		$this->db->where( 'expirydate <=', $today );
		$this->db->update( 'tbl_userslogoff' );
	}
	/*
	 * session control script radacct table will loggoff user from internet users
	 */
	function updateradchecktable( $username ) {
		$this->db->where( 'username', $username );
		$this->db->delete( 'radcheck' );

		return $this->db->affected_rows();
	}
	/*
	 * session control script this script will log off users  model
	 */
	function getlogofftime( $locationid ) {
		$this->db->select( 'schours,scminutes' );
		$this->db->where( 'locationid', $locationid );
		$this->db->from( 'tbl_location' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to add new user expiry date
	 * 
	 */
	function insertuserslogoff( $expirydateinfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_userslogoff', $expirydateinfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
}