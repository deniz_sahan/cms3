<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class classroom_model extends CI_Model {


	/**
	 * This function is used to get the course listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function courseListingCount( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_userprofiles as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		//$this->db->where( 'tc.status', 'active' );
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function is used to get the course listing values
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function teacherListing( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_teacher as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		//$this->db->where( 'tc.status', 'active' );
	//$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	
	
	/**
	 * This function is used to get the data of all classrooms listing values
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function getallclassroom( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_classroom as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.kimlik  LIKE '%" . $searchText . "%'
                            OR  tc.classroom  LIKE '%" . $searchText . "%'
                            OR  tc.course  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		//$this->db->where( 'tc.status', 'active' );
	//$this->db->limit( $page, $segment );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to add new Course to the system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewClassroom( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_teacher', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
	

}