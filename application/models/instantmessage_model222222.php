<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class instantmessage_model extends CI_Model {
	/**
	 * This function is used to get the user listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function getphonenumbersforinstantmessage( $searchText = '' ) {
		//        $this->db->select('sent.username');
		//        $this->db->from('sentmessages as sent');
		//        $this->db->where('sent.smscode', '1');
		//        $sub_query = $this->db->get_compiled_select();
		////        
		//        $this->db->select('sent.username');
		//        $this->db->from('sentmessages as sent');
		//        $this->db->where('sent.smscode', '2');
		//        $sub_query2 = $this->db->get_compiled_select();

		$this->db->select( 'r.username,r.acctstarttime,r.calledstationid, l.ad, l.soyad,l.username,rpa.reply' );
		$this->db->from( 'radacct as r' );
		$this->db->join( 'log as l', 'l.username = r.username' );
		$this->db->join( 'radpostauth as rpa', 'rpa.username = r.username' );
		//$this->db->join('sentmessages as s', 's.username = r.username');
		$this->db->where( 'rpa.authdate >=', $searchText );
		$this->db->where( 'rpa.reply', 'Access-Accept' );
		//$this->db->where("r.username IN ($sub_query)");
		//        $this->db->where("r.username NOT IN ($sub_query)");
		//        $this->db->or_where("r.username NOT IN ($sub_query2)");

		//$this->db->where('s.smscode !=', '1');
		//        $this->db->where('BaseTbl.roleId !=', 1);
		$this->db->group_by( 'r.username' );
		$query = $this->db->get();
		$result = $query->result();

		return $result;
	}

	/*
	 *get messsagess data from sent messages table
	 */
	function getallmessagesdata() {

		$this->db->select( '*' );
		$this->db->from( 'sentmessages' );
		$query = $this->db->get();
		$result = $query->result();

		return $result;

	}
	/*
	 *Insert new messages info
	 */
	function insertNewSentMessage( $messageInfo ) {

		$this->db->trans_start();
		$this->db->insert( 'sentmessages', $messageInfo );
		$insert_id = $this->db->insert_id();
		$this->db->trans_complete();

		return $insert_id;

	}

	function getDailymessageGraphModel() {
		//        $this->db->select('date_format(sent.date, "%Y-%m-%d") AS date,COUNT(sent.smsid) as total');
		//        $this->db->from('sentmessages as sent');
		//        $this->db->group_by('date');
		$this->db->select( 'DISTINCT DATE_FORMAT(`date`, "%Y-%m-%d") `date`, count(smsid) as total', FALSE )->from( 'sentmessages' )->group_by( 'DAY(date)' )->order_by( 'date', 'DESC' );
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to get the user listing count
	 * @param string $searchText : This is optional search text
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function userListing( $searchText = '', $page, $segment ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, Role.role' );
		$this->db->from( 'tbl_users as BaseTbl' );
		$this->db->join( 'tbl_roles as Role', 'Role.roleId = BaseTbl.roleId', 'left' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.name  LIKE '%" . $searchText . "%'
                            OR  BaseTbl.mobile  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'BaseTbl.isDeleted', 0 );
		$this->db->where( 'BaseTbl.roleId !=', 1 );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}

	/**
	 * This function is used to get the user roles information
	 * @return array $result : This is result of the query
	 */
	function getUserRoles() {
		$this->db->select( 'roleId, role' );
		$this->db->from( 'tbl_roles' );
		$this->db->where( 'roleId !=', 1 );
		$query = $this->db->get();

		return $query->result();
	}

	/**
	 * This function is used to check whether email id is already exist or not
	 * @param {string} $email : This is email id
	 * @param {number} $userId : This is user id
	 * @return {mixed} $result : This is searched result
	 */
	function checkEmailExists( $email, $userId = 0 ) {
		$this->db->select( "email" );
		$this->db->from( "tbl_users" );
		$this->db->where( "email", $email );
		$this->db->where( "isDeleted", 0 );
		if ( $userId != 0 ) {
			$this->db->where( "userId !=", $userId );
		}
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to add new user to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewUser( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_users', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getUserInfo( $userId ) {
		$this->db->select( 'userId, name, email, mobile, roleId' );
		$this->db->from( 'tbl_users' );
		$this->db->where( 'isDeleted', 0 );
		$this->db->where( 'roleId !=', 1 );
		$this->db->where( 'userId', $userId );
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to update the user information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editUser( $userInfo, $userId ) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'tbl_users', $userInfo );

		return TRUE;
	}



	/**
	 * This function is used to delete the user information
	 * @param number $userId : This is user id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteUser( $userId, $userInfo ) {
		$this->db->where( 'userId', $userId );
		$this->db->update( 'tbl_users', $userInfo );

		return $this->db->affected_rows();
	}


	/**
	 * This function is used to match users password for change password
	 * @param number $userId : This is user id
	 */
	function matchOldPassword( $userId, $oldPassword ) {
		$this->db->select( 'userId, password' );
		$this->db->where( 'userId', $userId );
		$this->db->where( 'isDeleted', 0 );
		$query = $this->db->get( 'tbl_users' );

		$user = $query->result();

		if ( !empty( $user ) ) {
			if ( verifyHashedPassword( $oldPassword, $user[ 0 ]->password ) ) {
				return $user;
			} else {
				return array();
			}
		} else {
			return array();
		}
	}

	/**
	 * This function is used to change users password
	 * @param number $userId : This is user id
	 * @param array $userInfo : This is user updation info
	 */
	function changePassword( $userId, $userInfo ) {
		$this->db->where( 'userId', $userId );
		$this->db->where( 'isDeleted', 0 );
		$this->db->update( 'tbl_users', $userInfo );

		return $this->db->affected_rows();
	}


	/**
	 * This function is used to get user login history
	 * @param number $userId : This is user id
	 */
	function loginHistoryCount( $userId, $searchText, $fromDate, $toDate ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $fromDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '" . date( 'Y-m-d', strtotime( $fromDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $toDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '" . date( 'Y-m-d', strtotime( $toDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'BaseTbl.userId', $userId );
		$this->db->from( 'tbl_last_login as BaseTbl' );
		$query = $this->db->get();

		return $query->num_rows();
	}

	/**
	 * This function is used to get user login history
	 * @param number $userId : This is user id
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function loginHistory( $userId, $searchText, $fromDate, $toDate, $page, $segment ) {
		$this->db->select( 'BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm' );
		$this->db->from( 'tbl_last_login as BaseTbl' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(BaseTbl.email  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $fromDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '" . date( 'Y-m-d', strtotime( $fromDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $toDate ) ) {
			$likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '" . date( 'Y-m-d', strtotime( $toDate ) ) . "'";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'BaseTbl.userId', $userId );
		$this->db->order_by( 'BaseTbl.id', 'DESC' );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getUserInfoById( $userId ) {
		$this->db->select( 'userId, name, email, mobile, roleId' );
		$this->db->from( 'tbl_users' );
		$this->db->where( 'isDeleted', 0 );
		$this->db->where( 'userId', $userId );
		$query = $this->db->get();

		return $query->row();
	}

}