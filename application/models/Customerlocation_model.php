<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

class customerlocation_model extends CI_Model {


	/**
	 * This function is used to get the customers listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function customerListingCount( $searchText = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_customer as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.email  LIKE '%" . $searchText . "%'
                            OR  tc.customername  LIKE '%" . $searchText . "%'
                            OR  tc.cusername  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'tc.status', 'active' );
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function is used to get the custmer listing count
	 */
	function customerListing( $searchText = '', $page, $segment ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_customer as tc' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tc.email  LIKE '%" . $searchText . "%'
                            OR  tc.customername  LIKE '%" . $searchText . "%'
                            OR  tc.cusername  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		$this->db->where( 'tc.status', 'active' );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}

	/**
	 * This function is used to get the user roles information
	 * @return array $result : This is result of the query
	 */
	function getUserRoles() {
		$this->db->select( 'roleId, role' );
		$this->db->from( 'tbl_roles' );
		$this->db->where( 'roleId !=', 1 );
		$query = $this->db->get();

		return $query->result();
	}

	/**
	 * This function is used to check whether email id is already exist or not
	 * @param {string} $email : This is email id
	 * @param {number} $userId : This is user id
	 * @return {mixed} $result : This is searched result
	 */
	function checkEmailExists( $email, $userId = 0 ) {
		$this->db->select( "email" );
		$this->db->from( "tbl_users" );
		$this->db->where( "email", $email );
		$this->db->where( "isDeleted", 0 );
		if ( $userId != 0 ) {
			$this->db->where( "userId !=", $userId );
		}
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to add new user to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewCustomer( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_customer', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}
	/**
	 * This function is used to add newbranch to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewBranch( $userInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_locationbranches', $userInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getCustomerInfo( $customerId ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_customer' );
		$this->db->where( 'customerid', $customerId );
		$query = $this->db->get();

		return $query->result();
	}


	/**
	 * This function is used to update the user information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editcustomer( $customerInfo, $customerid ) {
		$this->db->where( 'customerid', $customerid );
		$this->db->update( 'tbl_customer', $customerInfo );

		return TRUE;
	}



	/**
	 * This function is used to delete the Customer information
	 * @param number $customerid : This is Customer id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteCustomer( $customerid ) {
		$this->db->where( 'customerid', $customerid );
		$this->db->delete( 'tbl_customer' );

		return $this->db->affected_rows();
	}


	/**
	 * This function is used to get the customers listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function locationListingCount( $searchText = '', $customerid = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location as tl' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tl.locationname  LIKE '%" . $searchText . "%'
                            OR  tl.deviceid  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'tl.customerid', $customerid );
		}
		$this->db->where( 'tl.status', 'active' );
		$query = $this->db->get();

		return $query->num_rows();
	}


	/**
	 * This function is used to get the location listing count
	 */
	function locationListing( $searchText = '', $page = '', $segment = '', $customerid = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location as tl' );
		$this->db->join( 'tbl_devicestatus as ds', 'ds.deviceid = tl.deviceid', 'left' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(tl.locationname  LIKE '%" . $searchText . "%'
                            OR  tl.deviceid  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'tl.customerid', $customerid );
		}
		//$this->db->where('tl.status', 'active');
		//$this->db->limit($page, $segment);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}

	/**
	 * This function is used to add new user to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewLocation( $locInfo ) {
		$this->db->trans_start();
		$this->db->insert( 'tbl_location', $locInfo );

		$insert_id = $this->db->insert_id();

		$this->db->trans_complete();

		return $insert_id;
	}

	//to get Latitude and Longitude from database
	function getLatitude( $locationid ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationId', $locationid );
		}
		$query = $this->db->get();

		$result = $query->result();
		//pre($result);
		return $result;
	}



	/**
	 * This function is used to delete the Location information
	 * @param number $locationid : This is Location id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteLocation( $locationid ) {
		$this->db->where( 'locationid', $locationid );
		$this->db->delete( 'tbl_location' );

		return $this->db->affected_rows();
	}
	/**
	 * This function is used to update the location information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editlocation( $locationInfo, $locationid ) {
		$this->db->where( 'locationid', $locationid );
		$this->db->update( 'tbl_location', $locationInfo );

		return TRUE;
	}


	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getLocationInfo(  ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );
		//$this->db->where_in( 'locationid', $locationId );
		$query = $this->db->get();

		return $query->result();
	}
	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getLocationInfobyCustomerID( $customerId ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_location' );
		$this->db->where( 'customerid', $customerId );
		$query = $this->db->get();

		return $query->result();
	}
	/**
	 * This function is used to get the branch listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function branchListingCount( $searchText = '', $customerid = '' ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_locationbranches as lb' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(lb.bname  LIKE '%" . $searchText . "%'
                            OR  lb.bip  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'lb.customerid', $customerid );
		}
		$this->db->where( 'lb.status', 'active' );
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function is used to get the branch listing count
	 */
	function branchListing( $searchText = '', $page = '', $segment = '', $customerid ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_locationbranches as lb' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(lb.bname  LIKE '%" . $searchText . "%'
                            OR  lb.bip  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'lb.customerid', $customerid );
		}
		$this->db->where( 'lb.status', 'active' );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to delete the branch information
	 * @param number $branchid : This is branch id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteBranch( $branchid ) {
		$this->db->where( 'branchid', $branchid );
		$this->db->delete( 'tbl_locationbranches' );

		return $this->db->affected_rows();
	}
	/**
	 * This function is used to update the location information
	 * This function is used to update the location information
	 * @param array $userInfo : This is users updated information
	 * @param number $userId : This is user id
	 */
	function editbranch( $branchInfo, $branchid ) {
		$this->db->where( 'branchid', $branchid );
		$this->db->update( 'tbl_locationbranches', $branchInfo );

		return TRUE;
	}

	/**
	 * This function used to get user information by id
	 * @param number $userId : This is user id
	 * @return array $result : This is user information
	 */
	function getBranchInfo( $branchId ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_locationbranches' );
		$this->db->where( 'branchid', $branchId );
		$query = $this->db->get();

		return $query->result();
	}
	/*
	 *This function is used to to get USers mac ID
	 * 
	 */
	function getuserMacID( $username, $lastlogin, $locationid ) {
		$this->db->distinct( 'username' );
		$this->db->select( 'callingstationid,framedipaddress' );
		$this->db->from( 'radacct as rad' );
		$this->db->where( 'username', $username );
		$this->db->where( 'acctstarttime > DATE_SUB(NOW(),INTERVAL 1 DAY)' );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to update the userprofiles information
	 */
	function updateuserprofilestables( $username, $usersMacID, $frameipaddress, $locationid ) {
		$this->db->set( 'macid', $usersMacID );
		$this->db->set( 'ip', $frameipaddress );
		$this->db->where( 'username', $username );
		$this->db->where( 'locationid', $locationid );
		$this->db->update( 'tbl_userprofiles' );

		return TRUE;
	}
	/**
	 * This function is used to get phone number without with missing mac id
	 */
	function getUsersWithoutMac() {
		$this->db->select( 'username,lastlogin,locationid' );
		$this->db->from( 'tbl_userprofiles' );
		$this->db->where( 'macid', '' );
		$query = $this->db->get();

		$result = $query->result();
		return $result;

	}
	/**
	 * This function is used to get the wifivisitors listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function wifivisitorsListingCount( $searchText, $customerid = '', $locationid = '' ) {
		$this->db->select( 'up.*,lo.locationname' );
		$this->db->from( 'tbl_userprofiles as up' );
		$this->db->join( 'tbl_location as lo', 'lo.locationid = up.locationid' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(up.name  LIKE '%" . $searchText . "%'
                            OR  up.surname  LIKE '%" . $searchText . "%'
                            OR  up.username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'up.customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'up.locationid', $locationid );
		}
		$this->db->order_by( "up.userid", "desc" );
		$query = $this->db->get();
		return $query->num_rows();
	}
	/**
	 * This function is used to get the custmer listing 
	 */
	function wifivisitorsListing( $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
	
		$this->db->select( 'up.*,lo.locationname' );
		$this->db->from( 'tbl_userprofiles as up' );
		$this->db->join( 'tbl_location as lo', 'lo.locationid = up.locationid' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(up.name  LIKE '%" . $searchText . "%'
                            OR  up.surname  LIKE '%" . $searchText . "%'
                            OR  up.username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'up.customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'up.locationid', $locationid );
		}
		$this->db->order_by( "up.userid", "desc" );
		//$this->db->where('up.status', 'active');
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to delete the wifi visitor information
	 * @param number $branchid : This is branch id
	 * @return boolean $result : TRUE / FALSE
	 */
	function deletewifivisitor( $userid ) {
		$this->db->where( 'userid', $userid );
		$this->db->delete( 'tbl_userprofiles' );

		return $this->db->affected_rows();
	}
	/**
	 * This function is used to get the permmission list
	 */
	function addremovepermlist( $searchText = '', $customerid = '', $locationid = '', $page, $segment ) {
		$this->db->select( 'black.username' );
		$this->db->from( 'tbl_blacklist as black' );
		if ( !empty( $locationid ) ) {
			$this->db->where_in( 'black.locationid', $locationid, 'FALSE' );
		}
		$sub_query2 = $this->db->get_compiled_select();

		$this->db->select( '*' );
		$this->db->from( 'tbl_permisionlist' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->where( "username NOT IN ($sub_query2)" );
		$this->db->order_by( "pid", "desc" );
		$this->db->group_by( "username" );
		//$this->db->where('up.status', 'active');
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to get the black list
	 */
	function addremoveblacklist( $searchText = '', $customerid = '', $locationid = '', $page ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_blacklist' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->order_by( "bid", "desc" );
		$this->db->group_by( "username" );
		//$this->db->where('up.status', 'active');
		$this->db->limit( $page );
		$query = $this->db->get();

		$result = $query->result();
		return $result;
	}
	/**
	 * This function is used to get the permissionlist listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function addremovepermlistCount( $searchText = '', $customerid = '', $locationid = '' ) {
		$this->db->select( 'black.username' );
		$this->db->from( 'tbl_blacklist as black' );
		if ( !empty( $locationid ) ) {
			$this->db->where_in( 'black.locationid', $locationid, 'FALSE' );
		}
		$sub_query2 = $this->db->get_compiled_select();

		$this->db->select( '*' );
		$this->db->from( 'tbl_permisionlist' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->where( "username NOT IN ($sub_query2)" );
		$this->db->order_by( "pid", "desc" );
		$this->db->group_by( "username" );
		//$this->db->where('up.status', 'active');
		///$this->db->limit($page, $segment);
		$query = $this->db->get();

		return $query->num_rows();
	}
	/**
	 * This function is used to get the blacklist listing count
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function addremoveblacklistCount( $searchText, $customerid, $locationid ) {
		$this->db->select( '*' );
		$this->db->from( 'tbl_blacklist' );
		if ( !empty( $searchText ) ) {
			$likeCriteria = "(username  LIKE '%" . $searchText . "%')";
			$this->db->where( $likeCriteria );
		}
		if ( !empty( $customerid ) ) {
			$this->db->where( 'customerid', $customerid );
		}
		if ( !empty( $locationid ) ) {
			$this->db->where( 'locationid', $locationid );
		}
		$this->db->order_by( "bid", "desc" );
		$this->db->group_by( "username" );
		//$this->db->where('up.status', 'active');
		$query = $this->db->get();

		return $query->num_rows();
	}
	/* This function will be used for editing permissino list 
	 */
	function editphonenumberpermmodel( $pid, $username, $oldusername ) {
		//echo $message;exit;
		$this->db->set( 'username', $username );
		$this->db->where( 'username', $oldusername );
		$this->db->update( 'tbl_permisionlist' );
		return TRUE;
	}
	/* This function will be used for editing black list  
	 */
	function editphonenumberblackmodel( $bid, $username ) {
		//echo $message;exit;
		$this->db->set( 'username', $username, $oldusername );
		$this->db->where( 'username', $oldusername );
		$this->db->update( 'tbl_blacklist' );
		return TRUE;
	}
	/* This function will be used for delete username from permissino list 
	 */
	function deletephonenumberpermmodel( $username ) {
		$this->db->delete( 'tbl_permisionlist', array( 'username' => $username ) );

		return TRUE;
	}
	/* This function will be used for delete username from black list  
	 */
	function deletephonenumberblackmodel( $username ) {
		$this->db->delete( 'tbl_blacklist', array( 'username' => $username ) );

		return TRUE;
	}
	/**
	 * This function is used to check whether number id is already exist or not in Blacklist     
	 */
	function checkNumberExists( $numbers, $customerid = 0 ) {
		$this->db->select( "username" );
		$this->db->from( "tbl_blacklist" );
		$this->db->where_in( "username", $numbers );
		$this->db->where( "customerid", $customerid );
		$query = $this->db->get();

		return $query->result();
	}
	/**
	 * This function is used to check whether number id is already exist or not in Blacklist     
	 */
	function checkNumberinBlacklist( $number ) {
		$this->db->select( "username" );
		$this->db->from( "tbl_blacklist" );
		$this->db->where( "username", $number );
		$this->db->limit( 1 );
		$query = $this->db->get();

		return $query->result();
	}
	/**
	 * This function is used to get all accesss logs   
	 */
	function getaccesslogs( $page, $segment ) {
		$this->db->select( "Time,MethodId,SchemeId,HostID,PortNo,Resource,UserId" );
		$this->db->from( "access_log" );
		$this->db->order_by( "Time", "desc" );
		$this->db->group_by( "Time" );
		$this->db->limit( $page, $segment );
		$query = $this->db->get();

		return $query->result();
	}
	/**
	 * This function is used to get all counts of accesss logs   
	 */
	function getaccesslogscount() {
		$this->db->select( "Time,MethodId,SchemeId,HostId,PortNo,Resource,UserId" );
		$this->db->from( "access_log" );
		$this->db->group_by( "Time" );
		$query = $this->db->get();

		return $query->num_rows();
	}

}