<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "login";
$route['404_override'] = 'myerror';


/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";

$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['login-history'] = "user/loginHistoy";
$route['login-history/(:num)'] = "user/loginHistoy/$1";
$route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";
$route['instantMessages'] = 'instantmessages';
$route['startinstantmessages/(:any)'] = 'instantmessages/startinstantmessages/$1';
$route['startSendingInstantMessages'] = 'instantmessages/startSendingInstantMessages';
$route['importcsvdataInstantmessage'] = 'instantmessages/importcsvdataInstantmessage/';
$route['advancecampaignlist'] = 'instantmessages/advancecampaignlist/';
//$route['importcsvdataInstantmessage/(:any)'] = 'instantmessages/importcsvdataInstantmessage/$1';
$route['importcsvdataInstantmessage/(:num)'] = 'instantmessages/importcsvdataInstantmessage/$1';
$route['setInstantenddate'] = 'instantmessages/setInstantenddate';
$route['setInstantstartdate'] = 'instantmessages/setInstantstartdate';
$route['setInstantstatus/(:num)/(:num)/(:num)/(:num)/(:any)'] = 'instantmessages/setInstantstatus/$1/$2/$3/$4/$5';
$route['setInstantMessagetype'] = 'instantmessages/setInstantMessagetype';
$route['setInstantMessagetypexhour'] = 'instantmessages/setInstantMessagetypexhour';
$route['stopinstantmessages'] = 'instantmessages/stopinstantmessages';
$route['showcustomers'] = 'customerlocations/showcustomers';
//routes for courses
$route['courses'] = 'courses/showcourses';
$route['addNewcourse'] = "courses/addNewcourse";
$route['editcourse/(:any)'] = "courses/editcourse/$1";
//$route['editcus/(:any)'] = "customerlocations/editcus/$1";
$route['editcoursedata'] = "Courses/editcoursedata";
//routes for classrooms
$route['classroom'] = 'Classroom/showclassroom';
$route['addNewclassroom'] = "Classroom/addNewclassroom";

$route['showcustomers/(:num)'] = 'customerlocations/showcustomers/$1';
$route['showlocations'] = 'customerlocations/showlocations';
$route['showbranches'] = 'customerlocations/showbranches';
$route['addNewCustomer'] = "customerlocations/addNewCustomer";
$route['addNewcus'] = "customerlocations/addNewcus";
$route['deletecustomer'] = "customerlocations/deletecustomer";


$route['editcustomer'] = "customerlocations/editcustomer";
$route['addNewloc'] = "customerlocations/addNewloc";
$route['addNewLocation'] = "customerlocations/addNewLocation";
$route['deletelocation'] = "customerlocations/deletelocation";
$route['editloc/(:any)'] = "customerlocations/editloc/$1";
$route['editlocation'] = "customerlocations/editlocation";
$route['addNewBranch'] = "customerlocations/addNewBranch";
$route['addNewacc'] = "customerlocations/addNewacc";
$route['deletebranch'] = "customerlocations/deletebranch";
$route['editbra/(:any)'] = "customerlocations/editbra/$1";
$route['editbranch'] = "customerlocations/editbranch";
$route['wifivisitors'] = "customerlocations/wifivisitors";
$route['wifivisitors/(:num)'] = "customerlocations/wifivisitors/$1";
$route['DeleteNumbers/(:num)'] = "DeleteNumbers/index/$1";
$route['wifivisitors/(:num)/(:any)/(:any)'] = "customerlocations/wifivisitors/$1/$2/$3";
$route['deletewifivisitor'] = "customerlocations/deletewifivisitor";
$route['getallLocationbyCustomerID'] = "instantmessages/getallLocationbyCustomerID";
$route['userstats'] = "stats/userstats";
$route['checkuserstatus'] = "stats/checkuserstatus";
$route['userstats/(:num)'] = "stats/userstats/$1";
$route['barcharts'] = "stats/barcharts";
$route['barchartsdata'] = "stats/barchartsdata";
$route['bulkmessages'] = "instantmessages/bulkmessages";
$route['bulkmessagestogw'] = "instantmessages/bulkmessagestogw";
$route['getphonenumbersbylocation'] = "instantmessages/getphonenumbersbylocation";
$route['deleteCampaign'] = "instantmessages/deleteCampaign";
$route['editInstantmessage'] = "instantmessages/editInstantmessage";
$route['addremovepblist'] = "customerlocations/addremovepblist";
$route['addremovepblist/(:num)'] = "customerlocations/addremovepblist/$1";
$route['addremovepblist/(:num)/(:num)'] = "customerlocations/addremovepblist/$1/$2";
$route['editphonenumberperm'] = "customerlocations/editphonenumberperm";
$route['editphonenumberblack'] = "customerlocations/editphonenumberblack";
$route['deletephonenumberperm'] = "customerlocations/deletephonenumberperm";
$route['deletephonenumberblack'] = "customerlocations/deletephonenumberblack";
$route['addremovepermblacklist'] = "customerlocations/addremovepermblacklist";
$route['downloadexcel/(:num)/(:num)/(:any)'] = "stats/download/$1/$2/$3";
$route['addblacknumbertodb'] = "customerlocations/addblacknumbertodb";
$route['addpermnumbertodb'] = "customerlocations/addpermnumbertodb";
$route['cronscript'] = "Cronscript";
$route['getalltargetgroupsbyCustomerID'] = "instantmessages/getalltargetgroupsbyCustomerID";
$route['smsLimit'] = "stats/smsLimitReport";
$route['smsStats'] = "stats/smsstats";
$route['addSmslimit'] = "stats/addsmsLimit";
$route['addNewSmsLimit'] = "stats/addNewSmsLimit";
$route['transactions'] = "stats/transactions";
//$route['edittrans/(:any)'] = "stats/edittrans/$1";
$route['edittrans'] = "stats/edittransac";
$route['beacon'] = "customerlocations/beacon";
$route['viewwifilogs'] = "customerlocations/viewwifilogs";
$route['viewwifilogs/(:num)'] = "customerlocations/viewwifilogs/$1";
$route['listVisitorActivity'] = "user/listVisitorActivity";
$route['userprofiledetails/(:any)'] = "user/userprofiledetails/$1";
$route['students'] = "courses/showstudents";
$route['addNewstudent'] = "courses/addNewstudent";
/* End of file routes.php */
/* Location: ./application/config/routes.php */