<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class Courses extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public
	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language
		$this->load->model( 'customerlocation_model' );
		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'user_model' );
		$this->load->model( 'course_model' );
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public
	function index() {
		$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "dashboard", $this->global, NULL, NULL );
	}
/**
	 * This function is used to load the courses list for student
	 */
	function showcourses() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );

			$data[ 'searchText' ] = $searchText;

			$this->load->library( 'pagination' );

			$count = $this->course_model->courseListingCount( $searchText );

			$returns = $this->paginationCompress( "showcustomers/", $count, 5 );
			// exit;
			$data[ 'courseRecord' ] = $this->course_model->courseListing( $searchText, $returns[ "page" ], $returns[ "segment" ] );
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Customer Listings';

			$this->loadViews( "courses", $this->global, $data, NULL );
		}
	}
	
	/**
	 * This function is used to load the data and send it to the database for adding new Course 
	 */
    function addNewcourse() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$this->form_validation->set_rules( 'cname', 'Course Name', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctitle', 'Course Title', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'cinstructor', 'Course Instructor', 'trim|required|max_length[500]' );
            $this->form_validation->set_rules( 'cname', 'Course Name', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctitle', 'Course Title', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'datetimepickervaluestart', 'start date', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'datetimepickervalueend', 'end date', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'usr_starttime', 'start time', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'usr_endtime', 'end time', 'trim|required|max_length[20]' );
			//$this->form_validation->set_rules( 'selectday', 'days of week', 'trim|required|max_length[60]' );
			if ( $this->form_validation->run() == FALSE ) {
				$this->addcourse();
			} else {   				
				$coursename = $this->input->post( 'cname' );
				$coursetitle = $this->input->post( 'ctitle' );
				$courseinstructor = $this->input->post( 'cinstructor' );
				$classroom=$this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
				$startdate=$this->input->post( 'datetimepickervaluestart' );
				$enddate=$this->input->post( 'datetimepickervalueend' );
				$starttime=$this->input->post( 'usr_starttime' );
				$endtime=$this->input->post( 'usr_endtime' );
				$selectday=$this->security->xss_clean( $this->input->post( 'selectday' ) );
				$days=implode(',', $selectday);
				//pre($days);
				//exit;
				$snewdate=date('Y-m-d h:i:s', strtotime($startdate));
				$enewdate=date('Y-m-d h:i:s', strtotime($enddate));				
				$courseInfo = array( 'course_name' => $coursename, 'course_title' => $coursetitle, 'course_instructor' => $courseinstructor, 'classroom_id' => $classroom , 'createdate' => date( 'Y-m-d H:i:s' )
				,'cstartdate' => $snewdate, 'cenddate' => $enewdate	,  'weekdays' => $days, 'cstarttime' => $starttime, 'cendtime' => $endtime);
				$result = $this->course_model->addNewCouse( $courseInfo );
				$lastinsertcustomerid = $result;
				if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'New Course created successfully' );
					//exit;
				} else {
					$this->session->set_flashdata( 'error', 'Course creation failed' );
				}

				redirect( 'addNewcourse' );
			}
		}
	}
	
	/**
	 * This function is used to load the add new course form
	 */
	function addcourse() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->model( 'course_model' );
			$data[ 'roles' ] = $this->customerlocation_model->getUserRoles();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$data[ 'classroominfo' ] = $this->customerlocation_model->getLocationInfo(  );			
			$data[ 'location' ] = $this->customerlocation_model->getLocationInfo(  );
			
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Add New Customer';

			$this->loadViews( "addNewCourse", $this->global, $data, NULL );
		}
	}
	
	/**
	 * This function is used to edit Courses information
	 * @param number $userId : Optional : This is user id
	 */
	function editcourse( $courseid = NULL ) {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			if ( $courseid == null ) {
				redirect( 'courses' );
			}
			$this->load->model( 'course_model' );
			$mycourse=$this->course_model->getcourses( $courseid );
			$data[ 'classroominfo' ] = $this->customerlocation_model->getLocationInfo(  );	
			$data[ 'courseinfo' ] = $this->course_model->getcourses( $courseid );
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Edit Customer';

			$this->loadViews( "editcourse", $this->global, $data, NULL );
		}
	}


	/**
	 * This function is used to edit the Courses information
	 */
	function editcoursedata() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$courseid = $this->input->post( 'courseid' );

			$this->form_validation->set_rules( 'coursename', 'Course Name', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'coursetitle', 'Course Title', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'courseinstructor', 'Course Instructor', 'trim|required|max_length[500]' );
			//$this->form_validation->set_rules( 'coursetitle', 'Course Title', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'datetimepickervaluestart', 'start date', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'datetimepickervalueend', 'end date', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'usr_starttime', 'start time', 'trim|required|max_length[20]' );
			$this->form_validation->set_rules( 'usr_endtime', 'end time', 'trim|required|max_length[20]' );
			//$this->form_validation->set_rules( 'selectday', 'Select Day', 'trim|required|max_length[20]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->editcourse( $courseid );
			} else {
				$coursename = $this->input->post( 'coursename' );
				$coursetitle = $this->input->post( 'coursetitle' );
				$courseinstructor = $this->input->post( 'courseinstructor' );
				$classroom=$this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
				$startdate=$this->input->post( 'datetimepickervaluestart' );
				$enddate=$this->input->post( 'datetimepickervalueend' );
				$starttime=$this->input->post( 'usr_starttime' );
				$endtime=$this->input->post( 'usr_endtime' );
				$selectday=$this->security->xss_clean( $this->input->post( 'selectday' ) );
				$days=implode(',', $selectday);
				$snewdate=date('Y-m-d h:i:s', strtotime($startdate));
				$enewdate=date('Y-m-d h:i:s', strtotime($enddate));	
				$courseInfo = array( 'course_name' => $coursename, 'course_title' => $coursetitle, 'course_instructor' => $courseinstructor, 'classroom_id' => $classroom , 'createdate' => date( 'Y-m-d H:i:s' )
				,'cstartdate' => $snewdate, 'cenddate' => $enewdate	,  'weekdays' => $days, 'cstarttime' => $starttime, 'cendtime' => $endtime);
				$result = $this->course_model->editcourse( $courseInfo, $courseid );
				$lastinsertcustomerid = $result;
				if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'New Course created successfully' );
					//exit;
				} else {
					$this->session->set_flashdata( 'error', 'Course creation failed' );
				}

				redirect( 'courses' );
			}
		}
	}
	
	
	
        /**
	 * This function is used show all students
	 */
	function showstudents() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );

			$data[ 'searchText' ] = $searchText;

			$this->load->library( 'pagination' );

			$count = $this->course_model->studentListingCount( $searchText );

			$returns = $this->paginationCompress( "showstudents/", $count, 5 );
			// exit;
			$data[ 'studentRecord' ] = $this->course_model->studentListing( $searchText, $returns[ "page" ], $returns[ "segment" ] );
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Customer Listings';

			$this->loadViews( "students", $this->global, $data, NULL );
		}
	}
        /**
	 * This function is used to load the add new student
         */
	function addNewstudent() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->model( 'course_model' );
			$data[ 'courses' ] = $this->course_model->getallcourses();
                        $data[ 'locations' ] = $this->course_model->getalllocations();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Add New Customer';

			$this->loadViews( "addNewstudent", $this->global, $data, NULL );
		}
	}

	
}

?>