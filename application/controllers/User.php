<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class User extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public

	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language

		$this->load->model( 'customerlocation_model' );
		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'user_model' );
		$this->load->model( 'stats_model' );
		$test = $this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public

	function index() {

		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$this->global[ 'pageTitle' ] = 'LOCUS - ' . $this->customername . ' : Dashboard';
			$usercid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
			$usercid = '';
		}

		$data[ 'topsms' ] = $this->user_model->totalsmssent( $usercid );
		$loc = '';
		$data[ 'totalsmssent' ] = $this->user_model->totalsmssent( $usercid );
		//for all locations	  
		$myvalue1 = $this->user_model->getAverageAllTime( $usercid );
		$latValu = $this->customerlocation_model->getLatitude( $loc );
		$data[ 'locvalue' ] = $this->customerlocation_model->getLatitude( $loc );
		$dailytime = $myvalue1[ 0 ]->duration;
		if ( $dailytime < 60 ) {
			$sure = $dailytime . " seconds";
		} else {
			$gun = floor( $dailytime / 86400 );
			$saat = floor( ( $dailytime - $gun * 86400 ) / 3600 );
			$dakika = floor( ( $dailytime - $gun * 86400 - $saat * 3600 ) / 60 );
			$sure = "";
			if ( $dakika > 0 ) {
				$sure = $dakika . " minutes";
			}
			if ( $saat > 0 ) {
				$sure = $saat . " hours " . $sure;
			}
			if ( $gun > 0 ) {
				$sure = $gun . " day " . $sure;
			}
		}
		$data[ 'dailyavg' ] = $sure;
		$data[ 'totaluniqueusers' ] = $this->user_model->totaluniqueusers( $usercid );
		$data[ 'totaluniqueusers1' ] = $this->user_model->entries( $usercid );
		$countuniq = $this->user_model->totaluniqueusersdaily( $usercid );
		$data[ 'latitude' ] = 39.31872003597986;
		$data[ 'longitude' ] = 32.539612589551325;
		$data[ 'zoom' ] = 6;

		//$data[]=
		//to get form data here in this page
		if ( !empty( $this->input->post() ) ) {
			$postdata = $this->input->post();

		}
		$data[ 'today' ] = 0;

		if ( !empty( $postdata[ 'today' ] ) )
			if ( $postdata[ 'today' ] == 'today' ) {

				$loc = $postdata[ 'selectlocationmsg' ];
				$myvalue = $this->user_model->sentSms( $loc );
				$data[ 'totalsmssent' ] = $myvalue[ 0 ]->count;
				//for daily average time
				$myvalue1 = $this->user_model->getDailyAverageTime( $loc );
				$dailytime = $myvalue1[ 0 ]->duration;
				if ( $dailytime < 60 ) {
					$sure = $dailytime . " seconds";
				} else {
					$gun = floor( $dailytime / 86400 );
					$saat = floor( ( $dailytime - $gun * 86400 ) / 3600 );
					$dakika = floor( ( $dailytime - $gun * 86400 - $saat * 3600 ) / 60 );
					$sure = "";
					if ( $dakika > 0 ) {
						$sure = $dakika . " minutes";
					}
					if ( $saat > 0 ) {
						$sure = $saat . " hours " . $sure;
					}
					if ( $gun > 0 ) {
						$sure = $gun . " day " . $sure;
					}
				}

				$data[ 'dailyavg' ] = $sure;
				//this code is for daily unique users
				$countuniq = $this->user_model->totaluniqueusersdaily( $loc );
				$data[ 'totaluniqueusers1' ] = $countuniq;

				$latValu = $this->customerlocation_model->getLatitude( $loc );
				$data[ 'latitude' ] = $latValu[ '0' ]->lat;
				$data[ 'longitude' ] = $latValu[ '0' ]->long;
				$data[ 'zoom' ] = 12;
				//pre($latitude); 
			} else {
				$data[ 'today' ] = 0;
				$data[ 'totalsmssent' ] = $this->user_model->totalsmssent( $usercid );
			}
		if ( !empty( $postdata[ 'week' ] ) )
			if ( $postdata[ 'week' ] == 'week' ) {
				$data[ 'zoom' ] = 8;
				$monday = date( 'Y-m-d', strtotime( 'previous monday', strtotime( date( 'Y-m-d' ) ) ) );
				$sunday = date( 'Y-m-d', strtotime( 'sunday', strtotime( date( 'Y-m-d' ) ) ) );
				$loc = $postdata[ 'selectlocationmsg' ];
				$myweek = $this->user_model->sentSmsWeekly( $loc, $monday, $sunday );
				$data[ 'totalsmssent' ] = $myweek[ 0 ]->count;
				$data[ 'week' ] = 1;
				//for Weekly Average Time
				$myvalue1 = $this->user_model->getWeeklyAverageTime( $monday, $sunday, $loc );
				$dailytime = $myvalue1[ 0 ]->duration;
				if ( $dailytime < 60 ) {
					$sure = $dailytime . " seconds";
				} else {
					$gun = floor( $dailytime / 86400 );
					$saat = floor( ( $dailytime - $gun * 86400 ) / 3600 );
					$dakika = floor( ( $dailytime - $gun * 86400 - $saat * 3600 ) / 60 );
					$sure = "";
					if ( $dakika > 0 ) {
						$sure = $dakika . " minutes";
					}
					if ( $saat > 0 ) {
						$sure = $saat . " hours " . $sure;
					}
					if ( $gun > 0 ) {
						$sure = $gun . " day " . $sure;
					}
				}
				$data[ 'dailyavg' ] = $sure;
				//this code is for weekly unique users with respect to location
				$countuniq = $this->user_model->totaluniqueusersweekly( $monday, $sunday, $loc );
				$data[ 'totaluniqueusers1' ] = $countuniq;
				$latValu = $this->customerlocation_model->getLatitude( $loc );
				$data[ 'zoom' ] = 12;
				$data[ 'latitude' ] = $latValu[ '0' ]->lat;
				$data[ 'longitude' ] = $latValu[ '0' ]->long;
				$data[ 'zoom' ] = 12;
			} else {
				$data[ 'today' ] = 0;




			}
		if ( !empty( $postdata[ 'month' ] ) )
			if ( $postdata[ 'month' ] == 'month' ) {
				$loc = $postdata[ 'selectlocationmsg' ];
				$myweek = $this->user_model->sentSmsMonthly( $postdata, $loc );
				$data[ 'totalsmssent' ] = $myweek[ 0 ]->count;
				$data[ 'week' ] = 1;
				//for Monthly Average Time
				$myvalue1 = $this->user_model->getMonthlyAverageTime( $loc );
				$dailytime = $myvalue1[ 0 ]->duration;
				if ( $dailytime < 60 ) {
					$sure = $dailytime . " seconds";
				} else {
					$gun = floor( $dailytime / 86400 );
					$saat = floor( ( $dailytime - $gun * 86400 ) / 3600 );
					$dakika = floor( ( $dailytime - $gun * 86400 - $saat * 3600 ) / 60 );
					$sure = "";
					if ( $dakika > 0 ) {
						$sure = $dakika . " minutes";
					}
					if ( $saat > 0 ) {
						$sure = $saat . " hours " . $sure;
					}
					if ( $gun > 0 ) {
						$sure = $gun . " day " . $sure;
					}
				}
				$data[ 'dailyavg' ] = $sure;
				$dailytime = $myvalue1[ 0 ]->duration;
				if ( $dailytime < 60 ) {
					$sure = $dailytime . " seconds";
				} else {
					$gun = floor( $dailytime / 86400 );
					$saat = floor( ( $dailytime - $gun * 86400 ) / 3600 );
					$dakika = floor( ( $dailytime - $gun * 86400 - $saat * 3600 ) / 60 );
					$sure = "";
					if ( $dakika > 0 ) {
						$sure = $dakika . " minutes";
					}
					if ( $saat > 0 ) {
						$sure = $saat . " hours " . $sure;
					}
					if ( $gun > 0 ) {
						$sure = $gun . " day " . $sure;
					}
				}
				$data[ 'dailyavg' ] = $sure;
				//this code is for monthly unique users with respect to location
				$countuniq = $this->user_model->totaluniqueusersmonthly( $postdata, $loc );
				$data[ 'totaluniqueusers1' ] = $countuniq;
				$latValu = $this->customerlocation_model->getLatitude( $loc );
				$data[ 'latitude' ] = $latValu[ '0' ]->lat;
				$data[ 'longitude' ] = $latValu[ '0' ]->long;
				$data[ 'zoom' ] = 12;


			}
		$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $loc );
		//	$v= $this->customerlocation_model->getLocationInfo($loc);
		//pre($v);
		$data[ 'totalloginattempts' ] = $this->user_model->totalloginattempts( $usercid );
		//$data['totaluniqueusers'] = $this->user_model->totaluniqueusers($usercid);
		//$data['totalsmssent'] = $this->user_model->totalsmssent($usercid);
		$data[ 'totalsmssent1' ] = $this->stats_model->smsLimit( $usercid );
		$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $usercid );
		$myloc = $this->instantmessage_model->getalllocationsbyidforcustomer( $usercid );
		$statsreport = $this->user_model->getUsersStats( $usercid );

		//for stats Bar

		 if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
          $data['locations'] = $this->instantmessage_model->getalllocationsbyidforcustomer($usercid);
        }else{
		$emptyValue=0;
         $data['locations'] = $this->instantmessage_model->getalllocationsbyidforcustomer($emptyValue);
		 $locationid = $this->input->post('selectlocationmsg');
	
        }
		
        
        foreach($statsreport as $record){
                        $upload[]=round($record->acctoutputoctets/1024/1024,0);
			$download[]=round($record->acctinputoctets/1024/1024,0);
                                             
                        }
                        if(!empty($upload)){
                            $upload = array_sum($upload);
                        }else{
                            $upload = 0;
                        }
                        
                        if(!empty($download)){
                            $download = array_sum($download);
                        }else{
                            $download = 0;
                        }
//                        $upload = array_sum($upload);
//                        $download = array_sum($download);
                        $data['upload'] = $upload;
                        $data['download'] = $download;
        //////////////////////////////////////charts start
                /////Monthly users
       if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customerid = str_replace("8791", "", $this->customeridsess);
        }else{
            $customerid = '';
        }
            $postdata = '';
            $locationid = '';
            //echo $customerid;
            $barchartdata = $this->stats_model->barchartsmonthly($postdata,$customerid,$locationid);
            //pre($barchartdata);
            $nameOfmonth = date('m', strtotime(date('Y-m-d')));
                $nameOfyear = date('Y', strtotime(date('Y-m-d')));
                
            $daysofmonth = cal_days_in_month(CAL_GREGORIAN, $nameOfmonth, $nameOfyear);

             $label = array();
                    for($i=1;$i<=$daysofmonth;$i++){
                       $label[] = $i;
                    }
            ///create array for y axis
                    $total1 = array();
                    for($i=1;$i<=$daysofmonth;$i++){
                       $total1[] = 0;
                    }
                    $shift = $total1;
                    $total1 = array_combine(range(1,count($shift)), array_values($shift));
                    
	    ////assign counts to each day
                foreach ($barchartdata as $tot) {
                    $nameOfdays = date('d', strtotime($tot->month));
                    for($i=1;$i<count($total1);$i++) {
                        if($i == (int)$nameOfdays) {
                          $total1[$i] = $tot->count;
                         }
                    }
                }
                
                $onearr=array();

                foreach ($total1 as $key => $val )
                    $onearr[ $key-1 ] = $val;
                
                $total1 = $onearr;

            $barchartdatatext = $this->stats_model->barchartstextmonthly($postdata,$customerid,$locationid);
            //pre($barchartdatatext);
            $labeltextmessage = array();
                    for($ii=1;$ii<=$daysofmonth;$ii++){
                       $labeltextmessage[] = $ii;
                    }
            ///create array for y axis
                    $textcount = array();
                    for($iii=1;$iii<=$daysofmonth;$iii++){
                       $textcount[] = 0;
                    }
                    $shifttextcount = $textcount;
                    $textcount = array_combine(range(1,count($shifttextcount)), array_values($shifttextcount));  
                    ////assign counts to each day
                foreach ($barchartdatatext as $totext) {
                    $nameOfdaysinmonth = date('d', strtotime($totext->month));
                    for($iiii=1;$iiii<count($textcount);$iiii++) {
                        if($iiii == (int)$nameOfdaysinmonth) {
                          $textcount[$iiii] = $totext->count;
                         }
                    }
                }
                
                $onearrtext=array();

                foreach ($textcount as $keys => $vals )
                    $onearrtext[ $keys-1 ] = $vals;
                
                $textcount = $onearrtext;

        //////Text messages per month end
        ////// Download and upload chart
        $statsreportfordashboard = $this->user_model->getUsersStatsfordashboard($customerid);
        //echo $this->db->last_query();
        //pre($statsreportfordashboard);
        ///create array for y axis  Upload  Count
                    $ducount = array();
                    for($m=1;$m<=$daysofmonth;$m++){
                       $ducount[] = 0;
                    }
                    $shiftducount = $ducount;
                    $ducount = array_combine(range(1,count($shiftducount)), array_values($shiftducount)); 
//        ////assign counts to each day
                foreach ($statsreportfordashboard as $todu) {
                    $nameOfdaysinmonthdu = date('d', strtotime($todu->month));
                    for($j=1;$j<count($ducount);$j++) {
                        if($j == (int)$nameOfdaysinmonthdu) {
                          $ducount[$j] = round($todu->uploadcount/1024/1024);
                         }
                    }
                }
         
                $onearrdu=array();

                foreach ($ducount as $keyd => $vald )
                    $onearrdu[ $keyd-1 ] = $vald;
                
                $ducount = $onearrdu;
                
                ///////Download Count
                $udcount = array();
                    for($f=1;$f<=$daysofmonth;$f++){
                       $udcount[] = 0;
                    }
                    $shiftudcount = $udcount;
                    $udcount = array_combine(range(1,count($shiftudcount)), array_values($shiftudcount)); 
//        ////assign counts to each day
                foreach ($statsreportfordashboard as $toud) {
                    $nameOfdaysinmonthud = date('d', strtotime($toud->month));
                    for($k=1;$k<count($udcount);$k++) {
                        if($k == (int)$nameOfdaysinmonthud) {
                          $udcount[$k] = round($toud->downloadcount/1024/1024);
                         }
                    }
                }
                
                $onearrud=array();

                foreach ($udcount as $keyu => $valu )
                    $onearrud[ $keyu-1 ] = $valu;
                
                $udcount = $onearrud;
        ///////Download and upload ended
        //////// start pie chart for count users per customer
        //pre($statsreportfordashboard);
        //exit;
        $usercountspercustomer = $this->user_model->countusersbycustomers();
        $customer1 = '';
        $customer2 = '';
        foreach($usercountspercustomer as $records){
            if($records->customerid == '7'){
            $customer1 = $records->count;
            }elseif($records->customerid == '8'){
            $customer2 = $records->count;
            }
        }
        $data['baytgate'] = json_encode($customer1);
        $data['elogo'] = json_encode($customer2);
//        pre($usercountspercustomer);
//        exit;
        //////////////end pie chart
       
        //
        
        if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
        //////add mac id to userprofiles
        $this->addMacIDstoUsers();
        /////////////////////////////////////// charts ended
        //
        //////////printing monthly data for view
        $data['label'] = json_encode($label);//json_encode($label);
	$data['result'] = json_encode($total1); 
        //////////printing monthly data for view end
        //////////printing monthly text messages data for view
        $data['labeltext'] = json_encode($labeltextmessage);//json_encode($label);
	$data['resulttext'] = json_encode($textcount); 
        //////////printing monthly text messages data for view end
        //////////printing monthly data download for view
	$data['resultupload'] = json_encode($ducount); 
        //////////printing monthly text messages data for view end
        //////////printing monthly data download for view
	$data['resultdownload'] = json_encode($udcount); 
        //////////printing monthly data for view end
        //$data['totaldownloadupload'] = $this->user_model->totaldownloadupload();
        //pre($data);exit;
        $data['onlineusersdata'] = $this->user_model->getallonlineusers($customeruserid,'5');
        $data['onlinedevicesdata'] = $this->user_model->getallusersdevices($customeruserid);
        $this->loadViews("dashboard", $this->global, $data , NULL);
    }
    /*
     *Add Mac Ids to user profiles $this->addMacIDstoUsers();
     *       */
    function addMacIDstoUsers(){
    $missingMacID = $this->customerlocation_model->getUsersWithoutMac();
           if(!empty($missingMacID)){            
            foreach($missingMacID as $macmis){
                $usersMacID = $this->customerlocation_model->getuserMacID($macmis->username,$macmis->lastlogin,$macmis->locationid);
                foreach($usersMacID as $haveMac){
                 $this->customerlocation_model->updateuserprofilestables($macmis->username,$haveMac->callingstationid,$haveMac->framedipaddress,$macmis->locationid);                       
                }
            }
           }
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
//        if($this->isAdmin() == TRUE)
//        {
//            $this->loadThis();
//        }      
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
                  $customerid = str_replace("8791", "", $this->customeridsess);
            }else{
                  $customerid = '';
            }
            $count = $this->user_model->userListingCount($searchText,$customerid);

			$returns = $this->paginationCompress ( "userListing/", $count, 5 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"],$customerid);
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
            $this->global['pageTitle'] = 'LOCUS : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
//        if($this->isAdmin() == TRUE)
//        {
//            $this->loadThis();
//        }
            $this->load->model('user_model');
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
                  $customerid = str_replace("8791", "", $this->customeridsess);
            }else{
                  $customerid = '';
            }
            $data['roles'] = $this->user_model->getUserRoles($customerid);
            $data['customers'] = $this->instantmessage_model->getallcustomers();
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
            $this->global['pageTitle'] = 'LOCUS : Add New User';
            $this->loadViews("addNew", $this->global, $data, NULL);
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
//        if($this->isAdmin() == TRUE)
//        {
//            $this->loadThis();
//        }
       
          
		  $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('customeruser','customeruser','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
			//exit;
			 $image = $_FILES['image']['name'];
			 
             // image file directory
  	          $target = base_url()."assets/dist/img/";
              $uploadOk = 1;
			  $uploadOk1 = 1;
			 // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
              //$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			  
              $check = getimagesize($_FILES["image"]["tmp_name"]);
   //Check whether user upload picture
            if(!empty($_FILES["image"]["name"])){
                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('image')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }
			
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $customerid = $this->input->post('customeruser');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile,'customerid'=>$customerid, 'image_file' =>$picture,  'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
//        if($this->isAdmin() == TRUE || $userId == 1)
//        {
//            $this->loadThis();
//        }
            if($userId == null)
            {
                redirect('userListing');
            }
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
                  $customerid = str_replace("8791", "", $this->customeridsess);
            }else{
                  $customerid = '';
            }
            $data['roles'] = $this->user_model->getUserRoles($customerid);
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            $data['customers'] = $this->instantmessage_model->getallcustomers();
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
            $this->global['pageTitle'] = 'LOCUS : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
//        if($this->isAdmin() == TRUE)
//        {
//            $this->loadThis();
//        }

 $image = $_FILES['image']['name'];

            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('customeruser','customeruser','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
			   // image file directory
  	          $target = base_url()."assets/dist/img/";
              $uploadOk = 1;
			  $uploadOk1 = 1;
			 // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
              //$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			  
              $check = getimagesize($_FILES["image"]["tmp_name"]);
   //Check whether user upload picture
            if(!empty($_FILES["image"]["name"])){
                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['image']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('image')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }
			
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = $this->security->xss_clean($this->input->post('email'));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $customerid = $this->input->post('customeruser');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile,'customerid'=>$customerid, 'image_file' =>$picture ,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile,'customerid'=>$customerid, 'image_file' =>$picture , 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
//        if($this->isAdmin() == TRUE)
//        {
//            echo(json_encode(array('status'=>'access')));
//        }
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
    }
    
    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
        $this->global['pageTitle'] = 'LOCUS : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }

    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'LOCUS : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
//        if($this->isAdmin() == TRUE)
//        {
//            $this->loadThis();
//        }
            $userId = ($userId == NULL ? $this->session->userdata("userId") : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 5, 3);
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'LOCUS : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
    }
	
	  function sentSmsdashboard() {    
			
		if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
		
            $customeruserid = str_replace("8791", "", $this->customeridsess);
			 $result = $this->stats_model->sentSms($customeruserid);
			   //$result1 = $this->stats_model->smsLimit($customeruserid);
			   pre($result);
			    $data['smssent'] = $result;
			    $this->global['pageTitle'] = 'LOCUS : User Statistics';
			   $this->loadViews("dashboard", $this->global, $data, NULL);
			   }
        }
        /*
        * list all the data for unique users
        */
        function listVisitorActivity(){
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            
            $data['uniqueusersdata'] = $this->user_model->getalluniqueusers($customeruserid);
            $data['countrecords'] = count($data['uniqueusersdata']);
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
            $this->global['pageTitle'] = 'LOCUS : Visitor Login History';
            
            $this->loadViews("visitorActivityList", $this->global, $data, NULL);
            
  
        }
        /*
        * show uer profile with all related data
        */
        function userprofiledetails($username){
            
            $username = base64_decode($username);
            $data['thisuserdata'] = $this->user_model->getthisuserdata($username);
            $dataofuser = $data['thisuserdata'];
            //echo $dataofuser[0]->ipaddress;
            //pre($data['thisuserdata']);
            $data['visitedplaces'] = $this->user_model->getallLocationVisitbyThisUser($username);   
            
            //pre($data['visitedplaces']);
            //exit;
            $data['usertimeline'] = $this->user_model->getThisUserTimeline($username);   
            //pre($data['usertimeline']);
            //exit;
            $data['webstats'] = $this->user_model->getwebstatsofThisIP($dataofuser[0]->ipaddress);   
            //pre($data['webstats']);
            //exit;
            if($this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE){
            $customeruserid = str_replace("8791", "", $this->customeridsess);
            }else{
                $customeruserid = '';
            }
            
//            $data['uniqueusersdata'] = $this->user_model->getalluniqueusers($customeruserid);
//            $data['countrecords'] = count($data['uniqueusersdata']);
            $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
            $this->global['pageTitle'] = 'LOCUS : User Profile';
            
            $this->loadViews("userprofiledata", $this->global, $data, NULL);
            
  
        }
	
}

?>