<?php
ini_set( 'max_execution_time', 0 );
ini_set( 'memory_limit', '2048M' );
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Cronscript
 * This Class will use for cron scrip means it will run everysecond on server from client side. If cron job from godaddy diddn't work this will workk
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 24 April 2018
 */
class DeleteNumbers extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public

	function __construct() {
		parent::__construct();

		$this->load->model( 'DeleteNumbersModel' );
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public

	function index( $number ) {
		//        $datevariable = date('dmYHi', strtotime('2018-05-09 14:03:19 + 1 days'));
		//        echo $datevariable;
		//        exit;
		$this->removeallnumbers( $number );
	}
	/*This functin used to delete numbers from all tables of database*/
	function removeallnumbers( $number ) {
		if ( strlen( $number ) == 10 ) {
			$radcheckdelete = $this->deleteradcheck( $number );
			echo 'Deleted <br>';
			$userprofilesdelete = $this->deleteuserprofiles( $number );
			echo 'Deleted <br>';
			$permlistdelete = $this->deletepermlist( $number );
			echo 'Deleted <br>';
			$blacklistdelete = $this->deleteblacklist( $number );
			echo 'Deleted <br>';
			$microsmsdelete = $this->deletemicrosms( $number );
			echo 'Deleted <br>';
			echo $number . ' is deleted from database';
		} else {
			echo 'Invalid Phone Number';
		}
	}
	/*This functin used to delete numbers from radcheck table of database*/
	function deleteradcheck( $number ) {
		$radcheckdelete = $this->DeleteNumbersModel->deleteradcheckmodel( $number );
		return $radcheckdelete;
	}
	/*This functin used to delete numbers from permissionlist*/
	function deletepermlist( $number ) {
		$permlistdelete = $this->DeleteNumbersModel->deletepermlistmodel( $number );
		return $permlistdelete;
	}
	/*This functin used to delete numbers from blacklist*/
	function deleteblacklist( $number ) {
		$blacklistdelete = $this->DeleteNumbersModel->deleteblacklistmodel( $number );
		return $blacklistdelete;
	}
	/*This functin used to delete microsms */
	function deletemicrosms( $number ) {
		$microsmsdelete = $this->DeleteNumbersModel->deletemicrosms( $number );
		return $microsmsdelete;
	}
	/*This functin used to delete users*/
	function deleteuserprofiles( $number ) {
		$userprofilesdelete = $this->DeleteNumbersModel->deleteuserprofiles( $number );
		return $userprofilesdelete;
	}

}

?>