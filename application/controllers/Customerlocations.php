<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class customerlocations extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public
	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language
		$this->load->model( 'customerlocation_model' );
		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'user_model' );
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public
	function index() {
		$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "dashboard", $this->global, NULL, NULL );
	}

	/**
	 * This function is used to load the user list
	 */
	function showcustomers() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );

			$data[ 'searchText' ] = $searchText;

			$this->load->library( 'pagination' );

			$count = $this->customerlocation_model->customerListingCount( $searchText );

			$returns = $this->paginationCompress( "showcustomers/", $count, 5 );

			$data[ 'customerRecords' ] = $this->customerlocation_model->customerListing( $searchText, $returns[ "page" ], $returns[ "segment" ] );
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Customer Listings';

			$this->loadViews( "customers", $this->global, $data, NULL );
		}
	}

	/**
	 * This function is used to load the add new form
	 */
	function addNewcus() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->model( 'customerlocation_model' );
			$data[ 'roles' ] = $this->customerlocation_model->getUserRoles();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Add New Customer';

			$this->loadViews( "addNewCustomer", $this->global, $data, NULL );
		}
	}

	/**
	 * This function is used to check whether email already exist or not
	 */
	function checkEmailExists() {
		$userId = $this->input->post( "userId" );
		$email = $this->input->post( "email" );

		if ( empty( $userId ) ) {
			$result = $this->user_model->checkEmailExists( $email );
		} else {
			$result = $this->user_model->checkEmailExists( $email, $userId );
		}

		if ( empty( $result ) ) {
			echo( "true" );
		} else {
			echo( "false" );
		}
	}

	/**
	 * This function is used to add new user to the system
	 */
	function addNewCustomer() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$this->form_validation->set_rules( 'customername', 'Customer Name', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'femail', 'Firm Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'address', 'Address', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'city', 'City', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'firmdescription', 'Firm Description', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctname', 'Contact Person', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctsurname', 'Surname', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctphonenumber', 'Phone Number', 'required|min_length[10]' );
			$this->form_validation->set_rules( 'ctemail', 'Contact Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'password', 'Password', 'required|max_length[20]' );
			$this->form_validation->set_rules( 'cpassword', 'Confirm Password', 'trim|required|matches[password]|max_length[20]' );
			$this->form_validation->set_rules( 'ctdescription', 'Contact Description', 'trim|required|max_length[500]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->addNewcus();
			} else {
				$customername = $this->input->post( 'customername' );
				$femail = $this->security->xss_clean( $this->input->post( 'femail' ) );
				$address = $this->input->post( 'address' );
				$city = $this->input->post( 'city' );
				$firmdescription = $this->input->post( 'firmdescription' );
				$ctname = $this->input->post( 'ctname' );
				$ctsurname = $this->input->post( 'ctsurname' );
				$ctphonenumber = $this->security->xss_clean( $this->input->post( 'ctphonenumber' ) );
				$ctemail = $this->security->xss_clean( $this->input->post( 'ctemail' ) );
				$password = $this->input->post( 'password' );
				$ctdescription = $this->input->post( 'ctdescription' );
				$cusername = preg_replace( "/[^a-zA-Z]/", "", $customername );

				$customerInfo = array( 'customername' => $customername, 'adress' => $address, 'city' => $city, 'email' => $femail,
					'cusername' => $cusername, 'contractdate' => date( 'Y-m-d H:i:s' ), 'description' => $firmdescription, 'ctname' => $ctname,
					'stsurname' => $ctsurname, 'ctphonenumber' => $ctphonenumber, 'ctemail' => $ctemail, 'ctdescription' => $ctdescription, 'status' => 'active' );
				$namenewcustomer = $ctname . ' ' . $ctsurname;

				$result = $this->customerlocation_model->addNewCustomer( $customerInfo );
				$lastinsertcustomerid = $result;
				$customeruniqueID = '8791' . $lastinsertcustomerid;
				$this->load->model( 'user_model' );


				$userInfo = array( 'email' => $ctemail, 'password' => getHashedPassword( $password ), 'roleId' => '4', 'name' => $namenewcustomer,
					'mobile' => $ctphonenumber, 'customerid' => $lastinsertcustomerid, 'createdBy' => $this->vendorId, 'createdDtm' => date( 'Y-m-d H:i:s' ) );
				$userresult = $this->user_model->addNewUser( $userInfo );

				if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'New Customer created successfully' );
				} else {
					$this->session->set_flashdata( 'error', 'Customer creation failed' );
				}

				redirect( 'addNewcus' );
			}
		}
	}


	/**
	 * This function is used load user edit information
	 * @param number $userId : Optional : This is user id
	 */
	function editcus( $customerid = NULL ) {
		if ( $this->isAdmin() == TRUE || $customerid == 1 ) {
			$this->loadThis();
		} else {
			if ( $customerid == null ) {
				redirect( 'showcustomers' );
			}
			$this->load->model( 'customerlocation_model' );
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Edit Customer';

			$this->loadViews( "editcustomer", $this->global, $data, NULL );
		}
	}


	/**
	 * This function is used to edit the customer information
	 */
	function editcustomer() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$customerid = $this->input->post( 'customerid' );

			$this->form_validation->set_rules( 'customername', 'Customer Name', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'femail', 'Firm Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'address', 'Address', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'city', 'City', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'firmdescription', 'Firm Description', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctname', 'Contact Person', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctsurname', 'Surname', 'trim|required|max_length[500]' );
			$this->form_validation->set_rules( 'ctphonenumber', 'Phone Number', 'required|min_length[10]' );
			$this->form_validation->set_rules( 'ctemail', 'Contact Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'ctdescription', 'Contact Description', 'trim|required|max_length[500]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->editcus( $customerid );
			} else {
				$customername = $this->input->post( 'customername' );
				$femail = $this->security->xss_clean( $this->input->post( 'femail' ) );
				$address = $this->input->post( 'address' );
				$city = $this->input->post( 'city' );
				$firmdescription = $this->input->post( 'firmdescription' );
				$ctname = $this->input->post( 'ctname' );
				$ctsurname = $this->input->post( 'ctsurname' );
				$ctphonenumber = $this->security->xss_clean( $this->input->post( 'ctphonenumber' ) );
				$ctemail = $this->security->xss_clean( $this->input->post( 'ctemail' ) );
				$ctdescription = $this->input->post( 'ctdescription' );


				$customerInfo = array( 'customername' => $customername, 'adress' => $address, 'city' => $city, 'email' => $femail,
					'description' => $firmdescription, 'ctname' => $ctname, 'stsurname' => $ctsurname, 'ctphonenumber' => $ctphonenumber, 'ctemail' => $ctemail, 'ctdescription' => $ctdescription, 'status' => 'active' );

				$this->load->model( 'customerlocation_model' );
				$result = $this->customerlocation_model->editcustomer( $customerInfo, $customerid );

				if ( $result == true ) {
					$this->session->set_flashdata( 'success', 'User updated successfully' );
				} else {
					$this->session->set_flashdata( 'error', 'User updation failed' );
				}

				redirect( 'showcustomers' );
			}
		}
	}


	/**
	 * This function is used to delete the user using userId
	 * @return boolean $result : TRUE / FALSE
	 */
	function deletecustomer() {
		if ( $this->isAdmin() == TRUE ) {
			echo( json_encode( array( 'status' => 'access' ) ) );
		} else {
			$customerid = $this->input->post( 'customerid' );
			$result = $this->customerlocation_model->deleteCustomer( $customerid );

			if ( $result > 0 ) {
				echo( json_encode( array( 'status' => TRUE ) ) );
			} else {
				echo( json_encode( array( 'status' => FALSE ) ) );
			}
		}
	}

	/**
	 * This function is used to load the user list
	 */
	function showlocations() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }       
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$data[ 'searchText' ] = $searchText;

		$this->load->library( 'pagination' );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customerid = str_replace( "8791", "", $this->customeridsess );

		} else {
			$customerid = '';
		}
		$count = $this->customerlocation_model->locationListingCount( $searchText, $customerid );

		//$returns = $this->paginationCompress ( "showlocations/", $count, 5 );

		$data[ 'locationRecords' ] = $this->customerlocation_model->locationListing( $searchText, '', '', $customerid );
		//pre($data['locationRecords']);
		//exit;
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Location Listings';

		$this->loadViews( "locations", $this->global, $data, NULL );
	}

	/**
	 * This function is used to load the add new form
	 */
	function addNewloc() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->model( 'customerlocation_model' );
		$data[ 'roles' ] = $this->customerlocation_model->getUserRoles();
		$data[ 'customers' ] = $this->customerlocation_model->customerListing( '', '', '' );
		$this->global[ 'pageTitle' ] = 'LOCUS : Add New Location';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "addNewLocation", $this->global, $data, NULL );
	}
	/**
	 * This function is used to add new locatıon to the system
	 */
	function addNewLocation() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'locationname', 'Location Name', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'deviceid', 'Device Mac ID', 'trim|required|max_length[128]' );
		$this->form_validation->set_rules( 'selectcustomer', 'Customer ID', 'trim|required|max_length[128]' );
		$this->form_validation->set_rules( 'address', 'Address', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'lat', 'Lat', 'trim|required|max_length[50]' );
		$this->form_validation->set_rules( 'long', 'Long', 'trim|required|max_length[50]' );
		$this->form_validation->set_rules( 'lanip1', 'Lan IP 1', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'lanip2', 'Lan Ip 2', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'sessioncontrol1', 'Session Control Hours', 'trim|required|max_length[5]|numeric' );
		$this->form_validation->set_rules( 'sessioncontrol2', 'Session Control Minutes', 'trim|required|max_length[5]|numeric' );

		if ( $this->form_validation->run() == FALSE ) {
			$this->addNewloc();
		} else {
			$locationname = $this->input->post( 'locationname' );
			$deviceid = $this->security->xss_clean( $this->input->post( 'deviceid' ) );
			$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomer' ) );
			$address = $this->input->post( 'address' );
			$lat = $this->input->post( 'lat' );
			$long = $this->input->post( 'long' );
			$lanip1 = $this->input->post( 'lanip1' );
			$lanip2 = $this->input->post( 'lanip2' );
			$sessioncontrol1 = $this->input->post( 'sessioncontrol1' );
			$sessioncontrol2 = $this->input->post( 'sessioncontrol2' );

			$locationInfo = array( 'locationname' => $locationname, 'deviceid' => $deviceid, 'customerid' => $customerid, 'address' => $address, 'lanip' => $lanip1, 'lanip2' => $lanip2, 'schours' => $sessioncontrol1, 'scminutes' => $sessioncontrol2, 'lat' => $lat, 'long' => $long,
				'status' => 'active' );

			$this->load->model( 'customerlocation_model' );
			$result = $this->customerlocation_model->addNewLocation( $locationInfo );

			if ( $result > 0 ) {
				$this->session->set_flashdata( 'success', 'New Location created successfully' );
			} else {
				$this->session->set_flashdata( 'error', 'Location creation failed' );
			}

			redirect( 'addNewloc' );
		}
	}


	/**
	 * This function is used to delete the location using locationId
	 * @return boolean $result : TRUE / FALSE
	 */
	function deletelocation() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            echo(json_encode(array('status'=>'access')));
		//        }
		$locationid = $this->input->post( 'locationid' );
		$result = $this->customerlocation_model->deleteLocation( $locationid );

		if ( $result > 0 ) {
			echo( json_encode( array( 'status' => TRUE ) ) );
		} else {
			echo( json_encode( array( 'status' => FALSE ) ) );
		}
	}
	/**
	 * This function is used load user edit information
	 * @param number $userId : Optional : This is user id
	 */
	function editloc( $locationid = NULL ) {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		if ( $locationid == null ) {
			redirect( 'showlocations' );
		}
		$this->load->model( 'customerlocation_model' );
		$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Edit Location';

		$this->loadViews( "editlocation", $this->global, $data, NULL );
	}


	/**
	 * This function is used to edit the location information
	 */
	function editlocation() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->library( 'form_validation' );

		$locationid = $this->input->post( 'locationid' );

		$this->form_validation->set_rules( 'locationname', 'Location Name', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'deviceid', 'Device Mac ID', 'trim|required|max_length[128]' );
		$this->form_validation->set_rules( 'address', 'Address', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'lat', 'Lat', 'trim|required|max_length[50]' );
		$this->form_validation->set_rules( 'long', 'Long', 'trim|required|max_length[50]' );
		$this->form_validation->set_rules( 'lanip1', 'Lan Ip 1', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'lanip2', 'Lan Ip 2', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'sessioncontrol1', 'Session Control Hours', 'trim|required|max_length[5]|numeric' );
		$this->form_validation->set_rules( 'sessioncontrol2', 'Session Control Minutes', 'trim|required|max_length[5]|numeric' );


		if ( $this->form_validation->run() == FALSE ) {
			$this->editloc( $locationid );
		} else {
			$locationname = $this->input->post( 'locationname' );
			$deviceid = $this->security->xss_clean( $this->input->post( 'deviceid' ) );
			$address = $this->input->post( 'address' );
			$lanip1 = $this->input->post( 'lanip1' );
			$lanip2 = $this->input->post( 'lanip2' );
			$lat = $this->input->post( 'lat' );
			$long = $this->input->post( 'long' );
			$sessioncontrol1 = $this->input->post( 'sessioncontrol1' );
			$sessioncontrol2 = $this->input->post( 'sessioncontrol2' );

			$locationInfo = array( 'locationname' => $locationname, 'deviceid' => $deviceid, 'address' => $address, 'lanip' => $lanip1, 'lanip2' => $lanip2, 'schours' => $sessioncontrol1, 'scminutes' => $sessioncontrol2,
				'status' => 'active', 'lat' => $lat,
				'long' => $long );

			$this->load->model( 'customerlocation_model' );
			$result = $this->customerlocation_model->editlocation( $locationInfo, $locationid );

			if ( $result == true ) {
				$this->session->set_flashdata( 'success', 'User updated successfully' );
			} else {
				$this->session->set_flashdata( 'error', 'User updation failed' );
			}

			redirect( 'showlocations' );
		}
	}
	/**
	 * This function is used to load the branches list
	 */
	function showbranches() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }      
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$data[ 'searchText' ] = $searchText;

		$this->load->library( 'pagination' );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customerid = str_replace( "8791", "", $this->customeridsess );

		} else {
			$customerid = '';
		}
		$count = $this->customerlocation_model->branchListingCount( $searchText, $customerid );

		//$returns = $this->paginationCompress ( "showbranches/", $count, 5 );

		$data[ 'branchRecords' ] = $this->customerlocation_model->branchListing( $searchText, '', '', $customerid );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Access Point Listings';

		$this->loadViews( "branches", $this->global, $data, NULL );
	}
	/**
	 * This function is used to load the add new access point form
	 */
	function addNewacc() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->model( 'customerlocation_model' );
		$data[ 'roles' ] = $this->customerlocation_model->getUserRoles();
		$data[ 'customers' ] = $this->customerlocation_model->customerListing( '', '', '' );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customerid = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->customerlocation_model->getLocationInfobyCustomerID( $customerid );
		} else {
			$data[ 'locations' ] = $this->customerlocation_model->locationListing( '', '', '' );
		}

		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Add New Access Point';

		$this->loadViews( "addNewBranch", $this->global, $data, NULL );
	}
	/**
	 * This function is used to add new user to the system
	 */
	function addNewBranch() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'selectcustomer', 'Select Customer', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'selectlocation', 'Select Location', 'trim|required|max_length[128]' );
		$this->form_validation->set_rules( 'bname', 'Branch Name', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'bip', 'Branch IP', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'description', 'Description', 'trim|required|max_length[500]' );

		if ( $this->form_validation->run() == FALSE ) {
			$this->addNewacc();
		} else {
			$selectcustomer = $this->input->post( 'selectcustomer' );
			$selectlocation = $this->security->xss_clean( $this->input->post( 'selectlocation' ) );
			$bname = $this->input->post( 'bname' );
			$bip = $this->input->post( 'bip' );
			$description = $this->input->post( 'description' );

			$branchInfo = array( 'bname' => $bname, 'bip' => $bip, 'locationid' => $selectlocation,
				'customerid' => $selectcustomer, 'status' => 'active', 'descriptrion' => $description );

			$this->load->model( 'customerlocation_model' );
			$result = $this->customerlocation_model->addNewBranch( $branchInfo );

			if ( $result > 0 ) {
				$this->session->set_flashdata( 'success', 'New Customer created successfully' );
			} else {
				$this->session->set_flashdata( 'error', 'Customer creation failed' );
			}

			redirect( 'addNewacc' );
		}
	}
	/**
	 * This function is used to delete the access point using branchid
	 * @return boolean $result : TRUE / FALSE
	 */
	function deletebranch() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            echo(json_encode(array('status'=>'access')));
		//        }
		$branchid = $this->input->post( 'branchid' );
		$result = $this->customerlocation_model->deleteBranch( $branchid );

		if ( $result > 0 ) {
			echo( json_encode( array( 'status' => TRUE ) ) );
		} else {
			echo( json_encode( array( 'status' => FALSE ) ) );
		}
	}
	/**
	 * This function is used load Branch edit information
	 * @param number $userId : Optional : This is user id
	 */
	function editbra( $branchid = NULL ) {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		if ( $branchid == null ) {
			redirect( 'transactions' );
		}
		$this->load->model( 'customerlocation_model' );
		$data[ 'branchinfo' ] = $this->customerlocation_model->getBranchInfo( $branchid );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Edit Branch';

		$this->loadViews( "editbranch", $this->global, $data, NULL );
	}


	/**
	 * This function is used to edit the branch information
	 */
	function editbranch() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		$this->load->library( 'form_validation' );

		$branchid = $this->input->post( 'branchid' );

		$this->form_validation->set_rules( 'bname', 'Branch Name', 'trim|required|max_length[500]' );
		$this->form_validation->set_rules( 'bip', 'Branch IP', 'trim|required|max_length[128]' );
		$this->form_validation->set_rules( 'descriptrion', 'Descriptrion', 'trim|required|max_length[500]' );


		if ( $this->form_validation->run() == FALSE ) {
			$this->editbra( $branchid );
		} else {
			$bname = $this->input->post( 'bname' );
			$bip = $this->security->xss_clean( $this->input->post( 'bip' ) );
			$descriptrion = $this->input->post( 'descriptrion' );

			$branchInfo = array( 'bname' => $bname, 'bip' => $bip, 'descriptrion' => $descriptrion,
				'status' => 'active' );

			$this->load->model( 'customerlocation_model' );
			$result = $this->customerlocation_model->editbranch( $branchInfo, $branchid );

			if ( $result == true ) {
				$this->session->set_flashdata( 'success', 'AP updated successfully' );
			} else {
				$this->session->set_flashdata( 'error', 'AP updation failed' );
			}

			redirect( 'showbranches' );
		}
	}
	/**
	 * This function is used to load the wifivisitor list
	 */
	function wifivisitors() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }    
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$data[ 'searchText' ] = $searchText;
		if ( $this->input->post( 'selectcustomermsg' ) != '' ) {
			$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
			$locationid = $this->security->xss_clean( $this->input->post( 'selectlocationmsg' ) );
			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );
		} else {
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customerid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customerid = '';
			}

			$locationid = '';
		}
		$this->addMacIDstoUsers();

		$count = $this->customerlocation_model->wifivisitorsListingCount( $searchText, $customerid, $locationid );
		$returns = $this->paginationCompress( "wifivisitors/", $count, 10 );
		$data[ 'wifivisitors' ] = $this->customerlocation_model->wifivisitorsListing( $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );

		$data[ 'countrecords' ] = $count;
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {

		}
		//pre($data);
		$this->global[ 'pageTitle' ] = 'LOCUS : WifiVisitors Listings';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "wifivisitors", $this->global, $data, NULL );
	}
	/*
	 *Add Mac Ids to user profiles $this->addMacIDstoUsers();
	 *       */
	function addMacIDstoUsers() {
		$missingMacID = $this->customerlocation_model->getUsersWithoutMac();
		if ( !empty( $missingMacID ) ) {
			foreach ( $missingMacID as $macmis ) {
				$usersMacID = $this->customerlocation_model->getuserMacID( $macmis->username, $macmis->lastlogin, $macmis->locationid );
				foreach ( $usersMacID as $haveMac ) {
					$this->customerlocation_model->updateuserprofilestables( $macmis->username, $haveMac->callingstationid, $haveMac->framedipaddress, $macmis->locationid );
				}
			}
		}
	}
	/**
	 * This function is used to delete wifivisitor
	 * @return boolean $result : TRUE / FALSE
	 */
	function deletewifivisitor() {
		if ( $this->isAdmin() == TRUE ) {
			echo( json_encode( array( 'status' => 'access' ) ) );
		} else {
			$wifiuserid = $this->input->post( 'wifiuserid' );
			$result = $this->customerlocation_model->deletewifivisitor( $wifiuserid );

			if ( $result > 0 ) {
				echo( json_encode( array( 'status' => TRUE ) ) );
			} else {
				echo( json_encode( array( 'status' => FALSE ) ) );
			}
		}
	}
	/**
	 * This function is used to add and remove permission and black list
	 * @return boolean $result : TRUE / FALSE
	 */
	function addremovepblist() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }       
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$data[ 'searchText' ] = $searchText;
		if ( $this->input->post( 'selectcustomermsg' ) != '' ) {
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customerid = str_replace( "8791", "", $this->customeridsess );

			} else {
				$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );

			}

			$locationid = $this->security->xss_clean( $this->input->post( 'selectlocationmsg' ) );
			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );
			
		} else {
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customerid = str_replace( "8791", "", $this->customeridsess );
				$locationid = '';
			} else {
				$customerid = '';
				$locationid = '';
			}
		}
		$this->addMacIDstoUsers();

		$countperm = $this->customerlocation_model->addremovepermlistCount( $searchText, $customerid, $locationid );
		$returns = $this->paginationCompress( "addremovepblist/", $countperm, 15 );
		$data[ 'permissionlist' ] = $this->customerlocation_model->addremovepermlist( $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );


		$this->load->library( 'pagination' );

		$countblack = $this->customerlocation_model->addremoveblacklistCount( $searchText, $customerid, $locationid );
		$config1[ 'base_url' ] = base_url() . 'addremovepblist/0/';
		$config1[ 'total_rows' ] = $countblack;
		$config1[ 'per_page' ] = 15;
		$config1[ 'uri_segment' ] = 3;
		$config1[ 'num_links' ] = 5;
		$config1[ 'full_tag_open' ] = '<nav><ul class="pagination">';
		$config1[ 'full_tag_close' ] = '</ul></nav>';
		$config1[ 'first_tag_open' ] = '<li class="arrow">';
		$config1[ 'first_link' ] = 'First';
		$config1[ 'first_tag_close' ] = '</li>';
		$config1[ 'prev_link' ] = 'Previous';
		$config1[ 'prev_tag_open' ] = '<li class="arrow">';
		$config1[ 'prev_tag_close' ] = '</li>';
		$config1[ 'next_link' ] = 'Next';
		$config1[ 'next_tag_open' ] = '<li class="arrow">';
		$config1[ 'next_tag_close' ] = '</li>';
		$config1[ 'cur_tag_open' ] = '<li class="active"><a href="#">';
		$config1[ 'cur_tag_close' ] = '</a></li>';
		$config1[ 'num_tag_open' ] = '<li>';
		$config1[ 'num_tag_close' ] = '</li>';
		$config1[ 'last_tag_open' ] = '<li class="arrow">';
		$config1[ 'last_link' ] = 'Last';
		$config1[ 'last_tag_close' ] = '</li>';
		$pagination_1 = new CI_Pagination();
		$pagination_1->initialize( $config1 );
		$data[ 'pagination_link_1' ] = $pagination_1->create_links();


		//$returns2 = $this->paginationCompress ( "addremovepblist/", $countblack, 10); 
		$data[ 'blacklist' ] = $this->customerlocation_model->addremoveblacklist( $searchText, $customerid, $locationid, $config1[ 'per_page' ] );
		//echo $this->db->last_query();
		//pre($data['blacklist']);
		$data[ 'countperm' ] = $countperm;
		$data[ 'countblack' ] = $countblack;

		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {}
		//pre($data);
		$this->global[ 'pageTitle' ] = 'LOCUS : Permission and Black Lists';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "addremovepblist", $this->global, $data, NULL );
	}
	/*
	 *edit permission list
	 * 
	 */
	function editphonenumberperm() {

		// POST data
		$postData = $this->input->post();
		$data = $this->customerlocation_model->editphonenumberpermmodel( $postData[ 'pid' ], $postData[ 'username' ], $postData[ 'oldusername' ] );

		echo json_encode( $data );

	}
	/*
	 *edit blacklist list
	 * 
	 */
	function editphonenumberblack() {

		// POST data
		$postData = $this->input->post();
		$data = $this->customerlocation_model->editphonenumberblackmodel( $postData[ 'bid' ], $postData[ 'username' ], $postData[ 'oldusername' ] );

		echo json_encode( $data );

	}
	/*
	 *edit permission list
	 * 
	 */
	function deletephonenumberperm() {

		// POST data
		$postData = $this->input->post();
		$data = $this->customerlocation_model->deletephonenumberpermmodel( $postData[ 'username' ] );

		echo json_encode( $data );

	}
	/*
	 *edit blacklist list
	 * 
	 */
	function deletephonenumberblack() {

		// POST data
		$postData = $this->input->post();
		$data = $this->customerlocation_model->deletephonenumberblackmodel( $postData[ 'username' ] );

		echo json_encode( $data );

	}
	/*
	 *add mannual numbers to blacklist list
	 * 
	 */
	function addblacknumbertodb() {

		// POST data
		if ( !empty( $this->input->post() ) ) {
			$currentdate = date( 'Y-m-d H:m:s' );
			$postData = $this->input->post( 'blackednumbers' );
			$customerid = $this->input->post( 'customerid' );
			$locationid = $this->input->post( 'locationid' );
			$postData = explode( ",", $postData );
			for ( $incr = 0; $incr <= count( $postData ); $incr++ ) {
				if ( isset( $postData[ $incr ] ) != '' ) {
					if ( strlen( $postData[ $incr ] ) == 10 ) {
						$datablack[] = array(
							'username' => $postData[ $incr ],
							'customerid' => $customerid,
							'locationid' => $locationid,
							'date' => $currentdate
						);
					} else {
						$message_flash = 'Number Format are invalid';
						$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
						redirect( 'addremovepblist' );
					}
				}
			}
			$this->instantmessage_model->insertblactlist( $datablack );

			$message_flash = ' GSM numbers are added to Black List!';
			$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
			redirect( 'addremovepblist' );
		}

	}
	/*
	 *add mannual numbers to blacklist list
	 * 
	 */
	function addpermnumbertodb() {

		// POST data
		if ( !empty( $this->input->post() ) ) {
			$currentdate = date( 'Y-m-d H:m:s' );
			$postData = $this->input->post( 'permittednumbers' );
			$customerid = $this->input->post( 'customerid' );
			$locationid = $this->input->post( 'locationid' );
			$postData = explode( ",", $postData );
			for ( $incr = 0; $incr <= count( $postData ); $incr++ ) {
				if ( isset( $postData[ $incr ] ) != '' ) {
					$dataperm[] = array(
						'username' => $postData[ $incr ],
						'customerid' => $customerid,
						'locationid' => $locationid,
						'date' => $currentdate
					);
				}
			}
			$notExist = $this->customerlocation_model->checkNumberExists( $postData, $customerid );
			if ( empty( $notExist ) ) {
				$this->instantmessage_model->insertpermisiionlist( $dataperm );
				$message_flash = ' GSM numbers are added to Permission List!';
			} else {
				$message_flash = 'The number you try to upload is already in blacklist. Please remove it first.';
			}
			$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
			redirect( 'addremovepblist' );
		}
	}
	/*
	 *add permission and blacklist
	 * 
	 */
	function addremovepermblacklist() {

		$postData = $this->input->post();
		//pre($postData);
		//exit;
		if ( !empty( $_FILES[ "csvlistfile" ][ "tmp_name" ] ) ) {
			$currentdate = date( 'Y-m-d H:m:s' );
			$customerid = $postData[ 'customerid' ];
			$locationIDs = $postData[ 'locationid' ];
			if ( $postData[ 'selectpermblack' ] == 'perm' ) {

				$permisionlistfile = $this->csvimport->get_array( $_FILES[ "csvlistfile" ][ "tmp_name" ], array( '112233' ) );
				$numbers_array = array();
				foreach ( $permisionlistfile as $array ) {
					foreach ( $array as $key => $val ) {
						//array_push($numbers_array,$key);
						array_push( $numbers_array, $val );
					}
				}
				//pre($numbers_array);
				//exit;
				$data[ 'permissioncount' ] = count( $numbers_array );

				for ( $incr = 0; $incr <= count( $numbers_array ); $incr++ ) {
					if ( isset( $numbers_array[ $incr ] ) != '' ) {
						$notExist = $this->customerlocation_model->checkNumberinBlacklist( $numbers_array[ $incr ] );
						if ( empty( $notExist ) ) {

							$dataperm[] = array(
								'username' => $numbers_array[ $incr ],
								'customerid' => $customerid,
								'locationid' => $locationIDs,
								'date' => $currentdate
							);

						} else {
							$foundinBlacklist[] = $notExist;
						}
					}
				}
				//pre($foundinBlacklist);
				//exit;
				if ( !empty( $dataperm ) ) {
					$this->instantmessage_model->insertpermisiionlist( $dataperm );
				}
				if ( empty( $foundinBlacklist ) ) {
					$message_flash = $data[ 'permissioncount' ] . ' GSM numbers are added to Permission List!';
				} else {
					$arraytostring = $this->multidimensional_array_to_single_dimension_array( $foundinBlacklist );
					//pre($arraytostring);
					$blacknumbers = implode( " ", $arraytostring );
					///exit;
					$message_flash = $blacknumbers . ' found in Blacklist, Cannot be added to Permission list ';
				}
				$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
				redirect( 'addremovepblist' );
			} elseif ( $postData[ 'selectpermblack' ] == 'black' ) {
				if ( !empty( $_FILES[ "csvlistfile" ][ "tmp_name" ] ) ) {
					$blacklistfile = $this->csvimport->get_array( $_FILES[ "csvlistfile" ][ "tmp_name" ], array( '112233' ) );
					$numbers_array = array();
					foreach ( $blacklistfile as $array ) {
						foreach ( $array as $key => $val ) {
							//array_push($numbers_array,$key);
							array_push( $numbers_array, $val );
						}
					}
					//                   pre($numbers_array);
					//exit;
					$data[ 'blacklistcount' ] = count( $numbers_array );
					for ( $incr = 0; $incr <= count( $numbers_array ); $incr++ ) {
						if ( isset( $numbers_array[ $incr ] ) != '' ) {
							$datablack[] = array(
								'username' => $numbers_array[ $incr ],
								'customerid' => $customerid,
								'locationid' => $locationIDs,
								'date' => $currentdate
							);
						}
					}

					$this->instantmessage_model->insertblactlist( $datablack );
					$message_flash = $data[ 'blacklistcount' ] . ' GSM numbers are added to Black List!';
					$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
					redirect( 'addremovepblist' );

				}
			}

		} else {
			$message_flash = 'Please upload csv file!';
			$this->session->set_flashdata( 'flsh_msg_permblack', $message_flash );
			redirect( 'addremovepblist' );
		}
	}
	/*php convert multidimensional array to single dimension array

	convert multidimensional array to single array php
	*/

	function multidimensional_array_to_single_dimension_array( $array ) {
		if ( !$array ) return false;
		$flat = array();
		$iterator = new RecursiveIteratorIterator( new RecursiveArrayIterator( $array ) );

		foreach ( $iterator as $value )$singhal[] = $value;
		return $singhal;
	}
	/**

	 * Beacon Integration
	 *      */
	function beacon() {
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Beacon';
		$this->loadViews( "beaconview", $this->global, '', NULL );
	}
	/**
	 * This function is used display wifi logs
	 */
	function viewwifilogs() {
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customerid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customerid = '';
		}
		$countlogs = $this->customerlocation_model->getaccesslogscount();
		$returns = $this->paginationCompress( "viewwifilogs/", $countlogs, 50 );
		$data[ 'visitedwebsites' ] = $this->customerlocation_model->getaccesslogs( $returns[ "page" ], $returns[ "segment" ] );
		$data[ 'countrecords' ] = $countlogs;
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customerid );
		$this->global[ 'pageTitle' ] = 'LOCUS : View Wifi Logs';
		$this->loadViews( "viewwifilogs", $this->global, $data, NULL );
	}

}

?>