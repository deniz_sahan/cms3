<?php
ini_set( 'max_execution_time', 0 );
ini_set( 'memory_limit', '2048M' );
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );
date_default_timezone_set( 'Europe/Istanbul' );
require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Cronscript
 * This Class will use for cron scrip means it will run everysecond on server from client side. If cron job from godaddy diddn't work this will workk
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 24 April 2018
 */
class Cronscript extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public

	function __construct() {
		parent::__construct();

		$this->load->model( 'Cron_model' );
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public

	function index() {
		$this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
		//        $this->startscript();
		//        sleep(3);
	}

	function startscript() {
		$currentdateforauth = date( 'Y-m-d 00:00:00' );
		$currentdate = date( 'Y-m-d H:i:s' );
		$deactivateExpireCampaign = $this->Cron_model->deactivateExpireCampaign( $currentdate );
		$deletemicrosmsrough = $this->Cron_model->deletemicrosmsrough();
		$this->sessioncontrol();
		$targetlist = $this->Cron_model->gettargetlist( $currentdateforauth );
		//pre($targetlist);
		//exit;
		if ( !empty( $targetlist ) ) {
			$checkblacklist = $this->Cron_model->checkblackanddelete( $targetlist[ 0 ]->username );
			//$expirydateforuser = $this->setlogoffdateforuser($targetlist[0]->lastlogin,$targetlist[0]->locationid,$targetlist[0]->username);
			if ( empty( $checkblacklist ) ) {
				$this->runscript( $targetlist[ 0 ]->userid, $targetlist[ 0 ]->targetusers );
			} else {
				$deletemicrosms = $this->Cron_model->deletemicrosms( $targetlist[ 0 ]->username );
			}

		} else {
			echo 'not found';
		}

	}
	/*it is getting data from database in every second*/
	function runscript( $userid, $targetlist ) {

		$result = $this->Cron_model->getonlineusers( $userid, $targetlist );
		//echo $this->db->last_query();
		//pre($result);
		//exit;
		if ( !empty( $result ) ) {

			foreach ( $result as $row ) {
				if ( $row->customerid == '11' ) {
					$originator = 'ALPET';
				} else {
					$originator = 'LOGOELEKT';
				}
				if ( $row->messagetype == 'everytime' ) {
					$smscode = $this->requesttosendmessage( $row->message, $row->username, $originator );
					if ( strlen( $smscode ) > 4 ) {
						$smsInfo = array( 'username' => $row->username, 'smscode' => $smscode, 'message' => $row->message, 'locationid' => $row->locationid,
							'customerid' => $row->customerid, 'inmid' => $row->inmid, 'sent' => '1', 'messagetype' => $row->messagetype, 'date' => date( 'Y-m-d H:i:s' ) );
						$smsinsert = $this->Cron_model->insertsentmessages( $smsInfo );
						if ( !empty( $smsinsert ) ) {
							$this->Cron_model->updatemicrolocationsms( $row->userid );
							$this->Cron_model->updateuserprofiles( $row->message, $row->userid );
							$this->Cron_model->updateinstantmessages( $row->inmid );
							$this->Cron_model->deletemicrosmsdata( $row->username );
						}
						$smsinsert = '';
					}
				} elseif ( $row->messagetype == 'eachday' ) {
					$resulteveryday = $this->Cron_model->checksentmessagesEveryday( $row->username, $row->lastlogin );
					//            echo $this->db->last_query();
					//            pre($resulteveryday);
					//            exit;
					if ( empty( $resulteveryday ) ) {
						$smscode = $this->requesttosendmessage( $row->message, $row->username, $originator );
						if ( strlen( $smscode ) > 4 ) {
							$smsInfo = array( 'username' => $row->username, 'smscode' => $smscode, 'message' => $row->message, 'locationid' => $row->locationid,
								'customerid' => $row->customerid, 'inmid' => $row->inmid, 'sent' => '1', 'messagetype' => $row->messagetype, 'date' => date( 'Y-m-d H:i:s' ) );
							$smsinsert = $this->Cron_model->insertsentmessages( $smsInfo );
							if ( !empty( $smsinsert ) ) {
								$this->Cron_model->updatemicrolocationsms( $row->userid );
								$this->Cron_model->updateuserprofiles( $row->message, $row->userid );
								$this->Cron_model->updateinstantmessages( $row->inmid );
								$this->Cron_model->deletemicrosmsdata( $row->username );
							}
							$smsinsert = '';
						}
					} else {
						$smscode = array( 'nothing', 'found' );
					}
				} elseif ( $row->messagetype == 'xhours' && $row->variabledatetype > 1 ) {
					$resultxhours = $this->Cron_model->checksentmessagesxhours( $row->username, $row->variabledatetype );
					//            echo $this->db->last_query();
					//            pre($resulteveryday);
					//            exit;
					//if(empty($resultxhours)){
					$datevariable = date( 'dmYHi', strtotime( $row->lastlogin . ' + ' . $row->variabledatetype . ' days' ) );
					$datevariable2 = date( 'Y-m-d H:i:s', strtotime( $row->lastlogin . ' + ' . $row->variabledatetype . ' days' ) );
					$smscode = $this->requesttosendmessagexhour( $row->message, $row->username, $datevariable, $originator );
					if ( strlen( $smscode ) > 4 ) {
						$smsInfo = array( 'username' => $row->username, 'smscode' => $smscode, 'message' => $row->message, 'locationid' => $row->locationid,
							'customerid' => $row->customerid, 'inmid' => $row->inmid, 'sent' => '1', 'messagetype' => $row->messagetype, 'date' => $datevariable2 );
						$smsinsert = $this->Cron_model->insertsentmessages( $smsInfo );
						if ( !empty( $smsinsert ) ) {
							$this->Cron_model->updatemicrolocationsms( $row->userid );
							$this->Cron_model->updateuserprofiles( $row->message, $row->userid );
							$this->Cron_model->updateinstantmessages( $row->inmid );
							$this->Cron_model->deletemicrosmsdata( $row->username );
						}
						$smsinsert = '';
					}
					//        }else{
					//            $smscode = array('xhours','found');
					//        }
				}
			}
			echo json_encode( $smscode );
		} else {
			echo 'No online users found';
		}

	}
	/*
	 * set loggoff time for users 
	 */
	function setlogoffdateforuser( $lastlogin, $locationid, $username ) {

		$getlogofftime = $this->Cron_model->getlogofftime( $locationid );
		$converttominuts = ( $getlogofftime[ 0 ]->schours * 60 ) + $getlogofftime[ 0 ]->scminutes;
		$expirydate = date( 'Y-m-d H:i:s', strtotime( '+' . $converttominuts . ' minutes', strtotime( $lastlogin ) ) );
		//insert int userslogoff table for further investigation
		$logoffInfo = array( 'username' => $username, 'expirydate' => $expirydate, );
		$inserttouserprofile = $this->Cron_model->insertuserslogoff( $logoffInfo );
	}
	/*
	 * sign off users 
	 */
	function sessioncontrol() {
		$currentdatetime = date( 'Y-m-d H:i:s' );
		$userreadyforsignoff = $this->Cron_model->controlsession( $currentdatetime );

		if ( !empty( $userreadyforsignoff ) ) {
			$logoffuserfromradcheck = $this->Cron_model->updateradchecktable( $userreadyforsignoff[ 0 ]->username );
			$expirthisuser = $this->Cron_model->updateuserlogofftable( $userreadyforsignoff[ 0 ]->username, $currentdatetime );
		}
	}
	/*this function is for sending SMS with using Mobildev API (without time request)*/
	function requesttosendmessage( $message, $number, $originator ) {
		///////mobiledev
		//exit;
		$ApiUrl = "http://8bit.mobilus.net/";
		$SmsUser = 'elogo-LG1001';
		$SmsPass = '8975';
		$Companyode = 'LG1001';
		//$originator = 'ALPET';
		$strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$number</Numbers><Originator>$originator</Originator><SDate></SDate></MainmsgBody>";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $strXML );
		$result = curl_exec( $ch );

		$pieces = explode( " ", $result );
		$smscode = $pieces[ 1 ]; // piece2
		curl_close( $ch );
		return $smscode;
	}
	/*this function is for sending SMS with using Mobildev API (with time request)*/
	function requesttosendmessagexhour( $message, $number, $date, $originator ) {
		///////mobiledev
		//exit;
		$ApiUrl = "http://8bit.mobilus.net/";
		$SmsUser = 'elogo-LG1001';
		$SmsPass = '8975';
		$Companyode = 'LG1001';
		//$originator = 'ALPET';
		$strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$number</Numbers><Originator>$originator</Originator><SDate>$date</SDate></MainmsgBody>";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $strXML );
		$result = curl_exec( $ch );

		$pieces = explode( " ", $result );
		$smscode = $pieces[ 1 ]; // piece2
		curl_close( $ch );
		return $smscode;
	}

}

?>