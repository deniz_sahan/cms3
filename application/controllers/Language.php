<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class Language extends CI_Controller {
	public
	/*This function is for using english language*/
	function english() {
		$this->session->unset_userdata( 'lang' );
		$this->session->set_userdata( 'lang', 'english' );
		//redirect('/','refresh');
		redirect( '/dashboard' );


	}
	public
	/*This function is for using turkish language*/
	function turkish() {
		$this->session->unset_userdata( 'lang' );
		$this->session->set_userdata( 'lang', 'turkish' );
		//redirect('/','refresh');
		redirect( '/dashboard' );


	}


}

?>