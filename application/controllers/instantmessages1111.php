<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class InstantMessages extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public
	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language

		$this->load->model( 'instantmessage_model' );
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public
	function index() {
		$this->global[ 'pageTitle' ] = 'LOCUS : InstantMessages';

		$this->loadViews( "messages", $this->global, NULL, NULL );
	}

	/**
	 * This function is used to load the user list
	 */
	function startinstantmessages() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			if ( $this->session->userdata( 'startdatemessage' ) == "" ) {
				$startdate = date( 'Y-m-d H:m:s' );
				$this->session->set_userdata( 'startdatemessage', $startdate );
				//echo "created new one".$this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			} else {
				///echo $this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			}


			$data[ 'searchText' ] = $searchText;
			//$data['searchText'] = '2018-01-25 09:41:01';
			//$count = $this->user_model->getphonenumbersforinstantmessage($searchText);

			$data[ 'userRecords' ] = $this->instantmessage_model->getphonenumbersforinstantmessage( $searchText );
			$data[ 'smsRecords' ] = $this->instantmessage_model->getallmessagesdata();

			//echo $this->db->last_query();
			//pre($data['userRecords']);
			//pre($data['smsRecords']);
			/// exit;

			$messagetext1 = 'Welcome to the Zone';
			$messagetext2 = 'coffee message';
			$location1 = '192.168.1.1';
			//$location2 = '192.168.2.1';
			$location3 = '192.168.3.1';
			$location4 = '192.168.4.1';
			foreach ( $data[ 'userRecords' ] as $record ) {
				$message1 = "Sayin " . $record->ad . " " . $record->soyad . " ," . $messagetext1 . "";
				$message2 = "Sayin " . $record->ad . " " . $record->soyad . " ," . $messagetext2 . "";
				foreach ( $data[ 'smsRecords' ] as $smsrecord ) {

					if ( $smsrecord->username == $record->username && $smsrecord->smscode != '1' && $record->calledstationid == $location1 ) {
						$result = $this->sendRequest( $record->username, $message1 );
						if ( $result )
							echo 1;
						else
							echo 0;
						$insertMessageInfo1 = array( 'username' => $record->username, 'smscode' => '1', 'message' => $messagetext1, 'sent' => '1', 'date' => date( 'Y-m-d H:m:s' ) );
						$lastInsertedID1 = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo1 );
					}
					//                    elseif($smsrecord->username == $record->username && $smsrecord->smscode != '2' && $record->calledstationid == $location2){
					//                $result = $this->sendRequest($record->username, $message2);
					//                if ($result)
					//                    echo 1;
					//                else
					//                    echo 0;
					//                $insertMessageInfo2 = array('username'=>$record->username, 'smscode'=>'2', 'message'=>$messagetext2, 'sent'=> '1', 'date'=>date('Y-m-d H:m:s'));
					//                $lastInsertedID2 = $this->instantmessage_model->insertNewSentMessage($insertMessageInfo2);
					//                    }else{
					//                echo "";
					//                    }

					//echo $lastInsertedID;
					//exit;



					//$result = $this->sendRequest($record->username, $message);
					//if ($result)
					//    echo 1;
					//else
					//     echo 0;

					//echo $lastInsertedID;
					//echo $record->username;
				}

			}
			//echo '<br> Messeages sent to all those who loggedin after this time or date'.$this->session->userdata('startdatemessage');

			$this->global[ 'pageTitle' ] = 'LOCUS : Messaging';
			$countmessages = count( $data[ 'userRecords' ] );
			echo json_encode( $countmessages );
			//$this->loadViews("users", $this->global, $data, NULL);
		}
	}
	/*
	 * This function is for sending sms to all instant users
	 * */
	function sendRequest( $RandUser, $message ) {
		//die('SITENAME:'.$site_name.'SEND XML:'.$send_xml.'HEADER TYPE '.var_export($header_type,true));
		$ApiUrl = 'http://192.168.2.3:8091/SmsGwWebApplication/sendSms?';
		$SmsPass = 'Bayt1234!';
		$SmsUser = 'captiveportal';
		$SmsNo = 'NOKTAATISI';
		$appuurl = "<SMS_DATA><USER><NAME>" . $SmsUser . "</NAME><PWD>" . $SmsPass . "</PWD></USER><SMS><SOURCE>" . $SmsNo . "</SOURCE><DEST>" . $RandUser . "</DEST><TEXT>$message</TEXT><TID>test</TID><TYPE>0</TYPE><APPLICATION_ID>1941</APPLICATION_ID><MESSAGE_ID>12</MESSAGE_ID><DELIVERY_RECEIPT>1</DELIVERY_RECEIPT></SMS></SMS_DATA>";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_POST, 1 );
		// Following line is compulsary to add as it is:
		curl_setopt( $ch, CURLOPT_POSTFIELDS, "data=" . $appuurl );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$data = curl_exec( $ch );
		curl_close( $ch );


		return $data;
	}
	/*
	 * This fuction is for stoping instant messaging
	 * */
	function stopinstantmessages() {
		$this->session->unset_userdata( 'startdatemessage' );

		redirect( '/instantMessages' );
	}
	/*
	 * This method is for Bar Graph for daily messages sent
	 * */
	function showDailyMessagesGraph() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			//$count = $this->user_model->getphonenumbersforinstantmessage($searchText);

			$datagraph = $this->instantmessage_model->getDailymessageGraphModel();
			//echo $this->db->last_query();
			//exit;
			//pre($datagraph);
			// $countmessages = count($data['userRecords']);
			echo json_encode( $datagraph );
			//$this->loadViews("users", $this->global, $data, NULL);
		}
	}
	/**
	 * This function is used to load the add new form
	 */
	function addNew() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->model( 'user_model' );
			$data[ 'roles' ] = $this->user_model->getUserRoles();

			$this->global[ 'pageTitle' ] = 'LOCUS : Add New User';

			$this->loadViews( "addNew", $this->global, $data, NULL );
		}
	}

	/**
	 * This function is used to check whether email already exist or not
	 */
	function checkEmailExists() {
		$userId = $this->input->post( "userId" );
		$email = $this->input->post( "email" );

		if ( empty( $userId ) ) {
			$result = $this->user_model->checkEmailExists( $email );
		} else {
			$result = $this->user_model->checkEmailExists( $email, $userId );
		}

		if ( empty( $result ) ) {
			echo( "true" );
		} else {
			echo( "false" );
		}
	}

	/**
	 * This function is used to add new user to the system
	 */
	function addNewUser() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$this->form_validation->set_rules( 'fname', 'Full Name', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'password', 'Password', 'required|max_length[20]' );
			$this->form_validation->set_rules( 'cpassword', 'Confirm Password', 'trim|required|matches[password]|max_length[20]' );
			$this->form_validation->set_rules( 'role', 'Role', 'trim|required|numeric' );
			$this->form_validation->set_rules( 'mobile', 'Mobile Number', 'required|min_length[10]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->addNew();
			} else {
				$name = ucwords( strtolower( $this->security->xss_clean( $this->input->post( 'fname' ) ) ) );
				$email = $this->security->xss_clean( $this->input->post( 'email' ) );
				$password = $this->input->post( 'password' );
				$roleId = $this->input->post( 'role' );
				$mobile = $this->security->xss_clean( $this->input->post( 'mobile' ) );

				$userInfo = array( 'email' => $email, 'password' => getHashedPassword( $password ), 'roleId' => $roleId, 'name' => $name,
					'mobile' => $mobile, 'createdBy' => $this->vendorId, 'createdDtm' => date( 'Y-m-d H:i:s' ) );

				$this->load->model( 'user_model' );
				$result = $this->user_model->addNewUser( $userInfo );

				if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'New User created successfully' );
				} else {
					$this->session->set_flashdata( 'error', 'User creation failed' );
				}

				redirect( 'addNew' );
			}
		}
	}


	/**
	 * This function is used load user edit information
	 * @param number $userId : Optional : This is user id
	 */
	function editOld( $userId = NULL ) {
		if ( $this->isAdmin() == TRUE || $userId == 1 ) {
			$this->loadThis();
		} else {
			if ( $userId == null ) {
				redirect( 'userListing' );
			}

			$data[ 'roles' ] = $this->user_model->getUserRoles();
			$data[ 'userInfo' ] = $this->user_model->getUserInfo( $userId );

			$this->global[ 'pageTitle' ] = 'CodeInsect : Edit User';

			$this->loadViews( "editOld", $this->global, $data, NULL );
		}
	}


	/**
	 * This function is used to edit the user information
	 */
	function editUser() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$userId = $this->input->post( 'userId' );

			$this->form_validation->set_rules( 'fname', 'Full Name', 'trim|required|max_length[128]' );
			$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email|max_length[128]' );
			$this->form_validation->set_rules( 'password', 'Password', 'matches[cpassword]|max_length[20]' );
			$this->form_validation->set_rules( 'cpassword', 'Confirm Password', 'matches[password]|max_length[20]' );
			$this->form_validation->set_rules( 'role', 'Role', 'trim|required|numeric' );
			$this->form_validation->set_rules( 'mobile', 'Mobile Number', 'required|min_length[10]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->editOld( $userId );
			} else {
				$name = ucwords( strtolower( $this->security->xss_clean( $this->input->post( 'fname' ) ) ) );
				$email = $this->security->xss_clean( $this->input->post( 'email' ) );
				$password = $this->input->post( 'password' );
				$roleId = $this->input->post( 'role' );
				$mobile = $this->security->xss_clean( $this->input->post( 'mobile' ) );

				$userInfo = array();

				if ( empty( $password ) ) {
					$userInfo = array( 'email' => $email, 'roleId' => $roleId, 'name' => $name,
						'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'updatedDtm' => date( 'Y-m-d H:i:s' ) );
				} else {
					$userInfo = array( 'email' => $email, 'password' => getHashedPassword( $password ), 'roleId' => $roleId,
						'name' => ucwords( $name ), 'mobile' => $mobile, 'updatedBy' => $this->vendorId,
						'updatedDtm' => date( 'Y-m-d H:i:s' ) );
				}

				$result = $this->user_model->editUser( $userInfo, $userId );

				if ( $result == true ) {
					$this->session->set_flashdata( 'success', 'User updated successfully' );
				} else {
					$this->session->set_flashdata( 'error', 'User updation failed' );
				}

				redirect( 'userListing' );
			}
		}
	}


	/**
	 * This function is used to delete the user using userId
	 * @return boolean $result : TRUE / FALSE
	 */
	function deleteUser() {
		if ( $this->isAdmin() == TRUE ) {
			echo( json_encode( array( 'status' => 'access' ) ) );
		} else {
			$userId = $this->input->post( 'userId' );
			$userInfo = array( 'isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date( 'Y-m-d H:i:s' ) );

			$result = $this->user_model->deleteUser( $userId, $userInfo );

			if ( $result > 0 ) {
				echo( json_encode( array( 'status' => TRUE ) ) );
			} else {
				echo( json_encode( array( 'status' => FALSE ) ) );
			}
		}
	}

	/**
	 * This function is used to load the change password screen
	 */
	function loadChangePass() {
		$this->global[ 'pageTitle' ] = 'CodeInsect : Change Password';

		$this->loadViews( "changePassword", $this->global, NULL, NULL );
	}


	/**
	 * This function is used to change the password of the user
	 */
	function changePassword() {
		$this->load->library( 'form_validation' );

		$this->form_validation->set_rules( 'oldPassword', 'Old password', 'required|max_length[20]' );
		$this->form_validation->set_rules( 'newPassword', 'New password', 'required|max_length[20]' );
		$this->form_validation->set_rules( 'cNewPassword', 'Confirm new password', 'required|matches[newPassword]|max_length[20]' );

		if ( $this->form_validation->run() == FALSE ) {
			$this->loadChangePass();
		} else {
			$oldPassword = $this->input->post( 'oldPassword' );
			$newPassword = $this->input->post( 'newPassword' );

			$resultPas = $this->user_model->matchOldPassword( $this->vendorId, $oldPassword );

			if ( empty( $resultPas ) ) {
				$this->session->set_flashdata( 'nomatch', 'Your old password not correct' );
				redirect( 'loadChangePass' );
			} else {
				$usersData = array( 'password' => getHashedPassword( $newPassword ), 'updatedBy' => $this->vendorId,
					'updatedDtm' => date( 'Y-m-d H:i:s' ) );

				$result = $this->user_model->changePassword( $this->vendorId, $usersData );

				if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'Password updation successful' );
				} else {
					$this->session->set_flashdata( 'error', 'Password updation failed' );
				}

				redirect( 'loadChangePass' );
			}
		}
	}

	/**
	 * Page not found : error 404
	 */
	function pageNotFound() {
		$this->global[ 'pageTitle' ] = 'CodeInsect : 404 - Page Not Found';

		$this->loadViews( "404", $this->global, NULL, NULL );
	}

	/**
	 * This function used to show login history
	 * @param number $userId : This is user id
	 */
	function loginHistoy( $userId = NULL ) {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$userId = ( $userId == NULL ? $this->session->userdata( "userId" ) : $userId );

			$searchText = $this->input->post( 'searchText' );
			$fromDate = $this->input->post( 'fromDate' );
			$toDate = $this->input->post( 'toDate' );

			$data[ "userInfo" ] = $this->user_model->getUserInfoById( $userId );

			$data[ 'searchText' ] = $searchText;
			$data[ 'fromDate' ] = $fromDate;
			$data[ 'toDate' ] = $toDate;

			$this->load->library( 'pagination' );

			$count = $this->user_model->loginHistoryCount( $userId, $searchText, $fromDate, $toDate );

			$returns = $this->paginationCompress( "login-history/" . $userId . "/", $count, 5, 3 );

			$data[ 'userRecords' ] = $this->user_model->loginHistory( $userId, $searchText, $fromDate, $toDate, $returns[ "page" ], $returns[ "segment" ] );

			$this->global[ 'pageTitle' ] = 'CodeInsect : User Login History';

			$this->loadViews( "loginHistory", $this->global, $data, NULL );
		}
	}
}

?>