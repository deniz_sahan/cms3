<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class Classroom extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public
	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language
		$this->load->model( 'customerlocation_model' );
		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'user_model' );
		$this->load->model( 'course_model' );
		$this->load->model( 'classroom_model' );
		
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public
	function index() {
		$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "dashboard", $this->global, NULL, NULL );
	}
/**
	 * This function is used to load the Classroom list
	 */
	function showclassroom() {
	//exit;
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );

			$data[ 'searchText' ] = $searchText;

			$this->load->library( 'pagination' );

			$count = $this->course_model->courseListingCount( $searchText );

			$returns = $this->paginationCompress( "showcustomers/", $count, 5 );
			// exit;
			$data[ 'classroomRecord' ] = $this->classroom_model->teacherListing( $searchText, $returns[ "page" ], $returns[ "segment" ] );
           //$myvalue1=$this->classroom_model->teacherListing( $searchText, $returns[ "page" ], $returns[ "segment" ] );
		  // pre($myvalue1);
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Customer Listings';

			$this->loadViews( "classroom", $this->global, $data, NULL );
		}
	}
	
	/**
	 * This function is used to load the data and send it to the database for adding new Course 
	 */
    function addNewclassroom() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$this->form_validation->set_rules( 'cname', 'Course Name', 'trim|required|max_length[500]' );
		//	$this->form_validation->set_rules( 'ctitle', 'Course Title', 'trim|required|max_length[128]' );
			//$this->form_validation->set_rules( 'cinstructor', 'Course Instructor', 'trim|required|max_length[500]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->addclassroom();
			} else {   				
				$classroomname = $this->input->post( 'cname' );
				//$classroomloc = $this->input->post( 'ctitle' );
				//$courseinstructor = $this->input->post( 'cinstructor' );
				$course=$this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
				$class=$this->security->xss_clean( $this->input->post( 'selectclass' ) );
				$courseInfo = array( 't_name' => $classroomname, 'course_id' => $course , 'classroom_id' => $class, 'createdate' => date( 'Y-m-d H:i:s' ));
				$result = $this->classroom_model->addNewClassroom( $courseInfo );
				$lastinsertcustomerid = $result;
		   if ( $result > 0 ) {
					$this->session->set_flashdata( 'success', 'New Classroom created successfully' );
					//exit;
				} else {
					$this->session->set_flashdata( 'error', 'Classroom creation failed' );
				}

				redirect( 'addNewclassroom' );
			}
		}
	}
	
	/**
	 * This function is used to load the add new course form
	 */
	function addclassroom() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->model( 'classroom_model' );
			$data[ 'roles' ] = $this->customerlocation_model->getUserRoles();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$data[ 'customers' ] =  $this->course_model->getallcourses(  );
			$data[ 'courseInfo' ] = $this->course_model->getallcourses(  );
			$data[ 'classroominfo' ] = $this->customerlocation_model->getLocationInfo(  );
			
			$data[ 'location' ] = $this->customerlocation_model->getLocationInfo(  );
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Add New Classroom';

			$this->loadViews( "addNewClassroom", $this->global, $data, NULL );
		}
	}

	
}

?>