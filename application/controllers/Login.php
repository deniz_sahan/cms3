<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class Login extends CI_Controller {
	/**
	 * This is default constructor of the class
	 */
	public
	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language
        
        
        $this->load->model('login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->isLoggedIn();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('login');
        }
        else
        {
            redirect('/dashboard');
        }
    }
    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
		$this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->security->xss_clean($this->input->post('email'));
            $password = $this->input->post('password');
            
            $result = $this->login_model->loginMe($email, $password);
            
            if(count($result) > 0)
            {
                foreach ($result as $res)
                {
                    $lastLogin = $this->login_model->lastLoginInfo($res->userId);
                    $locationIDC = $this->login_model->locationviacustomerid($res->customerid);
                    if(!empty($locationIDC)){
                        $locID = $locationIDC[0]->locationid;
                    }else{
                        $locID = '99';
                    }
                    
                    $sessionArray = array('userId'=>$res->userId,                    
                                            'role'=>$res->roleId,
                                            'roleText'=>$res->role,
                                            'name'=>$res->name,
                                            'imagefile'=>$res->image_file,
                                            'customeridsess'=>'8791'.$res->customerid,
                                            'customername'=>$res->customername,
                                            'locID'=>$locID,
                                            'lastLogin'=> $lastLogin->createdDtm,
                                            'isLoggedIn' => TRUE
                                    );
                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['userId'], $sessionArray['isLoggedIn'], $sessionArray['lastLogin']);

                    $loginInfo = array("userId"=>$res->userId, "sessionData" => json_encode($sessionArray), "machineIp"=>$_SERVER['REMOTE_ADDR'], "userAgent"=>getBrowserAgent(), "agentString"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                    $this->login_model->lastLogin($loginInfo);
                    
                    redirect('/dashboard');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                redirect('/login');
            }
        }
    }

    /**
     * This function used to load forgot password view
     */
    public function forgotPassword()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('forgotPassword');
        }
        else
        {
            redirect('/dashboard');
        }
    }
    
    /**
     * This function used to generate reset password request link
     */
    function resetPasswordUser()
    {
        $status = '';
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('login_email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('login_mobile','Mobile','trim|required');
                
        if($this->form_validation->run() == FALSE)
        {
            $this->forgotPassword();
        }
        else 
        {
            $email = $this->security->xss_clean($this->input->post('login_email'));
            $mobile = $this->security->xss_clean($this->input->post('login_mobile'));
            
            if($this->login_model->checkEmailExist($email))
            {
                $encoded_email = urlencode($email);
                
                $this->load->helper('string');
                $data['email'] = $email;
                $data['activation_id'] = random_string('alnum',15);
                $data['createdDtm'] = date('Y-m-d H:i:s');
                $data['agent'] = getBrowserAgent();
                $data['client_ip'] = $this->input->ip_address();
                
                $save = $this->login_model->resetPasswordUser($data);                
                
                if($save)
                {
                    $data1['reset_link'] = base_url() . "resetPasswordConfirmUser/" . $data['activation_id'] . "/" . $encoded_email;
                    $userInfo = $this->login_model->getCustomerInfoByEmail($email,$mobile);
                    if(!empty($userInfo)){
                        $data1["name"] = $userInfo[0]->name;
                        $data1["email"] = $userInfo[0]->email;
                        $data1["mobile"] = $userInfo[0]->mobile;
                        $data1["message"] = "Reset Your Password";
                        $newpassword = random_string('numeric',6);
                        $resetpassworddb = $this->login_model->createPasswordUser($data1["email"], $newpassword);
                        $originator = 'LOGOELEKT';
                        $message = 'Merhaba,'.$data1["name"].' LOCUS için yeni giriş şifreniz: '.$newpassword;
                    $sendStatus = $this->requesttosendmessage($message,$data1["mobile"],$originator);
                    }
                    
//$sendStatus = resetPasswordEmail($data1);
                    if($sendStatus){
                        $status = "send";
                        setFlashData($status, "Şifreniz telefonunuza gönderilmiştir. Giriş yaptıktan sonra değiştirebilirsiniz.");
                    } else {
                        $status = "notsend";
                        setFlashData($status, "Bilgileriniz hatalı girilmiştir. Lütfen tekrar deneyiniz.");
                    }
                }
                else
                {
                    $status = 'unable';
                    setFlashData($status, "Bilgilerinizde bir yanlışlık söz konusu. Lütfen tekrar deneyiniz.");
                }
            }
            else
            {
                $status = 'invalid';
                setFlashData($status, "Girdiğiniz e-posta ya da telefon numarası sistemimize kayıtlı değildir. Lütfen tekrar deneyiniz.");
            }
            redirect('/forgotPassword');
        }
    }
/**
 * Sending password sms
 * **/
    function requesttosendmessage($message,$number,$originator){
        ///////mobiledev
        //exit;
        $ApiUrl = "http://8bit.mobilus.net/";
        $SmsUser  =  'elogo-LG1001';
        $SmsPass  =  '8975';
        $Companyode = 'LG1001';
        //$originator = 'ALPET';
        $strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$number</Numbers><Originator>$originator</Originator><SDate></SDate></MainmsgBody>";

                $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$ApiUrl); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $strXML); 
		$result = curl_exec($ch);
                
                $pieces = explode(" ", $result);
                $smscode = $pieces[1]; // piece2
                curl_close($ch);
                return $smscode;
    }
    /**
     * This function used to reset the password 
     * @param string $activation_id : This is unique id
     * @param string $email : This is user email
     */
    function resetPasswordConfirmUser($activation_id, $email)
    {
        // Get email and activation code from URL values at index 3-4
        $email = urldecode($email);
        
        // Check activation id in database
        $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);
        
        $data['email'] = $email;
        $data['activation_code'] = $activation_id;
        
        if ($is_correct == 1)
        {
            $this->load->view('newPassword', $data);
        }
        else
        {
            redirect('/login');
        }
    }
    
    /**
     * This function used to create new password for user
     */
    function createPasswordUser()
    {
        $status = '';
        $message = '';
        $email = $this->input->post("email");
        $activation_id = $this->input->post("activation_code");
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('password','Password','required|max_length[20]');
        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->resetPasswordConfirmUser($activation_id, urlencode($email));
        }
        else
        {
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            // Check activation id in database
            $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);
            
            if($is_correct == 1)
            {                
                $this->login_model->createPasswordUser($email, $password);
                
                $status = 'success';
                $message = 'Password changed successfully';
            }
            else
            {
                $status = 'error';
                $message = 'Password changed failed';
            }
            
            setFlashData($status, $message);

            redirect("/login");
        }
    }
}

?>