<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class InstantMessages extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public

	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language

		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'customerlocation_model' );
		$this->load->model( 'user_model' );
		$this->load->library( 'csvimport' );
		// load form_validation library
		$this->load->library( 'form_validation' );
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public

	function index() {
		$this->global[ 'pageTitle' ] = 'LOCUS : InstantMessages';
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		// pre($data);
		//exit;
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {

		}
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->loadViews( "messages", $this->global, $data, NULL );
	}

	/**
	 * This function is used to load the user list
	 */
	function startinstantmessages( $messagess ) {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			if ( $this->session->userdata( 'startdatemessage' ) == "" ) {
				$startdate = date( 'Y-m-d H:m:s' );
				$this->session->set_userdata( 'startdatemessage', $startdate );
				//echo "created new one".$this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			} else {
				///echo $this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			}
			$data[ 'searchText' ] = $searchText;
			if ( isset( $messagess ) ) {
				$messagetext = $messagess;
			} else {
				$messagetext = 'Welcome to the Zone';
			}
			//$something = $messagess;
			//echo $data['searchText'];
			//echo $something;exit;
			//$count = $this->user_model->getphonenumbersforinstantmessage($searchText);

			$data[ 'userRecords' ] = $this->instantmessage_model->getphonenumbersforinstantmessage( $searchText );
			//echo $this->db->last_query();

			//pre($data['userRecords']);exit;

			foreach ( $data[ 'userRecords' ] as $record ) {
				$message = "Sayin " . $record->ad . " " . $record->soyad . " ," . $messagetext . "";
				$result = $this->sendRequest( $record->username, $message );
				if ( $result )
					echo 1;
				else
					echo 0;
				$insertMessageInfo = array( 'username' => $record->username, 'smscode' => '1', 'message' => $messagetext, 'sent' => '1', 'date' => date( 'Y-m-d H:m:s' ) );
				$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
				//echo $lastInsertedID;
				//echo $record->username;
			}
			//echo '<br> Messeages sent to all those who loggedin after this time or date'.$this->session->userdata('startdatemessage');
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Messaging';
			$countmessages = count( $data[ 'userRecords' ] );
			echo json_encode( $countmessages );
			//$this->loadViews("users", $this->global, $data, NULL);
		}
	}
	/**
	 * This function is used send different type of messages
	 */
	function startSendingInstantMessages() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			if ( $this->session->userdata( 'startdatemessage' ) == "" ) {
				$startdate = date( 'Y-m-d H:m:s' );
				$this->session->set_userdata( 'startdatemessage', $startdate );
				//echo "created new one".$this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			} else {
				///echo $this->session->userdata('startdatemessage');
				$searchText = $this->session->userdata( 'startdatemessage' );
			}
			$data[ 'searchText' ] = $searchText;
			$postData = $this->input->post();
			//pre($postData);
			//exit;
			if ( isset( $postData ) ) {
				$inmname = $postData[ 'inmname' ];
				$customerid = $postData[ 'customerid' ];
				$locationIDs = $postData[ 'locationid' ];
				$messageText = $postData[ 'message' ];
				$messagetype = $postData[ 'messagetype' ];
				$testuser = $postData[ 'testuser' ];
				$datetimepicker = $postData[ 'datetimepicker' ];
				$permissionlist = $postData[ 'permissionlist' ];
				$blacklist = $postData[ 'blacklist' ];
			} else {
				echo 'Fill the form correctly';
			}
			pre( $postData );
			echo $permissionlist;
			echo $blacklist;
			//pre($locationIDs);exit;
			//////For infinity
			/////////// start campaign insertion

			$insertInstantCampaign = array( 'inmname' => $inmname, 'customerid' => $customerid, 'locationid' => $locationIDs, 'messagetype' => $messagetype, 'testphonetype' => $testuser, 'permisionlist' => $permissionlist, 'blacklist' => $blacklist, 'message' => $messageText, 'variabledatetype' => $datetimepicker, 'startdate' => date( 'Y-m-d H:m:s' ), 'enddate' => date( 'Y-m-d H:m:s' ), 'countsms' => '', 'status' => 'active' );
			//$file_data = $this->csvimport->get_array($_FILES["permissionlist"]["tmp_name"]);
			pre( $insertInstantCampaign );
			exit;
			$lastInsertedIDCampaing = $this->instantmessage_model->insertcampaigntbl( $insertInstantCampaign );
			////////////end campaign insertion
			if ( $messagetype == 'onetimeinlife' ) {
				////send message on each login
				$data[ 'userRecords' ] = $this->instantmessage_model->getallphonenumbersbylocationID( $locationIDs, $searchText, '2', 'newuser' );
				//echo $this->db->last_query();
				//pre($data['userRecords']);
				//exit;

				foreach ( $data[ 'userRecords' ] as $record ) {
					$message = "Sayin " . $record->name . " " . $record->surname . " ," . $messageText . "";
					$result = $this->sendRequesttosmsGW( $record->username, $message );

					$xml = simplexml_load_string( $result );
					$json = json_encode( $xml );
					$arr = json_decode( $json, true );
					$temp = array();
					foreach ( $arr as $k => $v ) {
						foreach ( $v as $k1 => $v1 ) {
							$temp[ $k ][ $k1 ] = $v1;
						}
					}
					$smscode = $temp[ 'INSERT' ][ '@attributes' ][ 'id' ];
					$sent = $temp[ 'INSERT' ][ '@attributes' ][ 'res' ];

					if ( $result )
						echo 1;
					else
						echo 0;

					$this->instantmessage_model->updateuserprofilespage( $record->userid, 'olduser', '2' );
					$insertMessageInfo = array( 'username' => $record->username, 'smscode' => $smscode, 'message' => $messageText, 'sent' => $sent, 'date' => date( 'Y-m-d H:m:s' ) );
					$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
					//echo $lastInsertedID;
					//echo $record->username;
				}
			} elseif ( $messagetype == 'onemessageinoneday' ) {
				////send message on each login
				$data[ 'userRecords' ] = $this->instantmessage_model->getallphonenumbersbylocationIDdaily( $locationIDs, $searchText, '3', 'olduserloginagain' );
				//echo $this->db->last_query();

				//pre($data['userRecords']);
				//exit;

				foreach ( $data[ 'userRecords' ] as $record ) {
					$message = "Sayin " . $record->name . " " . $record->surname . " ," . $messageText . "";
					$result = $this->sendRequesttosmsGW( $record->username, $message );
					$xml = simplexml_load_string( $result );
					$json = json_encode( $xml );
					$arr = json_decode( $json, true );
					$temp = array();
					foreach ( $arr as $k => $v ) {
						foreach ( $v as $k1 => $v1 ) {
							$temp[ $k ][ $k1 ] = $v1;
						}
					}
					$smscode = $temp[ 'INSERT' ][ '@attributes' ][ 'id' ];
					$sent = $temp[ 'INSERT' ][ '@attributes' ][ 'res' ];

					if ( $result )
						echo 1;
					else
						echo 0;

					$this->instantmessage_model->updateuserprofilespage( $record->userid, $messageText, '3', 'olduser' );
					$insertMessageInfo = array( 'username' => $record->username, 'smscode' => $smscode, 'message' => $messageText, 'sent' => $sent, 'date' => date( 'Y-m-d H:m:s' ) );
					$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
					//echo $lastInsertedID;
					//echo $record->username;
				}
			} elseif ( $messagetype == 'testmessage' ) {
				//echo $testuser;
				//exit;
				$data[ 'userRecords' ] = '';
				$result = $this->sendRequesttosmsGW( $testuser, $messageText );
				//pre($result);
				$xml = simplexml_load_string( $result );
				$json = json_encode( $xml );
				$arr = json_decode( $json, true );
				$temp = array();
				foreach ( $arr as $k => $v ) {
					foreach ( $v as $k1 => $v1 ) {
						$temp[ $k ][ $k1 ] = $v1;
					}
				}
				$smscode = $temp[ 'INSERT' ][ '@attributes' ][ 'id' ];
				$sent = $temp[ 'INSERT' ][ '@attributes' ][ 'res' ];
				if ( !empty( $smscode ) ) {
					$insertMessageInfo = array( 'username' => $testuser, 'smscode' => $smscode, 'message' => $messageText, 'sent' => $sent, 'date' => date( 'Y-m-d H:m:s' ) );
					$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
				}
				//echo 'Infinity Messages are not set yet';
			} elseif ( $messagetype == 'datetimemessage' ) {
					////send message on each login

					///////get instatn users
					$data[ 'getinstant' ] = $this->instantmessage_model->getallphonenumbersbylocationIDdaily( $locationIDs, $searchText, '3', 'olduserloginagain' );
					//pre($data['getinstant']);                
					////////////insert datetime compaign
					foreach ( $data[ 'getinstant' ] as $instantrecord ) {
						$insertMessageInfo = array( 'username' => $instantrecord->username, 'smscode' => 'datetimeuser', 'message' => $messageText, 'sent' => '0', 'date' => $datetimepicker );
						$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
					}
					//////get date numbers less than set date                
					$data[ 'userRecords' ] = $this->instantmessage_model->getallphonenumbersbylocationIDdatetime( $locationIDs, $searchText, '3', 'olduserloginagain', date( 'Y-m-d H:m:s' ) );
					//echo $this->db->last_query();

					pre( $data[ 'userRecords' ] );
					//exit;

					foreach ( $data[ 'userRecords' ] as $record ) {
						$message = "Sayin " . $record->name . " " . $record->surname . " ," . $messageText . "";
						$result = $this->sendRequesttosmsGW( $record->username, $message );
						$xml = simplexml_load_string( $result );
						$json = json_encode( $xml );
						$arr = json_decode( $json, true );
						$temp = array();
						foreach ( $arr as $k => $v ) {
							foreach ( $v as $k1 => $v1 ) {
								$temp[ $k ][ $k1 ] = $v1;
							}
						}
						$smscode = $temp[ 'INSERT' ][ '@attributes' ][ 'id' ];
						$sent = $temp[ 'INSERT' ][ '@attributes' ][ 'res' ];

						if ( $result )
							echo 1;
						else
							echo 0;

						$this->instantmessage_model->updateuserprofilespage( $record->userid, $messageText, '3', 'olduser' );
						$insertMessageInfo = array( 'username' => $record->username, 'smscode' => $smscode, 'message' => $messageText, 'sent' => $sent, 'date' => $datetimepicker );
						$lastInsertedID = $this->instantmessage_model->insertNewSentMessage( $insertMessageInfo );
						//echo $lastInsertedID;
						//echo $record->username;
					}
				}
				//echo '<br> Messeages sent to all those who loggedin after this time or date'.$this->session->userdata('startdatemessage');
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Messaging';
			$countmessages = count( $data[ 'userRecords' ] );
			echo json_encode( $countmessages );
			//$this->loadViews("users", $this->global, $data, NULL);
		}
	}
	/*
	 * This function is for sending data from csv file to userprofiles table
	 * */
	function importcsvdataInstantmessage() {

		if ( $this->input->post( 'running' ) == 'active' ) {
			$status = 'active';
			//action for active here
		} else if ( $this->input->post( 'notrunning' ) == 'notactive' ) {
			//action for not active
			$status = 'notactive';
		} else {
			$status = '';
		}
		$postData = $this->input->post();
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$data[ 'searchText' ] = $searchText;
		if ( !empty( $this->input->post( 'inmname' ) ) ) {
			$inmname = $postData[ 'inmname' ];
			$customerid = $postData[ 'selectcustomermsg' ];
			$locationIDs = $postData[ 'selectlocationmsg' ];
			$messageText = $postData[ 'instantmessagetext' ];
			$priority = $postData[ 'priority' ];
			$targetgroup = $postData[ 'targetlistcheckselect' ];
			$campaignstartdate = $postData[ 'campaignstartdate' ];
			$campaignstartdate = date( 'Y-m-d H:i:s', strtotime( $campaignstartdate ) );
			//$status = 'notactive';
			$locationIDs = implode( ",", $locationIDs );
			//pre($postData);
			// exit;
			//$messagetype = $postData['messagetype'];
			//$testuser = $postData['testuser'];
			//$datetimepicker = $postData['datetimepicker'];
			if ( empty( $postData[ 'targetlistcheckselect' ] ) ) {
				if ( empty( $postData[ 'targetlistcheck' ] ) ) {
					$permissionlist = 'permissionlist';
				} else {
					$permissionlist = $postData[ 'targetlistcheck' ];
				}
			} else {
				$permissionlist = $postData[ 'targetlistcheckselect' ];
			}
			if ( empty( $blacklist ) ) {
				$blacklist = 'blacklist';
			} else {
				$blacklist = $postData[ 'blacklistcheck' ];
			}
			//pre($postData);
			//exit;
			$currentdate = date( 'Y-m-d H:m:s' );
			if ( !empty( $_FILES[ "targetlistfile" ][ "tmp_name" ] ) ) {
				$permisionlistfile = $this->csvimport->get_array( $_FILES[ "targetlistfile" ][ "tmp_name" ], array( '112233' ) );
				$numbers_array = array();
				foreach ( $permisionlistfile as $array ) {
					foreach ( $array as $key => $val ) {
						//array_push($numbers_array,$key);
						array_push( $numbers_array, $val );
					}
				}
				//pre($numbers_array);
				//exit;
				$data[ 'permissioncount' ] = count( $numbers_array );

				for ( $incr = 0; $incr <= count( $numbers_array ); $incr++ ) {
					if ( isset( $numbers_array[ $incr ] ) != '' ) {
						$dataperm[] = array(
							'username' => $numbers_array[ $incr ],
							'customerid' => $customerid,
							'locationid' => $locationIDs,
							'targetlist' => $permissionlist,
							'date' => $currentdate
						);
					}
				}
				//pre($dataperm);
				//exit;
				$this->instantmessage_model->insertpermisiionlist( $dataperm );
			}
			if ( !empty( $_FILES[ "blacklistfile" ][ "tmp_name" ] ) ) {
				$blacklistfile = $this->csvimport->get_array( $_FILES[ "blacklistfile" ][ "tmp_name" ] );
				$data[ 'blacklistcount' ] = count( $blacklistfile );
				foreach ( $blacklistfile as $row2 ) {
					$datablack[] = array(
						'username' => $row2[ "phonenumbers" ],
						'customerid' => $customerid,
						'locationid' => $locationIDs,
						'date' => $currentdate
					);
				}
				$this->instantmessage_model->insertblactlist( $datablack );
			}
			$insertInstantCampaign = array( 'inmname' => $inmname, 'customerid' => $customerid, 'locationid' => $locationIDs, 'permisionlist' => $permissionlist, 'blacklist' => $blacklist, 'message' => $messageText, 'startdate' => $campaignstartdate, 'priority' => $priority, 'status' => 'not active' );
			//$file_data = $this->csvimport->get_array($_FILES["permissionlist"]["tmp_name"]);

			$lastInsertedIDCampaing = $this->instantmessage_model->insertcampaigntbl( $insertInstantCampaign );
		}
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeriduser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeriduser );
		} else {
			$customeriduser = '';
		}
		$countcampaigns = $this->instantmessage_model->getcountinstantmessagecampaing( $searchText, $customeriduser, $status );

		$returns = $this->paginationCompress( "importcsvdataInstantmessage/", $countcampaigns, 10 );
		$data[ 'results' ] = $this->instantmessage_model->getinstantmessagecampaingviaID( $searchText, $customeriduser, $returns[ "page" ], $returns[ "segment" ], $status );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();

		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : InstantMessages';
		//pre($data);
		$this->loadViews( "instantmessageresult", $this->global, $data, NULL );

	}
	/*
	 * Show campaign list for advace search for
	 * */
	function advancecampaignlist() {
		$postData = $this->input->post();
		//pre($postData);

		//exit;
		$advsearchText = $this->security->xss_clean( $this->input->post( 'advsearchText' ) );
		$statusactive = $this->input->post( 'statusactive' );
		$data[ 'advsearchText' ] = $advsearchText;
		$data[ 'searchText' ] = '';
		$data[ 'datetimepickervaluestart' ] = $postData[ 'datetimepickervaluestart' ];
		$data[ 'datetimepickervalueend' ] = $postData[ 'datetimepickervalueend' ];
		$datetimepickervaluestart = $postData[ 'datetimepickervaluestart' ];
		if ( !empty( $datetimepickervaluestart ) ) {
			$datetimepickervaluestart = date( 'Y-m-d H:i:s', strtotime( $datetimepickervaluestart ) );
		}
		$datetimepickervalueend = $postData[ 'datetimepickervalueend' ];
		if ( !empty( $datetimepickervalueend ) ) {
			$datetimepickervalueend = date( 'Y-m-d H:i:s', strtotime( $datetimepickervalueend ) );
		}
		if ( !empty( $this->input->post( 'selectlocationmsg' ) ) ) {
			$locationIDs = $postData[ 'selectlocationmsg' ];
			$locationIDs = implode( ",", $locationIDs );
		} else {
			$locationIDs = '';
		}
		if ( !empty( $this->input->post( 'selectcustomermsg' ) ) ) {
			$customerid = $postData[ 'selectcustomermsg' ];
		} else {
			$customerid = '';
		}
		$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
		$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationIDs );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeriduser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeriduser );
		} else {
			$customeriduser = '';
		}
		if ( $customeriduser == '' ) {
			$customeriduser = $customerid;
		}
		$locationmultiids = '';
		if ( count( $locationIDs ) > 1 ) {
			$locationmultiids = implode( ",", $locationIDs );
		} else {
			$locationIDs = $locationIDs;
		}
		$countadvcampaigns = $this->instantmessage_model->getadvcampaignlistcount( $advsearchText, $customeriduser, $locationmultiids, $datetimepickervaluestart, $datetimepickervalueend, $statusactive );
		$returns = $this->paginationCompress( "advancecampaignlist/", $countadvcampaigns, 10 );
		$data[ 'results' ] = $this->instantmessage_model->getadvcampaignlist( $advsearchText, $customeriduser, $locationmultiids, $datetimepickervaluestart, $datetimepickervalueend, $returns[ "page" ], $returns[ "segment" ], $statusactive );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();

		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : InstantMessages';
		//pre($data);
		$this->loadViews( "instantmessageresult", $this->global, $data, NULL );

	}
	/*
	 *set instant message type
	 * 
	 */
	function setInstantstatus( $inmid, $status, $customerid, $locationid, $priority ) {

		if ( $status == '0' ) {
			$statusc = 'active';
		} else {
			$statusc = 'not active';
		}
		$check = $this->instantmessage_model->getallcampaigncusloc( $customerid, $locationid, $priority );
		//echo $check[0]->inmname;
		//pre($check);
		//exit;
		if ( $status == '1' ) {
			$data = $this->instantmessage_model->setinstantstatus( $inmid, $statusc );
			redirect( 'importcsvdataInstantmessage', 'location' );
		} elseif ( $status == '0' ) {
			if ( empty( $check ) ) {
				$data = $this->instantmessage_model->setinstantstatus( $inmid, $statusc );
				redirect( 'importcsvdataInstantmessage', 'location' );
			} else {
				$this->session->set_flashdata( 'reportmessage', 'This Campaign ' . $check[ 0 ]->inmname . ' is running with same priority. Please change priority or stop this ' . $check[ 0 ]->inmname . ' campaign ' );
				redirect( 'importcsvdataInstantmessage', 'location' );
			}
		}


	}
	/*
	 *set instant messages status
	 * 
	 */
	function setInstantMessagetype() {

		// POST data
		$postData = $this->input->post();
		//echo $postData['inmid'];
		//pre($postData);
		// get data
		$data = $this->instantmessage_model->setinstantmessagetype( $postData[ 'inmid' ], $postData[ 'messagetype' ] );

		echo json_encode( $data );

	}
	/*
	 *set instant messages status
	 * 
	 */
	function setInstantMessagetypexhour() {

		// POST data
		$postData = $this->input->post();
		if ( $postData[ 'datetimepickervaluexhour' ] == 0 ) {
			$data = $this->instantmessage_model->setinstantmessagetypexhour( $postData[ 'inmidxhour' ], 'everytime', '0' );
		} elseif ( $postData[ 'datetimepickervaluexhour' ] == 1 ) {
			$data = $this->instantmessage_model->setinstantmessagetypexhour( $postData[ 'inmidxhour' ], 'eachday', '0' );
		} else {
			$data = $this->instantmessage_model->setinstantmessagetypexhour( $postData[ 'inmidxhour' ], $postData[ 'messagexhour' ], $postData[ 'datetimepickervaluexhour' ] );
		}
		echo json_encode( $data );

	}
	/*
	 *set instant message enddate
	 * 
	 */
	function setInstantenddate() {

		// POST data
		$postData = $this->input->post();
		//pre($postData);
		//exit;
		$postData[ 'inmidenddate' ];
		$postData[ 'datetimepickervalueend' ];

		$postData[ 'datetimepickervalueend' ] = date( 'Y-m-d H:i:s', strtotime( $postData[ 'datetimepickervalueend' ] ) );
		$data = $this->instantmessage_model->setinstantenddate( $postData[ 'inmidenddate' ], $postData[ 'datetimepickervalueend' ] );

		echo json_encode( $data );
	}
	/*
	 *edit instant message 
	 * 
	 */
	function editInstantmessage() {

		// POST data
		$postData = $this->input->post();
		//pre($postData);
		//exit;
		$postData[ 'inmideditmessage' ];
		$postData[ 'instantmessagetext' ];
		$postData[ 'priority' ];
		$data = $this->instantmessage_model->editInstantmessage( $postData[ 'inmideditmessage' ], $postData[ 'instantmessagetext' ], $postData[ 'priority' ] );

		echo json_encode( $data );
	}
	/*
	 *set instant message enddate
	 * 
	 */
	function setInstantstartdate() {

		// POST data
		$postData = $this->input->post();
		$postData[ 'startdate' ];
		$postData[ 'startdate' ] = date( 'Y-m-d H:i:s', strtotime( $postData[ 'startdate' ] ) );
		//                $houradd = '+'.$postData['timeinhour'].' hour';
		//                $enddate = date('Y-m-d H:i:s',strtotime($houradd,strtotime($postData['startdate'])));
		//echo $cenvertedTime;
		//pre($postData);
		//exit;
		// get data
		$data = $this->instantmessage_model->setinstantstartdate( $postData[ 'inmid' ], $postData[ 'start' ] );

		echo json_encode( $data );

	}
	/*
	 *show instant message campaigns
	 * 
	 */
	function showallcampaigns( $info ) {


		$lastInsertedIDCampaing = $this->instantmessage_model->insertcampaigntbl( $insertInstantCampaign );
		$data[ 'results' ] = $this->instantmessage_model->getinstantmessagecampaingviaID( $lastInsertedIDCampaing );
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : InstantMessages';
		//pre($data);
		$this->loadViews( "instantmessageresult", $this->global, $data, NULL );

	}
	/*
	 * This function is for sending sms to all instant users
	 * */
	function sendRequest( $RandUser, $message ) {
		//die('SITENAME:'.$site_name.'SEND XML:'.$send_xml.'HEADER TYPE '.var_export($header_type,true));
		$ApiUrl = 'http://192.168.2.3:8091/SmsGwWebApplication/sendSms?';
		$SmsPass = 'Bayt1234!';
		$SmsUser = 'captiveportal';
		$SmsNo = 'NOKTAATISI';
		$appuurl = "<SMS_DATA><USER><NAME>" . $SmsUser . "</NAME><PWD>" . $SmsPass . "</PWD></USER><SMS><SOURCE>" . $SmsNo . "</SOURCE><DEST>" . $RandUser . "</DEST><TEXT>$message</TEXT><TID>test</TID><TYPE>0</TYPE><APPLICATION_ID>1941</APPLICATION_ID><MESSAGE_ID>12</MESSAGE_ID><DELIVERY_RECEIPT>1</DELIVERY_RECEIPT></SMS></SMS_DATA>";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_POST, 1 );
		// Following line is compulsary to add as it is:
		curl_setopt( $ch, CURLOPT_POSTFIELDS, "data=" . $appuurl );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$data = curl_exec( $ch );
		curl_close( $ch );


		return $data;
	}
	/*

 * This function is for sending sms to all instant users testing

 * */

	function sendRequesttosmsGW( $RandUser, $message ) {

		///http://192.168.2.3:8091/SmsGwWebApplication/sendSms?

		$ApiUrl = 'http://www.postaguvercini.com/api_xml/Sms_insreq.asp';

		$SmsPass = 'elogopg';

		$SmsUser = 'elogopgsms';

		//$Sm0sNo = 'NOKTAATISI';

		$appuurl = "<SMS-InsRequest>

<CLIENT user=\"$SmsUser\" pwd=\"$SmsPass\" />

<INSERTMSG text=\"$message\">

<TO>$RandUser</TO>


</INSERTMSG>

</SMS-InsRequest>";


		//echo $appuurl; 
		//exit;
		$ch = curl_init();

		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );

		curl_setopt( $ch, CURLOPT_POST, true );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: text/xml' ) );

		//curl_setopt($ch, CURLOPT_POST, 1);

		// Following line is compulsary to add as it is:

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		curl_setopt( $ch, CURLOPT_POSTFIELDS, $appuurl );



		$data = curl_exec( $ch );

		curl_close( $ch );
		/////pre($data);
		return $data;

	}
	/*
	 * This fuction is for stoping instant messaging
	 * */
	function stopinstantmessages() {
		$this->session->unset_userdata( 'startdatemessage' );

		redirect( '/instantMessages' );
	}
	/*
	 * This method is for Bar Graph for daily messages sent
	 * */
	function showDailyMessagesGraph() {

		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			//$count = $this->user_model->getphonenumbersforinstantmessage($searchText);

			$datagraph = $this->instantmessage_model->getDailymessageGraphModel();
			//echo $this->db->last_query();
			//exit;
			//pre($datagraph);
			// $countmessages = count($data['userRecords']);
			echo json_encode( $datagraph );
			//$this->loadViews("users", $this->global, $data, NULL);
		}
	}
	/*
	 * This method is used for getting location by customerid     
	 */
	function getallLocationbyCustomerID() {
		// POST data
		$postData = $this->input->post();
		// get data
		$data = $this->instantmessage_model->getalllocationsbyid( $postData );

		echo json_encode( $data );
	}
	/*
	 * This method is used for getting targetgroups  by customerid     
	 */
	function getalltargetgroupsbyCustomerID() {
		// POST data
		$postData = $this->input->post();
		// get data
		$data = $this->instantmessage_model->getalltargetgroupbyid( $postData );

		echo json_encode( $data );
	}
	////////////////////////bulk messagese start
	/**
	 * This function used to load the first screen of the user getphonenumbersbylocation
	 */
	public

	function bulkmessages() {

		$this->global[ 'pageTitle' ] = 'LOCUS : BulkMessages';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {

		}
		//pre($data);
		//exit;
		$this->loadViews( "bulkmessages", $this->global, $data, NULL );


	}
	/*
	 *this u=is used for sending message to gateway
	 *       
	 */
	public

	function bulkmessagestogw() {

		//            if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }      
		$postData = $this->input->post();
		if ( isset( $postData ) ) {
			$customerid = $postData[ 'customerid' ];
			$locationIDs = $postData[ 'locationid' ];
			$startdatenum = $postData[ 'datetimepickervaluestart' ];
			$startdatenum = date( 'Y-m-d H:i:s', strtotime( $startdatenum ) );
			$enddatenum = $postData[ 'datetimepickervalueend' ];
			$enddatenum = date( 'Y-m-d H:i:s', strtotime( $enddatenum ) );
			$message = $postData[ 'bulkmessagetext' ];

		} else {
			echo 'Fill the form correctly';
		}
		////send message on each login

		$data[ 'userRecords' ] = $this->instantmessage_model->getphonenumbersbylocationmodel( $locationIDs, $startdatenum, $enddatenum );
		$usersphonenumber = array();
		foreach ( $data[ 'userRecords' ] as $record ) {
			$usersphonenumber[] = $record->username;
		}
		//echo $usersphonenumber;
		//exit;
		if ( !empty( $data[ 'userRecords' ] ) ) {
			$result = $this->sendRequesttosmsGWforbulk( $usersphonenumber, $message );


			$smscode = $result;
			$sent = '1';
			$locationsplitids = implode( ",", $locationIDs );

			$countmessages = count( $data[ 'userRecords' ] );
			$insertcampaignInfo = array( 'cpname' => 'bulkmessage', 'cpsgwresponse' => $smscode, 'numcounts' => $countmessages, 'cpstartdate' => $startdatenum, 'cpenddate' => $enddatenum, 'customerid' => $customerid, 'locationid' => $locationsplitids, 'message' => $message, 'status' => $sent, 'campagindate' => date( 'Y-m-d H:m:s' ) );
			$lastInsertedIDcam = $this->instantmessage_model->addcampaign( $insertcampaignInfo );
		}
		//echo $lastInsertedIDcam;
		$this->global[ 'pageTitle' ] = 'LOCUS : BulkMessages';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		// pre($data);
		//exit;
		//$this->loadViews("bulkmessages", $this->global, $data , NULL);
		echo json_encode( count( $data[ 'userRecords' ] ) );
	}

	/**
	 * This function used to load the first screen of the user getphonenumbersbylocation
	 */
	public

	function getphonenumbersbylocation() {
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        }
		//            
		$postData = $this->input->post();
		if ( isset( $postData ) ) {
			$customerid = $postData[ 'customerid' ];
			$locationIDs = $postData[ 'locationid' ];
			$startdatenum = $postData[ 'datetimepickervaluestart' ];

			$startdatenum = date( 'Y-m-d H:i:s', strtotime( $startdatenum ) );
			$enddatenum = $postData[ 'datetimepickervalueend' ];

			$enddatenum = date( 'Y-m-d H:i:s', strtotime( $enddatenum ) );
		} else {
			echo 'Fill the form correctly';
		}
		////send message on each login
		$data[ 'userRecords' ] = $this->instantmessage_model->getphonenumbersbylocationmodel( $locationIDs, $startdatenum, $enddatenum );

		//pre($data);

		$this->global[ 'pageTitle' ] = 'LOCUS : Messaging';
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$countmessages = count( $data[ 'userRecords' ] );
		//            pre($data['userRecords']);
		//            exit;
		echo json_encode( $countmessages );
		//$this->loadViews("users", $this->global, $data, NULL);

	}
	/*

 * This function is for sending bulkmessages

 * */

	function sendRequesttosmsGWforbulk( $RandUsers, $message ) {

		$RandUsers = implode( ", ", $RandUsers );
		$ApiUrl = "http://8bit.mobilus.net/";
		$SmsUser = 'elogo-LG1001';
		$SmsPass = '8975';
		$Companyode = 'LG1001';
		$originator = 'LOGOELEKT';

		$strXML = "<MainmsgBody><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>40</Action><Mesgbody>$message</Mesgbody><Numbers>$RandUsers</Numbers><Originator>$originator</Originator><SDate></SDate></MainmsgBody>";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $strXML );
		$result = curl_exec( $ch );
		$pieces = explode( " ", $result );
		$smscode = $pieces[ 1 ]; // piece2
		curl_close( $ch );
		/////pre($data);
		return $smscode;

	}
	/*
	 * This function will use for deleting campaign 
	 *
	 */
	function deleteCampaign() {
		if ( $this->isAdmin() == TRUE ) {
			echo( json_encode( array( 'status' => 'access' ) ) );
		} else {
			$inmid = $this->input->post( 'inmid' );
			$result = $this->instantmessage_model->deleteCampaign( $inmid );
			if ( $result > 0 ) {
				echo( json_encode( array( 'status' => TRUE ) ) );
			} else {
				echo( json_encode( array( 'status' => FALSE ) ) );
			}
		}
	}
}

?>