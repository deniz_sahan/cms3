<?php
if ( !defined( 'BASEPATH' ) )exit( 'No direct script access allowed' );

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Mansoor Khan
 * @version : 1.1
 * @since : 19 January 2018
 */
class stats extends BaseController {
	/**
	 * This is default constructor of the class
	 */
	public

	function __construct() {
		parent::__construct();
		// added for Multi Language by abdul qadir
		$lang = ( $this->session->userdata( 'lang' ) ) ?
			$this->session->userdata( 'lang' ) : config_item( 'language' );
		/* This is line for setting by Default Language*/
		//$this->lang->load('menu', 'turkish');
		$this->lang->load( 'menu', $lang );
		//abdul qadir code end here for language



		$this->load->model( 'stats_model' );
		$this->load->model( 'instantmessage_model' );
		$this->load->model( 'user_model' );
		$this->load->model( 'customerlocation_model' );
		$this->isLoggedIn();
	}

	/**
	 * This function used to load the first screen of the user
	 */
	public

	function index() {
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
		$this->loadViews( "dashboard", $this->global, NULL, NULL );

	}



	//this controller is for REporst
	function userstats() {
		///pre($this->input->post());
		//        if($this->isAdmin() == TRUE)
		//        {
		//            $this->loadThis();
		//        } 
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$customerid = $this->input->post( 'selectcustomermsg' );
		$locationid = $this->input->post( 'selectlocationmsg' );
		$startdate = $this->input->post( 'datetimepickervaluestart' );
		$enddate = $this->input->post( 'datetimepickervalueend' );
		//converting date to database format
		$sDate = date( 'Y-m-d H:i:s', strtotime( $startdate ) );
		$eDate = date( 'Y-m-d H:i:s', strtotime( $enddate ) );
		// to get seletion value from view
		$postdata = $this->input->post( 'barselectdata' );
		$data[ 'customerid' ] = $customerid;
		$data[ 'locationid' ] = $locationid;
		$data[ 'postdata' ] = $postdata;
		if ( $this->input->post( 'selectcustomermsg' ) != '' ) {
			$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
			$locationid = $this->security->xss_clean( $this->input->post( 'selectlocationmsg' ) );


			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );

		} else {
			$customerid = '';
			$locationid = '';
		}
		$startdate = $this->input->post( 'datetimepickervaluestart' );
		$enddate = $this->input->post( 'datetimepickervalueend' );

		//converting date to database format
		$sDate = date( 'Y-m-d H:i:s', strtotime( $startdate ) );
		$eDate = date( 'Y-m-d H:i:s', strtotime( $enddate ) );
		// to get seletion value from view
		$postdata = $this->input->post( 'barselectdata' );

		$data[ 'sDate' ] = $sDate;
		$data[ 'eDate' ] = $eDate;
		$data[ 'postdata' ] = $postdata;
		$data[ 'searchText' ] = $searchText;

		//to check for daily filter data
		if ( $postdata == 'daily' ) {
			$countsearch = $this->stats_model->getCountDailyReports( $searchText, $customerid, $locationid );
			$data[ 'countrecords' ] = $countsearch;
			$returns = $this->paginationCompress( "userstats/", $countsearch, 20 );
			$data[ 'statsreport' ] = $this->stats_model->getDailyReports( $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );
		}
		//to check for weekly filter data	 
		elseif ( $postdata == 'week' ) {
				$monday = date( 'Y-m-d', strtotime( 'previous monday', strtotime( date( 'Y-m-d' ) ) ) );
				$sunday = date( 'Y-m-d', strtotime( 'sunday', strtotime( date( 'Y-m-d' ) ) ) );
				$countsearch = $this->stats_model->getCountWeeklyReports( $monday, $sunday, $searchText, $customerid, $locationid );
				$data[ 'countrecords' ] = $countsearch;
				$returns = $this->paginationCompress( "userstats/", $countsearch, 20 );
				$data[ 'statsreport' ] = $this->stats_model->getWeeklyReports( $monday, $sunday, $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );
			}
			//to check for monthly filter data	
		elseif ( $postdata == 'monthly' ) {
			$countsearch = $this->stats_model->getCountMonthlyReports( $searchText, $customerid, $locationid );
			$data[ 'countrecords' ] = $countsearch;
			$returns = $this->paginationCompress( "userstats/", $countsearch, 20 );
			$data[ 'statsreport' ] = $this->stats_model->getMonthlyReports( $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );
		}
		elseif ( $postdata == 'selectdate' ) {
			$countsearch = $this->stats_model->getCountsBetweenDatesReports( $searchText, $customerid, $locationid, $sDate, $eDate );
			$data[ 'countrecords' ] = $countsearch;
			$returns = $this->paginationCompress( "userstats/", $countsearch, 500 );
			$data[ 'statsreport' ] = $this->stats_model->getBetweenDatesReports( $searchText, $customerid, $locationid, $sDate, $eDate, $returns[ "page" ], $returns[ "segment" ] );
		} else {
			$countsearch = $this->stats_model->getCountUsersStats( $searchText, $customerid, $locationid );
			$data[ 'countrecords' ] = $countsearch;
			$returns = $this->paginationCompress( "userstats/", $countsearch, 20 );
			$data[ 'statsreport' ] = $this->stats_model->getUsersStats( $searchText, $customerid, $locationid, $returns[ "page" ], $returns[ "segment" ] );
		}
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			if ( empty( $customerid ) ) {
				$customerid = str_replace( "8791", "", $this->customeridsess );
			}
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customerid );
		}




		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		$data[ 'cusidfordownload' ] = $customerid;
		$data[ 'locidfordownload' ] = $locationid;
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		$this->loadViews( "reports", $this->global, $data, NULL );
	}
	/*
	 * This fuction is for showing bar charts for all users 
	 *      */
	function barcharts() {
		$data[ 'customers' ] = $this->stats_model->getallcustomers();
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {

		}
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';
		$this->loadViews( "statsbarchart", $this->global, $data, NULL );
		$countWeekUser = 0;
		$countsentmessages = 0;


	}
	/* 
	 * This fuction is for showing bar charts for all users weekly 
	 *      */
	function barchartsdata() {
		$postdata = $this->input->post( 'barselectdata' );
		$customerid = $this->input->post( 'selectcustomermsg' );
		$locationid = $this->input->post( 'selectlocationmsg' );
		$label = '';
		$total1 = '';
		$total2 = '';
		$countWeekUser = 0;
		$countsentmessages = 0;
		if ( $this->input->post( 'selectcustomermsg' ) != '' ) {
			$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
			$locationid = $this->security->xss_clean( $this->input->post( 'selectlocationmsg' ) );

			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );
		} else {
			$customerid = '';
			$locationid = '';
		}
		if ( $postdata == '' && $customerid == '' && $locationid == '' ) {
			$postdata = 'month';
			$customerid = str_replace( "8791", "", $this->customeridsess );
			$locationid = $this->locID;
			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );

			///////
			//
			//
			///////////

			$barchartdata = $this->stats_model->barchartsmonthly( $postdata, $customerid, $locationid );
			$countWeekUser = 0;
			$nameOfmonth = date( 'm', strtotime( date( 'Y-m-d' ) ) );
			$nameOfyear = date( 'Y', strtotime( date( 'Y-m-d' ) ) );
			$daysofmonth = cal_days_in_month( CAL_GREGORIAN, $nameOfmonth, $nameOfyear );
			$label = array();
			for ( $i = 1; $i <= $daysofmonth; $i++ ) {
				$label[] = $i;
			}
			///create array for y axis
			$total1 = array();
			for ( $i = 1; $i <= $daysofmonth; $i++ ) {
				$total1[] = 0;
			}
			$shift = $total1;
			$total1 = array_combine( range( 1, count( $shift ) ), array_values( $shift ) );

			////assign counts to each day
			foreach ( $barchartdata as $tot ) {
				$nameOfdays = date( 'd', strtotime( $tot->month ) );
				for ( $i = 1; $i < count( $total1 ); $i++ ) {
					if ( $i == ( int )$nameOfdays ) {
						$total1[ $i ] = $tot->count;
						$countWeekUser = $countWeekUser + $total1[ $i ];
					}

				}
			}
			$onearr = array();
			foreach ( $total1 as $key => $val )
				$onearr[ $key - 1 ] = $val;
			$total1 = $onearr;

			/**Sent Messages Monthly Start here start **/
			////// Sent Text messages per month start
			$barchartdatatext = $this->stats_model->barchartstextmonthly( $postdata, $customerid, $locationid );
			$countsentmessages = 0;
			$labeltextmessage = array();
			for ( $ii = 1; $ii <= $daysofmonth; $ii++ ) {
				$labeltextmessage[] = $ii;
			}
			///create array for y axis
			$total2 = array();
			for ( $iii = 1; $iii <= $daysofmonth; $iii++ ) {
				$total2[] = 0;
			}
			$shifttextcount = $total2;
			$total2 = array_combine( range( 1, count( $shifttextcount ) ), array_values( $shifttextcount ) );
			////assign counts to each day
			foreach ( $barchartdatatext as $totext ) {
				$nameOfdaysinmonth = date( 'd', strtotime( $totext->month ) );
				for ( $iiii = 1; $iiii < count( $total2 ); $iiii++ ) {
					if ( $iiii == ( int )$nameOfdaysinmonth ) {
						$total2[ $iiii ] = $totext->count;
						$countsentmessages = $countsentmessages + $total2[ $iiii ];
					}

				}
			}
			$onearrtext = array();
			foreach ( $total2 as $keys => $vals )
				$onearrtext[ $keys - 1 ] = $vals;

			$total2 = $onearrtext;
			/**Sent Messages Monthly end here**/


			// here sending the data for Sending Messages
			$data[ 'labeltext' ] = json_encode( $label ); //json_encode($label);
			$data[ 'resulttext' ] = json_encode( $total2 );

			$data[ 'messageselected' ] = $postdata;

			// here sending the data for Wifi Users
			$data[ 'label' ] = json_encode( $label ); //json_encode($label);
			$data[ 'result' ] = json_encode( $total1 );
			$data[ 'customers' ] = $this->stats_model->getallcustomers();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruser = str_replace( "8791", "", $this->customeridsess );
				$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
			} else {

			}
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';

			$this->loadViews( "statsbarchart", $this->global, $data, NULL );
			//$this->barcharts();
		} else {

			/**Weekly Wifi User Start here start **/
			////// Wifi Users per Week start

			if ( $postdata == 'week' ) {

				//change here from previous monday to monday we were taking the wrong data for 14 days now it's ok
				$monday = date( 'Y-m-d', strtotime( 'previous monday', strtotime( date( 'Y-m-d' ) ) ) );
				$sunday = date( 'Y-m-d', strtotime( 'sunday', strtotime( date( 'Y-m-d' ) ) ) );
				$barchartdata = $this->stats_model->barchartsweek( $monday, $sunday, $customerid, $locationid );
				$myDay = array( 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' );
				$total1 = array( 0, 0, 0, 0, 0, 0, 0 );
				$currentday = date( 'D', strtotime( date( 'Y-m-d' ) ) );



				$countWeekUser = 0;
				foreach ( $barchartdata as $tot ) {
					$nameOfDay = date( 'D', strtotime( $tot->day ) );
					$total1[] = $tot->count;
					for ( $i = 0; $i < count( $total1 ); $i++ ) {
						//$total1[$i] = $tot->count;
						if ( !empty( $myDay[ $i ] ) ) {
							//$countWeekUser=0;	
							if ( $currentday == 'Mon' ) {
								$countWeekUser = 0;
								$total1[ 0 ] = $tot->count;
								$countWeekUser = $countWeekUser + $total1[ 0 ];
							} elseif ( $myDay[ $i ] == $nameOfDay ) {
								$total1[ $i ] = $tot->count;
								$countWeekUser = $countWeekUser + $total1[ $i ];
							}
						}

					}

				}




				$barchartdata = $this->stats_model->barchartsweektext( $monday, $sunday, $customerid, $locationid );
				$total2 = array();
				$myDay2 = array( 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' );
				$total2 = array( 0, 0, 0, 0, 0, 0, 0 );
				$countsentmessages = 0;
				foreach ( $barchartdata as $tot ) {
					$nameOfDay = date( 'D', strtotime( $tot->day ) );
					$total2[] = $tot->count;
					for ( $i = 0; $i < count( $total1 ); $i++ ) {
						//$total1[$i] = $tot->count;
						if ( !empty( $myDay2[ $i ] ) ) {
							$countsentmessages = 0;
							if ( $currentday == 'Mon' ) {
								$total2[ 0 ] = $tot->count;
								$countsentmessages = $countsentmessages + $total2[ 0 ];
							} elseif ( $myDay2[ $i ] == $nameOfDay ) {
								$total2[ $i ] = $tot->count;
								$countsentmessages = $countsentmessages + $total2[ $i ];
							}
						}

					}
				}
				$label = array( 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday' );

			}
			//weekly data end here

			//condition for daily
			elseif ( $postdata == 'daily' ) {
					$barchartdata = $this->stats_model->barchartsdaily( $postdata, $customerid, $locationid );
					//pre($barchartdata);
					$countWeekUser = 0;
					//  exit;
					///create array for y axis
					$total1 = array( 6 => '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' );
					////assign counts to each hour
					foreach ( $barchartdata as $tot ) {

						$nameOfHour = date( 'H', strtotime( $tot->hour ) );
						for ( $i = 6; $i < count( $total1 ); $i++ ) {
							if ( $i == ( int )$nameOfHour ) {
								$total1[ $i ] = $tot->count;
								$countWeekUser = $countWeekUser + $total1[ $i ];
							}

						}
					}
					$shift = $total1;
					$total1 = array_combine( range( 1, count( $shift ) ), array_values( $shift ) );

					$onearr = array();

					foreach ( $total1 as $key => $val )
						$onearr[ $key - 1 ] = $val;

					$total1 = $onearr;


					/**Sent Messages Daily Start here start **/
					////// Sent Text messages per day start
					// sending the data for Sending Messages
					$barchartdata = $this->stats_model->barchartstextdaily( $postdata, $customerid, $locationid );
					//  pre($barchartdata);
					// exit;
					///create array for y axis
					$countsentmessages = 0;
					$total2 = array( 6 => '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' );
					////assign counts to each hour


					foreach ( $barchartdata as $tot ) {

						$nameOfHour = date( 'H', strtotime( $tot->hour ) );
						for ( $i = 6; $i < count( $total2 ); $i++ ) {
							if ( $i == ( int )$nameOfHour ) {
								$total2[ $i ] = $tot->count;
								$countsentmessages = $countsentmessages + $total2[ $i ];
							}

						}
					}
					$shift = $total2;
					$total2 = array_combine( range( 1, count( $shift ) ), array_values( $shift ) );

					$onearr = array();

					foreach ( $total2 as $key => $val )
						$onearr[ $key - 1 ] = $val;

					$total2 = $onearr;
					/**Sent Messages Daily end here start **/


					$label = array( '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24' );
				}
				//condition for daily end here
			elseif ( $postdata == 'monthly' ) {
				$barchartdata = $this->stats_model->barchartsmonthly( $postdata, $customerid, $locationid );
				$countWeekUser = 0;
				$nameOfmonth = date( 'm', strtotime( date( 'Y-m-d' ) ) );
				$nameOfyear = date( 'Y', strtotime( date( 'Y-m-d' ) ) );
				$daysofmonth = cal_days_in_month( CAL_GREGORIAN, $nameOfmonth, $nameOfyear );
				$label = array();
				for ( $i = 1; $i <= $daysofmonth; $i++ ) {
					$label[] = $i;
				}
				///create array for y axis
				$total1 = array();
				for ( $i = 1; $i <= $daysofmonth; $i++ ) {
					$total1[] = 0;
				}
				$shift = $total1;
				$total1 = array_combine( range( 1, count( $shift ) ), array_values( $shift ) );

				////assign counts to each day
				foreach ( $barchartdata as $tot ) {
					$nameOfdays = date( 'd', strtotime( $tot->month ) );
					for ( $i = 1; $i < count( $total1 ); $i++ ) {
						if ( $i == ( int )$nameOfdays ) {
							$total1[ $i ] = $tot->count;
							$countWeekUser = $countWeekUser + $total1[ $i ];
						}

					}
				}
				$onearr = array();
				foreach ( $total1 as $key => $val )
					$onearr[ $key - 1 ] = $val;
				$total1 = $onearr;

				/**Sent Messages Monthly Start here start **/
				////// Sent Text messages per month start
				$barchartdatatext = $this->stats_model->barchartstextmonthly( $postdata, $customerid, $locationid );
				$countsentmessages = 0;
				$labeltextmessage = array();
				for ( $ii = 1; $ii <= $daysofmonth; $ii++ ) {
					$labeltextmessage[] = $ii;
				}
				///create array for y axis
				$total2 = array();
				for ( $iii = 1; $iii <= $daysofmonth; $iii++ ) {
					$total2[] = 0;
				}
				$shifttextcount = $total2;
				$total2 = array_combine( range( 1, count( $shifttextcount ) ), array_values( $shifttextcount ) );
				////assign counts to each day
				foreach ( $barchartdatatext as $totext ) {
					$nameOfdaysinmonth = date( 'd', strtotime( $totext->month ) );
					for ( $iiii = 1; $iiii < count( $total2 ); $iiii++ ) {
						if ( $iiii == ( int )$nameOfdaysinmonth ) {
							$total2[ $iiii ] = $totext->count;
							$countsentmessages = $countsentmessages + $total2[ $iiii ];
						}

					}
				}
				$onearrtext = array();
				foreach ( $total2 as $keys => $vals )
					$onearrtext[ $keys - 1 ] = $vals;

				$total2 = $onearrtext;
				/**Sent Messages Monthly end here**/

			}

			//for year data barchar
			//condition for daily end here
			elseif ( $postdata == 'yearly' ) {
				$barchartdata = $this->stats_model->barchartsyearly( $postdata, $customerid, $locationid );
				for ( $m = 1; $m <= 12; ++$m ) {
					//  echo date('F', mktime(0, 0, 0, $m, 1)).'<br>';
					$mymonth = date( 'F', mktime( 0, 0, 0, $m, 1 ) );
				}

				$countWeekUser = 0;
				$nameOfmonth = date( 'm', strtotime( date( 'Y-m-d' ) ) );
				$nameOfyear = date( 'Y', strtotime( date( 'Y-m-d' ) ) );
				$daysofmonth = cal_days_in_month( CAL_GREGORIAN, $nameOfmonth, $nameOfyear );
				$label = array();
				for ( $i = 1; $i <= 12; $i++ ) {
					$mymonth = date( 'F', mktime( 0, 0, 0, $i, 1 ) );
					$label[] = $mymonth;
				}
				///create array for y axis
				$total1 = array();
				for ( $i = 1; $i <= 12; $i++ ) {
					$total1[] = 0;
				}
				$shift = $total1;
				$total1 = array_combine( range( 1, count( $shift ) ), array_values( $shift ) );

				////assign counts to each day
				foreach ( $barchartdata as $tot ) {
					$nameOfmonth = date( 'm', strtotime( $tot->month ) );

					for ( $i = 1; $i < count( $total1 ); $i++ ) {
						if ( $i == ( int )$nameOfmonth ) {
							$total1[ $i ] = $tot->count;
							$countWeekUser = $countWeekUser + $total1[ $i ];
						}

					}
				}
				$onearr = array();
				foreach ( $total1 as $key => $val )
					$onearr[ $key - 1 ] = $val;
				$total1 = $onearr;

				/**Sent Messages Monthly Start here start **/
				////// Sent Text messages per month start
				$barchartdatatext = $this->stats_model->barchartstextyearly( $postdata, $customerid, $locationid );
				$countsentmessages = 0;
				$labeltextmessage = array();
				for ( $ii = 1; $ii <= 12; $ii++ ) {
					$labeltextmessage[] = $ii;
				}
				///create array for y axis
				$total2 = array();
				for ( $iii = 1; $iii <= 12; $iii++ ) {
					$mymonth = date( 'F', mktime( 0, 0, 0, $i, 1 ) );
					$total2[] = $mymonth;
				}
				$shifttextcount = $total2;
				$total2 = array_combine( range( 1, count( $shifttextcount ) ), array_values( $shifttextcount ) );
				////assign counts to each day
				foreach ( $barchartdatatext as $totext ) {
					$nameOfdaysinmonth = date( 'm', strtotime( $totext->month ) );
					for ( $iiii = 1; $iiii < count( $total2 ); $iiii++ ) {
						if ( $iiii == ( int )$nameOfdaysinmonth ) {
							$total2[ $iiii ] = $totext->count;
							$countsentmessages = $countsentmessages + $total2[ $iiii ];
						}

					}
				}
				$onearrtext = array();
				foreach ( $total2 as $keys => $vals )
					$onearrtext[ $keys - 1 ] = $vals;

				$total2 = $onearrtext;
				/**Sent Messages Monthly end here**/

			}



			//here is sending the wifi weekly User count to Dashboard per location
			$data[ 'weekCount' ] = json_encode( $countWeekUser ); //json_encode($label);
			//here is sending the sentmessages  User count to Dashboard per location
			$data[ 'MessageweekCount' ] = json_encode( $countsentmessages ); //json_encode($label);

			// here sending the data for Sending Messages
			$data[ 'labeltext' ] = json_encode( $label ); //json_encode($label);
			$data[ 'resulttext' ] = json_encode( $total2 );

			$data[ 'messageselected' ] = $postdata;

			// here sending the data for Wifi Users
			$data[ 'label' ] = json_encode( $label ); //json_encode($label);
			$data[ 'result' ] = json_encode( $total1 );
			$data[ 'customers' ] = $this->stats_model->getallcustomers();
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruser = str_replace( "8791", "", $this->customeridsess );
				$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
			} else {

			}
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$customeruserid = str_replace( "8791", "", $this->customeridsess );
			} else {
				$customeruserid = '';
			}
			$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
			$this->global[ 'pageTitle' ] = 'LOCUS : Dashboard';

			$this->loadViews( "statsbarchart", $this->global, $data, NULL );
		}
	}
	/*This function is for downloadin report from reports.php*/
	public

	function download( $customerid, $locationid, $searchText ) {


		$allData = $this->stats_model->get_users( $customerid, $locationid, $searchText ); // this will return all data into array
		$dataToExports = [];
		foreach ( $allData as $data ) {

			$upload = round( $data[ 'uploads' ] / 1024 / 1024, 2 );
			$download = round( $data[ 'downloads' ] / 1024 / 1024, 2 );

			if ( $data[ 'duration' ] < 60 ) {
				$sure = $data[ 'duration' ] . " seconds";
			} else {
				$gun = floor( $data[ 'duration' ] / 86400 );
				$saat = floor( ( $data[ 'duration' ] - $gun * 86400 ) / 3600 );
				$dakika = floor( ( $data[ 'duration' ] - $gun * 86400 - $saat * 3600 ) / 60 );
				$sure = "";
				if ( $dakika > 0 ) {
					$sure = $dakika . " minutes";
				}
				if ( $saat > 0 ) {
					$sure = $saat . " hours " . $sure;
				}
				if ( $gun > 0 ) {
					$sure = $gun . " day " . $sure;
				}
			}
			if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
				$arrangeData[ 'Username' ] = $data[ 'username' ];
			} else {
				$arrangeData[ 'Username' ] = substr_replace( $data[ 'username' ], "****", 4 );
			}
			$arrangeData[ 'Start Date' ] = date( "d-m-Y H:i", strtotime( '+10 hours', strtotime( $data[ 'logindate' ] ) ) );
			$arrangeData[ 'Last seen' ] = date( "d-m-Y H:i", strtotime( '+10 hours', strtotime( $data[ 'lastseen' ] ) ) );
			$arrangeData[ 'Downloads' ] = $data[ 'downloads' ];
			$arrangeData[ 'Uploads' ] = $data[ 'uploads' ];
			$arrangeData[ 'IP' ] = $data[ 'framedipaddress' ];
			$arrangeData[ 'MAC' ] = $data[ 'callingstationid' ];
			$arrangeData[ 'Total Session' ] = $sure;
			$dataToExports[] = $arrangeData;
		}
		// set header
		$filename = "LOCUS.xls";
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-Disposition: attachment; filename=\"$filename\"" );
		$this->exportExcelData( $dataToExports );
	}
	/*This function is for exporting report as a excel file from reports.php*/
	public

	function exportExcelData( $records ) {
		$heading = false;
		if ( !empty( $records ) )
			foreach ( $records as $row ) {
				if ( !$heading ) {
					// display field/column names as a first row
					echo implode( "\t", array_keys( $row ) ) . "\n";
					$heading = true;
				}
				echo implode( "\t", ( $row ) ) . "\n";
			}
	}

	//////excel end    

	// SMS LIMIT
	function smsLimitReport() {

		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {

			$customeruserid = str_replace( "8791", "", $this->customeridsess );
			$result = $this->stats_model->sentSms( $customeruserid );
			$result1 = $this->stats_model->smsLimit( $customeruserid );
			//pre($result1);
			//exit;
			if ( empty( $result1 ) ) {

				$data[ 'packagename' ] = 0;
				$data[ 'sentsms' ] = 0;
				$data[ 'smsplan' ] = 0;
				$data[ 'smsdate' ] = 0;
				$data[ 'smsremain' ] = 0;
				$data[ 'customername' ] = $this->customername;

				//to store data in database for used SMS
				//$result1 = $this->stats_model->storeSms($customeruserid, $smsSent,$remain);   

				$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';


				$this->loadViews( "smsLimit", $this->global, $data, NULL );

			} else {
				$smsSent = 0;
				foreach ( $result as $tot ) {
					$smsSent = ( int )$smsSent + $tot->sent;
					//$smslimit=+$tot->;	
				}
				//pre($smsSent);
				//exit;
				foreach ( $result1 as $tot ) {
					$smsLimit = $tot->smsplan;
					$smsplandate = $tot->date;
					$cusid = $tot->customerid;
					//$smslimit=+$tot->;	
				}

				$remain = $smsLimit - $smsSent;
				$data[ 'packagename' ] = $cusid;
				$data[ 'sentsms' ] = $smsSent;
				$data[ 'smsplan' ] = $smsLimit;
				$data[ 'smsdate' ] = $smsplandate;
				$data[ 'smsremain' ] = $remain;
				$data[ 'customername' ] = $this->customername;


				//to store data in database for used SMS
				$result1 = $this->stats_model->storeSms( $customeruserid, $smsSent, $remain );

				$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';


				$this->loadViews( "smsLimit", $this->global, $data, NULL );


			}


		} else {
			$customeruserid = '';
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
			$totalSms = 200000;
			//to get Sent Sms from admin 
			$result = $this->stats_model->sentSms( $customeruserid );
			//to get sms limit
			$result1 = $this->stats_model->smsLimit( $customeruserid );
			// to get total sms assign to customers
			$result3 = $this->stats_model->getsms();
			$customersms = 0;
			foreach ( $result3 as $tot ) {
				$customersms += $tot->smsplan;
				//$smslimit=+$tot->;	
			}
			// $totalSms=$totalSms-$customersms;

			//  pre($customersms);
			$smsSent = 0;
			$smsLimit = 0;
			foreach ( $result as $tot ) {
				$smsSent = ( int )$smsSent + $tot->sent;
				//$smslimit=+$tot->;	
			}
			foreach ( $result1 as $tot ) {
				$smsLimit = $tot->smsplan;
				$smsplandate = $tot->date;
				$cusid = $tot->customerid;
				//$smslimit=+$tot->;	
			}
			$remain = $smsLimit - $smsSent;
			//$remain=			


			//$$ApiUrl = "http://8bit.mobilus.net/"; 
			$ApiUrl = "http://gateway.mobilus.net/com.mobilus";
			$SmsUser = 'elogo-LG1001';
			$SmsPass = '8975';
			$strXML = "<MainReportRoot><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord><Action>7</Action></MainReportRoot>";
			$strXML1 = "<MainReportRoot><UserName>$SmsUser</UserName><PassWord>$SmsPass</PassWord>8<Action>8</Action></MainReportRoot>";

			//for first details
			$ch = curl_init();
			$ch1 = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $ApiUrl );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $strXML );
			//for second details
			curl_setopt( $ch1, CURLOPT_URL, $ApiUrl );
			curl_setopt( $ch1, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch1, CURLOPT_TIMEOUT, 30 );
			curl_setopt( $ch1, CURLOPT_POSTFIELDS, $strXML1 );
			$result = curl_exec( $ch );
			$result1 = curl_exec( $ch1 );
			// pre($result1);
			//	exit;		
			curl_close( $ch );
			//we need to change it for future
			//this is the message remaing from company
			//	$totalRemain=$totalSms-(int)$result;
			//$totalRemain=$totalRemain+$customersms;
			$totalRemain = $smsSent + $customersms;
			$totalSmsLeft = $totalSms - $totalRemain;
			//pre($totalRemain);
			$data[ 'smslimit' ] = $totalSmsLeft;
			$data[ 'smsuname' ] = $result1;
			$data[ 'smsused' ] = $totalRemain;
			$data[ 'totalsms1' ] = $totalSms;
			$data[ 'customername' ] = $this->customername;

			$result1 = $this->stats_model->storeSms( $customeruserid, $smsSent, $remain );


			$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';


			$this->loadViews( "smsLimit", $this->global, $data, NULL );


		}





		/////pre($data);



	}
	/*This function is for showing sms statistics*/
	function smsstats() {

		$postdata = $this->input->post( 'barselectdata' );

		//  $data['sDate'] = $sDate;
		//  $data['eDate'] = $eDate;
		$data[ 'postdata' ] = $postdata;
		// $data['searchText'] = $searchText;
		$data[ 'smsstats' ] = $this->stats_model->smsstatsmodel();

		//here is to select the customer and Location
		$searchText = $this->security->xss_clean( $this->input->post( 'searchText' ) );
		$customerid = $this->input->post( 'selectcustomermsg' );
		$locationid = $this->input->post( 'selectlocationmsg' );
		$startdate = $this->input->post( 'datetimepickervaluestart' );
		$enddate = $this->input->post( 'datetimepickervalueend' );
		//converting date to database format
		$sDate = date( 'Y-m-d H:i:s', strtotime( $startdate ) );
		$eDate = date( 'Y-m-d H:i:s', strtotime( $enddate ) );
		// to get seletion value from view
		$postdata = $this->input->post( 'barselectdata' );
		$data[ 'customerid' ] = $customerid;
		$data[ 'locationid' ] = $locationid;
		$data[ 'postdata' ] = $postdata;
		$data[ 'searchText' ] = $searchText;
		if ( $this->input->post( 'selectcustomermsg' ) != '' ) {
			$customerid = $this->security->xss_clean( $this->input->post( 'selectcustomermsg' ) );
			$locationid = $this->security->xss_clean( $this->input->post( 'selectlocationmsg' ) );


			$data[ 'customerid' ] = $customerid;
			$data[ 'locationid' ] = $locationid;
			$data[ 'customerinfo' ] = $this->customerlocation_model->getCustomerInfo( $customerid );
			$data[ 'locationinfo' ] = $this->customerlocation_model->getLocationInfo( $locationid );

		} else {
			$customerid = '';
			$locationid = '';
		}
		$startdate = $this->input->post( 'datetimepickervaluestart' );
		$enddate = $this->input->post( 'datetimepickervalueend' );

		//converting date to database format
		$sDate = date( 'Y-m-d H:i:s', strtotime( $startdate ) );
		$eDate = date( 'Y-m-d H:i:s', strtotime( $enddate ) );
		// to get seletion value from view
		$postdata = $this->input->post( 'barselectdata' );

		$data[ 'sDate' ] = $sDate;
		$data[ 'eDate' ] = $eDate;
		$data[ 'postdata' ] = $postdata;
		$data[ 'searchText' ] = $searchText;
		//end here selectoin of customer and location




		if ( $postdata == 'daily' ) {
			$data[ 'smsstats' ] = $this->stats_model->dailysmsStats( $searchText, $customerid, $locationid );

		}
		if ( $postdata == 'week' ) {
			$monday = date( 'Y-m-d', strtotime( 'previous monday', strtotime( date( 'Y-m-d' ) ) ) );
			$sunday = date( 'Y-m-d', strtotime( 'sunday', strtotime( date( 'Y-m-d' ) ) ) );
			$data[ 'smsstats' ] = $this->stats_model->weeklysmsStats( $searchText, $monday, $sunday, $customerid, $locationid );

		}
		if ( $postdata == 'monthly' ) {
			$data[ 'smsstats' ] = $this->stats_model->monthlysmsStats( $searchText, $customerid, $locationid );

		}
		if ( $postdata == 'selectdate' ) {
			$data[ 'smsstats' ] = $this->stats_model->smsStatsBetweenDates( $searchText, $customerid, $locationid, $sDate, $eDate );

		}



		$data[ 'customers' ] = $this->instantmessage_model->getallcustomers();
		$data[ 'cusidfordownload' ] = $customerid;
		$data[ 'locidfordownload' ] = $locationid;
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}

		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		$this->loadViews( "smsStats", $this->global, $data, NULL );
		//pre($value);



	}
	/*This function is creating a sms limitation for a customer*/
	function addsmsLimit() {

		$data[ 'customers' ] = $this->stats_model->getallcustomers();
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruser = str_replace( "8791", "", $this->customeridsess );
			$data[ 'locations' ] = $this->instantmessage_model->getalllocationsbyidforcustomer( $customeruser );
		} else {

		}
		if ( $this->role == ROLE_CLIENT_MANAGER || $this->role == ROLE_CLIENT_EMPLOYEE ) {
			$customeruserid = str_replace( "8791", "", $this->customeridsess );
		} else {
			$customeruserid = '';
		}
		$this->global[ 'getrunningcampaign' ] = $this->user_model->checkinstantcampaigns( $customeruserid );
		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		$this->loadViews( "addSmslimit", $this->global, $data, NULL );

	}

	//for adding new SMS LIMIT




	//For adding new smsLimit
	function addNewSmsLimit() {
		if ( $this->isAdmin() == TRUE ) {
			$this->loadThis();
		} else {
			$this->load->library( 'form_validation' );

			$this->form_validation->set_rules( 'pakagename', 'Pakage Name', 'trim|required|max_length[500]' );

			if ( $this->form_validation->run() == FALSE ) {
				$this->addsmsLimit();
			} else {
				//pre($this->global);

				$customerid = $this->input->post( 'selectcustomermsg' );
				$locationid = $this->input->post( 'selectlocationmsg' );
				$pakagname = $this->input->post( 'pakagename' );
				$checkpackage = $this->stats_model->package_exists( $customerid );

				if ( $checkpackage == true ) {
					$this->session->set_flashdata( 'error', "Package already Exist" );
					$this->addsmsLimit();
				} else {
					$pakageInfo = array( 'smsplan' => $pakagname, 'customerid' => $customerid, 'locationid' => $locationid, 'USERID' => $this->vendorId,
						'date' => date( 'Y-m-d H:i:s' ), 'updated' => date( 'Y-m-d H:i:s' ) );

					$result = $this->stats_model->addNewSmsLimitmodel( $pakageInfo );
					if ( $result == true ) {
						$this->session->set_flashdata( 'success', "Package Added Successful" );
					} else {
						$this->session->set_flashdata( 'error', "Package not Added" );

					}
					$this->addsmsLimit();
				}
			}
		}
	}

	function sentSmsLimit() {
		$result = $this->stats_model->sentSms();
		foreach ( $result as $tot ) {
			//pre($tot->sent);
		}
		//exit;

	}
	/*This function is for creating transaction for sms limitation system*/
	function transactions() {
		$data[ 'transaction' ] = $this->stats_model->getTransaction();
		//		   	foreach ($result as $tot) {
		//pre($tot->sent);
		//  }
		//  $this->global['getrunningcampaign'] = $this->user_model->checkinstantcampaigns($customeruserid);
		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		$this->loadViews( "transactions", $this->global, $data, NULL );
		//$this->session->set_userdata('referred_form', current_url());
		//exit;

	}
	/*This function is for deleting transaction */
	function deletetrans( $slid ) {
		$id = $this->input->post( 'slid' );
		$this->stats_model->deletetrans( $id );

		if ( $this->stats_model->deletetrans( $id ) ) {
			$data[ 'transaction' ] = $this->stats_model->getTransaction();
			$this->loadViews( "transactions", $this->global, $data, NULL );
			// redirect('transactions');
			$this->transactions();
		}
		//   $data['transaction'] = $this->stats_model->getTransaction();
		//$this->loadViews("transactions", $this->global ,$data, NULL);	
		//exit;

	}
	/*This function is for editing transaction */
	function edittransac() {
		$id = $this->input->get( 'id' );
		$this->load->model( 'stats_model' );
		$data[ 'record' ] = $this->stats_model->getTransactionByid( $id );
		$this->load->helper( 'form' );
		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		$this->loadViews( "edittrans", $this->global, $data, NULL );

	}
	/*This function is for updateting transaction */
	function updatetrans() {
		//$customername=$this->input->post('customername');
		$customerid = $this->input->post( 'customerid' );
		$packetlimit = $this->input->post( 'packetlimit' );
		$id = $this->input->post( 'slid' );
		$data = array( 'customerid' => $customerid, 'smsplan' => $packetlimit, 'updated' => date( 'Y-m-d H:i:s' ) );
		$this->load->model( 'stats_model' );
		$data[ 'data' ] = $this->stats_model->updatetransbyid( $id, $data );
		$this->load->helper( 'form' );
		$this->loadViews( "edittrans", $this->global, $data, NULL );
		$this->global[ 'pageTitle' ] = 'LOCUS : User Statistics';
		if ( $data ) {
			// $this->session->set_flashdata('success',"Package Updated Successful");
			redirect( 'transactions', 'refresh' );
			//	 $this->transactions();
		} else {
			$this->session->set_flashdata( 'error', "Package not Added" );

		}


	}
	/*
	 *check user status for last seen and online users
	 */
	function checkuserstatus() {
		$username = $this->input->post( 'username' );
		$downloaddata = $this->input->post( 'downloaddata' );
		echo $downloaddata . "download<br>";
		$date = date( "Y-m-d H:i:s" );
		$time = strtotime( $date );
		$time = $time - ( 1 * 60 );
		$date = date( "Y-m-d H:i:s", $time );
		$checkdownload = $this->stats_model->checkuserdownloads( $username );
		pre( $checkdownload );
		exit;
		$checkstatusdate = $this->stats_model->checkuserstatsmodel( $username, $downloaddata );
		pre( $checkstatusdate );
		exit;
		if ( $checkstatusdate[ 0 ]->authdate > $date ) {
			$status = "<span class='label label-success'>Active users</span>";
			echo json_encode( $status );
		} else {
			echo json_encode( $checkstatusdate[ 0 ]->authdate );
		}
	}


}
?>