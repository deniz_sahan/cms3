<?php
//header translation
$lang['executivedashboard_menu']="Executive Dashboard";
$lang['dashboardperlocation_menu']="Course Stats";
$lang['wifi_menu']="Wifi Solutions";
$lang['beacon_menu']="Beacon Solutions";
$lang['dashboard_menu']="Dashboard";
$lang['campaignlist']="Campaign List";
$lang['per_menu']="per";
$lang['locations_menu']="Locations";
$lang['messages_menu']="Campaign Management ";
$lang['customers_menu']="Universitys";
$lang['wifivisitors_menu']="All Students";
$lang['wifistatistic_menu']="Students Stats";
$lang['website_menu']="Websites";
$lang['visited_time']="Visited Time";
$lang['visited_websites']="Visited Websites";
$lang['users_menu']="Personel Management";
$lang['reports_menu']="Student Reports";
$lang['usermanagment_menu']="Personel Management";
$lang['add_menu']="Add";
$lang['delete_menu']="Delete";
$lang['edit_menu']="Edit";
$lang['addnew_menu']="Add New Personel";
$lang['userlist_menu']="Personels List";
$lang['edituser_menu']="Edit Personel";
$lang['id_menu']="Id";
$lang['name_menu']="Name";
$lang['email_menu']="Email";
$lang['mobile_menu']="Mobile";
$lang['role_menu']="Role";
$lang['actions_menu']="Actions";
$lang['user_menu']="Personel";
$lang['enteruserdetail_menu']="Enter Personel Details";
$lang['fullname_menu']="Full Name";
$lang['emailaddress_menu']="Email Address";
$lang['password_menu']="Password";
$lang['confirmpassword_menu']="Confirm Password";
$lang['mobilenumber_menu']="Mobile Number";
$lang['role_menu']="Role";
$lang['selectrole_menu']="Select Role";
$lang['submit_menu']="Submit";
$lang['reset_menu']="Reset";
$lang['userspermonth_menu']="Students Per Month";
$lang['textmessagespermonth_menu']="Text Messages Per Month";
$lang['downloadsanduploadsinmbs_menu']="Downloads and Uploads In MB'S";
$lang['piechartforuserspermonth_menu']="Pie Chart For Users Per Month";
$lang['totalusersperlocation_menu']="Total Students Per Location";
$lang['controlpanel_menu']="Control Panel";
$lang['signups_menu']="Sign Ups";
$lang['totaluniqueusers_menu']="Total Unique Student";
$lang['totalsmssent_menu']="Used  /  Total";
$lang['currentdownloadsuploads_menu']="Current Downloads/Uploads";
$lang['wifivisitorslist_menu']="Wifi Visitors";
$lang['Change_Password']=" Change Password";
$lang['signout_menu']=" Sign Out";
$lang['sentmessages_menu']="Number of Sent Messages";
$lang['average_time']="Average Visit Time";
$lang['useractivity_menu']="Last Students";
$lang['lastcheck_menu']="Last Check Time";
$lang['devicestatus_menu']="University's Device Status";
$lang['viewalluser_menu']="View All Students";
//this is for Student Reports
$lang['username_menu']="Student Name";
$lang['userstats_menu']="Student Stats";
$lang['startdate_menu']="Start Date";
$lang['enddate_menu']="End Date";
$lang['downloads_menu']="Uploads";
$lang['uploads_menu']="Downloads";
$lang['sessiontime_menu']="Total Session Time";
//this part is for instant message
$lang['instantmessages_menu']="Classroom Based Campaign ";
$lang['bulkmessages_menu']="Intelligent SMS";
$lang['selectcustomer_menu']="Select University";
$lang['selectlocation_menu']="Select Location";

$lang['selectcustomer_menu']="University";
$lang['selectlocation_menu']="Course";
$lang['filterresults_menu']="Filter Results";
$lang['fullname_menu']="Full Name";
$lang['phonenumber_menu']="Phone Number";
$lang['lastlogin_menu']="Last Login";
$lang['phonnumber_menu']="Phone Number";
$lang['instantmessagesection_menu']="Classroom SMS Control";
$lang['nameforInstantmessagecampaign_menu']="Campaign Name";
$lang['selectpermissionorblacklist_menu']="Select Permission or Black List_menu";
$lang['selectfilters_menu']="Select Filters";
$lang['uploadpermissionlist_menu']="Upload Permission List";
$lang['format_menu']="Format";
$lang['permissionlist_menu']="Permission";
$lang['uploadblacklist_menu']="Upload Black List";
$lang['blacklist_menu']="Black List";
$lang['stopsendingmessage_menu']="Stop Campaign";
$lang['continuetosetup_menu']="Create Campaign";
$lang['bulkmessagesection_menu']="Intelligent SMS Section";
$lang['campaigninformation_menu']="Campaign Information";
$lang['campaigntext_menu']="Message Text";
$lang['campaigndate_menu']="Campaign Date";
$lang['message_menu']="Message";

$lang['bulkmessagesection_menu']="Intelligent SMS Section";
$lang['checkphonenumber_menu']="Total number of Intelligent Message ";
$lang['sendbulkmessages_menu']="Send Intelligent SMS";
$lang['usingsmsgwformessageswithusername_menu']="Using SmsGW for Messages with username";
//for customermanagement_menu
$lang['customerid_menu']="University ID";
$lang['customermanagement_menu']="University Management";
$lang['customerlist_menu']="University List";
$lang['customername_menu']="University Name";
$lang['customeruserrname_menu']="University Personel Name";
$lang['companyemail_menu']="Company Email";
$lang['contactperson_menu']="Contact Person";
//for location
$lang['locationmanagement_menu']="Location Management";
$lang['addnewlocation_menu']="Add New Location";
$lang['location_list']="Location List";
$lang['locationid_menu']="Location ID";
$lang['locationname_menu']="Location Name";
$lang['deviceid_menu']="Device Id";
$lang['address_menu']="Address";
$lang['enterlocationdetail_menu']="Enter Location Detail";
$lang['session_control']="Session Time";
//for branches
$lang['accesspointmanagement_menu']="Classrooms";
$lang['addnewlocation_menu']="Add New Location";
$lang['branchid_menu']="Device Id";
$lang['branchname_menu']="Device Name";
$lang['accesspointname_menu']="Classroom Name";
$lang['addnewaccesspoint_menu']="Add New Classroom";
$lang['description']="Description";
$lang['branchip_menu']="Device IP";
$lang['accesspointlist_menu']="Classroom List";
//Dashboard Per Location
$lang['selectoption_menu']="Select Option";
$lang['showresultsfor_menu']="Show Results For";
$lang['graphrepresentationforwifiusers_menu']="Graph Representation For Students";
$lang['graphrepresentationforsentmessages_menu']="Graph Representation For Sent Messages";
$lang['time_interval']="Time Interval";
$lang['today_menu']="Today";
$lang['lastweek_menu']="This Week";
$lang['lastmonth_menu']="This Month";
$lang['lastyear_menu']="This Year";
$lang['showresults_menu']="Show Results";
$lang['numberofwifiusers_menu']="Number Of Students";
$lang['numberoftextmessages_menu']="Number Of Messages";
$lang['totalusersperlocation_menu']="Total Students Per Location";
$lang['numberofuserspercustomer_menu']="Number Of Students Per Customer";
$lang['totalwifiuser_menu']="Total Number of Students: ";
$lang['totalsentmessage_menu']="Total Number of Sent Messages: ";
//$lang['address_menu']="Address";
$lang['locationlist_menu']="Location Management";
$lang['accesspoint_menu']="Classrooms";
$lang['branches_menu']="Branches";
//for add new customer
$lang['home_menu']="Home";
$lang['entercustomerdetail_menu']="Enter Customer Detail";
$lang['addnewcustomer_menu']="Add New Customer";
$lang['firmemailaddress_menu']="Firm Email Address";
$lang['city_menu']="City";
$lang['firmdescription_menu']="Firm Description";
$lang['firstname_menu']="First Name";
$lang['surname_menu']="Surname";
$lang['setnewpassword_menu']="Set New Password";
$lang['enterdetails_menu']="Enter Details";
$lang['newpassword_menu']="New Password";
$lang['confirmnewpassword_menu']="Confirm New Password";
$lang['oldpassword_menu']="Old Password";
//for Branch Edit
$lang['branchmanagement_menu']="Branch Management";
$lang['editbranch_menu']="Edit Branch";
$lang['enterbranchdetails_menu']="Edit Branch Details";
$lang['accesspointip_menu']="Access Point IP";
$lang['details_menu']="Details";
//new words added for the page instant messages
$lang['campaignid_menu']="Campaign ID";
$lang['campaignname_menu']="Campaign Name";
$lang['repeatmessage_menu']="Message Repeat ";
$lang['smscount_menu']="Message Count";
$lang['status_menu']="Status";
$lang['operations_menu']="Operations";

$lang['everytime_menu']="Each Login";
$lang['inevery24hours_menu']="In Every 24 Hours";
$lang['manualrepeattime_menu']="Manual Repeat Time";
$lang['selectrepeattime_menu']="End Date";
$lang['savechanges_menu']="Save Changes";
$lang['setenddateandtime_menu']="Set End Date And Time";
$lang['setenddateandtime_menu']="Set End Date And Time";
$lang['edittext_menu']="Edit Text";
$lang['settingenddateforcampaign_menu']="Setting End Date For Campaign";
$lang['messagetext_menu']="Message Text";
$lang['close_menu']="Close";
$lang['messagerepeat_menu']="Message Repeat";
$lang['setrepeatmessage_menu']="Not Set";
$lang['startdate2_menu']="Start Date";
$lang['enddate2_menu']="End Date";
$lang['editlocation_menu']="Edit Location";
$lang['addremovepblist_menu']="Permission - Black List";
$lang['addremovepblisttitle_menu']="Permission List and Black list";
$lang['intelligentsms_menu']="Intelligent SMS";
$lang['show_menu']="Show";
$lang['addnumber_menu']="Add Number";
$lang['lastseen_menu']="Last Attendance";
$lang['selectdate_menu']="Select Date";
$lang['numberofentrances_menu']="Number of Entrances ";
//SMSLimit 
$lang['smsLimit_menu']="Package Status";
$lang['Customer_Name']="Customer Name";
$lang['Packet_Limit']="Packet Limit";
$lang['Send_Messages']="Amount of Use ";
$lang['SMS_Remaining']="Remaining Sending Limit";
$lang['Activation_Date']="Package Activation Date";
$lang['Transaction_menu']="Customer Packages";
$lang['addSmslimit']="New Package";
//UserProfileData
$lang['sendSMS_menu']="Send Message";
$lang['visited_locations']="Visited Locations";






?>

